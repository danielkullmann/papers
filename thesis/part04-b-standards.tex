% up: part04---related-work.tex
\section{Standards}
\label{sec:related-standards}

Surveys such as the \gls{NIST} \gls{Smart Grid} Interoperability Standards
Roadmap~\cite{NISTStandardRoadmap}, the \gls{IEC} \gls{Smart Grid} Standardization
Roadmap~\cite{IECRoadMap}, or the German Roadmap E-Energy / Smart
Grid~\cite{DKERoadMap} illustrate both the importance of standards for the \gls{Smart Grid},
and the large number of standards that are related to the \gls{Smart Grid}. 

This section covers the two main standards concerned with enabling
higher-level communication and several other related standards:

\begin{itemize}
	\item \gls{IEC} 61970, also called \glsfirst{CIM}
  \item \gls{IEC} 61850, the \gls{SCADA}-like standard
  \item \gls{IEC} 61499, the function blocks standard
  \item \gls{IEC} 62351, the security standard
  \item \gls{IEC} TR 62357, the \gls{Smart Grid} reference architecture
  \item The Smart Inverter Standard from the \gls{EPRI}
  \item The OpenADR standard \phantom{\ref{sec:related-openadr}}
\end{itemize}

In the context of high-level communication, two questions are of main interest when evaluating these standards:

\begin{itemize}
  \item How much high-level communication do they support already?
  \item Can extended data formats be used?
\end{itemize}

\subsection{IEC 61970}
\label{sec:related-iec-61970}

% The abbreviation \gls{CIM} should be known by now. The next \gls{CIM} should not use the long form.
\glsunset{CIM}

The \gls{IEC} 61970~\cite{iec61970-1} and \gls{IEC} 61968~\cite{iec61968-1}
standards define a common data model for power systems, called the
\glsfirst{CIM}\footnote{This should not be confused with another standard with
the name Common Information Model, which is used for \gls{IT} asset
management~\cite{it-cim-spec}}. The \gls{CIM} provides an extensive data model
for different aspects of power systems, such as technical, economical,
structural, and asset management; this data model provides an \gls{ontology}
for power systems.  Figure \ref{fig:cim-resource-hierarchy} shows a small part
of the class hierarchy of the \gls{CIM} with classes that model physical
components in a power system.

\begin{figure}
  \centering
  \includegraphics[width=.90\linewidth]{figures/resource-hierarchy.png}
  \caption[Part of the CIM Class Hierarchy]
    {Part of the CIM Class Hierarchy (Source: \gls{XMI} model of \gls{CIM} \cite{cimug})}
  \label{fig:cim-resource-hierarchy}
\end{figure}


The \gls{CIM} does not only
contain classes for many different types of equipment used in power systems
(e.g.~cables, transformers, generators, measurement equipment), but also
classes for abstract concepts, such as \texttt{Measurement} (equipment that is
producing \texttt{MeasurementValue} objects), \texttt{MeasurementValue} (which
is what is produced by measurement equipment), \texttt{BaseVoltage}, and
\texttt{Bid} (for a bid on a market).

The \gls{CIM} data model is maintained as a \gls{UML} file,
which can be converted into other formats, e.g.~\gls{OWL}. The \gls{CIM} User
Group \cite{cimug} manages current and older
releases of the \gls{CIM} data model, mainly in the data format that is used by the
Enterprise Architect \gls{UML} design tool. Paying
members of the \gls{CIM} user group get free access to those files.

The \gls{CIM} only defines a data model; it does not
define an \gls{API} for communication between units in the system. To actually make
use of the \gls{CIM}, one has to define messages that use
the concepts defined in the \gls{CIM}. The standard itself does not define any
messages.


\subsection{IEC 61850}
\label{sec:related-iec-61850}

The \gls{IEC} 61850 standard~\cite{iec61850-1} and derived standards, such as
\gls{IEC} 61400-25~\cite{iec61400-25-1} (the wind turbine standard for the
\gls{Smart Grid}), are standards meant to build new \gls{SCADA}-like systems.
These standards define data models which can be used to exchange data about
components and to send commands to these components.

\gls{IEC} 61850 was originally designed to be a standard for substation automation, but this original
goal has been expanded, permitting the description of whole power systems.

\gls{IEC} 61850 provides several specifications: \glsunset{SCL}
\begin{itemize}

  \item A data model for describing power system components (see below).

  \item The \glsfirst{SCL}, an \gls{XML} data format that is used to
    describe the structure of parts of power systems, e.g.~the components of a
    substation, and communication services to access these components.

  \item The \gls{ACSI}, an abstract communication interface that enables remote
    systems to access the components. To actually communicate with the
    components, a mapping from \gls{ACSI} to a real communication system is
    necessary.

  \item A mapping of \gls{ACSI} to \glsfirst{MMS}, one of the standards for
    message exchange in automation systems. Further mappings are planned,
    e.g.~one to \glspl{web service}.

\end{itemize}

The \gls{IEC} data model consists of the following components
\cite{Harmonization}:

\begin{itemize}

  \glsunset{LD}
  \item \glsfirstplural{LD}: the top-level construct; they describe a single
    device, and consist of groups of \glspl{LN}. The \gls{SCL} describes
    \glspl{LD}. An example for a \gls{LD} is \texttt{ECP} (DER plant
    electrical connection point) which ``defines the characteristics of the
    DER plant at the point of electrical connection'' \cite{iec61850-7-420}.

  \item \glsfirstplural{LN}: those describe several aspects of a device, and
    consist of data objects. An examples is \texttt{DSCH} (DER energy
    and/or ancillary services schedule) which describes an energy schedule,
    i.e.~a list of energy values for a range of time periods.

  \item Data Objects: describe the data elements of a \gls{LN}; their type is
    described using \glspl{CDC}. An example is the \texttt{Pos} (Position) data
    object of a circuit breaker, which gives information about the state (open
    or closed) of the breaker.

  \item \glsfirstplural{CDC}: describe the structure of data objects; they
    contain one or more fields that constitute the data class. Examples are
    \texttt{DPL} (Device name plate) which gives nameplate information for the
    device (manufacturer, model, etc.), and \texttt{INS} (Integer status), which
    describes a single integer status value, and contains additional
    information such as quality and timestamp.

  \item Common Attributes: these are used in \glspl{CDC} for common values,
    e.g.~for quality codes, which can have one of the four values
    \texttt{good}, \texttt{invalid}, \texttt{reserved} and
    \texttt{questionable} .
 
  \item Standard Data Types: the values in \glspl{CDC} can be members of a
    number of basic data types, such as floating point, integer, and binary.

\end{itemize}

The two data models of \gls{IEC} 61970 and \gls{IEC} 61850 have much in common, but 
many differences as well. This makes it difficult to convert information seamlessly between
the two \cite{cim-61850-harmonisation}.

One of the main differences between the two standards is that \gls{IEC} 61850 employs
a more traditional \gls{SCADA} approach, thus focusing more on communicating
\emph{with} components, while \gls{IEC} 61970 provides an \gls{ontology}, thus focusing more
on communicating \emph{about} components. This is not a strict distinction, however:
\gls{IEC} 61850's \gls{SCL} can be used to document the structure of substations,
while the \gls{XML} messages that can be defined using \gls{IEC} 61970's data model
can be used to communicate directly with units.

The \gls{CIM} is therefore more suited for use as an ontology of power systems,
and as a data model for describing power systems, but less suited for control
communication.  \gls{IEC} 61850, on the other hand, was designed to be used for
\gls{SCADA}-like applications, i.e.~for the exchange of status data and control
commands.


\subsection{MMS}

\gls{MMS} is not only used for exchanging \gls{IEC} 61850-based data; it has a much
broader application range.  The goal of \gls{MMS} was to define a
technology-independent communication stack for arbitrary applications; the
basic concept that \gls{MMS} is based upon is that of variables that can be
read and written. The standard defines several elements
\cite{mms-standard,mms-presentation}:

\begin{itemize}
  \item A set of standard objects
  \item A set of standard messages
  \item A set of encoding rules for messages
  \item A set of protocols
\end{itemize}

To quote the ``Overview and Introduction to \gls{MMS}'' from SISCO:

\begin{quote}
  \gls{MMS} also provides definition, structure, and meaning to the messages
  that significantly enhances the likelihood of two independently developed
  applications interoperating. \cite{sisco-mms-overview}
\end{quote}

The push towards a \gls{web service} mapping for \gls{IEC} 61850, however, was in
part motivated by the failure of \gls{MMS} to deliver on the promises of rich
semantics and improved interoperability. For rich semantics, the concept of
read-write variables seems too have been overly simplistic; interoperability
was hindered by the high cost of the existing \gls{IEC} 61850 \gls{MMS} stack, which
makes it less attractive for use in research settings. 


\subsection{IEC 61499}
\label{sec:related-iec-61499}

A different kind of standard is the \gls{IEC} 61499 standard, also called the
\emph{function block} standard \cite{iec61499-1}. This standard enables the
definition of encapsulated units of behaviour, the so-called \emph{function blocks}. These
blocks can be combined with communication channels to define the behaviour of a
distributed system. The standard's main focus is on embedded systems.

An interesting feature of \gls{IEC} 61499 is the possibility to reconfigure
systems of function blocks during run time. This makes it possible to
push new functionality onto embedded devices.

The standard uses several languages to describe the behaviour of function
blocks. The \gls{ECC} is a state machine that determines which algorithms to
execute, depending on input events and internal state. The algorithms can be
described in one of the languages specified by the \gls{IEC} 61131-3
standard~\cite{iec61131-3}. Function blocks can either be basic function
blocks, whose behaviour is described using a programming language, or composite
function blocks, which consist of other basic and composite function blocks.

\gls{IEC} 61499 provides an option for describing behaviour. Therefore, it is
relevant for this work. However, its focus on small embedded devices and lack
of a message format definition makes it less attractive to use for high-level
communication.


\subsection{IEC 62351}
\label{sec:related-iec-62351}

\gls{IEC} 62351 \cite{iec62351-1} addresses security considerations of several
other \gls{IEC} standards, including \gls{IEC} 61850, \gls{IEC} 61968 and
\gls{IEC} 61970. The topics include encryption, node and message
authentication, authorisation, and network security.  The proposed technologies
for this are \gls{TLS} encryption, digital signatures, \glsfirst{RBAC}, and
network and systems management.

This standard provides security guidelines for implementations of ICT systems
that are controlling or monitoring power system components.


\subsection{IEC TR 62357}
\label{sec:related-iec-62357}

\gls{IEC} TR 62357 \cite{iec62357}, which is a technical report, not a standard,
describes a reference architecture for the \gls{Smart Grid}. It lists
the \gls{IEC} standards that are relevant for realising the \gls{Smart Grid}, and it
gives recommendations for the reference architecture and the data
model to be used in \gls{Smart Grid} \gls{ICT} systems.

Figure \ref{fig:tc57-reference-arch} shows the proposed reference architecture.
It shows the prominent roles of both the \gls{IEC} 61970 and \gls{IEC} 61850 standards,
which complement one another: the role of the \gls{IEC} 61850 standard in the
reference architecture is to provide \gls{SCADA} communication with components
(\emph{field devices}), while the \gls{CIM} is for exchange of data, e.g.~for \gls{B2B}
applications.

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{figures/tc57-reference-architecture.pdf}
  \caption[IEC TC57 Reference Architecture -- Scope and Layers]%
  {IEC TC57 Reference Architecture -- Scope and Layers \hfill \\
  Source: \url{http://tinyurl.com/tc57-reference-architecture}}
  % DONTDO cite? that link is just a one-page pdf, so not really suitable for a bibentry
  %\url{http://xanthus-consulting.com/Publications/documents/TC57\_reference\_architecture.pdf}
  \label{fig:tc57-reference-arch}
\end{figure}

This technical report gives a good overview over the standards related to
\glspl{Smart Grid} and how they relate to each other.


\subsection{Smart Inverter Standard}
\label{sec:related-smart-inverter-standard}

A standard for communication with \emph{smart} inverters has recently been
published by \gls{EPRI} \cite{SmartInverters10,smart-inverter-standard}. \emph{Smart} in this
context denotes the ability of the inverters to support advanced functionality. The following
functionality is described \cite{smart-inverter-standard}:

\begin{itemize}
  \item Connect/disconnect from the grid
  \item Adjust maximum power delivery to the grid
  \item Power factor control and smart volt/\gls{VAR} control (including schedules)
  \item Charge/discharge storage management by price or command (including schedules)
  \item State/status monitoring
  \item Event/history logs
  \item Time setting/adjustment
\end{itemize}

The standard uses the data model and communication infrastructure of the \gls{IEC}
61850 standard for specifying the access to the inverter functionality.

This standard can be used as a building block for high-level communication: it
describes certain functionality of inverters and how this functionality can be
activated.


\subsection{OpenADR}
\label{sec:related-openadr}

% The first occurrence of \gls{ADR} after this command should use the short form.
\glsunset{ADR}

OpenADR is a specification published by the \glsfirst{CEC} in 2009
\cite{openadr-homepage,openadr-standard}.  It defines a client/server
architecture for \glsfirst{ADR}. The main concept is the \gls{DRAS}, which
manages \gls{ADR}.

The types of tasks that are mediated by the \gls{DRAS} are automated bidding,
management of \gls{ADR} programs and the participation in these programs,
distribution of \gls{DR} events, and logging of \gls{DRAS} transactions.
OpenADR defines several \glspl{API} that can be implemented by the
participants, depending on their role in the distributed system; these
\glspl{API} are described using \gls{WSDL}.

The specification defines a data model for all relevant data types such as
\texttt{Bid}, \texttt{EventInfo}, or \texttt{ResponseSchedule}; the data model
is described using \gls{XSD}.  The \texttt{ResponseSchedule} data type defines how
\gls{DRAS} clients will respond to specific \gls{DR} events; this is done using
a simple rule format for mapping event values to client states, which are
called \emph{client levels}. Four client levels are available: \texttt{NORMAL},
\texttt{MODERATE}, \texttt{HIGH} and \texttt{SPECIAL}.

The only security consideration this standard makes is the use of \gls{TLS} for
encrypting data transfer.

The OpenADR specification demonstrates that active integration of the
demand-side can be based on a domain \gls{ontology} that provides a set of
predefined interaction patterns, like participation in \gls{ADR} programs.
However, the data model offers only a limited set of supported capabilities for clients. It
would benefit such a system if more client responses were available, and if
these responses could be combined to allow complex client behaviour.  The goal
of this thesis is to provide tools to allow the description of such complex
client behaviour.


