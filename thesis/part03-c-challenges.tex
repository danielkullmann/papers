% up: part03---problem-description.tex
\section{Challenges}
\label{sec:challenges}

From the problem description, we can identify a number of challenges that have
to be addressed for the goal of realising a \gls{Smart Grid}. They will be
described here.


\paragraph{Scalability}

The increase in the number of power-generating units, together with the
inclusion of demand-side components, means that a very large number of
components will have to be controlled at the same time. This situation will
lead to scalability problems in the existing control system infrastructure.  

\label{tag:challenges-real-time}
One part of that challenge is the number of components involved. The other part
is the requirement for real-time control of components, i.e.~the need for a control
loop with low round-trip times. What ``low round-trip time'' means depends on the
component, and what it is supposed to do, but usually, this is a duration well
under one second.


\paragraph{Flexibility}

Various types of components will have to be actively integrated into the 
control of power systems; this implies that supervisory controllers will have to be
more flexible. Flexibility comes in two different flavours: one type of flexibility
is the flexibility to be able to support many different types of components;
another type is the ability to support many different capabilities these
components can offer.

On the other side of the communication channel, components should also be
flexible, and there are two types of flexibility as well: the flexibility to
provide various services to the power system, and the flexibility to react to
various events such as change in frequency, change in voltage, or a new power price.


\paragraph{Extensibility}

Extensibility is the ability to easily integrate new types of components and
new capabilities of these components into the control system. This becomes more
important when the type of components that are integrated become smaller and
cheaper, e.g.~when household appliances are actively integrated into the power
system. This is because these kinds of components can evolve faster than large
power system components, and thus will exhibit new capabilities that
should be exploited. 


\paragraph{Reliability}

This is the major concern of control systems: to keep the system in a state in
which it can provide the services it is supposed to provide. In power systems,
the term has two meanings: one comes from the viewpoint of the power system
engineer, who is concerned with security of supply. The other comes from  the
viewpoint of the software engineer developing the control system, who is
concerned with the stability of the software systems.  These two types of
reliability are connected, in that you cannot have one without the other, but
one is more concerned with the power system components (the ``hardware''), the
other with the software system controlling these components. This work is
investigating the software side of control systems and therefore the
reliability of software systems.

The software reliability issue can be split up into two independent
issues.

\subparagraph{Reliability of Software}

The software that makes up the control system must itself be reliable.
Techniques for making software more reliable are subsumed under the
keyword fault-tolerant software. The key feature of fault-tolerant software is
that it must be able to recover from any errors; often, this is done by restarting
tasks that fail. In a distributed system, special care must be taken by each
node of the distributed system, so that unexpected messages or expected
messages that do not arrive do not result in erroneous behaviour.

This kind of reliability is not addressed by this work.


\subparagraph{Reliability of Communication Links}
\label{sec:challenges-unreliable-communication-links}
\label{tag:cost-vs-reliability}

The second issue concerning system reliability is the reliability of
communication links. The components that are integrated into the power system
become smaller on average, and they are distributed over a large geographical
area. This results in a large increase in communication links, both wired and
wireless. The whole system should be cost-effective, so the communication has
to be cost-effective as well. Unfortunately, there is a trade-off between cost
and quality, meaning that cost-effective communication links are less reliable,
so they do not necessarily provide the quality and performance guarantees that
are demanded by traditional \gls{SCADA} systems. 
% DONTDO see refnpage{???} => This would be too early here..


\paragraph{Balancing Control and Autonomy}
\label{sec:balancing-control-and-autonomy}

One key for overcoming the challenges listed above is to
automate the control system to a much higher degree than this is
currently done.
Automation implies that the different parts of the system have to decide without
human help how to deal with normal as well as emergency situations. 
This in turn implies that parts of the systems have to become more autonomous.

When trying to solve the problem of how to structure the control system of
power systems, a central question is how much autonomy local components should
have.  The existing control structure expects only little autonomy
in local components; autonomy is restricted to executing schedules, following
droop curves or similar tasks. For many control tasks, this is actually
desirable: a component that is supposed to provide ancillary services, for
example, should not be allowed the autonomy not to provide these services; the
provision of ancillary services is so crucial to the whole power
system that tight supervision over the units providing those services is desirable.
This approach, however, puts a large burden on the central controllers, when
they become responsible for supervising the actual operation of these
components. 

The other extreme is to make local components fully autonomous. Market-based
systems make components more autonomous: the market agents behave according
to their own strategies; the only coordinating instance is the market clearance
agent. As explained in the previous paragraph, this is a good strategy
in many cases, but not for all cases: some services are so critical for the
stability of the whole system that full autonomy becomes harmful.
The decision of which market entity
will provide a critical system service can be made using a market, but the actual
activation of the service will have to use a mechanism that is more
deterministic.

The solution lies in finding a good balance between autonomy of the distributed 
units on one hand, and control by a hierarchy of controllers on the other hand. 
A single centralised controller is not desirable, if only because it introduces a
single point of failure into the system, which should be avoided at all costs.

A combination of control and autonomy provides a good solution for this
challenge: allow autonomous behaviour of units to some degree, but have the
units act according to some rules. In such a case, the supervisory controller
is able to anticipate the behaviour of the units, so it can make sure that the
critical service is actually provided.  This is possible because the local
decisions are predictable. This is the approach taken by behaviour
descriptions, which are introduced in chapter \ref{sec:behaviour-descriptions},
starting at page \pageref{sec:behaviour-descriptions}.


\paragraph{Local Decision Making}
\label{sec:local-decision-making}

Automation of the control system not only implies more local autonomy, but local
intelligence as well.  A solution where one central intelligent controller
controls every aspect of the system is not feasible, if only due to the size of
the system and the number of components that have to be coordinated.

Some of the decision making that was formerly done by a central controller should
be done by the distributed units, so these units have to display some intelligent
behaviour. This results in some of the responsibility for keeping the system in
a stable state moving down in the control hierarchy, from the central
controllers towards the local controllers.

This raises the question of what kind of decision making processes can be moved
to the local units. It should be avoided to force the distributed units to obtain too much
information about the rest of the system. This is bad due to the quantity of 
communication necessary to collect that information, to distribute it
to every unit that has to make decisions, and to keep it current. 
Furthermore, the coordination between units should be reduced to the minimal
extent necessary, due to the same communication concerns.

This implies that certain kinds of decision making are better suited for the
bottom levels of the control hierarchy than others. Certain
indications can be used to evaluate the suitability of a control
algorithm for the bottom levels of the hierarchy:

\begin{itemize}

  \item It uses mostly information that can be obtained locally for making
    decisions; the canonical example for this is the system frequency, which
    can be measured anywhere.

  \item It requires only a limited amount of information about the state of the
    rest of the system.

\end{itemize}

