% up: part04---related-work.tex
\section{Distributed Systems}

As already explained in section \ref{sec:bg-distributed-systems}, in \gls{ICT}
a distributed system is characterised as a collection of autonomous computers
communicating through a computer network, working towards a common goal.  By
this definition, power systems are by their very nature distributed systems. 

\subsection{The Eight Fallacies of Distributed Computing}
\label{sec:8-fallacies}
%\indexb{The Eight Fallacies of Distributed Computing}

Peter Deutsch has created a list of the ``Eight Fallacies of Distributed
Computing'', to warn people implementing distributed systems about assumptions
that are usually made during implementation~\cite{deutsch-8-fallacies}:

\begin{quote}
  Essentially everyone, when they first build a distributed
  application, makes the following eight assumptions. All prove 
  to be false in the long run and all cause big trouble and painful 
  learning experiences. 
 
  \begin{enumerate}
    \item The network is reliable
    \item Latency is zero
    \item Bandwidth is infinite
    \item The network is secure
    \item Topology doesn't change
    \item There is one administrator
    \item Transport cost is zero
    \item The network is homogeneous
  \end{enumerate}

\end{quote}

The typical control system for power systems, the dynamic \gls{closed-loop}
control, does at least assume items 1, 2 (in the variation
``Latency is low'') and 4 to hold.

Item 5 is especially interesting in the context of power systems,
due to the two separate networks whose topology can change: the
electrical power grid, and the communication network used to coordinate
the actions of components in the power grid. Components that offer a 
service to another unit (via the communication network) have to make sure that
the service can be delivered over the power grid.

Bullet points 6 and 8 pose a significant challenge to
power systems. Individual components are owned and administrated by
different owners. The owners have to make sure that no unwanted data
exchange can take place, i.e.~neither is confidential information 
transmitted, nor do components react to unwanted control signals.
Furthermore, components are of different type, and support different
communication technologies and/or standards.

It is interesting to note that of those eight fallacies, seven are about
communication failures.  This demonstrates that a large part of the challenge
of building distributed systems lies in the communication that happens between
the nodes constituting the distributed system.


\subsection{Architectures for Distributed Systems}

A number of different methods are available to describe and implement
distributed software systems. The two main methods are \glsfirstplural{MAS}
and \glsfirst{CSP}.


\subsubsection{Multi-Agent Systems (MAS)}
\label{sec:related-multi-agent-systems}
%\indexb{Multi-Agent System (MAS)}

A \gls{multi-agent system} is a distributed system where the single nodes of
the system are modelled as \emph{\glspl{agent}} \cite{Wooldridge1995Intelligent}. 
According to~\cite{PW04}, an agent is:

\begin{enumerate}
  \item Situated, i.e.~it can interact with its environment using sensors and actuators
  \item Autonomous, i.e.~it acts according to its own goals
  \item Reactive, i.e.~it reacts to received messages and changes in its environment
  \item Proactive, i.e.~it tries to reach its goals
  \item Flexible, i.e.~it can try different means to reach its goals
  \item Robust, i.e.~it can deal with failures
  \item Social, i.e.~it interacts with other agents to reach its goals
\end{enumerate}

The \gls{FIPA} has published a set of 25 standards concerning agent
communication, agent transport, agent management, abstract architecture and
applications \cite{fipa}. The bulk of these standards is concerned with agent
communication.

The \gls{JADE} library provides a framework to implement and execute agents
based on \gls{FIPA} standards, using the \gls{Java} programming
language \cite{JADE}. Agents are implemented by creating so-called behaviours,
which are behavioural patterns that the agent can use to reach its
goals. Behaviours can be arbitrarily added to and removed from an agent.

The agent execution platform of \gls{JADE} provides containers in which
agents are executed. Additionally, \gls{JADE} provides a directory service agent
where other agents can register their services and look up agents
providing a particular service.

Containers can be started on many different computers, and these containers can
be joined together, so that agents can transparently communicate over computer
boundaries, and use a central directory service. This feature is convenient,
but it makes an agent system based on \gls{JADE} less robust, because failures
of the main container lead to complete failure of the whole agent system. It is
possible to start separate containers, but it is more difficult to interact
with agents residing in unconnected containers. 

This means that \gls{JADE} is not necessarily the best choice for implementing a
distributed power system. Nevertheless, agents give a good abstraction
for designing and implementing distributed systems. 

The \emph{agent} concept has been used in this thesis, even though the
implementation of the system does not depend on \gls{JADE}, and the agents do
not necessarily employ \gls{FIPA} standards for communication.


\paragraph{Contract Net Interaction Protocol}
\label{sec:related-cnp}

\glsunset{CNP}
The \glsfirst{CNP} is one of the interaction protocols defined by \gls{FIPA}
\cite{contract-net-interaction-protocol}. The protocol allows agents in a
\gls{MAS} to agree on contracts for carrying out tasks. It consists of several
messages \cite{contract-net-interaction-protocol}:

\begin{itemize}

  \item call-for-proposal (cfp): an agent A asks another agent B to carry out a
    task T for him

  \item refuse: Agent B can refuse a cfp

  \item propose: Agent B can make a proposal for carrying out task T

  \item reject-proposal: Agent A can reject the proposal

  \item accept-proposal: Agent A can accept the proposal

  \item failure: Agent B informs agent A on the failure to accomplish task T

  \item inform-done: Agent B informs agent A that task T was successfully
    accomplished

  \item inform-result: Agent B informs agent A that task T was successfully
    accomplished, and includes the result of task T in the message

\end{itemize}

The \gls{UML} sequence diagram for this protocol is shown in figure
\ref{fig:fipa-cnp-sequence-diagram}. The CNP has been used in this thesis as a
model for designing the negotiation protocol (see section
\refnpage{sec:policy-negotiation-protocol}).

\begin{figure}
  \centering
  \includegraphics[width=0.5\linewidth]{figures/fipa-cnp-sequence-diagram.pdf}
  \caption[Sequence diagram for FIPA Contract Net Interaction Protocol]%
  {Sequence diagram for FIPA Contract Net Interaction Protocol (Source:
   \cite{contract-net-interaction-protocol})}
  \label{fig:fipa-cnp-sequence-diagram}
\end{figure}


\paragraph{Holonic Multi-Agent Systems}

A variant of agent systems are those that consist of a hierarchy of agent
systems; each single system in that hierarchy contains an elected
representative agent that represents this agent system in higher levels of the
hierarchy.  This type of \gls{MAS} is called \emph{\gls{holonic multi-agent system}}
\cite{holonicmas03}, from the Greek word \emph{holon}, ``whole''. 

In a holonic \gls{MAS}, an agent is at the same time part of a \gls{MAS} and
itself a \gls{MAS} consisting of holonic agents; this type of agent is called a
\emph{holon}. The advantage of a holonic \gls{MAS}, in comparison with a normal
\gls{MAS}, is that it provides structure to the \gls{MAS}. When communicating
with a holon, one only has to communicate with the representative agent, not
with all agents that are part of the holon; this reduces communication
overhead.

The concept of holonic agents can be used to implement aggregation hierarchies
with \gls{MAS}.
 

\subsubsection{Communicating Sequential Processes (CSP)}

\gls{CSP} \cite{CSP} is a formal notation for the definition of distributed
systems. It allows the specification of the different processes that are
executed in the system and the interactions between these processes.
Interactions between processes and between a process and its environment are
specified by the external events a process can participate in and by the
messages it can receive and send. 

The notation is solely concerned with how the processes interact. What the
processes are actually doing is not part of the \gls{CSP} specification
language, unless these actions can be modelled with events or messages.
Nevertheless, the intent of a process definition can be made quite clear by
naming processes, events and messages appropriately.

A simple example of the notation is given in figure \ref{fig:csp-example}: this
is a description of a vending machine process. The vending machine accepts
one- and two-penny coins (\texttt{in1p} and \texttt{in2p}), offers two
different items one can buy (\texttt{small} and \texttt{large}), and even
returns change (\texttt{out1p}). It also contains an error: it will stop after
accepting three one-penny coins in a row. The definition of the process is
recursive, which is typical for \gls{CSP}: all branches in the process that
do not end in \texttt{STOP} just restart the process by calling it by its name,
\texttt{VMC}. In this example, all interactions between the vending machine and its environment
are modelled using events (\texttt{in1p}, \texttt{in2p}, \texttt{small},
\texttt{large}, \texttt{out1p}).

\begin{figure}
  \centering
  \includegraphics[width=0.6\linewidth]{figures/csp-vending-machine.png}
  \caption[CSP Example: A Vending Machine]%
    {CSP Example: A Vending Machine (Source: \cite{CSP})}
    % DONTDO Is this fair use? Or should I recreate that example?
  \label{fig:csp-example}
\end{figure}


JCSP \cite{JCSP} is a \gls{Java} library for implementing distributed systems
using the \gls{CSP} concepts. Besides \gls{UML} sequence diagrams, \gls{CSP} is
the only available method to describe the message exchange in a distributed
system. 


\subsubsection{IEC 61499}

\gls{IEC} 61499 \cite{iec61499-1} (see section \refnpage{sec:related-iec-61499}) also
allows for distribution of function blocks over several systems. The
distributed function blocks communicate via event and/or data connections; the
data is sent transparently over the network. 

However, \gls{IEC} 61499 is very low-level; it does not define data
formats for communication, but treats communication as either signals or
arbitrary data streams.


