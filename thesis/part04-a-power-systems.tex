% up: part04---related-work.tex
\section{Power Systems}
\label{sec:related-power-systems}

A number of control schemes have been proposed to realise the \gls{Smart Grid}
vision. They can be categorised by the main ideas behind them:
\begin{itemize}
  \item Market-based 
  \item Hierarchical 
	\item Aggregated
  \item Distributed \phantom{\ref{sec:related-distributed-control-approaches}}
\end{itemize}

These are not completely separated categories; the three categories do have
some overlap.

Control schemes such as those described in the following sections will play a
significant role in the automation of the control system.  It is therefore
important to investigate how these control schemes could fit into a future
distributed control system.


\subsection{Market-Based Systems}
\label{sec:related-market-based}

These are methods that facilitate power markets to coordinate the control of
power systems. Two different control schemes are in this category. The first
scheme provides their participants with a market where generation offers and
consumption bids are placed; a market clearing agent then matches bids to
offers and determines the power price during that process. In the second
scheme, the power price is determined by an external procedure; the
participants in this scheme are just notified of price changes, and are
expected to control their consumption and/or generation based on that price.
The two schemes are sufficiently different to give the second scheme a separate
name here, \emph{price-based}.

Three types of agents are part of a market-based control network: Agents that
offer power to the market, agents that bid on consumption of power, and
clearing agents that map offers to bids. The basic data types that are
exchanged are bids, offers, and the resulting mapping between bids and offers
or the power price that is produced by the market clearing agent.


Most power systems do have markets in place, for example:
\begin{itemize}

  \item The Nordpool market\footnote{http://www.nordpoolspot.com} covers the
    Nordic countries Denmark, Sweden, Norway, Finland and Estonia, as well as
    the United Kingdom via its N2EX market\footnote{http://www.n2ex.com}. It
    offers day-ahead and intra-day markets.

  \item The European Power Exchange\footnote{http://www.epexspot.com/en/}
    covers the central-European countries Germany, France, Austria and
    Switzerland. It also offers day-ahead and intra-day markets.

\end{itemize}


One of the market-based systems is PowerMatcher~\cite{KWK+06}. It is an
hierarchically structured \gls{multi-agent system} that consists of agents with
different roles: Device agents control local components,
concentrator agents aggregate a set of device or concentrator agents, and auctioneer agents are
responsible for the clearing of the market. Optionally, an objective agent can 
be used to guide the auctioneer agent by providing additional
information. The general structure of a PowerMatcher network is illustrated in
figure \ref{fig:powermatcher}.

\begin{figure}
  \begin{center}
    \includegraphics[width=\textwidth]{figures/Powermatcher-Overview.png}
  \end{center}
  \caption[Structure of the PowerMatcher System]{Structure of the PowerMatcher System \hfill \break (Source: \url{http://www.powermatcher.net}) }
  \label{fig:powermatcher}
\end{figure}


The DEZENT project~\cite{WeddeLRK08} employed a similar method: a hierarchical
\gls{multi-agent system}, where agents put bids and offers on a market. The main
difference lies in how often the market is cleared: the DEZENT system does this
every half second. The argument of the authors for this short time span is that
it makes additional control systems unnecessary: they claim that the
half-second time span is short enough to actually ensure security of supply
just by using this market-based system.

Market-based systems are inherently distributed, which means that they can be
used to control distributed power systems. The degree of autonomy that they
allow, however, implies that they might not be applicable for the provision of
critical system services, which have to be coordinated by some supervisor that
monitors the state of the system.

The FlexPower project employs a price-based system \cite{flexpower-project}.
The goal of the project is to create a market design that facilitates the
participation of small units in the provision of regulating power. A one-way
price signal is broadcast, and participating units are reacting to that price
by controlling their consumption. The price is calculated from the actual
demand in reserve power and information such as historical data that allows
the prediction of reactions to power prices.  

This project takes a different approach from this thesis: it assumes that a
one-way broadcast signal, plus historical data about reactions to prices, is
enough to provide a critical system service. This thesis proposes a supervisory
control scheme that requires two-way communication links to the controlled
components, but does not require these communication links to be available at
all times; the underlying assumption is that two-way communication is necessary
for providing critical system services.


\subsection{Hierarchical Concepts}
\label{sec:related-hierarchical}

The Microgrids and More Microgrids~\cite{H06} projects investigated the
concept of microgrids, which are low voltage distribution systems with \gls{DG}
that can be operated either connected to the main grid or in islanded mode.
Energinet's Cell project~\cite{CellProject} proposed
the similar concept of a \emph{cell}, which is a contiguous part of a power system
that is controlled by a single controller, the Cell Controller. Both concepts
split the power system into smaller areas that are individually controlled
by their own controller.

Such systems fit very well into the control hierarchy structure assumption that
this thesis is based upon (see section \refnpage{sec:aggregation-scenario}).
Power systems can be split into smaller parts, which are 
responsible for the reliable operation of their part of the grid.


\subsection{Aggregation Approaches}
\label{sec:related-aggregation}

Aggregation methods have been suggested as a means to cope with the complexity
of the system, i.e.~with the number of components that have to be coordinated. 
An example is the research on Virtual Power Plants (VPPs), e.g.~\cite{DPCR06,Pudjianto07}. 
In contrast to microgrids or cells, the \gls{DER} that make up a \gls{VPP} can
be connected to the power grid at various locations.

Aggregation is greatly simplified when components can be accessed through
unified interfaces; Gehrke~\cite{OG08} proposes a role-based system to
organise components in such a fashion. Listing \ref{lst:syslab-roles} presents the
roles that Gehrke identified in the \gls{SYSLAB} power system laboratory; these role
interfaces offer numerous methods (not
shown for the sake of conciseness) for accessing information about the components
and for controlling them. 

%\Show one interface in full, with all methods => This is done in sec:type-agnostic

\lstinputlisting[float,
  caption={Power System Roles},
  label=lst:syslab-roles
]{listings/syslab-roles.txt}

The concept of power system services (see below) can likewise be used to create
unified interfaces to components; this is a central proposition of this thesis.
Using such unified interfaces, the actual type of the component becomes
irrelevant, and only capabilities relevant to the task at hand are considered.

These kinds of schemes fit well into the assumed control hierarchy
structure, just as the hierarchical methods described above. Two questions
that aggregation schemes raise is how the communication between an aggregator
and its aggregated components is done, i.e.~how do aggregators know about the
components they aggregate and their capabilities, and how these
capabilities are activated.


\subsection{Distributed Approaches}
\label{sec:related-distributed-control-approaches}

The idea to treat the power system as a distributed system whose units can act
autonomously is an ambitious proposition; the current control structure relies
on tight supervisory control of all controlled units. However, some research
has gone into that direction; market-based systems are the prime example for
that \cite{KWK+06}. These schemes are usually concerned with long-term balance
of generation and consumption, which do not necessarily need supervisory
control, in contrast to schemes that provide \glspl{ancillary service} which do
need supervisory control.

The DEZENT project proposed a market-based system that should provide also
ancillary services by using a very short market clearing interval of half a
second. However, the feasibility of such a scheme in a real power system
has not been demonstrated.

Saleem describes a distributed, multi-agent system that provides
decentralised protection and control \cite{saleem10}. A power grid is split up into zones, with
each zone being controlled by a so-called \emph{relay agent}, which coordinates
the work of the \emph{\gls{DG} agents} responsible for controlling generation
and the \emph{load agents} controlling the loads. The relay agent therefore
assumes the role of a supervisory controller.

This demonstrates that even distributed control systems use a kind of control
hierarchy when they have to coordinate the provision of critical system
services.


\subsection{Unreliable Communication}
\label{sec:related-reliable-comm-assumption}


All these schemes assume implicitly that communication just happens; the
possibility of failure is silently ignored. However, communication problems do
occur, and they pose a problem in systems where the two communicating parties
are closely coupled to each other. This is the case in control systems that use
synchronous communication, which is the standard approach in control systems; see
section \refnpage{sec:pd-closed-loop-control}.

This implies that one of the main challenges of a new control system structure
is how it deals with reliability issues of communication links.


\subsection{Activating the Demand Side}
\label{sec:related-demand-side}

The question of how to integrate the demand side actively into the power
control system has spawned research into control algorithms for demand side
components. Numerous articles have been published that describe algorithms for
controlling demand side components, such as fridges, freezers, and space
heating, e.g.~\cite{NOeTH10,YiZong10,LS10}. 

The referenced articles assume the existence of a supervisory controller that
executes the algorithm in question and issues control commands to the
controlled appliances.

Individual access to the appliances in a household from a supervisory
controller does not make much sense, due to scalability issues and the
understandable reluctance of appliance owners to cede control over their
appliances completely to an external entity. Due to this, home gateways
have been suggested as an important component of demand-side participation.
These are computers that act as gateways between the individual appliances at a
household and the supervisory controller that coordinates the operation of
these appliances. They introduce a central location of control for each
household, where the house owner can change settings regarding the supervisory
control of its appliances and get an overview over past and present state as
well as events. 

The \gls{BEMI} is such a home gateway \cite{bemi2007}. Its usability
outside of laboratory experiments has been demonstrated in a field test in
Mannheim \cite{bemi2009}. The \gls{BEMI} supports communication via \gls{IEC} 61850;
new \glsplural{LN}\footnote{See section \refnpage{sec:related-iec-61850} for a
definition of \gls{LN}.} have been defined to map the data model of the
\gls{BEMI} to this standard.

The \gls{OGEMA} ``provides an open software platform for energy management''
\cite{ogema-factsheet}, i.e.~it provides a software platform to be executed on a
home gateway. It has a modular architecture based on the \gls{OSGi}
technology. The modular approach allows for easy addition and removal of
communication drivers (\gls{IEC} 61850 \glstext{MMS}, \gls{Z-Wave}, \gls{KNX}, etc.), resource
controllers (fridge, freezer, energy price source), and applications that 
control the appliances, as well as easy access to management functions such as
logging, database administration and resource administration.

Home gateways provide an extra level in the aggregation hierarchy. Since
appliances usually do not have much processing power, smart control algorithms
controlling appliances will have to be executed on the home gateways.


\subsection{Power System Services}
\label{sec:related-power-system-services}

A central concept for power systems are power system services.
Usually this implies ancillary services, a term that denotes tasks that are not
directly concerned with the generation and consumption of energy, but rather
with reliably supplying power to the consumers and keeping the power
system stable.

Rebours et.~al~\cite{Rebours07a, Rebours07b} survey the existing ancillary
services in different countries. They split ancillary services into six basic
groups:

\begin{itemize}
  \item Primary frequency control
  \item Secondary frequency control
  \item Tertiary frequency control
  \item Basic voltage control
  \item Secondary voltage control
  \item Tertiary voltage control
\end{itemize}

Furthermore, they distinguish between system services, which are services the
system operator provides to all its users, and ancillary services that are
provided by some of the users to the system operator. This shows that they
treat power generation as a system service. 

Nyeng \cite{Nyeng2007} lists the ancillary services that are available in the
Danish power system and evaluates the possibilities to use distributed
generators for supplying ancillary services. He lists four basic types of
ancillary services:

\begin{itemize}
  \item Primary frequency control
  \item Secondary voltage control
  \item Voltage control
  \item Black start capability
\end{itemize}

Since services are a central concept for power systems, the concept of service
should be used as a general abstraction for all types of tasks that are
provided by power system components. This is one of the central conclusions of
this thesis, which is covered in more detail in chapter \ref{sec:services}
starting at page \pageref{sec:services}.


\subsection{Functional Modelling of Control Systems}
\label{sec:related-functional-modelling}

The concept of functional modelling is nothing new in control engineering.
\glsfirst{MFM}, for example, has existed for more than
30 years \cite{lind82}. The application of \gls{MFM} to the control of power
systems, however, has only happened recently \cite{heussen09}.

\gls{MFM} allows to model control systems along two dimensions.  The first
dimension is the whole-part
dimension. It describes a system as consisting of smaller parts, which itself
can be composed of parts. This gives a hierarchical description of the
structure of the system. 

The other dimension is the means-ends dimension. It describes the ends of the
control system, i.e.~the goals it is supposed to meet, and the means that are
used to accomplish these ends. Means and their ends can be put into a
hierarchical structure as well: higher-level objectives, such as ``safe
transfer of power from generators to consumers'', can be split up into lower
level goals, such as frequency control, balance of power, voltage control, and
protection mechanisms. These goals can be accomplished by various means; this
results in a structured description of the control system.

\gls{MFM} is a graphical language, i.e.~it supports the creation of diagrams
that show the structure of control systems in regard to the two dimensions
explained above. The graphical representation can be converted into a
textual one, which can be used for further processing. The \gls{MFM}
Workbench supports conversion of \gls{MFM} models into sets of
\gls{Jess} \cite{jess-homepage} facts, which are used to reason about the
state of the control system.

If nothing else, \gls{MFM} teaches us that context is important: it is not only
important to know the behaviour of the control system, but likewise the reasons for
this behaviour.

