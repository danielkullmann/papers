% up: part10---standardization.tex
\section{Extending the CIM}
\label{sec:beh-desc-in-cim}


The \gls{CIM} provides a rich \gls{ontology} for power systems, and an \gls{XML}
format for defining messages containing information using this \gls{ontology}.
Nevertheless, the \gls{CIM} standard does not pre-define actual any messages;
these have to be defined on a case-by-case basis.

The \gls{CIM} data model is already used to exchange data about power systems.
Extensions to that standard are encouraged, and might be integrated into the
standard when they are deemed to be of general interest. 
% DONTDO missing \cite => didn't find one

This section therefore outlines a definition of services, the negotiation
protocol, component descriptions and behaviour descriptions using the \gls{CIM}
data model.


\section{Power System Ontology}
\label{sec:std-cim-power-system-ontology}

A number of \gls{CIM} classes are relevant for building a basic ontology for
power systems, which can then be used as a basis for service definitions,
component descriptions and behaviour descriptions. See also section
\refnpage{sec:power-system-ontology}.

\begin{itemize}

  \item \texttt{Measurement}: represents the ability to make measurements.
    Child classes specify the kind of measurement, e.g.~\texttt{Analog},
    \texttt{Discrete}, \texttt{StringMeasurement}
    This class has an association to a \texttt{PowerSystemResource}.
    
  \item \texttt{MeasurementValue}: actual measurements. Several other classes
    inherit from this class, such as \texttt{AnalogValue} and
    \texttt{DiscreteValue}. The connection between the
    classes \texttt{Measurement} and \texttt{MeasurementValue} goes over the
    child classes, e.g.~\texttt{Analog} is associated with
    \texttt{AnalogValue}.

  \item \texttt{MeasurementType}: the type of a measurement, such as bus
    voltage. Each \texttt{Measurement} object has an association with a \texttt{MeasurementType}.
    Unfortunately, no concrete \texttt{MeasurementType} instances are defined. 
    However, the \gls{CIM} also provides the unrelated classes
    \texttt{ActivePower}, \texttt{ReactivePower}, \texttt{Frequency},
    \texttt{Voltage}, etc. in the package \texttt{Domain}; those are basically
    measurement value classes that also contain type information. 

  \item Basic data types such as \texttt{Float}, \texttt{Boolean},
    \texttt{Hours}, \texttt{Minutes}, \texttt{Seconds}, \texttt{Money},
    \texttt{Currency} and \texttt{PU} (per unit) are also provided by the
    package \texttt{Domain}.

  \item \texttt{UnitSymbol}: provides all relevant physical units,
    e.g.~\texttt{W}, \texttt{V}, \texttt{A}, \texttt{Wh}, \texttt{Hz}.
    The classes \texttt{ActivePower}, \texttt{ReactivePower}, etc.~contain a
    \texttt{UnitSymbol} field, but \texttt{MeasurementType} does not.

  \item \texttt{RemoteUnit}: represents a node in the distributed
    control system. A subclass of \texttt{PowerSystemResource}.

  \item Schedules can be described by using the class
    \texttt{BasicIntervalSchedule} and its subclasses such as
    \texttt{RegularIntervalSchedule}.

\end{itemize}


\section{Services}
\label{sec:std-cim-services}

The usage of the \gls{CIM} for expressing services is not at all
straightforward. The main problem is the lack of a service concept that is
compatible to the one used in behaviour descriptions. The term \emph{service}
is used in the \gls{CIM}, but it is used in two different meanings that are
both not completely compatible with the strong service concept this thesis proposes.

The first meaning is that of a service provided to a customer; related \gls{CIM}
classes are \texttt{ServiceKind}, \texttt{ServiceLocation} and
\texttt{ServiceDeliveryPoint}. 

The second meaning is that of an ancillary service; it is captured in the class
\texttt{AncillaryService}. However, the CIM uses that concept more in a
contractual setting; the attributes of the class \texttt{AncillaryService} are
\texttt{ControlAreaOperator}, \texttt{TransmissionProvider},
\texttt{OpenAccessProduct} and \texttt{ServiceReservation}. This class is
suited for exchange of information about the current state, but less suited for
commands telling a unit to provide that service.

The class \texttt{AreaReserveSpec} contains the reserve specification for a
certain control area, and thus could provide a basis for a reserve service
activation data format. In its current form, however, it is too specialised to be of use
in a general reserve specification, because it is tailored to a
specific ancillary service definition; e.g.~the spinning reserve is required
to be available within 10 minutes. Since the exact definition of ancillary
services is provided by the system responsible of an area, such a definition
should not be part of an international standard.

Thus, the service concept from section \refnpage{sec:service-definition} is
mapped to new CIM classes:

\begin{itemize}
  \item Service name: this is just a name, i.e.~an entity of type \texttt{String}
  \item Service definition: this relates a service name to the following pieces of information:
  \begin{itemize}
    \item Service specification, given as a \gls{URL}, i.e.~a \texttt{String}
    \item Service description: refers to the type of the service description (see below)
    \item Service activation: refers to the type of the service activation (see below)
  \end{itemize}
  \item Service description: this consists of two fields:
    \begin{itemize}
      \item Service name: the name of the service
      \item Parameters: a simple list of key-value pairs, containing the parameters;
        the list of allowed parameters names and their types is part of the
        service definition
    \end{itemize}
  \item Service activation: a service activation has the same format as a
    service description, but the list of allowed parameter names and types is
    different
\end{itemize}

Service descriptions and activations are exchanged between controllers and
their controlled component, i.e.~two data formats are necessary for the two
separate pieces of information. However, fixing the actual format to a certain
technology, such as \gls{XML}, will limit the usability of service definitions.
For this reason, there should be no fixed format for service descriptions and
activations, but the abstract data format given above which can be mapped to
many different communication standards and data formats. 

Listings \ref{lst:example-service-description} and
\ref{lst:example-service-activation} show example \gls{XML} data formats for
service descriptions and service activations.

\lstinputlisting[%
  language=xml,numbers=left,float,%
  caption={Example Service Description},%
  label=lst:example-service-description]%
  {listings/service-description.xml}

\lstinputlisting[%
  language=xml,numbers=left,float,%
  caption={Example Service Activation},%
  label=lst:example-service-activation]%
  {listings/service-activation.xml}


The service implementation does not need a separate \gls{CIM} representation,
since it is not transferred in communication; it is rather the client in the
negotiation protocol. This can be represented by a \texttt{RemoteUnit} instance
(see above).

\section{Negotiation Protocol}
\label{sec:std-cim-protocol}

The negotiation protocol defines a number of messages, and these messages have
to be defined in terms of the \gls{CIM} data model. Negotiation messages contain
several different pieces of data:

\begin{itemize}

  \item Addresses of supervisor or component: these can usually be just
    expressed as \glspl{URL}, i.e.~a \texttt{String}

  \item Component information: see below.

  \item The actual behaviour descriptions: see below.

  \item A digital signature of the content of the message. This is used for
    security reasons, and is just a \texttt{String}.

\end{itemize}

The various message types are listed in section
\refnpage{sec:policy-negotiation-protocol}; these are mapped one-to-one to
\gls{CIM} messages.


\section{Component Descriptions}

Component description messages contain various pieces of information:

\begin{itemize}

  \item General information about the component,
    such as its actual type, some general characteristics, etc. 
    %\reduwave{In
    %practice, this information will be ignored by most applications, but can be
    %useful to have to identify problems}.

  \item List of supported services: the format of a service description has been 
    given above, in section \ref{sec:std-cim-services}.

  \item Dependencies between services (see section
    \refnpage{sec:service-conflicts}): This is expressed by services depending
    on certain capabilities of the components; services depending on the same
    capabilities might be conflicting.

\end{itemize}
  

\section{Behaviour Descriptions}

The \gls{CIM} classes that are related to behaviour descriptions are \texttt{Agreement} and
its child classes such as \texttt{CustomerAgreement}, \texttt{EnergyProduct},
\texttt{IntSchedAgreement} and \texttt{OpenAccessProduct}, as well as
\texttt{DemandResponseProgram} and \texttt{EndDeviceControl}, which contain an
association to \texttt{CustomerAgreement} and describe certain kinds of control
schemes that can  be part of an agreement. However, these classes cannot be
directly used to describe behaviour descriptions.

Some \gls{CIM} classes that can be used as parts of a behaviour description
have already been described in section \ref{sec:std-cim-power-system-ontology}, while 
a CIM mapping for services, which are a central part of behaviour descriptions,
has been outlined in section \ref{sec:std-cim-services}.  

The actual format of behaviour descriptions will depend on the actual
implementation used for behaviour descriptions; the next section outlines such a
format for rule-based behaviour descriptions.


\section{Rule-Based Behaviour Descriptions}

Numerous options exist for expressing behaviour descriptions (see section
\refnpage{sec:levels-of-beh-desc} for some examples), but in this thesis, a
rule-based system has been chosen, due to its flexibility in how
services can be activated. The rule-based system requires several additional
capabilities from the behaviour description data format:

\begin{itemize}
  \item if-then rule structure
  \item conditions
  \item actions
\end{itemize}

\gls{CIM} does not contain any classes that could be used to build rules.
Therefore, the CIM has to be extended to support rules. A simple procedure would
be to embed an existing \gls{XML} format for rule bases, such as the one from
\gls{Drools} that was used in this work. However, that \gls{XML} format is just a
translation of the language-like format into an \gls{XML} format; the action
part of rules is the verbatim \gls{Java} code.

Therefore, a simple XML format for rules was created that allows the expression
of conditions and actions. The structure of a rule was illustrated in figure
\refnpage{fig:policy-basic-rule-structure}. A rule base consists of a sequence of rules, which
have a condition part and an action part. The condition part supports Boolean
operators (\texttt{and}, \texttt{or}, \texttt{exclusive-or}, \texttt{not}),
comparison operators (smaller, greaterorequal, equals, etc.), arithmetic
operators (plus, minus, multiplication, division ), variables, constants,
method calls, and trigger signal values. The condition part allows the
activation of services. This supports the same kinds of 
expressions that were used in the \gls{Drools} rule base examples, e.g.~in listing
\refnpage{lst:simple-example-rulebase}.

An example rule base using that format is shown in listing
\ref{lst:xml-rulebase}. It is very similar to the \gls{Drools}-based rule base
that was shown in listing \refnpage{lst:real-example-rulebase}. The main
difference is that the service activations are not put into a special
\texttt{setup} rule as in the \gls{Drools} rule-base, but instead are provided
separately, which is a cleaner method than having the \texttt{setup} rule.
The \gls{XML} format is also not directly tied to a \gls{Java} implementation of rules; it can
treat services as first class objects, instead of just as \gls{Java} objects.

\lstinputlisting[language=xml,%
  caption={XML Rule Base},%
  label=lst:xml-rulebase%
]{listings/policy.xml}


