#!/usr/bin/env python

import sys, glob, re

files = glob.glob("part*.tex") + glob.glob("unfiled*.tex") + glob.glob("warming-up*.tex")

labelRegex = re.compile( r"\\label{([^}]+)}" )
label2Regex = re.compile( r"label=([^,%}\]]+)" )

oFile = open( 'labels.txt', 'w' )

def matches():
  for file in files:
    with open(file, "r") as fh:
      for line in fh.readlines():
        idx = line.find('%')
        if idx >= 0:
          line = line[0:idx]
        for match in labelRegex.finditer(line):
          yield match.group(1)
        for match in label2Regex.finditer(line):
          yield match.group(1)

for match in matches():
  oFile.write( match + "\n" )
  if len(sys.argv) > 1:
    output = True
    for item in sys.argv[1:]:
      if item not in match:
        output = False
        break
    if output:
      print match
  else:
    print match

