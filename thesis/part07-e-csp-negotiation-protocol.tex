% up: part07---policies.tex
\subsection{CSP Notation of Protocol}
\label{sec:policy-negotiation-protocol-csp}

In this section, the policy negotiation protocol will be described using
\gls{CSP} notation. This is done to better define the protocol.

The protocol happens between two parties, the controlled unit, called the
client, and the supervisory controller, called server.  The protocol is
described as two processes, one executing on the client named $P$, the other on
the server named $Q$. Each process has its own \gls{CSP} input and output channels
with which messages are exchanged. They are called \texttt{in} and \texttt{out}
in both processes, i.e.~the \texttt{in} channel of the client is connected to
the \texttt{out} channel of the server, and the \texttt{out} channel of the
server is connected to the \texttt{in} channel of the client.

The notation is described in great detail in \cite{CSP}; here is a short
reference:
\begin{itemize}
  \item Process definition: $\mbox{Process name} \;\; = \;\; \mbox{Process definition}$
  \item Sending a message: $\mbox{channel} \; ! \; \mbox{MessageType}$
  \item Receiving a message: $\mbox{channel} \; ? \; \mbox{MessageType}$
  \item Event: $\mbox{event name}$ \\ 
    This is an external event that happens; the event is not a message, because
    it is not sent by a different process, but comes from the environment. 
  \item Sequential execution: $\mbox{Process}_{1} \rightarrow \mbox{Process}_{2}$ \\
    First Process$_{1}$ is executed, and then Process$_{2}$ is executed
  \item Choosing an option: $\mbox{Process}_{1} \; | \; \mbox{Process}_{2}$ \\
    Either Process$_{1}$ or Process$_{2}$ are executed

\end{itemize}

Options are executed non-deterministically, i.e.~\gls{CSP} does not define
which of the options is going to be executed. In this protocol, they are used to
react to received messages or external events; which of the options is chosen
depends on the next message or event that is received.

Each process is subdivided into single steps, e.g.~P$_{1}$ to P$_{5}$, which
define the steps of the protocol. The regular line of messages
exchange is pretty easy to follow; additional measures ensure that the
connection is working:

\begin{enumerate}

  \item When a response message is expected, and none comes after a certain
    period of time (the implementation uses 120 seconds), the whole process is restarted
    again from the beginning. This is expressed as $\mbox{wait}_{x}$.

  \item Waiting for a behaviour description can take a long time, but the
    client should not wait forever.  This is expressed as $\mbox{wait}_{y}$.

  \item The client regularly sends an \emph{alive} message to the server.

\end{enumerate}

The \gls{CSP} notation is not complete here. The different types of messages
are handled here as though they are constants, i.e.~messages without content.
This is how \gls{CSP} handles messages. The main reason for providing the
\gls{CSP} description is to describe the flow of messages; therefore, the
actual content of the messages can be safely ignored here.


\begin{figure}
  \begin{center}
  \begin{eqnarray*}
  P\hspace*{2px} &=& P_0 \\[.2cm]
  %
  P_0 &=& \mbox{out} \; ! \; \mbox{RegisterClient} \rightarrow P_1 \\[.2cm]
  %
  P_1 &=& \mbox{out} \; ! \; \mbox{RequestNegotiation} \rightarrow P_2 | \\
      & & \mbox{wait}_{x} \rightarrow P_1 \\[.2cm]
  %
  P_2 &=& \mbox{in} \; ? \; \mbox{ProposePolicy} \rightarrow P_3 | \\
      & & \mbox{in} \; ? \; \mbox{other} \rightarrow P_5 | \\
      & & \mbox{out} \; ! \; \mbox{AliveMessage }\rightarrow P_2 | \\
      & & \mbox{wait}_{y} \rightarrow P_1 \\[.2cm]
  %
  P_3 &=& \mbox{out} \; ! \; \mbox{AcceptPolicy} \rightarrow P_4 | \\
      & & \mbox{out} \; ! \; \mbox{RejectPolicy} \rightarrow P_2 | \\
      & & \mbox{wait}_{x} \rightarrow \mbox{out} \; ! \; \mbox{AliveMessage} \rightarrow P_3 \\[.2cm]
  %
  P_4 &=& \mbox{policyExpires} \rightarrow P_1 | \\
      & & \mbox{in} \; ? \; \mbox{ServerRequestNegotiation} \rightarrow P_1 \\
      & & \mbox{in} \; ? \; \mbox{other} \rightarrow P_5 | \\[.2cm]
  %
  P_5 &=& \mbox{out} \; ! \; \mbox{NotUnderstood} \rightarrow P_0 \\
  %
  \end{eqnarray*}
  \end{center}
  \caption{CSP Definition of Protocol Client Process}
  \label{fig:csp-policy-client}
\end{figure}

Figure \ref{fig:csp-policy-client} contains the \gls{CSP} definition of the client.
The process definition for the server in figure \ref{fig:csp-policy-server}
shows the exchange with a single client. Many such exchanges can occur in
parallel at the server, without the processes interfering with each other.

\begin{figure}
  \begin{center}
  \begin{eqnarray*}
  %
  Q\phantom{_0} &=& Q_0 \\[.1cm]
  %
  Q_0 &=& \mbox{in} \; ? \; \mbox{RegisterClient} \rightarrow Q_1 \\[.2cm]
  %
  Q_1 &=& \mbox{in} \; ? \; \mbox{RequestNegotiation} \rightarrow Q_2 | \\
      & & \mbox{in} \; ? \; \mbox{RegisterClient} \rightarrow Q_1 | \\
      & & \mbox{in} \; ? \; \mbox{AliveMessage} \rightarrow Q_1 | \\
      & & \mbox{wait}_x \rightarrow Q_t \\[.2cm]
  %
  Q_2 &=& \mbox{out} \; ! \; \mbox{ProposePolicy} \rightarrow Q_3 | \\
      & & \mbox{in} \; ? \; \mbox{RegisterClient} \rightarrow Q_1 | \\
      & & \mbox{in} \; ? \; \mbox{RegisterClient} \rightarrow Q_1 | \\
      & & \mbox{in} \; ? \; \mbox{RequestNegotiation} \rightarrow Q_2 | \\
      & & \mbox{in} \; ? \; \mbox{AliveMessage} \rightarrow Q_2 \\[.2cm]
  %
  Q_3 &=& \mbox{in} \; ? \; \mbox{AcceptPolicy} \rightarrow Q_4 | \\
      & & \mbox{in} \; ? \; \mbox{RejectPolicy} \rightarrow Q_2 | \\
      & & \mbox{in} \; ? \; \mbox{RegisterClient} \rightarrow Q_1 | \\
      & & \mbox{in} \; ? \; \mbox{RequestNegotiation} \rightarrow Q_2 | \\
      & & \mbox{in} \; ? \; \mbox{AliveMessage} \rightarrow Q_3 | \\
      & & \mbox{wait}_x \rightarrow Q_t \\[.2cm]
  %
  Q_4 &=& \mbox{policyExpires} \rightarrow Q_1 | \\
      & & \mbox{in} \; ? \; \mbox{RegisterClient} \rightarrow Q_1 | \\
      & & \mbox{in} \; ? \; \mbox{RequestNegotiation} \rightarrow Q_2 \\
      & & \mbox{in} \; ? \; \mbox{AliveMessage} \rightarrow Q_4 | \\[.2cm]
  %
  Q_t &=& \mbox{out} \; ! \; \mbox{ServerRequestNegotiation} \rightarrow Q_1 \\[.2cm]
  %
  \end{eqnarray*}
  \end{center}
  \caption{CSP Definition of Protocol Server Process}
  \label{fig:csp-policy-server}
\end{figure}


The protocol must be able to handle the loss of any message of the
protocol, as well as temporary downtime of any party in the protocol.
This is done on the client side by timeouts, i.e.~the client
waits only for a certain period of time for a reply from the
server. When the client does not receive such a message, it restarts
the protocol from the beginning.

On the server side, errors handled in various ways:

\begin{enumerate}

  \item Unexpected \texttt{RequestNegotiation} and \texttt{RegisterClient}
    messages lead to a restart of the negotiation protocol.

  \item When waiting for a message from the client, timeouts are used, which
    result in restarting the negotiation.

\end{enumerate}


