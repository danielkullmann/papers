% up: part07---policies.tex
\section{Implementation Using a Rule-Based System}
\label{sec:implementation-using-rules}

Behaviour descriptions can be implemented in many different ways.  Actually,
any \gls{SCADA}-based system can be said to use an implicit behaviour
description: the behaviour description basically says ``react to the setpoints
from the supervisory controller'', and the trigger signals are the setpoint
messages. 

The method that was chosen to implement behaviour descriptions for this work is a
rule-based system.  Here, a behaviour description consists of a set of rules. A
rule has this form:

\begin{lstlisting}[language=pseudo]
  if condition then actions
\end{lstlisting}

The \texttt{if} part contains the condition of the rule, i.e.~a Boolean
expression. When the expression evaluates to true, the actions in the
\texttt{then} part are executed.  The structure of a single rule is illustrated
in figure \ref{fig:policy-basic-rule-structure}.  Each rule consists of a
condition part and an action part. The condition part can contain conditions,
which are Boolean values. Parts of a condition can be combined using Boolean
operators. Boolean values can also be created by using comparison operators
that compare values. Values are either constants, signals, the results of
method calls, or combinations of other values with operators such as plus or
multiply. For the use with behaviour descriptions, actions are mostly 
service activations, but can also be used to set values or activate other rules.

\begin{figure}
  \centering
  \includegraphics[width=0.9\linewidth]{generated/policy-rules.pdf}
  \caption{Basic Structure of a Rule}
  \label{fig:policy-basic-rule-structure}
\end{figure}


How a rule set is used in a component is illustrated in
Figure~\ref{fig:behaviour-descriptions}: the rule base is embedded in the
environment of the component, the conditions reference values from the
environment, and the actions change the environment.
The actual behaviour of the components is encapsulated in services, therefore
actions generally just activate services, and conditions reflect the
circumstances for a service activation.

\begin{figure}
  \begin{center}
    \includegraphics[width=0.9\linewidth]{generated/behaviour-wide.pdf}
  \end{center}
  \caption{A Behaviour Description and its Environment}
  \label{fig:behaviour-descriptions}
\end{figure}

An essential part of a rule-based system is the environment the rule is
situated in; without it, a rule would have no access to the state of the
physical system (e.g.~measurements) and would not be able to perform any
actions. Since behaviour descriptions are meant to be a generic expression of
control, it is crucial that the environment is standardised. This demonstrates the
demand for a common \gls{ontology}. A suitable environment consists of the following
parts:
\begin{itemize}
  \item A facility to get access to trigger signals, such
    as local measurements and broadcast messages received.
  \item References to the services that are currently provided.
  \item A mechanism to activate a particular service.
\end{itemize}

\subsection{Example Rule Base}
\label{sec:example-rule-base}

In figure \ref{lst:simple-example-rulebase}, an example of such a rule base for
the control of heaters in a household is shown; similar rule bases were
generated in the experiment that is described in section
\ref{sec:experiment-1}. The language used to express the rule base comes from
the \gls{Drools} library; see section \refnpage{sec:related-rule-bases}. 

\lstinputlisting[language=drools,float,%
  caption={Example Rule Base},%
  label=lst:simple-example-rulebase%
]{listings/policy.drl}

The behaviour description has three rules, \texttt{frequency-1},
\texttt{power-2}, and \texttt{thermostat-3}. The rules all reference a service
scheduler responsible for scheduling services.  Each
rule also references the service that it controls, and when this service should 
be active (this is done by the \texttt{shouldBeActive == true} part), the
scheduler is told to make this service active. The three rules are mutually exclusive;
only the first rule that fires is actually activated.


\subsection{Disambiguation Strategy}
\label{sec:disambiguation-strategy}

When a rule base consists of multiple rules, there is always the 
possibility that more than one rule will fire for a given state of the system.
When the actions taken by these rules contradict each other, it is
necessary to mutually exclude these rules, i.e.~one of the fired rules has to
be chosen as the one whose action is actually executed. This is done by a
so-called disambiguation strategy. 

The simplest strategy is to give each rule a priority level, i.e.~to prefer
rules with higher priority.  This is realised by having the first rule
that matches win. The rules have to be ordered by descending priority,
e.g.~rules dealing with emergencies come first, rules containing less important
services come later.  This strategy will be sufficient for
many rule bases, because the priorities are already settled when defining the
rule base.

A more flexible system is to calculate the priorities in the rules
themselves, and select the service with the highest priority to be activated.  


\subsection{Flexibility}

A central argument in favour of using such a system is that it is extremely
flexible.  Several parts of a rule base, such as the one from listing
\ref{lst:simple-example-rulebase}, can be changed: 

\begin{itemize}
  \item Other \textit{services} can be used.
  \item Other \textit{trigger signals} can be used.
  \item The rule base can contain an arbitrary number of rules.
  \item The conditions in the rules can be changed.
  \item Rules can activate the firing of other rules.
\end{itemize}


\subsection{Discussion}

The big advantage of using a rule-based system is its flexibility. It can be
used to control many different types of devices, because it is based on service
descriptions and service activations. The rules can be arbitrarily complex,
ranging from the example displayed in listing \ref{lst:simple-example-rulebase}
to very complicated rule bases.

A rule-base contains a fully self-contained description of behaviour; the
controlled component does not need any extra information from the supervisory
controller to execute it.

Other advantages of rule-bases are that they are relatively easy to understand
for humans, that encoding simple behaviours results in simple rule-bases, that
it is easy to compile a rule base into a binary program, and that services can
be easily integrated into rule bases. The example rule base shown earlier
demonstrates that a service can be treated as a black box in a rule base,
meaning that the actual implementation of the service does not matter, given
that the service implementation provides the correct API: producing service
descriptions, accepting service activations, responding to the ``do you want to
be activated'' request (\texttt{shouldBeActive} in the example above), and
providing hooks to activate and deactivate the service.

The biggest disadvantage of using a rule-based system is the complexity of
getting all the parts to work together: Trigger signals, services, 
behaviour descriptions and the execution environment for a rule base all have
to be in place. 

The computational load of executing such a system might prove to be too
much for some of the devices that should support such a control scheme.
Household appliances, for example, will probably never have the ability to
support behaviour descriptions directly; it will be the task of a home gateway
computer to control the appliances. Another solution for this issue lies in
converting the behaviour description into a binary program that can be executed
on the embedded device, as explained in section \refnpage{sec:bd-compiled}.

The advantages that rule-based behaviour descriptions provide outweigh the
disadvantages for most applications. Therefore, this option has been chosen for
implementation in this thesis.

