% up: part09---experiments.tex
\section{Simulation: Heater Control}
\label{sec:sim-flexpower}

The experiment described in the last section illustrated one major issue with
running experiments like these in a laboratory setting such as \gls{SYSLAB}:
only a few controllable components are available; therefore, the scalability of
a system based on behaviour descriptions cannot be evaluated by experiment
alone.  The first simulation, described in section \ref{sec:sim-ev-charging},
only simulated the exchange of messages, but did not simulate the actual
effects of the behaviour descriptions on the power system.

Therefore, another simulation was carried out that used more components than the
experiment and simulated the effects of the behaviour descriptions in the
power system. This test simulated heater control in many houses; it is related to
\nameref{sec:usecase1-services} and \nameref{sec:usecase2-demandside}.

The simulation package that was used for this was originally developed for the
FlexPower project \cite{flexpower-project}. It had to be changed significantly
to be applicable for usage with behaviour descriptions; the main changes were
that frequency had to be supported as a trigger signal, the time step used for a
simulation had to be configurable, and the whole simulation execution was
automated. As a result, around 25\% of the source code of the simulation package
has been written by the author, which amounts to around 1400 lines of code (no
blank lines, no comments).

Initially, it was intended to run this simulation with hardware in the loop,
i.e.~one of the houses in the simulation should be the actual
\gls{PowerFlexhouse}.  Unfortunately, this could not be realised, owing to time
constraints.


\subsection{Setup}

The simulation used a similar setup as the heater control experiment; the
biggest change was that the simulation controlled many houses with 10 heaters each,
instead of a single house with 10 heaters as in the experiment.

The source code of both behaviour description implementation and simulation
package had to be changed to enable them to work together seamlessly. This
proved to be a challenge, because both packages expect to have full control.
Conflicts between policy framework and simulation package appeared in several
places:

\begin{itemize}

  \item Timestamping: the simulation has its own time keeping, but behaviour
    descriptions expect to run in real time.

  \item Frequency: the original simulation package supported only power price
    as an input signal; it had to be extended to support frequency as well.

  \item Managing trigger signals: the trigger signals (frequency, price)
    used by the simulation had to be routed through the simulation models and
    controllers to the controllers of the policy framework.

\end{itemize}

Two trigger signals were used in this simulation: the frequency was measured
directly at \gls{PowerFlexhouse}, while the power price was precomputed. The
reason for precomputing the power price was to get more response from the
price-based controller; other price data, such as the Nordic system price,
did not show enough variation.

\begin{figure}
  \centering
  \includegraphics[width=0.9\linewidth]{generated/flex-power-policies-simulation.pdf}
  \caption{Structure of the Simulation Setup}
  \label{fig:flexpower-simulation-setup}
\end{figure}

The resulting structure of the simulation is illustrated in figure
\ref{fig:flexpower-simulation-setup}. The simulator and model from the
simulation package could be used mostly unchanged (support for frequency
signals had to be implemented), but a new  controller supporting behaviour
descriptions had to be implemented. Each FlexPower controller started a policy
client, which participated in the negotiation protocol. The received behaviour
descriptions resulted in the start of the service controllers for frequency
control and price-based control, which could access the simulated heaters
through the FlexPower controller. 

The simulation system used a thermal model to simulate effects of heater
control actions to the inside temperature, and collected the aggregated heater
power consumption together with other state information.

The FlexPower simulation package provides different price controllers that 
simulations can use. These controller use different strategies to
calculate the temperature setpoint for thermostat control. The controller that
was chosen compares the current price to past prices, and adjusts the
temperature setpoint accordingly. A different controller using \gls{MPC} was
also evaluated, but it turned out that it consumed too much memory. The reason
for the memory consumption was that the controller kept state information for
every time step in its prediction horizon; since the time step was one second
and the prediction horizon 5 hours, each house controller stored 18000
instances of that state information, which proved to be too much when more than
50 houses had to be simulated. 

The price controllers described in the previous paragraph were not developed by
the author, but were already present in the FlexPower simulation package.

The server implementation was almost the same as in the experiment, the only
difference was that other classes were used to represent the trigger signals,
because these had to be made compatible to the simulation package.

The main changes to the client were:

\begin{itemize}
  \item Making the trigger signal classes compatible.
  \item Implementing a new heater controller that accessed the simulation model
    instead of real heaters.
  \item Changes to the data collection system due to the change from real
    heaters to simulated heaters.
\end{itemize}



\subsection{Simulation}

The simulation simulated 100 houses with $10$ $1\,\si{kW}$ heaters each.  As in
the experiment, the price-based service was offered by all clients, while the
frequency control service was offered only by a part of the clients. The
system frequency was measured at the point of common coupling of
\gls{PowerFlexhouse}, while the price was a precalculated curve so that
reactions could be better observed. Since frequency was measured from a real
power grid, the simulation also had to run in real-time.

As in the experiment, the houses provided intermittent frequency response,
i.e.~they were using frequency control algorithm \#3, as described 
\vpageref{sec:experiment-freq-algos}. % \vpageref adds "on page xxx" text


\subsection{Observations}

\begin{figure}
  \centering
  \includegraphics[width=0.8\linewidth]{generated/simulation2-result1-crop.pdf}
  \caption{Simulation 2: Trigger Signals and Consumption}
  \label{fig:sim2-result1}
\end{figure}

Figure \ref{fig:sim2-result1} shows results of one of the simulation runs,
which simulated 8 hours in real-time.  The figure graphs the power consumption
and both trigger signals over the whole simulation time. The correlation
between power price and power consumption is clearly visible: power consumption
is generally greater when the price is low. The correlation between frequency
and power consumption is not as visible, mainly due to the frequency staying
close to 50 Hz for most of the time. The significant drop in frequency during
the last hour of the experiment, however, results in low power consumption,
even though the power price is very low. This is caused by the higher priority
of the frequency control service.

%\todo{Better observations?}

\subsection{Results}

This simulation demonstrated that many components can be controlled at the same
time using behaviour descriptions. This indicates that behaviour descriptions
can help to scale a system, due to reduced communication requirements between
controller and controlled components. The amount of reduction depends of course
on the control algorithm that is used.

%\todo{Content}

