% up: part07---policies.tex
\section{Behaviour Descriptions}
\label{sec:behaviour-descriptions}

The simplest possible definition of behaviour descriptions is that they
are encapsulated, self-contained descriptions of behaviour of a
unit; the behaviour depends on a set of input signals that are locally
observable by the unit. A supervisory controller negotiates a behaviour
description with the
controlled unit, which then acts according to it. 

A simple example for a
behaviour description is to tie power consumption to the current power price:
When the price is high, reduce consumption as much as possible; when the price
is low, increase consumption as much as possible. Thus, a behaviour description 
encodes a local behavioural strategy for a single unit.

In section \ref{sec:onewaybroadcast-twowayunicast}, two different categories of
communication channels were examined, one-way broadcast and two-way unicast.
The former is very easy to make sufficiently reliable, but does not 
provide a back channel, whereas the latter is comparably unreliable, but
does provide a back channel. In that section, I expressed the desire to 
combine the two approaches meaningfully. Behaviour descriptions do that.
They provide the data format for the two-way unicast communication link, whereas
the behaviours in the behaviour description are triggered by the one-way broadcast
signals, which I therefore call trigger signals.

Behaviour descriptions are sent to the controlled device some time before the
behaviour is actually needed. This means that communication between supervisory
controller and controlled device only needs to be available during the negotiation
of the behaviour description, but not necessarily during the execution of the
behaviour description. This is possible due to the autonomous execution of the
behaviour description.

The behaviours that can be put into a behaviour description depend on the
capabilities of the device: which data can it obtain from its environment, and
what services can it provide based on that data? In the simple example above,
the component can obtain a dynamic power price from its environment, and it can
provide a simple consumption-control service.

Behaviour descriptions and service fit well together: a behaviour description
can contain one or more service activations. Behaviour descriptions therefore
allow the negotiation of service provision between a supervisory controller and
a controlled component.

Since a behaviour description describes future behaviour, it has a validity
period associated with it. It will only be activated at the start of
the validity period, and before the period ends, a new behaviour description
will have to be negotiated (see section \refnpage{sec:policy-negotiation-protocol}).



\subsection{Multiple Levels}

One of the features of behaviour descriptions is that it is possible to have
behaviour descriptions that act on multiple levels. This denotes the ability of
a behaviour description to offer multiple services at the same time, such as
frequency response, voltage control, or simply a schedule-following service.
The activation of these services is governed by a process that makes
sure that the services do not interfere with each other.

A simple example for such a system is a power-consuming component that
offers both a frequency control service to the power grid and a price-reduction
service to its owner. Since both services control the active power consumption,
only one of them can be active at any given time. This can be accomplished by
ordering them in a hierarchy: the frequency-control service has a higher priority,
so it should be activated when it is needed; in the time periods when it is not
needed, the price-reducing service can execute and help to optimise the
consumption of the component.


\subsection{Flexibility}

Behaviour descriptions offer several kinds of flexibility: 

\begin{itemize}

  \item They do not depend on a certain type of equipment; they only depend on
    the ability to communicate using the negotiation protocol (see below) that
    is used to find a suitable behaviour description.  

  \item They can activate any service that is supported by the
    component. 

  \item Behaviour descriptions can be tailored to the capabilities of the
    specific device. This includes the ability to respect the constraints a
    component has to comply to.  

  \item Many different trigger signals, either measurements or events, can be
    used to guide the behaviour of the component.

\end{itemize}

The flexibility of behaviour descriptions comes from the fact that the
communication is not based on an exchange of measurements and setpoints, but
rather on an exchange of capabilities, constraints and services. A controlled
component sends the set of supported services and the set of constraints
it has to adhere to to the supervisory controller. The
supervisory controller creates a behaviour description based on the information
from all controlled components and sends it to the component. The
behaviour description will reference the services the component can support, and
give the necessary parameters to the service so it can be executed on the
component. Example for parameters for a service is the droop value for a droop
controller, or an actual consumption schedule for a scheduled
consumption service.


\subsection{Comparison With Contracts}

A behaviour description can be interpreted as a contract: both parties,
supervisory controller and controlled device, agree on that contract, and 
both sides have to adhere to that contract: the controlled device has to
provide the service that was agreed upon, and the supervisory controller must
provide the compensation for provision of that service, such as a lower power
price. A behaviour description is a binding document which forces the component
to behave in a certain manner. 


\subsection{Metering the Behaviour of Components}
% non-adherence, non-observance
\label{sec:monitoring-behaviour}

The comparison of behaviour descriptions with contracts raises two questions:
\begin{enumerate}
  \item How can the supervisor check whether a component adheres to its
    behaviour description?
  \item What happens when the component does not adhere to the behaviour
    description?
\end{enumerate}

Answering the second question is outside the scope of this thesis. Anyway, this
will be part of the contracts between the owners of the supervisory controller
on one hand and the owners of the controlled components on the other hand.
Remuneration for provided services, as well as fines for the failure to provide
negotiated services should be part of these contracts.


Detecting a breach of contract will often be difficult. Take
for example a behaviour description that controls a number of loads in a
household, and which reduces their consumption in times of high load. One might
think that the correct behaviour can be checked by measuring the power
consumption at the grid connection of the house; however, that measurement
contains also power consumption of uncontrolled devices.  When controlled loads
reduce their consumption, and at the same time uncontrolled loads increase
their consumption, the consumption of the whole household will not change in
the expected manner. The service provision will still have happened, but it is
impossible to prove that, due to the distortion of the measurement by the
uncontrolled loads.

When comparing this with a \gls{SCADA}-based system where a component is
directly controlled by the supervisory controller, one can see that such a
system has a similar problem: the supervisory controller must be certain
that the component will react as expected to signals that are sent to it. If it
does not have full control over the component, it will have to resort to the
same measures as when control is based on behaviour descriptions.

Two options can be used to address that challenge:

\begin{itemize}

  \item Secure the component in question against tampering; this allows
    local logging of actions of the local controller, which the supervisory
    controller can trust. Unfortunately, this is not easy to accomplish,
    since all monitoring logic would have to be done on the local controller
    (the controlled component usually will not have enough computing power to do
    that itself), and this implies that the control signals sent from the local
    controller could be intercepted without much problem. However, the risk of
    tampering is still relatively small, because the effort to effectively
    subvert the control system is relatively high, compared with the benefits
    that could be expected.

  \item Measure power at the point of common coupling, and compare those
    measurements with expected behaviour. As explained above, the measurements
    can be misleading due to interfering uncontrolled loads; however, the
    measurements will still show the correct behaviour in many cases.

\end{itemize}

Since both methods are not completely satisfactory, they should be combined:
make equipment tamper-proof, and measure the effects of their actions. The
collected data can be analysed to assess how well the component has performed.


\subsection{Balancing Control and Autonomy}

In section \refnpage{sec:balancing-control-and-autonomy}, the challenge of
balancing autonomy and control in a distributed control system was explained:
it is important for the power system to move part of the decision making process down in
the control hierarchy, i.e.~to make distributed components more autonomous, but at
the same time it is important for many types of services to limit the extent to
which autonomy is allowed. 

Behaviour descriptions offer an interesting solution to this issue: the
behaviour description is a contract between the supervisor and controlled
component, which means that the controlled component commits to behave in a
certain way. Some of the responsibility is transferred to the controlled
component, which means that the supervisory controller does not need to monitor
the component as closely as it otherwise would have to.

This controlled autonomy makes sense especially in situations where the service
rendered by the component is crucial to the whole system, e.g.~when ancillary
services are provided.  Components controlled through behaviour descriptions
therefore act semi-au\-to\-no\-mous\-ly.


\subsection{Local Decision Making}

In section \ref{sec:local-decision-making}, I discussed the requirements for a
distributed control system. Behaviour descriptions follow the advice outlined
there, namely that distributed units should make decisions largely based on
information that they can obtain locally. System frequency and voltage are
examples for this kind of information. Furthermore, behaviour descriptions
encourage the use of one-way broadcast messages for the distribution of
information, such as a dynamic power price. Distributed units can
additionally use this information
to obtain a better picture over the over-all system state.


\subsection{Balancing Different Needs}

It can be seen from the explanation of behaviour descriptions that they 
balance different needs of controller and controlled component:

\begin{itemize}

  \item Control vs.~autonomy: a controller might need control over the 
    components it supervises, while the controlled component is able to
    perform some tasks autonomously. Behaviour descriptions allow both.

  \item Synchronous vs.~asynchronous communication: some control schemes require
    synchronous communication to function, such as in a \gls{closed-loop} 
    control, but it is unwise to have a supervisory controller
    control its components with \gls{closed-loop} control. Instead, it should use 
    asynchronous communication, such as \gls{open-loop}
    control, or at least \gls{closed-loop} control with large time constants.

  \item Local vs.~global knowledge: a local controller can efficiently only get
    a limited view about the state of the whole system; in most cases, it will
    have to work with information that it can obtain locally. A central
    controller, on the other hand, has a much better overview over the whole
    system. These two extremes are balanced by having the local controller
    execute behaviour descriptions received from the supervisory controller;
    the behaviour descriptions contain important information about the state of
    the whole system in processed form.

\end{itemize}


\subsection{A Behaviour-Description Based System}
\label{sec:parts-of-beh-desc-system}

A system that is based on behaviour descriptions consists of several parts:
\begin{enumerate}
  \item The data format of the behaviour descriptions: later in this chapter,
    a data format based on rules will be presented.
  \item The data format for device descriptions: this is necessary to provide 
    information about the connected devices to the next higher level, e.g.~an
    aggregator. The information contains the description of services that can 
    be activated, and the constraints that the device will have to comply with.
  \item A negotiation protocol: this is used to exchange device descriptions
    and behaviour descriptions; this will be presented in section 
    \refnpage{sec:policy-negotiation-protocol}.
  \item A mechanism for generating behaviour descriptions; this depends on the
    behaviour description data format, on the services that will be provided,
    on the supported device constraints, and on the goals of the entity generating
    the behaviour descriptions. 
  \item A local environment for executing the behaviour descriptions; this consists of
    several parts:
    \begin{itemize}
      \item Access to external events, such as updates of a dynamic power price
      \item Access to local measurements, such as frequency and voltage 
      \item Access to the services that are provided by the local component;
        this includes the ability to schedule the execution of these services
    \end{itemize}
\end{enumerate}


The policy framework that will be introduced in chapter \ref{sec:policy-framework}
implements the negotiation protocol and has extension points to integrate
actual implementations of behaviour descriptions, device descriptions, and
agents for the supervisory and the local controller.

The experiments that have been carried out in \gls{SYSLAB} have implementations for the
device descriptions, the behaviour-descriptions generators, and the local environment.
They will be described in the appropriate sections in chapter \ref{sec:experiments}.

