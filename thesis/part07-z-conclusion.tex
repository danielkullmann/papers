% up: part07---policies.tex
\section{Conclusion}
\label{sec:bd-conclusion}

This chapter described the second central concept that this thesis puts forward,
behaviour descriptions.

A behaviour description is a self-contained description of behaviour which is
agreed upon between a supervisory controller and one of its controlled component.

Agreement on a behaviour description is accomplished by using the negotiation
protocol, which is very similar to the \gls{CNP} protocol defined for
\gls{MAS}.

In its simplest form, a behaviour description is just a wrapper around a single
service activation. The flexibility of the approach is increased by combining
several service activations. Additional flexibility is achieved
by adding logic to the behaviour description that decides which service or
services to actually activate.

Several options for behaviour descriptions have been given; the one that has
actually been implemented is a rule-based system, which exhibits
the greatest flexibility of all options. 

In section \refnpage{sec:8-fallacies}, the eight fallacies of distributed
computing were listed. It is time to revisit them here, to see how well
behaviour descriptions and the negotiation protocol perform:

\begin{enumerate}
  \item \emph{The network is reliable}: 
    This is never assumed; behaviour descriptions were specifically designed to
    counter unreliable networks. The negotiation protocol restarts after a
    timeout, so it does not assume a reliable network either.
  \item \emph{Latency is zero}: 
    This is not assumed, neither by behaviour descriptions nor by the protocol.
  \item \emph{Bandwidth is infinite}: 
    Behaviour descriptions are negotiated some time before they are used, so
    limited bandwidth is not a problem.  However, bandwidth should not be too
    small.
  \item \emph{The network is secure}: 
    The policy framework ensures the integrity of the messages by digitally
    signing them; this prevents forging and changing of messages, and provides
    non-repudiation (i.e.~a unit that has sent a signed message cannot claim
    that it did not sent that message)
  \item \emph{Topology does not change}: 
    Topology changes in the communication network are treated by some mechanism on a lower
    layer of the communication system.  Changes in the topology of the power
    grid are not directly addressed by behaviour descriptions or the
    negotiation protocol.  An aggregator has to be notified of topology
    changes, because that might interfere with its ability to provide services
    to certain parts of the power system (e.g.~when dealing with congestion). 
  \item \emph{There is one administrator}: 
    This indicates two sources of issues: 1. that different legal entities with
    different goals are owning components; 2. that one party cannot expect to
    have full control over the components of a second party.  Behaviour
    descriptions can be interpreted as short-term contracts which are
    mediated via long-term contracts that have been set up between the
    involved parties. The long-term contracts specify the actions that the 
    supervisory controller is allowed to initiate in a controlled component, so
    multiple administrators do not interfere with the operation of the system.
  \item \emph{Transport cost is zero}: 
    The amount of data exchanged is relatively small, and the negotiation
    protocol tolerates long negotiation periods, so this does not introduce 
    problems.
  \item \emph{The network is homogeneous}: 
    The network is expected to be heterogeneous, both in terms of the
    communication technology that is used, and in the components that are
    connected through the network.
\end{enumerate}

This shows that behaviour descriptions and the negotiation protocol do not make
any of these assumptions.  Actual implementations of behaviour descriptions,
however, must make sure not do make any of these assumptions on their own,
e.g.~regarding a reliable network.

