#!/usr/bin/python
# coding=utf-8

import sys, os, stat, glob, re #, subprocess, operator, argparse

# For check: "Capitalised Words in Section Headers"
reHeaders = re.compile(r"\\(subsection|section|subsubsection|chapter|caption){([^}\\{]*)[}\\{]", re.MULTILINE)
wordSplitter = re.compile( "[- /~\"]" )
fillerWords = "the a of an in and vs. for be is on to it over per by as when with".split() # This words should not be capitalized
capitalizedWords = "IT ICT MAS CSP IEC SCADA UML IEC TR CNP OpenADR".split() # This words are allowed to be capitalized
removeChars = re.compile( "[()]" )
abbreviations = None # Will be filled in later

# For check: "All .tex Files are Included Somewhere
reIncludes = re.compile( r"^([^%]*)\\(include|input){([^}]*)}" )
reAcronym = re.compile( r"\\nacronym{([^}]+)}" )
reGlossary = re.compile( r"\\nglossary{([^}]+)}" )
reSpaceBeforeCite = re.compile( r"(^|[^~])\\cite{[^}]*}" )
reSpaceAfterIeEg = re.compile( r"(e\.g\.|i\.e\.)($|[^~])" )

includedFiles={}
definedAcronymns=[]
definedGlossary=[]

reFindAcronyms = None

def findLine(str, start, end):
  sindex = str.rfind("\n", 0, start)
  if sindex < 0: sindex = 0
  eindex = str.find("\n", end, len(str)) 
  if eindex < 0: eindex = len(str)
  return str[sindex:eindex].replace("\n", " ")

def checkFile(fileName, content, lines):
  global includedFiles, definedAcronymns, definedGlossary
 
  # For check: all .tex files are included somewhere
  included = []
  for line in lines:
    for m in reIncludes.finditer( line ):
      incFile = m.group(3)
      included.append(incFile + ".tex")
  includedFiles[fileName] = included
  
  for m in reAcronym.finditer( content ):
    definedAcronymns.append(m.group(1))
  
  for m in reGlossary.finditer( content ):
    definedGlossary.append(m.group(1))

#  for m in reSpaceBeforeCite.finditer( content ):
#    print fileName + ":", "\\cite w/o ~", findLine( m.group(0), m.start(0), m.end(0) )

  for m in reSpaceAfterIeEg.finditer( content ):
    print fileName + ":", "i.e./e.g. w/o ~", findLine( m.group(0), m.start(0), m.end(0) )


texFiles = glob.glob('[a-tvx-z]*.tex')
checkFiles = texFiles
if len(sys.argv) > 1:
  checkFiles = sys.argv[1:]

for file in checkFiles:
  f = open( file, "r" )
  lines = f.readlines()
  content = ''.join(lines)
  checkFile( file, content, lines )

allTerms = definedAcronymns[:]
for t in definedGlossary: allTerms.append( t )
reFindAcronyms = re.compile( r"[^{(]\b(" + "|".join(allTerms)  + r")\b[^})]" )
abbreviations = allTerms

for fileName in texFiles:
  f = open( fileName, "r" )
  lines = f.readlines()
  content = ''.join(lines)

  if fileName != "acronyms.tex":
    for m in reFindAcronyms.finditer( content ):
      term = m.group(1)
      start = m.start(1)
      end = m.end(1)
      # TODO filter out occurrences of acronym/glossary term that are
      #      already inside a \gls or similar
      #rightStart = (content[start-len("\\gls{"):start] == "\\gls{" or
      #  content[start-len("\\nacronym{"):start] == "\\nacronym{" or
      #  content[start-len("\\index{"):start] == "\\index{" or
      #  content[start-len("\\indexb{"):start] == "\\indexb{" or
      #  content[start-len("\\nglossary{"):start] == "\\nglossary{")
      #if not rightStart:
      #  ...
      # TODO too much output right now..
      # print fileName, "glossary term", term, ":", findLine(content, start, end)

  # For check: capitalised words in section headers
  for m in reHeaders.finditer( content ):
    header = m.group(2)
  
    # List of words in header
    words = wordSplitter.split( header )

    # First word should be capitalized
    if words[0].lower() in fillerWords:
      if not words[0][0:1].isupper():
        print fileName + ":", str(m.start()), "Capitalize First Words in Headers, even filler words", header

    fillers = [ removeChars.sub( "", w ) for w in words[1:] if w in fillerWords and w[0:1] != '\\' ]
    if any( [w[0:1].isupper() for w in fillers] ): 
      print fileName + ":", str(m.start()), "Capitalize Words in Headers, but not filler words like the, a, of", header

    words = [ removeChars.sub( "", w ) for w in words if not w in fillerWords and w[0:1] != '\\' ]
    if any( [w[0:1].islower() for w in words] ): 
      print fileName + ":", str(m.start()), "Capitalize Words in Headers (except filler words like the, a, of)", header

    for w in words:
      if (not w in capitalizedWords) and (not w.upper() in capitalizedWords) and not w in abbreviations and w != w.lower().capitalize():
        print fileName + ":", str(m.start()), "Wrong Capitalization of Words in Headers", header
 
# For check: "All .tex Files are Included Somewhere
# Start with daku-thesis.tex, and put all files included in that file into the list incFiles
# Do this again for the included files
waiting = ['daku-thesis.tex']
incFiles = []
while len(waiting) > 0:
  item = waiting[0]
  waiting = waiting[1:]
  incFiles.append(item)
  if item in includedFiles:
    for f in includedFiles[item]: waiting.append( f )
  else:
    print "WARNING: Unknown file", item



# incFiles should now contain all files in texFiles
for f in checkFiles:
  if not f in incFiles and f != "just-the-text.tex":
    print f + ":", "File is never included"

#print definedAcronymns
#print definedGlossary

# Search warnings in LaTeX's log files
os.system("""egrep -iH "warning|undefined" _output/*.log | grep -v "Package: infwarerr .* Providing info/warning/message" """)

