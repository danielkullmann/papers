% up: part09---experiments.tex
\section{Simulation: EV Charging}
\label{sec:sim-ev-charging}

The first test of behaviour descriptions was a very simple simulation of
\gls{EV} charging, i.e.~\nameref{sec:usecase3-charging}. The goals of the
simulation were to demonstrate the generation and distribution of individual
behaviour descriptions for each controlled component, and to evaluate the time
needed to distribute behaviour descriptions to many clients.  

\subsection{Setup}

This simulation used a very simple format for behaviour descriptions, not the
rule-based behaviour description format used by the other tests described in
this chapter.

A behaviour description consists of a calculated consumption schedule.
The schedule is calculated depending on the requirements of the \glspl{EV}.
These are expressed as two values: the amount of energy that has to be used for
charging the battery of the \gls{EV} (measured in kWh), and the point in time
when the \gls{EV} will be used again.

The simulation was based on several assumptions:
\begin{itemize}
  \item Each electric vehicle has a $1\,\si{kW}$ charger
  \item The charger can be either on or off, i.e.~it consumes either $0\,\si{kW}$ or
    $1\,\si{kW}$
  \item Round-trip efficiency of the charging process was assumed to be 60\%
    % DONTDO where does that number come from (i.e.~give a cite?) Ask Oscar for
    %"real" efficiency value
  \item Only a limited number of electric vehicles can be charged at the same time;
    this was set to 50\% of all vehicles
  \item The schedule is given as a charging flag (i.e.~1 $\rightarrow$
    charge, or 0 $\rightarrow$ do not charge) for every 15 minute
    period of the charging period
\end{itemize}

The schedules were optimised using linear programming by employing the \gls{GLPK}
\cite{glpk}, the objective function being to minimise the cost of charging the
batteries:

\begin{equation*}
  \begin{array}{l l}
    f = & \sum_{c \in C, t \in T} p_t e_{c,t} \\
    C: & \mbox{set of controlled clients} \\
    T: & \mbox{time steps in planning horizon} \\
    p_t: & \mbox{power price at time $t$, including round-trip efficiency} \\
    e_{c,t}: & \mbox{whether client $c$ should charge at time step $t$: 
    0 $\rightarrow$ no, 1 $\rightarrow$ yes} \\
  \end{array}
\end{equation*}

The power price was the system spot price of the Nordic power system, which was
obtained from the Nordpool electricity market.

\subsection{Case Simulation}

Because the test did only simulate the exchange of messages, but not the
response of the power system, it did not employ any simulation package. It just
executed a server thread and many client threads on the same machine. 250
\glspl{EV} were simulated.  Each \gls{EV}
agent randomly selected a current state of charge, required state of charge,
and point in time that the required state of charge needed to be reached.

The server process waited for all client agents to connect, and then used linear programming
to find optimal schedules, which were transferred to the clients. The clients
logged the received behaviour descriptions to a log file, for later analysis.
After all clients had received a behaviour description, the program shut down.

The actual charging process was not simulated, the only concern of the
simulation was the calculation of schedules and the exchange of data (\gls{EV}
description and behaviour descriptions).


\subsection{Observations}

\begin{figure}
  \centering
  \includegraphics[width=0.9\linewidth]{generated/evc1-consumption.pdf}
  \caption{Planned Schedule}
  \label{fig:sim1-schedule}
\end{figure}

The duration of the schedule calculation for 250 \glspl{EV} varied from one to
several minutes. Sometimes, the optimiser could not find an optimal solution,
since the maximum number of vehicles that were allowed to charge at the same
time was limited. The distribution of the behaviour descriptions using the
negotiation protocol took another few minutes, which can mostly be attributed
to the simulated delays in the communication (see below), and to the load the
computer was under ($\approx 250$ concurrent threads).

Figure \ref{fig:sim1-schedule} shows the results of the planning algorithm for
one particular simulation run. It shows that the algorithm chose the times of
lowest power price for charging the electric vehicles. The main disadvantage of
the planning algorithm is that it was not able to spread charging over a longer
period of time, as would be necessary for situations with constrained charging
capacity. Nevertheless, it was not the goal of this thesis to develop better
planning algorithms, so the result is acceptable.

The progression of clients through the states of the negotiation protocol is
shown in figure \ref{fig:sim1-client-states}; it displays one round of the
negotiation protocol for 250 clients. Each red line is a client progressing
through the 4 states in the negotiation protocol (see section
\refnpage{sec:policy-client-states}). The simulation in question was run on a
single computer, but used a custom network socket factory, created by the
author, which emulated network delays. The delays used for that simulation were
$100\pm 50\,\si{ms}$ latency, $20\pm 1\,\si{MB/s}$ bandwidth from server to
client, and $2\pm 0.5\,\si{MB/s}$ bandwidth from client to server; these delays
(bandwidth was converted to $\si{ms/byte}$) were applied in addition to the
inherent performance characteristics of the local network connection.

% DONTDO Simulate with and without network delays? 
% Simulate 100, 250, 500, 750, 1000 clients?

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{generated/evc1-states.pdf}
  \caption{Client States over Time}
  \label{fig:sim1-client-states}
\end{figure}


\subsection{Results}

This was a simple simulation of the exchange of behaviour descriptions
using the negotiation protocol, and a simple implementation of a planner
algorithm for scheduling the charging of \glspl{EV}.

The observations show that the exchange of behaviour descriptions is carried
out in a relatively short period of time. This indicates that a system based on
behaviour descriptions is scalable.

The generated schedules were not optimal; an optimisation library accepting
non-linear problem descriptions could be used instead of the linear programming
library.

The simple simulation demonstrated flexibility of the supervisory
controller: schedules were calculated to adhere to the constraints of the
individual \glspl{EV}. 

%\todo{Claims: Balancing control and autonomy? $\rightarrow$ Not much
%autonomy}

Even with extra delays in the communication links, the distribution of
behaviour descriptions was reliable. The complete failure of a communication
link was not simulated. %\todo{This was done in the second experiment.}


