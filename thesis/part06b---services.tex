\chapter{Services}
\label{sec:services}

In this chapter, I argue that a strong \gls{service} concept is an important part of
the realisation of the \gls{Smart Grid}. All tasks that have to be carried out
in a power system can be represented as services. In that context, it is
important to note that the term \emph{service} can refer to two different
concepts:

\begin{enumerate}

  \item The services that are provided on the power grid, which refer to
    physical tasks that components offer, e.g.~real and reactive power output.

  \item The services that are provided via \gls{ICT}, over the communication
    network; a \gls{web service} is such a service.

\end{enumerate}

The power grid services are activated through the \gls{ICT} services, resulting in
a dependence on the availability of the \gls{ICT}
services.  A failure in the communication links of the \gls{ICT} network might
imply that power grid services cannot be activated; a failure in the power grid
might imply that services that have been requested through the \gls{ICT}
network cannot be provided, in case no power flow can be established. 

In power system operations, the concept of \glspl{ancillary service} has been
used for a long time. These are services in the power system that are required
to accomplish secure grid operation and power supply. But not only ancillary
services have to be provided, also the primary services power generation and
power consumption. This demonstrates that the concept of service is central to
power systems. 

The services that are provided in power systems can be organised into several
basic categories; see also \cite{Rebours07a,Rebours07b,Nyeng2007} and section
\refnpage{sec:related-power-system-services}:

\begin{itemize}

  \item Generation: includes all types of generation, controllable as well
    as uncontrollable generation. 

  \item Consumption: includes various kinds of services, such as consumption
    that can be scheduled or time-shifted. The services that belong in this
    category are not very well developed, because this is still an active
    research area.

  \item Frequency control: the services that fall in this category depend
    on the ancillary services definition for that particular power grid area.

  \item Power balance: again, the services in this category depend on the 
    particular ancillary services definition.

  \item Voltage control: this service is for the most part independent from
    frequency and power balance services, although the capabilities for
    providing these two kinds of services, active and reactive power, might
    depend on each other in non-trivial ways.

  \item Protection services: these are provided by sensors and breakers
    in the grid: when a fault is detected, it is located and then isolated by
    opening breakers.

  \item Black start services: when a grid had a complete blackout, generators
    with black start capability have to put the grid into a working state again,
    before other generation can be reconnected.

  \item Islanding services: in case of problems in the transmission system, a
    small part of a power grid can disconnect itself from the rest of the grid
    and operate in island mode. Preconditions for this are that enough
    generation capacity is available in the islanded part of the grid, and that
    the necessary ancillary services such as frequency control and voltage
    control can be provided.

\end{itemize}

Each of these services can be accomplished by various components using
different means, e.g.~a power generation service can be provided by many
different types of generators and energy storages.

\glspl{SOA}~\cite{soa-rm} have been suggested as an architecture for
information systems in the power system context \cite{postina10,iec62357}. A
\gls{SOA} proposes the use of services to access encapsulated pieces of
functionality. This seems to imply that the concept of service is already
widely accepted as a central concept for power system.  However, the mentioned
references focus rather on data exchange, \gls{EAI} and \gls{B2B} transactions
than on power system control. To illustrate this, here is an excerpt from the
\gls{Smart Grid} Standardization Roadmap, which contains the following
paragraph about the \gls{SOA} \gls{Smart Grid} Standard \gls{IEC} 62357
\cite[section 6.2]{IECRoadMap}:

\begin{quote}
    The services of a control system comprise:
    \begin{itemize}
      \item Data services with which, for example, the databases of the core
        applications can be accessed, e.g.~readout of the operational equipment
        affected by a fault incident in the power supply system
      \item Functional logic services, e.g.~for starting a computing program
        for calculating the load flow in the power supply system
      \item Business logic services that coordinate the business logic for
        specific energy management work processes of the participating systems,
        e.g.~fault management in the network control system within the customer
        information system at the power supply company
    \end{itemize}
\end{quote}


The recently published Smart Inverter standard \cite{smart-inverter-standard}
from \gls{EPRI} defines several services that can be provided by modern
inverters; see also section \refnpage{sec:related-smart-inverter-standard}.
This standard defines the properties of these services, and a mapping to the
\gls{IEC} 61850 data model and \gls{ACSI}. The term \emph{service} occurs in
this standard just once in the meaning of power system service that is provided
by an inverter; other mentions of the term refer to communication services.
Furthermore, the services described in this standard are expressed in terms of
IEC 61850 data classes, and not in a self-contained form.

The Smart Inverter standard could be used as a base for high-level
communication: the various inverter functionalities described by this standard
could be encapsulated into generic data formats. These data formats could be
used for exchanging information about which types of functionality a certain
component supports, and also for activating certain functionalities in a
component. Service definitions, which are described in the next section,
provide these data formats.


\section{Service Definition}
\label{sec:service-definition}

Services without standardisation are not very useful, due to the need for both
communication partners to agree on the meaning of the service. The
standardisation issue does not only involve the standardisation of the
individual service, but also of the data format that is used to exchange
information about services.

It is therefore necessary to use a general
architecture that allows any kind of service to be used. This implies the demand
for a standard vocabulary for talking about services. In this section, the
concept of a \emph{service definition}, a thorough description of a service, is
explained, and a specification is presented. This concept has been influenced
by other service description formats, such as \glspl{web service} \cite{ws-glossary}. 

The basic use case for service-based control is that a component
first sends a message to its supervisory controller detailing which services it
offers.  The supervisory controller then sends a message to the component
containing a command to provide a certain service. This exchange is explained
in more detail in section \refnpage{sec:policy-negotiation-protocol}, which
describes a protocol for exchanging this kind of information.

To be able to use services in control communication, a number of parts have to
be present. A \emph{service definition} consists of these 5 essential parts:

\begin{itemize}

  \item The \emph{service name}: this identifies the service unambiguously, which
    implies that the name must be unique. This name will be used to represent
    this specific service type.

  \item The \emph{service specification}: this gives an exact semantic meaning of the
    service name, i.e.~when one uses the name, this implicates the service
    specification. The service specification should be fixed, i.e.~standardised.
    The specification also defines the data formats for service descriptions
    and service activations (see below).

  \item The \emph{service description}: this describes a service that is offered by a
    particular component. In most cases, this will be just the service name,
    which will already be enough to completely describe the service. In some
    cases though, the service definition will give components some freedom in
    how the service is offered; in this case, the service description will
    contain additional information about the form in which the service is
    actually provided. This includes constraints, which are further discussed
    in section \ref{sec:service-constraints}.
    
  \item The \emph{service activation}: this describes a service that is actually
    provided by a component. Most service definitions will specify parameters for
    a service activation, such as the droop value for a droop service. The
    Service definition does not prescribe a fixed value for this parameter, but
    a provided service needs to have a value for this parameter.  The service
    activation is sent to a particular component, telling it to provide this
    service with the specified parameters.

  \item The \emph{service implementation}: a component offering a service must have an
    implementation that can provide this service. Components are free to
    implement a service as they wish; they only have to make sure that the
    service provided by the service implementation is compatible to
    the service definition. That includes that it must give service descriptions in the
    correct format, accept service activations that have the correct format,
    and provide the service as laid down in the service specification.

\end{itemize}

The service specification is the central part of the service definition; it
gives exact semantics for the service name, and specifies the data formats that
have to be used. Figure \ref{fig:service-api} shows the 5 parts of the Service
\gls{API}. It lacks the specification part of the service definition, because
that is an implicit, not an explicit part of the communication. The figure
shows an additional entity, the service registry, which ensures that service
names are unique, and which can be used to look up the service definition for a
specific service name.

\begin{figure}
  \centering
  \includegraphics[width=0.75\linewidth]{generated/service-api.pdf}
  \caption{The Service API in UML Notation}
  \label{fig:service-api}
\end{figure}

%The use of \gls{WSDL} to describe power system services was also evaluated;
%\gls{WSDL} is a \gls{XML} language to describe \glspl{web service}. However, it
%was decided that \gls{WSDL} is unsuitable for describing power system
%services, due to a principal difference between the two service concepts:
%\glspl{web service} are directly requested and provided through a
%communication link, whereas power system services are requested through
%communication links, but provided via the power grid. Additionally, one of the
%primary use cases for services in this thesis is to use them as building
%blocks in behaviour descriptions, which will be introduced in the next
%chapter.

To illustrate the 5 parts of a service definition, here is an example:

\begin{itemize} 
  
  \item Service name: \texttt{frequency-control:droop:v1.0} \\ 
    This represents a certain frequency control service. This one is configured
    by giving a droop value; its version is \texttt{v1.0}. 

  \item Service specification: the most important part of the specification is
    an exact description of how the service is provided; this part is however
    omitted from this example. A service description contains the name of the
    service and the parameter \texttt{minimum-response-time}, the minimum
    response time in seconds that the component can offer. A service activation
    contains the service name and two values: one \texttt{deadband} value, and
    one \texttt{droop} value. The physical unit of the deadband is frequency
    ($\si{Hz}$), the unit of the droop value is in frequency change per active
    power output change ($\si{Hz/kW}$).

  \item An example service description is:
    \begin{itemize}
      \item Name: \texttt{frequency-control:droop:v1.0}
      \item Parameter \texttt{minimum-response-time}: 1.0 (unit is seconds, as
        specified in the service definition)
    \end{itemize}

  \item An example service activation is:
    \begin{itemize}
      \item Name: \texttt{frequency-control:droop:v1.0}
      \item Parameter \texttt{deadband}: 0.1 [$\si{Hz}$]
      \item Parameter \texttt{droop}: 0.1 [$\si{Hz/kW}$] 
    \end{itemize}

\end{itemize}

The service implementation is not given here, but every
component that wants to provide the service has to have one. For users of the
service, it is of no concern how the service is implemented; it is sufficient
to know the service specification, description and activation to know how the
component will react to frequency changes while providing that service.


\section{Service Naming Schemes}
\label{sec:service-naming-scheme}

A naming scheme provides a set of rules to come up with new names for services.
A good naming scheme should

\begin{itemize}
  \item Make it easy to create unambiguous names, i.e.~it makes it difficult 
    to create the same name for different services
  \item Favours names that can be easily remembered by humans
  \item Favours names that themselves contain information about the provided service
\end{itemize}

Examples for bad naming schemes are

\begin{itemize}
  \item \glspl{UUID}; this are globally unique IDs. Using \glspl{UUID} is a
    bad choice because it is easy to make mistakes while copying them, and they
    do not contain any semantic information about the service. Take the two
    \glspl{UUID} 1627d933-47d8-4d15-b93c-5f38c202d622 and
    dda9c7a4-23f4-4ea5-a3d3-84698e0259ec as an example: which one is the
    frequency control service?
  \item A SHA-256 hash of the text of the service definition; this ties service
    name and service definition closely together; when the definition changes,
    the name changes as well. However, this has the same shortcomings as using
    a \gls{UUID}.
\end{itemize}

Good choices for naming services are

\begin{itemize}

  \item \glsplural{URI}; This system is e.g.~used for distinguishing \gls{XML}
    namespaces; \glspl{URL} are a type of \gls{URI}.

  \item \gls{Java} package naming convention \cite[section 6.1]{jls7}: this uses the
    reversed hostname of the entity providing a \gls{Java} class or library as the
    start of the full class name; e.g.~the package for all classes of the
    policy framework should be \texttt{dk.risoe.syslab.policy}.

\end{itemize}

In the naming scheme that has been chosen, each service name consists of three parts:

\begin{itemize}
  \item The type of service, e.g.~\texttt{frequency-control}; this describes the
    type of service that is provided
  \item The type of service provision, e.g.~\texttt{droop}; this describes how the
    service is provided
  \item The version of the service, e.g.~\texttt{v1.0}; Service definitions may
    have to change sometimes, due to various reasons such as a too vague
    service definition.  When this happens, the new service definition must be
    distinguishable from the old definition; this can be accomplished by
    incrementing the version number in the service name. The version does not
    have to be a number; it can reference the standard the service definition
    comes from.
\end{itemize}

The three parts of that name are concatenated using single colon characters;
thus, the service name for the example above is
\texttt{frequency-control:droop:v1.0}.

The reason for using this naming scheme is that the names are concise while at
the same time describing very clearly what they are about. Both \gls{Java}
package names and \glspl{URI} contain much organisational information
(i.e.~which organisation this name comes from) which distract from the
actual meaning of the name.

It is however not a requirement to use the same naming scheme; the only
requirement for names is that the names are character strings.

Service names must be unique, i.e.~there must be exactly one service
definition for a given service name. This requirement can be maintained by
establishing a service name registry, where everyone that wants to use a
service name has to register its service with. Service names can have
additional information registered with them, such as a complete service
definition, but this is not a requirement; private service definitions can be
used, as long as the service name has been registered.

Various standards, such as the \gls{HTTP} standard \cite{http-standard}, have
long had the feature that standard and non-standard names, such as the names
for \gls{HTTP} headers, are put into separate namespaces. The regular method
for \gls{HTTP} headers is to prefix a non-standard name with \texttt{X-}, while all other
prefixes are reserved for standard names. However, this practice is in the
process of being deprecated \cite{xdash}; instead, the use of registries is
proposed to prevent name clashes. For this work, using registries for service
names is suggested for the same reasons.


\section{Putting it Together}
\label{sec:svc-putting-it-together}

The missing piece of the service infrastructure is the service instance,
i.e.~the particular instantiation of the service implementation that executes
the given service on a specific component. The service instance provides the
service description to the supervisory controller, and provides the service as
described in the service activation.

Services do not necessarily have to be standardised. Companies can define their
own services for internal use; as long as all communicating parties agree on
the semantics of a service name, any service definition can be used. However,
it is advised to use a service registry, so that name collisions, whether
with other internal services or with external ones, are impossible.

The whole structure of the service infrastructure is illustrated in figure
\ref{fig:service-structure}.

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{generated/service-structure.pdf}
  \caption{The Structure of the Service Framework}
  \label{fig:service-structure}
\end{figure}

\paragraph{Integration into Communication Standards}
\label{sec:svc-integration-into-stds}

To make the service concept introduced in this chapter applicable in many
control situations, communication
standards like \gls{IEC} 61970 and \gls{IEC} 61850 must support it. The best
way to achieve this is to devise a general data format with which service
descriptions and service activations can be exchanged. Communication standards
that support this general format are then able to support all services that can
be expressed in that way. A suggestion for such a data format is presented in section
\refnpage{sec:std-cim-services}.

This approach makes services independent of communication standards, so the same
service definition can be used unchanged in different standards. The only 
condition for this to work is that communication partners agree on which service
definition a certain service name refers to.

\paragraph{A Service API}
\label{sec:svc-service-api}

This is the first step towards integration of services into communication
standards; it has also been implemented for use in the tests, see chapters
\refnpage{sec:policy-framework} and \refnpage{sec:experiments}.

This \gls{API} should be as simple as possible, yet be flexible
enough to support not only existing services, but any kind of service that
could be used in the future. Therefore, services are described in a very
general manner. 

A service description consists of two parts:

\begin{itemize}
  \item The service name, a character string
  \item A list of parameters, i.e.~name/value pairs; the service definition lists all parameters that
    can or must be part of a service description. The parameters are strings,
    the values can in principle be complex data types, but usually they will be
    simple string or numerical values
\end{itemize}

A service activation consists of two parts:

\begin{itemize}
  \item The service name, a character string
  \item A list of parameters; the comments from the service description
    parameter list apply here as well.
\end{itemize}



\section{A Service Ontology}

%\index{Ontology!for Services}
Since service descriptions and activations are to be exchanged between
different computer systems, most probably from different manufacturers, it is
necessary to define a proper \gls{ontology} for services, at least informally.
The ontology ensures that everybody has the same understanding of the terms
\emph{service name}, \emph{service description} and \emph{service activation}. 
A power system ontology as outlined in \refnpage{sec:power-system-ontology} is
also part of the service ontology, because service descriptions and activations
might contain information based on that ontology.


\section{Service Constraints}
\label{sec:service-constraints}

An important part of service descriptions is the type of constraints that limit
the provision of a service. Examples for such constraints are:

\begin{itemize}

  \item a minimum response time, i.e.~the component does not guarantee a lower
    response time than the one given in the service description

  \item ramp rates for up and down regulation, e.g.~of real power

  \item time periods when the service will not be available

\end{itemize}

How such constraints have to be expressed depends on the service in question.
This will be be part of the \emph{service description}.



\section{Conflicts between Services}
\label{sec:service-conflicts}

A different kind of constraint are conflicts between multiple services.
Services provided by a certain component can depend on each other in various
ways.  The easiest kind of conflict to deal with is when two services
are mutually exclusive, i.e.~they cannot be provided at the same time. Other
conflicts are more subtle; the best example for this is a component that
provides two services, one based on active power output (e.g.~frequency
control), the other based on reactive power output (e.g.~voltage control). The two
services conflict with each other, due to dependencies between active and reactive
power output; an inverter will have different limits for reactive power
output for different setpoints of active power output. This is illustrated by the
P-Q diagram of a component, which shows the dependencies between real and
reactive power output of a component.

Whether two services conflict with each other, and which form this conflict can
take, depends on the actual component that provides these services. When a
component tells its supervisor which services it can provide, it therefore
should also provide conflict information. This raises the question
of how these conflicts are expressed, which can be split up into two
different issues:

\begin{itemize}

  \item Specify the type of conflict: Services depend on the capabilities of
    a component, such as the ability to provide active power output. The number
    of such capabilities is limited, as is the number of possible conflicts
    between those. So, the conflicts can in principle be just enumerated.

  \item Specify a quantity or measure for the conflict: in the example above,
    the conflict between active and reactive power output, the extent of this
    dependency can be specified by a P-Q diagram. In many cases, however,
    this will be too much information for the supervisor, and therefore will not
    be included into the planning algorithm. It might in fact be completely
    sufficient that the supervisor knows that the power output can be
    constrained. The planning algorithm can decide whether to allow
    simultaneous activation of the two conflicting services, or whether the
    services should be treated as mutually exclusive.

\end{itemize}

This is a list of capabilities of components that are of most interest for
control purposes:
\begin{itemize}
  \item Active Power Output (P)
  \item Reactive Power Output (Q)
  \item Ability to Black Start
  \item Protection Ability (detect failure and open circuit breaker)
\end{itemize}

% TODO can I write this like that? I don't know how else to write it
In principle, each of these capabilities conflicts to some extent with the
others, with the exception of the black start ability, which is only needed
when a part of the power system has been shut down.

Conflicts between services can be treated in several ways:

\begin{itemize}

  \item Ignore: the supervisor ignores them, and assumes that the effects of 
    conflicting services will be sufficiently small to have no effect.

  \item Treat statistically: Statistics can help the supervisor to assess how much
    effect a conflict will have on units. When the supervisor has aggregated
    enough units with similar properties, the effects will be smoothed out.

  \item Treat specifically: the planning algorithm of the supervisor might be
    able to deal with certain conflict types. In this case, the conflict
    information will have to be fed into the planning algorithm.

  \item Make services mutually exclusive: the supervisor tells the component
    that it cannot provide the two conflicting services at the same time.

\end{itemize}

Describing the influences between services is important when multiple services
should be provided at the same time; the next chapter will present a concept
that makes that possible.  Even though it is difficult to devise a general
framework for these conflicts, most of these influences can be reduced to
the component's capabilities that a service uses, such as active power.

To give an example, let's assume that a component can provide several services:

\begin{itemize}
  \item A frequency control service
  \item A price-based consumption service
  \item A load scheduling service
\end{itemize}

All these services depend on the ability to control active power output.  This
conflict can be noted in the service description; the supervisor will then be
able to determine that the three services are mutually exclusive. The easiest
mode at note the conflict is when each service description refers to the
component capabilities is depends on, i.e.~a new parameter
"depends-on-capability" is added to the service description, which in the case
above contains "\texttt{ActivePowerOutput}" for all three services.


\section{Conclusion}
\label{sec:services-conclusion}

The main observation of this chapter is that the high-level function
descriptions that the previous chapter alluded to are services.

%The services that are available in the power system can be very diverse, and
%there may be several means to provide a specific service. Therefore, the notion
%of service should be as broad as possible. As an example, an aggregator
%provides a service to the power system, but also to the components it controls
%(the service to aggregate their capabilities). The controlled components, on
%the other hand, also provide a service to the aggregator, which is negotiated
%between the two parties.

Flexibility and extensibility of this system comes from the extraction of
component functionality into abstract, generic services. Supervisory
controllers can pick the provided service that best fits their needs, and new
components can be easily integrated by exposing their functionality through
existing services.

To get the most out of this shift to high-level communications, a strong
service concept must be used. This means that a service is specified
using a service definition, which consists of several parts: 

\begin{itemize}
  \item The service name
  \item The service specification
  \item The service description data format
  \item The service activation data format
  \item The service implementation
\end{itemize}

The service name is the central concept, because it ties the other parts
together. For this to work, service names have to be unique, i.e.~a service
name must point to exactly one specification, description data format and
activation data format. This is ensured by enforcing the use of a service
registry.

The semantics of the service come from the service specification, which is a
formal document, usually a standard, describing exactly what the extent of the
service is. 

The service concept should be integrated into communication standards by adding
a general service framework to these standards; this framework must support the
exchange of service descriptions and service activations.

A step towards more autonomy in the power system is to make services
automatically negotiable, i.e.~there should be a mechanism that allows to
automatically negotiate how a service should be provided. A concept for doing
this, behaviour descriptions, is described in the following chapter.

