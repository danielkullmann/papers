% up: part05---communication.tex
\section{Properties of Communication Links}
\label{sec:comm-link-properties}

In a distributed system such as a power system, many different kinds of
communication technologies are used, from standard wired \gls{TCP}/\gls{IP}
over wireless \gls{TCP}/\gls{IP} and mobile communication systems (\gls{GPRS},
\gls{UMTS}) to more specialised communication technologies such as
\gls{Zigbee}, \gls{Z-Wave}, \gls{Bluetooth} and \gls{KNX}. 

These different communication technologies have their strengths and weaknesses;
besides the cost of deploying these technologies, the following three
properties are of main interest for this work:

\begin{itemize}

\item \Gls{latency}: how long does it take a single packet to get transferred through
  the network?  [s]

\item \Gls{bandwidth}: how much data can be transferred in a
  certain span of time? [byte/s]

\item \Gls{availability}: how reliable is the communication link? [\%]

\end{itemize}

\subsection{Latency}
\label{sec:comm-latency}

The latency of communication links limits the speed a closed-loop control system can work
with, because it adds to the time required for a single iteration in the
control loop. If a control system demands a fast feedback loop time, a
large latency can have negative impacts on the working of that system. Other
control systems might not be affected as much; \gls{open-loop} control systems,
in particular, often do not care much about latency. Even those, however, can
be negatively affected by very large latencies, because the controlled
component might get control signals too late.

\subsection{Bandwidth}
\label{sec:comm-bandwidth}

Bandwidth is usually not a limiting factor in control systems, because the
amount of data exchanged is typically rather small. Sending a few setpoints to
a controlled component requires only a limited amount of data. 

Still, control systems assume that data arrives in a reasonable time
span, so they expect a guarantee for a certain bandwidth, even if that
bandwidth is small.

\subsection{Availability}
\label{sec:comm-availability}

When a communication link fails to work, the control system usually
suffers tremendously. \Gls{closed-loop} systems cease to work completely,
because they demand a constant flow of data back and forth between
controller and controlled component. \Gls{open-loop} systems might not be
affected as much as \gls{closed-loop} systems, because the controller
does not expect feedback from the controlled system, but the
controller still expects its commands to arrive at the controlled
component. This is not the case when the link fails, so the behaviour of
the controlled device will deviate from the expected behaviour when
that happens. How much such failures influence the overall performance of the
system depends on the control algorithm in use. 

The usual physical unit of availability is percent, but it can also be
expressed as maximum downtime (hours, minutes, seconds) per year; an
availability of 99.999\% (``five nines'') corresponds to a maximum of 5:16
minutes of unavailability per year.

This measure, however, does not contain information on how long a link usually
is unavailable.  It is a big difference for a control system whether a
communication link fails 1000 times for 1 second, or whether it fails once for
1000 seconds. Nonetheless, availability is always specified using the
percentage value.


