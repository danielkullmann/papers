% up: part03---problem-description.tex
\section{Definition of Frequent Terms}
\label{sec:common-terms}
\label{sec:frequent}

This thesis is a cross-disciplinary work, written from a computer scientist for
an audience of mostly power engineers and control engineers. Some terms that
are used here might have different meanings in these other fields.  To avoid
confusion, it is therefore crucial that these terms are explained before the
main part of this thesis. Therefore, this list is put here and not into the
glossary section at the end.

Some of the terms are used as synonyms in this thesis, and this might not be
obvious to the reader. Other terms had to be coined; their definition is
established here for the rest of this thesis.

Readers are advised to refer to the glossary and list of acronyms, starting at
page \pageref{main}, if they encounter terms or abbreviations they do not
know; the practice in this thesis is to use the abbreviations, except
the first time the abbreviation appears. This rule is 
disregarded when this enhances the readability of the text.

\begin{description}

\item[component, device] \hfill \\ 
  A component is a piece of equipment that is controlled by a supervisory
  controller. Examples are wind turbines, \gls{PV} and appliances such as
  fridges and electrical space heaters. Note that the term \emph{component} has
  a different meaning in the context of ICT: there, it is a self-contained
  piece of a larger software system. Nevertheless, in this thesis, the term is
  not used in the \gls{ICT} meaning.

\item[unit, node] \hfill \\ 
  This is a participant in a distributed system, i.e.~in the context here this
  is either a supervisory controller, an intermediate controller somewhere in
  the control hierarchy, or a controlled component.

\item[supervisory] \hfill \\
  This refers to nodes on the upper levels of the control hierarchy tree, such
  as the root of the hierarchy. This is the opposite of \emph{local}. 

\item[local, controlled component/device] \hfill \\
  This refers to components in leaves of the control hierarchy tree. These are
  the components that will be controlled in the end, such as \gls{RES} or
  demand-side components. \emph{Local} is the opposite of \emph{supervisory}.

\item[local controller, device controller, client] \hfill \\
  A device often has a controller directly attached to it, and the device is
  controlled through that controller. This is in contrast to the term
  \emph{supervisory controller}, which is at a different location, often a large
  distance from the local controller. In the context of communication between
  local and supervisory controller, the local controller is called
  \emph{client}.

\item[intermediate controller] \hfill \\
  This is a controller that is neither a local controller, nor is it located
  on the top level of the control hierarchy. In an aggregation hierarchy, this
  is a controller that is one of the non-leaf, non-root nodes in the
  hierarchy.

\item[supervisor, supervisory controller, server] \hfill \\ 
  Supervisory controllers are the central instances that control many
  components at the same time, either directly or indirectly via intermediate
  controllers. They are usually located far away from the components they
  control. In the context of communication between local and supervisory
  controller, the supervisory controller is called \emph{server}.

\item[high-level communication, abstract communication] \hfill \\ 
  These are terms that have no generally accepted, established meaning; in the
  context of this thesis, they are used as synonyms and designate communication
  that is using data formats that are more expressive than the
  setpoint/measurement data formats that are typically used in \gls{SCADA}
  systems. The goal is to enable behaviour that is more flexible, extensible,
  intelligent and autonomous. 

\item[heterogeneous control system] \hfill \\ 
  Power systems are heterogeneous, which in this context means that they
  consist of many different types of units, controllers as well as controlled
  components, which can provide different services, have different properties, and belong to
  different entities. Both these factors, the existence of different types of
  components with different capabilities, and that those components belong to
  different entities with possibly conflicting goals, make the construction of
  a control system more difficult, especially in the light of necessary
  automation of that system.

\item[security] \hfill \\ 
  In the area of power engineering, this usually implies security of supply.
  In the context of this thesis, however, the usual meaning, unless explicitly
  noted, is that of \gls{IT} security. This denotes the security of software
  systems and the communication networks that they use, i.e.~the prevention of
  cyber attacks on these systems. 

\item[power, energy] \hfill \\ 
  These two terms can refer to different types of energy,
  such as mechanical, thermodynamical and electrical energy. This thesis,
  however, focuses solely on the generation and consumption of \emph{electrical}
  energy. Thus, these two terms always refer to the electrical kind.

\item[control system, system control] \hfill \\ 
  In this thesis, whenever  one of these terms  is used without a reference to
  what this controls, it refers to the control system of a power system.

\item[synchronous, asynchronous] \hfill \\
  These two terms are antonyms which have different meanings in power systems and communication.
  Power systems are called synchronous when they share the same frequency; they
  are called asynchronous when they can have differing frequencies.
  In communication, the message exchange between units is called synchronous
  when the sending unit waits until it receives the response message; in an
  asynchronous message exchange, a unit does not wait for a response message.

\item[direct control, indirect control] \hfill \\
  Direct control is a control scheme where a controlled device is obliged to
  respect the commands it receives, i.e.~it has to execute commands
  if possible and inform its controller about a failure to execute a command.
  In indirect control schemes, a unit is free to do anything, and commands or
  events effect no guaranteed behaviour. However, the behaviour of the units
  can often be forecasted: when the signal is a power price, a lower power
  price will generally lead to higher consumption, and vice versa.

\item[service, task, function] \hfill \\
  \emph{\Gls{service}} is a central term for this thesis, which is used with different
  connotations in power engineering and computer science. In power systems,
  \emph{service} usually implies \emph{\gls{ancillary service}}, which subsumes all services
  that are not directly concerned with power generation, but rather with
  ensuring the stability of the power system. In \gls{ICT}, the term service
  has a much broader meaning: it subsumes all kinds of tasks that can be
  provided by one system to another. \Glspl{web service} are an example for this.
  \emph{Task} and \emph{function} are used as synonyms
  of \emph{service} in this thesis. 
  % DONTDO remove "task" and "function" here. Do I use these two words in the
  % thesis? => Well, to some extent

\item[home gateway] \hfill \\
  These have been suggested in response to the desire to integrate household
  appliances actively into the control system; they act as gateways between all
  appliances in a single home and the supervisory controller responsible for
  coordinating the operation of the appliances. This concept has also been
  called \emph{Energy butler} \cite{energy-butler}, and it has been suggested that
  smart meters act as home gateways \cite{ModernGridBenefits}.

\item[partner, party] \hfill \\
  The superordinate name for \emph{server} and \emph{client} is communication
  partner or communication party.

\end{description}



