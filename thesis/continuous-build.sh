#!/bin/bash

TARGETS="default pages"
if test -n "$0"; then
  TARGETS="$@"
fi
while true; do 
  # Check whether make should run, and run it when necessary
  # (This prevents the "make: Nothing to be done for `all'." message)
  # Sleeps longer when there are errors
  make -q $TARGETS || make $TARGETS || sleep 28
  # Do that every 2 seconds
  sleep 2
done

