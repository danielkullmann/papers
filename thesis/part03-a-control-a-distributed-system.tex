% up: part03---problem-description.tex
\section{Task: A Better Control Structure}

An electrical power system is a distributed system in the best sense: it
consists of many, geographically distributed units that have to coordinate 
their actions to reach some common goals. 

The principal goal of an electrical power system is to generate energy and
supply it to customers, while maintaining the control goals of the system:

\begin{itemize}
  \item Security of supply: consumers should get the power they require.
  \item Security of equipment: the operation of the system should not damage
    equipment or shorten its lifetime disproportionately.
  \item Cost-effectiveness: the cost of operating and maintaining the power
    system should be as low as possible.
\end{itemize}
% DONTDO Put "Fairness" into this list? => No

These primary goals can be split up into sub goals, such as keeping a stable
system frequency, voltage control and protection schemes. The task of the
control system is to guarantee the adherence to these control goals. This task
is becoming more difficult with the increasing penetration levels of \gls{RES}
and with the increasing number of components that actively participate in the
control system.

The target of realising the \gls{Smart Grid} vision implies the need to control
a large number of components on both the generation side and the consumption
side. This in turn demonstrates the requirement to automate a large part of the
control process. 


\subsection{Control Systems}

The control system of a power system is a conglomerate of systems at various
hierarchical levels, controlled by various stakeholders (see below) with
different goals, and with different states of automation, from fully manual to
fully automated.  It is not one single control system, but rather a system of
control systems.  The emergence of Smart Grids, which comes with increased
penetration levels of small energy sources as well as active inclusion of the
consumption side, will aggravate this problem due to the
increased number of components that have to be integrated into that control
system.

Automation of the control system will be a major factor in the realisation of
the Smart Grid. Together with the scale of components that have to be
controlled, this implies that a new structure of the control system will be
necessary, where many decisions that are nowadays being done by human operators
are automated, and where the scalability challenge is approached by delegation
of control decisions to units that reside on the lower levels of the control
hierarchy. An important aspect of automation are the  control schemes and control
algorithms that are used to achieve an automated control system. 

\label{sec:pd-closed-loop-control} 
Two major control schemes are used in power systems. For controlling single
components, synchronous communication is mostly used, where a controlled device
responds immediately to the control command of a controller. An example for
this is the classical \gls{closed-loop} control (see
figure~\ref{fig:closed-loop-control}), as employed e.g.~in the droop control of
a generator. 

\begin{figure}
  \centering%
  \includegraphics[width=\textwidth]{generated/Feedback_loop_with_descriptions.pdf}
  \caption[Closed-Loop Control]{Closed-Loop Control \hfill \break (Source: \url{http://www.wikimedia.org}) \hfill}
  \label{fig:closed-loop-control}
\end{figure}

For supervisory control, manual control is still the prevalent mode of control:
The operators in one control room contact the operators in another control
room, e.g.~of a power plant, and tell them to do something. For dealing
with increasing numbers of components, large parts of these manual processes have
to become automated. 

% Should the discussion be part of this? Or rather moved into the Communication
% chapter?
Synchronous communication approaches, however, are not suitable for automating
supervisory control.  The two main reasons for that are the relatively high
communication overhead of a fast executing control loop (a single central
controller cannot coordinate tens of thousands components at the same time) and
the inherent unreliability of communication links: when a communication link
fails, the control loop stops working. 
%Is this repeated somewhere? 
% => it is somewhat referenced in sec:communication-analysis

This implies that supervisory control should not be based on a communication
scheme that exhibits these problems. Instead, asynchronous communication should
be used, which is less demanding on the communication links; however, it limits
the type of control algorithms that can be used with supervisory control. 

This applies just to supervisory control; local control
communication, which is the communication between a local controller and its
controlled device, will still be done mostly using synchronous communication.
This is necessary because effects of control actions have to be visible to the
controller (via the feedback loop) as soon as possible; it is feasible because
the controller is located right next to controlled device.

These considerations illustrate the importance of communication for an
automated control system. Especially high-level communication is a
facilitator for achieving that goal; control schemes can employ semantically
rich data formats to specify how components should behave.


\subsection{Generic Aggregation Scenario}
\label{sec:aggregation-scenario}

\begin{figure}
  \begin{center}
    \includegraphics[width=0.9\linewidth]{generated/dsm-aggregation.pdf}
  \end{center}
  \caption{Assumed System Structure}
  \label{fig:dsm-aggregation}
\end{figure}

To make the discussions about control structures simpler, this thesis assumes
that the control system will be based on a generic aggregation scheme.

This scenario assumes a specific structure of a control system for power system
components: an aggregation hierarchy that controls many, often small,
resources. This is exemplified in Figure~\ref{fig:dsm-aggregation}. The
components that are controlled, in this case household appliances, are directly
controlled by a house gateway. The capacity of a single household to
provide system \glspl{service} is not large enough to be of interest to the system;
therefore an aggregator is needed to combine the capabilities of a large number
of households. The aggregator controls the components indirectly, via the house
gateway.

This scenario includes the assumption of a certain task sharing between
controllers: a supervisory controller with more information about the state of
the whole system sends commands to local controllers which execute these tasks
autonomously. This is illustrated in figure
\ref{fig:supervisory+local-communication}.  This setup is already in place in
basically all generators (even though they use only simple services 
such as droop control), and it should be used by demand-side components as
well. A significant contribution towards this control structure will come from
algorithms that intelligently exploit the capabilities of controlled
components.

This control structure can be applied to many other control scenarios, such as
the charging of electric vehicles or controlling many small \gls{RES}.
When the number of controlled components increases, additional levels can be
added to the aggregator hierarchy. The intermediate levels contain 
aggregator nodes as well: they aggregate the nodes below them in the tree, and
represent those to the aggregator node that is located on the level above.

\begin{figure}
  \begin{center}
    \includegraphics[width=0.8\linewidth]{generated/supervisory+local-communication.pdf}
  \end{center}
  \caption{Supervisory and Local Control Communication}
  \label{fig:supervisory+local-communication}
\end{figure}

Such an aggregation structure is based on the premise that the combined
capabilities of many components can be described in a simple manner. This
concentration of capabilities is carried out by the so-called
aggregation function. The development of aggregation functions is however
outside the scope of this thesis. 

Other control scenarios for power systems are actually very similar to this one;
in consequence, the aggregation scenario can be used without loss of generality:

\begin{itemize}

  \item A centralised control scenario is an aggregation scenario without
    intermediate aggregation levels, i.e.~with just one aggregator (the
    central controller) and the controlled components. 

  \item A market-based scenario (e.g.~using a \gls{multi-agent system}) still uses a
    central entity, the market-clearing agent. The aggregation function
    calculates a power bid for consumers, and a power offer for
    generators. The PowerMatcher system~\cite{KWK+06} has a hierarchy of
    market-clearing agents, which clearly is an aggregation hierarchy; the
    intermediate agents are called \emph{Concentrators} there.

  \item The various control schemes that are explained in section
    \refnpage{sec:related-power-systems} are hierarchical in nature, so they map
    well to the aggregator scheme.

\end{itemize}


\subsection{Stakeholders}
\label{sec:stakeholders}

A power system is not a monolithic system that is controlled by a single
entity. On the contrary, various stakeholders with different goals are
involved. The unbundling of power markets that has happened in several
countries has amplified this situation \cite{unbundling-survey,
unbundling-annexes}; services that could previously be supplied by one company
now have to be provided by different companies.

Following is a list of stakeholders in power systems. Depending on the legal
situation, companies can have several of these roles at the same time.

\begin{itemize}

  \item System Responsible: 
    responsible for the operation of a power system.

  \item \gls{TSO}: 
    provides energy transmission on the high voltage levels.

  \item \gls{DSO}: 
    provides energy distribution to the lower voltage levels and finally to the
    end consumers.

  \item \gls{BRE}: 
    responsible for balancing generation and consumption.

  \item Generator: 
    generates electrical energy; the item can be split up into several categories,
    depending on the extent to which power output can be controlled.

  \item Consumer: consumes electrical energy; as generators, consumers are
    split up into categories depending on the ability to control the
    consumption process. Examples are uncontrollable components, components
    able to time-shift consumption, and the temporary up/down regulation of
    consumption.

  \item Aggregator: 
    aggregators combine the capabilities of multiple generators and/or
    consumers; these aggregated capabilities can be traded on power
    markets. An example for an aggregator system are \glspl{VPP}.

  \item Market operator:
    provides a forum for buying and selling power in various contractual forms,
    as well as of ancillary services such as balancing power or frequency control.

  \item Regulator:
    plays a part in the power system by giving the legal framework that
    controls how power systems work, e.g.~through unbundling laws  or by giving
    incentives to certain types of generation; some energy policies have been
    described in section \refnpage{sec:bg-energy-policy}.

\end{itemize}


This list demonstrates the heterogeneous nature of power systems also from an
organisational standpoint. For a functioning automated control system, many of
these stakeholders will have to coordinate their actions. Flexible data formats
for communication will certainly facilitate this process.


