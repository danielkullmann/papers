// This is a remote connection to the client
client = ...;

// Retrieve data about the client
ClientInfo clientInfo = client.retreiveClientInfo();

// Calculate behaviour description, based on clientInfo
..

// Remove all existing services from the client service list
client.clearAllServices();

// Add a service with highest precedence 
// (because it is the first to be added)
FrequencyControlService fs = new FrequencyControlService(..);
client.addService(fs);

// Add a service with lower precedence
// (because it is the second)
PriceBasedService ps = new PriceBasedService(..);
client.addService(ps);

// Add a service with the lowest precedence
// (because it is the last to be added)
Schedule sch =  new Schedule(...);
ScheduleService scs = new ScheduleService(sch, ..);
client.addService(scs);

