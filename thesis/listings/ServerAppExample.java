package risoe.syslab.control.policy.impl.examples;

// Imports omitted

/** Simple server application example */
public class ServerAppExample {

  public static void main( String[] args ) {

    String hostname;
    try {
      hostname = InetAddress.getLocalHost().getHostName();
    } catch ( UnknownHostException e ) {
      throw new AssertionError( "No local hostname" );
    }

    System.setProperty( "java.util.logging.config.file", 
        "conf/logging-verbose.properties" );
    System.setProperty( "log4j.configuration", 
        "conf/log4j.properties" );
    PolicyUtils.setup( "conf/policy-framework.properties" );

    CommunicationSystem commSystem = 
      new HttpCommunicationSystem( "", hostname );
    PolicyUtils.setCommunicationSystem( commSystem );
    
    PolicyServerImplementation srvImpl = 
      new PolicyServerImplementation();
    
    AbstractAddress srvAddress = 
      commSystem.createAddress( "server", srvImpl );
    PolicyUtils.createServerAgent( srvAddress );

    while (true) {    
      try { Thread.sleep( 10000 );
      } catch (InterruptedException e) { break; }
    }
  }
}

