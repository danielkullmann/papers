// This is the list of service instances that were created in response to the
// three addService(..) calls from the sever
services = ...;

// Iterate through list of services
for (Service service: services) {
  // If this service should be used..
  if (service.shouldBeActive()) {
    // ..tell scheduler to use the service
    scheduler.activateService(service);
    // Leave for-loop prematurely
    break;
  }
}

