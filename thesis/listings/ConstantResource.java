public interface ConstantResource {

  //Configuration class used for HMI layout
  public abstract CommonDeviceConfig getNodeConfiguration();
  
  //Properties of controlled loads
  public abstract boolean isPSetpointEnabled();
  public abstract boolean isQSetpointEnabled();
  public abstract boolean isUSetpointEnabled();
  public abstract boolean isfSetpointEnabled();
  public abstract boolean isCosPhiSetpointEnabled();
  
  //Setpoints for generic controlled P-Q loads
  public abstract double getPSetpoint();
  public abstract double getQSetpoint();
  public abstract double getUSetpoint();
  public abstract double getfSetpoint();
  public abstract double getCosPhiSetpoint();
  public abstract void setP(double d);
  public abstract void setQ(double d);
  public abstract void setU(double d);
  public abstract void setf(double d);
  public abstract void setCosPhi(double d);
  
  //Controller deadband characteristics and setpoints
  public abstract double getPDeadband();
  public abstract double getQDeadband();
  public abstract double getUDeadband();
  public abstract double getfDeadband();
  public abstract double getCosPhiDeadband();
  public abstract void setPDeadband(double d);
  public abstract void setQDeadband(double d);
  public abstract void setUDeadband(double d);
  public abstract void setfDeadband(double d);
  public abstract void setCosPhiDeadband(double d);
  
  //Proportional controller characteristics and setpoints
  public abstract double getProportionalP();
  public abstract double getProportionalQ();
  ...
  
