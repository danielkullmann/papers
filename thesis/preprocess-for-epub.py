#!/usr/bin/env python

import sys,re,os

includeRe = re.compile("^\\s*\\\\input{([^}]+)}")
skipRe = re.compile("\\\\label{[^}]+}|\\\\addcontentsline{[^{}]+}{[^{}]+}{[^{}]+}")
justTextRe = re.compile("\\\\(?:gls|glspl){(?:glo:)?([^}]+)}")

# TODO implement \ref support by parsing the .aux file,
# which contains lines like these:
# \newlabel{sec:standardization}{{\M@TitleReference {10}{Standardisation}}{79}{\relax }{chapter.10}{}}
# problem here is that a simple regex won't work in all cases here, because the {} are nested
refRe = re.compile("\\\\ref(?:npage)?{(?P<name>[^}]+)}")

#                           \newlabel{sec:standardizat}{{   \M@TitleReference {100000}{Standardisation  }}{79            }{\relax   }{chapter.10    }{}}
auxLabelRe = re.compile("\\\\newlabel{(?P<name>[^{}]+)}{{\\\\M@TitleReference {[^{}]+}{(?P<title>[^{}]+)}}{(?P<page>\\d+)}{(?:[^}]+)}{(?P<sec>[^}]+)}")

incGraphRe = re.compile(r"^(?P<before>.*)\\includegraphics(?P<opt>[^{}]*)\{(?P<file>[^{}]+)\}(?P<after>.*)$")

acronymRe = re.compile(r"\\nacronym\{(?P<abbr>[^}]*)\}\{(?P<longform>[^}]*)\}")
glsRe = re.compile(r"\\(?P<cmd>[gG]ls(|text|pl))\{(?P<name>[^}]*)\}")

def parseAuxFiles(files):
  aux = {}
  for filename in files:
    with open(filename) as input:
      for line in input:
        match = auxLabelRe.search(line)
        if match != None:
          aux[ match.group("name") ] = match.group("title")
  return aux


def parseAcronyms(filename):
  acronyms = {}
  with open(filename) as input:
    content = ""
    for line in input:
      content = content + line

  iter = acronymRe.finditer(content)
  for elem in iter:
    acronyms[ elem.group("abbr") ] = (elem.group("longform"), False)
  return acronyms

def glsSubst(match):
  cmd = match.group("cmd")
  name = match.group("name")
  if name.startswith("glo:"):
    return ">>" + name[4:]
  if name in acronyms.keys():
    entry = acronyms[name]
    acronyms[name] = (entry[0],True)
    return "<<" + entry[0]
  return name


def process(filename, aux, acronyms):
  def refReplacer(match):
    name = match.group("name")
    try:
      return aux[ name ]
    except KeyError:
      return "<"+name+">"

  with open(filename) as input:
    for line in input:
      match = includeRe.match(line)
      if match != None:
        filename = match.group(1)
        if not os.path.exists(filename):
          filename = filename + ".tex"
        process(filename,aux,acronyms)
        continue
      match = incGraphRe.match(line)
      if match != None:
        print match
        if not os.path.exists(match.group("file")):
          print match.group("file"), "not found"
      line = skipRe.sub("", line)
      line = refRe.sub(refReplacer, line)
      line = justTextRe.sub("\\1", line)
      line = glsRe.sub(glsSubst, line)
      print line,

import glob
auxFiles = glob.glob("*.aux") +  glob.glob("**/*.aux")
aux = parseAuxFiles(auxFiles)
acronyms = parseAcronyms("acronyms.tex")

process(sys.argv[1], aux, acronyms)

