% up: part02---background.tex
\section{Distributed Systems}
\label{sec:bg-distributed-systems}

%%%% Intro to distributed systems
A distributed system is ``a collection of independent computers that appears to
its users as a single coherent system'' \cite{tanenbaum-ds}.
In other words, a distributed system consists of multiple autonomous computers
that communicate through a computer network while working towards a common goal.

%%% Distributed vs. centralised
Distributed systems have a number of advantages as well as
disadvantages compared to centralised systems. 

The main advantage of a distributed system is that it is more resilient to
errors than a centralised system. However, this increased reliability depends
on redundancy built into the system; if each task in the distributed system can
only be accomplished by a single unit, the system is in fact less reliable than
a corresponding one-unit system.  This implies that for each task, multiple
units must be present that can accomplish that task.  An example for this is
the task \emph{power generation}, which can be provided by many units in the
power system, thus making the system more robust against failures of single
nodes. 

A general advantage of having a system consisting of many small units is
illustrated by the \emph{law of large numbers}: the average behaviour of a large number of
units should be close to the normal, expected behaviour of a single unit.  This
happens because errors typically occur only sporadically. A distributed system with
many units can take advantage of that law.

A distributed system usually makes it easier to scale the system,
e.g.~to integrate more power generation into a power system. If the
power system had just one generator, this generator would have to
be upgraded to provide more generation. In a
distributed system, it is possible to just connect another power plant.

The main disadvantage of distributed systems is that they are more
complex. This makes them more difficult to understand completely,
e.g. to enumerate all failure modes of the system. 

Additionally, distributed programs are difficult to implement. Every operation
that is delegated to another node in the distributed system can fail, and this
failure has to be anticipated and dealt with. In non-distributed programs,
operations are executed on the same computer and without participation of a
communication link, therefore most of them can be expected to be accomplished
without an error.

The main reason for the difficulty in implementing distributed systems is that
they work asynchronously (see also \refnpage{sec:bg-asynchronous-communication}).
The asynchronous nature of those systems results in many parallel threads of
execution. Because of this, the behaviour of such a system is more difficult to
understand. This is illustrated by the number of states the system can be in,
which is the product of the number of states of the individual nodes.

%%%% power systems are distributed systems
An automated power system is a very good example for a distributed
system: each automated component in the power system is controlled by
a computer (a node) which participates in the distributed system controlling
the power system. 
Nodes can have a various goals: the most basic is to have a
stable and secure power grid, with the additional constraint that this should
be achieved in a cost-efficient manner. This top-level goal can be split up
into sub-goals, such as generating and consuming power, providing ancillary
services to the grid, and so on.

%%%% power systems are a bit untypical: geographically distributed, unreliable comm., heterogeneous

Power systems have properties that are rather unusual for
distributed systems:

\begin{itemize}
  \item The computers participating are distributed over a large area.

  \item The communication links are not necessary high throughput, or even reliable.

  \item The participating nodes are heterogeneous, i.e.~they have
    different capabilities.  Power system components are highly specialised,
    and can provide only a certain set of tasks, such as power generation,
    power consumption or specific ancillary services. Furthermore, they provide
    different means to accomplish these tasks; an example for this are ramp
    rates of power plants.

\end{itemize}

