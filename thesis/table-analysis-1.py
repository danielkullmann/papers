#!/usr/bin/env python

kB=1024
kBit=kB/8
MB=1024*1024
MBit=MB/8

APPROACHES = [
    {
      "name": "SCADA",
      "bytes": [10,10,10,2], # [bytes]
      "frequency": 1, # [1/s], every second
    },

    {
      "name": "Behaviour descriptions (no extra communication)",
      # the first 6 entries are for messages of the negotiation protocol,
      # the last 3 entries means the data exchanged to get the power price every minute
      "bytes": [256,256,256,5120,10240,5120], # [bytes]
      "frequency": 1.0/(15*60), # [1/s], every 15 minutes
      "": "",
    },

    {
      "name": "Behaviour descriptions (15 min. power price)",
      # the first 6 entries are for messages of the negotiation protocol,
      # the last 3 entries means the data exchanged to get the power price every minute
      "bytes": [256,256,256,5120,10240,5120] + 1*[256], # [bytes]
      "frequency": 1.0/(15*60), # [1/s], every 15 minutes
      "": "",
    },

    {
      "name": "Behaviour descriptions (5 min. power price)",
      # the first 6 entries are for messages of the negotiation protocol,
      # the last 3 entries means the data exchanged to get the power price every minute
      "bytes": [256,256,256,5120,10240,5120] + 3*[256], # [bytes]
      "frequency": 1.0/(15*60), # [1/s], every 15 minutes
      "": "",
    },

    {
      "name": "Behaviour descriptions (1 min. power price)",
      # the first 6 entries are for messages of the negotiation protocol,
      # the last 3 entries means the data exchanged to get the power price every minute
      "bytes": [256,256,256,5120,10240,5120] + 15*[256], # [bytes]
      "frequency": 1.0/(15*60), # [1/s], every 15 minutes
      "": "",
    },

    {
      "name": "Behaviour descriptions (10 sec. power price)",
      # the first 6 entries are for messages of the negotiation protocol,
      # the last 3 entries means the data exchanged to get the power price every minute
      "bytes": [256,256,256,5120,10240,5120] + 6*15*[256], # [bytes]
      "frequency": 1.0/(15*60), # [1/s], every 15 minutes
      "": "",
    },

]


COMM_TECH = [
    {
      "name": "Internet",
      "bandwidth-down": 8*MBit, # [bytes/s]
      "bandwidth-up": 1*MBit, # [bytes/s]
      "latency": 0.015, # [s]
    },

    {
      "name": "UMTS",
      "bandwidth-down": 384*kBit, # [bytes/s]
      "bandwidth-up": 384*kBit, # [bytes/s]
      "latency": 0.150, # [s]
    },

    {
      "name": "GPRS",
      "bandwidth-down": 57.6*kBit, # [bytes/s]
      "bandwidth-up": 28.8*kBit, # [bytes/s]
      "latency": 0.400, # [s]
    },

]



table1 = open( "generated/table-analysis-1a.tex", "w" )
table2 = open( "generated/table-analysis-1b.tex", "w" )
tables = [table1, table2]

for tech in COMM_TECH:
  for t in tables:
    t.write( " & " )
    t.write( r"\multicolumn{1}{c}{" + tech["name"] + "}"  )

for t in tables:
  t.write( r"\\\hline" + "\n" )

for approach in APPROACHES:
  bytes = approach["bytes"]
  sumBytesDown = sum( [ bytes[i] for i in range(len(bytes)) if i%1==0 ] )
  sumBytesUp = sum( [ bytes[i] for i in range(len(bytes)) if i%1==1 ] )
  numMessages = len( [ x for x in bytes if x != 0 ] )

  for t in tables:
    t.write( approach["name"] + "&" )

  for tech in COMM_TECH:
    secondsPerRequest = sumBytesDown / tech["bandwidth-down"] + sumBytesUp / tech["bandwidth-up"] + numMessages * tech["latency"]
    secondsInAnHour = secondsPerRequest * 3600 * approach["frequency"]
    table1.write( "%.2f" % secondsPerRequest )
    if secondsInAnHour > 3600:
      table2.write( r"\textcolor{darkred}{%.2f}" % secondsInAnHour )
    else:
      table2.write( "%.2f" % secondsInAnHour )

    for t in tables:
      t.write( "&" )

  for t in tables:
    t.write( r"\\" + "\n" )

for t in tables:
  t.close()
