% up: part05---communication.tex
\section{Categories of Communication} 
\label{sec:onewaybroadcast-twowayunicast}
\label{sec:types-of-communication}

At least 4 different categories of communication are used in power systems
today; in this section, communication simply denotes the transfer of information.

\begin{itemize}

  \item State of the Power System (\emph{implicit communication}): this is not
    communication in the typical sense, but it is information about the power
    system that is available in distributed nodes.  Examples for this include
    system frequency and voltage, which can be measured anywhere in the power
    system. Frequency is the same across a power system, i.e.~all units measure
    the same frequency. Furthermore, frequency gives important information about
    the health of the power system. That's also why frequency control is such
    an important ancillary service.

  \item Broadcast communication: this has been used for a long time in power
    systems, often for load management. The canonical example are ripple
    control systems, but data can also be transferred using other means of
    communication, such as radio.

  \item Dedicated point-to-point connections: dedicated connections are widely
    used for \gls{SCADA} communication. These have the advantage of being
    relatively reliable, but they are rather expensive.

  \item Non-dedicated connections: with the integration of large numbers of
    \gls{DER}, it will be necessary, if only for cost reasons, to use existing
    communication links for communication with those resources. This includes a
    wide range of possible options for the underlying physical message
    transport (see \refnpage{sec:comm-link-properties}. This type of communication
    link has the main disadvantage that quality and performance of the links
    is reduced, as compared to dedicated links.

\end{itemize}

The categories have each their advantages and
disadvantages. These are listed in the table
\ref{tab:compare-4-comm-types}.  The columns of the table are:

\begin{itemize}
  \item Latency, bandwidth and availability have already been discussed in
    section \ref{sec:comm-link-properties}
  \item Individual addressing: most types of communication can send messages to
    an individual target unit, which has to have a unique address. Broadcast 
    or multicast messages do not provide this feature.
  \item Bidirectional communication: most types of communication support
    bidirectional sending of messages, but some only support the sending of
    messages in one direction; one example for the latter type is radio.
  \item Cost: the cost of deploying the given communication type
\end{itemize}

\begin{table}
  \begin{tabular}{ p{3cm} | c c c}
      & Latency & Bandwidth & Availability \\\hline
    Implicit      & very low &  low &  high \\
    Broadcast     & low & low & high \\
    Dedicated     & low & high & high \\
    Non-dedicated & medium - high & high & medium \\
  \end{tabular}

  \vspace*{2ex}

  \begin{tabular}{ p{3cm} | c c c}
      & Indiv. Addressing & Bidir. Comm. & Cost \\\hline
    Implicit      & no &  no & very low \\
    Broadcast     & no & no & low \\
    Dedicated     & yes & yes & high \\
    Non-dedicated & yes & yes & medium \\
  \end{tabular}
  \caption{Four Categories of Communication}
  \label{tab:compare-4-comm-types}
\end{table}

The tables do not give values for the individual entries, but rather relative
measures. This is because the values depend on many factors.

The table reveals that there is a trade-off between cost on the one hand, and
throughput and flexibility on the other hand. Especially one-way broadcast
communication can be realised very cost-efficiently. It will be of great value
if this kind of communication is utilised in a more flexible manner than it
is today. Ripple control systems, for example, provide only a fixed response to the
received signal. The ripple-control channel cannot be used for changing the 
response to the signal; it only transfers a single value and does not
allow for individual addressing.

It makes sense to distinguish between supervisory and local communication here.
Local communication links are usually easier to keep reliable, mainly due to
the much shorter distance that has to be bridged. This implies that local
control communication can use control algorithms that demand reliable
communication.  Supervisory communication links, on the other hand, will have
to rely on communication links that are much less reliable, such as Internet
broadband connections.  Therefore, supervisory control communication should be
less demanding towards the quality of communication links than local control
communication.

% DONTDO rewrite, because this text is mostly from olge
In order to exploit the full potential of a controllable component, a control
system must have an individually addressable, bidirectional communication
link to that component. When using synchronous control communication, that
communication link must also have a high availability.

As the table above shows, these requirements are difficult to fulfil at the same
time.  A solution to this problem can be found by a divide-and-conquer
approach: if the individual communication acts which form the communication
process can be organised in such a way that each of the individual
communication acts has lower requirements to the communication link compared
to the whole communication process, a combination of different low-cost
technologies could offer equal performance to a single, more expensive
technology.

In particular, the communication for many control tasks can be separated into
communication acts which require unicast addressing and bidirectional
communication but are not time-critical, and other communication acts which
demand reliable communication with low latency but can be served by a
unidirectional broadcast medium. Both technologies are inexpensive and readily
available: Broadband Internet connections offer unicast and bidirectional
communication but are unreliable and do not give any latency guarantees.
Broadcast media like radio transmitters and tone-frequency ripple control
systems provide high reliability and defined latency, but do not support
bidirectional communication or unicasting.

This is the basic idea behind behaviour descriptions: behaviour descriptions
are exchanged over the non-time-critical bidirectional communication, while the
components will react to trigger signals like system frequency or a broadcast
power price; how the components react to those trigger signals is governed by
the behaviour descriptions.  Behaviour descriptions are presented in section
\ref{sec:behaviour-descriptions}. 

