\chapter{Experiments and Simulations}
\label{sec:experiments}

In order to test the proposed concepts as well as the developed framework, a
number of tests have been carried out using both simulation tools and real
hardware.

The first test was a simple \gls{EV} charging simulation to evaluate the
exchange of messages in the negotiation protocol and the scalability of a
behaviour description based approach. This test did not involve a simulation of
the power system. 

The second test was an experiment controlling the heaters of the intelligent
office building \gls{PowerFlexhouse} that is part of the \gls{SYSLAB}
experimental power grid. Its main goal was to evaluate the performance of real
components that are controlled with behaviour descriptions.

The third test simulated the effects of behaviour-description-based control of
heaters in 100 houses by employing thermal models of the houses.  The goal of
the simulation was to investigate the performance of a larger number of
controlled components.

%\todo{The fourth test was conducted to quantify the effects of using a
%behaviour-based approach as compared to a traditional direct control approach.}

All the code for experiments and simulations has been developed by the author,
unless explicitly noted.

Some general notes regarding experiments and simulations are necessary before
starting to describe them in detail.

\paragraph{Control Algorithms}

Behaviour descriptions encapsulate one or more control algorithms, but it was
not the topic of this thesis to develop any control algorithms. Therefore, the
control algorithms used here are not very sophisticated. The goal of the three
tests was to demonstrate the feasibility of using behaviour descriptions for
control of power systems and to investigate performance of
behaviour-description-based control.

\paragraph{Simulation Time Steps}

Simulations are usually focused on a certain time span. Some use very short
simulation time steps; e.g.~to investigate short-term effects of generator
failures. Other simulations use longer time spans, e.g.~to investigate
long-term power balance.

Most examples for behaviour descriptions combine control algorithms, or
services, that work on very different time scales.
A frequency control service will have a time scale of seconds to minutes,
whereas a price-based service will have a much longer time scale such as five
minutes to one hour, depending on how often new power prices are determined.

Due to the different time scales, it is time consuming to simulate such a
system.  The frequency control service requires a simulation with small time
steps, i.e.~a second or less, but the price-based service will only produce
usable results if the simulation time contains many of its time steps. 

This issue prevented the use of a sophisticated control algorithm in the
second simulation, since this used too much processing power to still have a
real-time simulation; see section \ref{sec:sim-flexpower}.

\paragraph{Trigger Signals}

Two types of trigger signals were used for the experiment and the simulations:
system frequency and a dynamic power price. These signals were obtained from
various sources:

\begin{itemize}

  \item The system frequency was measured at the grid connection point, i.e.~in
    the \gls{SYSLAB} \gls{PowerFlexhouse}. This was accomplished using existing
    \gls{SYSLAB} functionality, i.e.~the code doing the actual measurement was
    not written by the author.

  \item One of the price signals used was the spot price of the Nordic power
    system, It was obtained from the Nordpool electricity market home page,
    \url{http://www.nordpoolspot.com}. The spot prices are determined every day
    at 12:00 for the next day, as hourly prices. This functionality comes
    from the \gls{SYSLAB} code base as well.

  \item Another price signal that was used was a real-time artificial price
    signal based on
    production and consumption data of the Danish power system. The price
    calculation is based on the rationale that a high penetration of wind
    energy works towards lower spot prices, since the marginal production cost
    of wind energy is very low. 
    The rationale for using this artificial price signals was that the Nordic
    power system does not currently have a mechanism for the generation or
    broadcast of fast dynamic power price signals, although such a mechanism is
    likely to exist in the future. Again, this functionality comes from the
    \gls{SYSLAB} code base.

  \item One of the simulations used a precalculated price signal following a
    sine curve. It was chosen due to the increased variation it offered,
    compared to the other two price signals presented here.

\end{itemize}

None of these price signals was originally implemented by the author, but
improvements were made to them for the tests described here.


\input{part09-b-simulation1}
\input{part09-a-experiment1}
\input{part09-d-simulation2}
%\input{part09-e-experiment2}
\input{part09-z-conclusion}

