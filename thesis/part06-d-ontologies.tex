\section{A Power System Ontology}
\label{sec:power-system-ontology}

Ontologies have been introduced in section \refnpage{sec:related-ontologies}.
In this section, an \gls{ontology} for power systems is outlined. 

The reason for using an ontology is that it provides defined semantics for the
described terms, which reduces the risk that messages are misinterpreted.
Ontologies can also be processed by computers, which means that certain
messages transformations, like the conversion of physical units, can be
carried out automatically.

For the purpose of control of power systems, an \gls{ontology} has to provide the
following types of definitions: 

\begin{itemize}
  \item A description of basic types of components, such as generators,
    consumers, transformers, and storage equipment
  \item A structural view of the power system, i.e.~how the components are
    connected together
  \item A functional view of the components, i.e.~what capabilities the
    components provide, and how they can be accessed
  \item A description of signals that are relevant to power system control,
    such as measurements of system frequency and voltage, current power price,
    and actions of breakers (open/close)
  \item A list of the relevant physical units (m, s, W, etc.), optionally with
    conversion mechanisms (e.g.~from Ws to kWh); these units must be used in
    descriptions for values that have a physical unit
\end{itemize}

These are the basic means of description that an \gls{ontology} must provide.
Depending on the application, other types of information might also be
necessary, e.g.~information about contracts between entities in the power
system.

The \gls{CIM} provides such a power system \gls{ontology}. Its data model can
be used to describe the components and structure of a power system to some
detail. This data model is, however, only suitable for a description of the
system, and not suitable for actual control of the system. 

Another source for a common power system \gls{ontology} is the \gls{IEC} 61850 standard
(see section \refnpage{sec:related-iec-61850}). It provides a structural view
on the power system and much information about the components, including means
to control the components. 

However, \gls{IEC} 61850 is less suited for \gls{high-level communication} than
\gls{IEC} 61970; it was designed as a standard for \gls{SCADA}-like systems,
and as such follows the measurement/setpoint mind set. This does not imply that
it is impossible to use \gls{IEC} 61850 for such purposes; it means that it is
less suitable for it than \gls{IEC} 61970.

Section \refnpage{sec:std-cim-power-system-ontology} lists some classes form
the \gls{CIM} that will be part of such a power system ontology.


\section{Type-Agnostic Access to Components}
\label{sec:type-agnostic}

Much flexibility is lost when aggregators have to know exactly which types of
devices are connected to them; this makes the integration of new components more
difficult. Enabling access to these components in a manner that does not rely
on knowing the exact type of the components simplifies access to the
components, and makes integrating new types of devices less costly.

\gls{IEC} 61850 already provides a type-agnostic access to components, but it
should be investigated whether a different approach could provide even more
flexibility.

Gehrke \cite{OG08} proposed a role-based system, where components
are not accessed via their explicit type, but rather via the roles they can
take. Examples for such roles are \texttt{GridComponent},
\texttt{EnergyResource}, \texttt{ConstantResource}, and
\texttt{StochasticResource}, just to name a few roles for generators. To
illustrate the concept, the \texttt{ConstantResource} interface is displayed
in listing \ref{lst:constant-resource-role}, in a shortened version that fits
on a page.  The full interface contains 46 methods, i.e.~18 methods are missing
from that listing.

The granularity of a role-based system seems to be too coarse; there
are too many methods/parameters for a role.  
The approach taken in this work is to use the concept of services as a basis
to negotiate behaviour; services encapsulate a much smaller piece of
functionality into a single unit than the role-based approach, resulting in a
granularity that seems to be better manageable. See the next chapter, starting
\vpageref{sec:services}, for a thorough presentation.

% Fits exactly on one page (b5paper):
\lstinputlisting[language={[daku]java},numbers=left,float,%
  caption={Interface for Role \texttt{ConstantResource}},
  label=lst:constant-resource-role%
]{listings/ConstantResource.java}

