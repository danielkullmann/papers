@echo off
pdflatex -output-directory _output %1
cd _output
makeglossaries %1
copy /Y ..\*.bib .
bibtex %1
cd ..
pdflatex -output-directory _output %1
pdflatex -output-directory _output %1


