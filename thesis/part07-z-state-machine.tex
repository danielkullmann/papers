%up: part07---policies.tex
\section{Alternative Implementations}
\label{sec:other-implementations-for-beh-desc}

Several alternative implementations of behaviour descriptions are feasible.
A few are presented in this section, and compared to the rule-based system that
was actually used in this thesis. 


\subsection{State Machines}
\label{sec:state-machines}

One option to specify behaviour descriptions is by using state machines: states
activate certain services, and the transitions between states are governed by
trigger signals, eventually comparing them to thresholds. Nested state
machines, where single states can themselves contain a complete state machine,
make it easy to specify complex behaviour.

An example for such a state machine for power system control has been created
by two colleagues, Fridrik Rafn Isleifsson and Cosmin Koch-Ciobotaru; it is
shown in figure \ref{fig:state-machine-example}. This state machine provides
both temperature control and voltage control, with voltage control 
having a higher priority.

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{generated/SYSLAB_PV_FH_control_EXP_Chart-crop.pdf}
  \caption{Example State Machine}
  \label{fig:state-machine-example}
\end{figure}

This state machine monitors changes in the two input signals $V_{\mbox{bus}}$
(bus voltage) and $I_{\mbox{inside}}$ (inside temperature), which are compared
to various thresholds, four for voltage ($V_{\mbox{min,low}}$,
$V_{\mbox{min,high}}$, $V_{\mbox{max,low}}$, $V_{\mbox{max,high}}$) and two for
temperature ($T_{\mbox{min}}$ and $T_{\mbox{max}}$). The states trigger actions
by increasing or decreasing heater consumption in $1\,\si{kW}$ steps; the
corresponding output variable is $P_H$. Transitions are not triggered each time
an input signals changes; instead, the \texttt{after} command introduces delays of several
seconds after each state change.

This state machine contains explicit implementations of two services, a
voltage control service (the two state boxes on the top), and a thermostatic
control service (the bottom box). Rule-based behaviour descriptions, as they
are envisioned by this work, try to avoid explicit implementation of services
in the behaviour description. Using a explicit implementation implies that the
service concept as introduced in chapter \refnpage{sec:services} cannot be
used, because the service provider is not free to choose its own implementation
of the service. State machines could of course also employ services; this
particular example does not do this.

This state machine is very similar to the rule-based example 
described in section \ref{sec:example-rule-base}, which was combining frequency
control with price-based control. In fact, a behaviour description based on a
state machine can be converted to one using a rule-based system: a state
corresponds to the body of a rule, and a state transition corresponds to a
condition of a rule; the current state is stored in a variable in the rule
base. Nested state machines, such as in the example, are converted by making
rules representing outer states invoke the rules for the inner states.

Conversion in the other direction is also possible, but the resulting state
machine has to have transitions from every state to every other state, because each
rule is considered at every time step, while a transition is only considered
when its source state is the currently active state.

This implies that state-machine based behaviour descriptions do not offer
anything that rule-based behaviour descriptions cannot provide. For certain
applications, when the state machine is easier to understand than the
corresponding rule base, using state machines might be preferable though.


\subsection{API-Based Behaviour Descriptions}
\label{sec:api-based-behaviour}

\begin{figure}
  \centering
  \includegraphics[width=0.6\linewidth]{generated/api-based-policies.pdf}
  \caption{API-Based Behaviour Description}
  \label{fig:api-based-behaviour}
\end{figure}

A completely different approach is to map the functionality of behaviour
descriptions to an \gls{API}, i.e.~to a number of remote method calls.  Using
the example rule-base from section \ref{sec:example-rule-base} as a basis, a
similar effect could be realised by a series of remote method calls such as in
figure \ref{fig:api-based-behaviour}, which shows the remote method calls
that are executed by a server to set a specific set of services that
a client has to provide. 
Before anything happens, the client has to register with its server.  The
server establishes a connection to the client, retrieves information about that
client, and calculates the parameters for the services that should be provided.
In this example, three services are provided: frequency control, price-based
control, and thermostat control. The server then calls the \gls{API} function
\texttt{addService} three times, each time with the specific instance of the
service that should be provided.
The priority of each services is implicitly given by the order of the
\texttt{addService} calls: the first call to \texttt{addService} gives the most
important service.

\lstinputlisting[language={[daku]java},float,%
  caption={API-Based Behaviour Description, Client Code},%
  label=lst:api-behaviour-description-client%
]{listings/api-behaviour-description-client.java}

The corresponding pseudo-code on the client side is shown in listing
\ref{lst:api-behaviour-description-client}. Each time the client has to make
the decision of which service to activate, it iterates through the most recent
list of services it has received, and activates the first service that requests
to be activated; all following services are ignored.

The name \gls{API}-based behaviour descriptions derives from the usage of a
remote \gls{API} to specify behaviour descriptions. The \gls{API}
consists in this simplified example of the three functions
\texttt{retrieveClientInfo}, \texttt{clearAllServices} and \texttt{addService}.

The downside of this method is reduced flexibility. A rule-based system
allows to add additional clauses to the condition part of a rule, which is
difficult to emulate using the \gls{API} method. Furthermore, more complex
rule sets, where rules activate other rules, can not be mapped to this method
without considerably extending the \gls{API}. This would result in an overly
complex \gls{API}, which would have to emulate the features of a rule base.
In such a case, it is actually easier to use the rule-based system.

\subsection{Decision Trees}
\label{sec:bd-decision-trees}

Decisions trees are an important technique in decision support systems
\cite{aima}. The basic idea, when applied to behaviour descriptions, is that
the solution to a problem can be split into two or more sub problems when one
question is answered.  Each answer corresponds to a parent-child relation in
the tree, and the tree is traversed by answering the question at each visited
node. In the end, a leaf node is reached, which gives the solution to the
problem.

An example of a decision tree, providing the same functionality as the simple
example rule base from listing \refnpage{lst:simple-example-rulebase}, is given
in figure \ref{fig:decision-tree}.

\begin{figure}
  \centering
  \includegraphics[width=0.75\linewidth]{generated/decision-tree.pdf}
  \caption{An example decision tree}
  \label{fig:decision-tree}
\end{figure}

Such simple decision trees can be mapped to rule bases in a very simple way:
Each path through the decision tree is converted to a single rule, with the
condition of the rule being the conjunction of all answers on the path. This
works of course also for complex decision trees, but the number of rules, and
the complexity of the rule conditions, increases and makes them more difficult
to understand. In that case, a more advanced mapping could be used where each
parent-child connection in the tree is mapped to a single rule. All non-leaf
rules just activate a different set of rules.

This illustrates that decision trees are just a simple kind of rule base.

\subsection{Compiled Programs}
\label{sec:bd-compiled}

Another possible implementation of behaviour descriptions would be sending complete
compiled applications, i.e.~binary blobs of data, to device controllers that
are controlled by the supervisor. The big disadvantage of this approach is that
it is complicated to have an automated controller create non-trivial
applications, and the programs are almost impossible to understand
and evaluate by computers and human beings.

For small embedded devices, however, this method could make sense: a
supervisor could generate a high-level behaviour description based
on rule systems, and convert the behaviour description into a compiled program.
Using that method, under-powered embedded devices could be controlled by a
rule-base, even though those devices are not powerful enough to process the rule
base in the original text-based format.

A variation of this idea uses programs written in a scripting language to
describe behaviour.

\subsection{Function blocks}

This is another variation on the previous section. Function blocks, as defined
in the IEC 61499 standard \cite{iec61499-1}, allow the expression of internal
logic using several programming languages defined in IEC 61131-3
\cite{iec61131-3}: Sequential Flow Chart (SFC), Function Block Diagram (FBD),
Ladder (LD), Instruction List (IL), and Structured Text (ST). Those are
programming languages that are used for programming \glspl{PLC-ctrl}.

An interesting feature of the IEC 61499 standard is the ability to upload
function blocks to remote units. This feature could be used to transfer
behaviour descriptions to embedded devices that support the IEC 61499 standard.



