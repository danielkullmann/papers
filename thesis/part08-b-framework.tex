% up: part08---policy-framework.tex
\section{Description of Framework}
\label{sec:policy-framework}

The framework has been implemented in \gls{Java}. Its basic structure is illustrated in the
\gls{UML} diagram in figure \ref{fig:policy-framework-structure-uml}. Not all classes
that comprise the framework are shown; only the classes that are used by users
of the framework are displayed. 

The classes that must be implemented by actual behaviour description
implementations are the five classes at the bottom of the figure:
\texttt{ServerBehaviour}, \texttt{ServerClientBehaviour},
\texttt{ClientBehaviour}, \texttt{Policy} and \texttt{DeviceInfo}.  

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{generated/policy-framework-structure-uml.pdf}
	\caption{Policy Framework UML Class Diagram}
	\label{fig:policy-framework-structure-uml}
\end{figure}

The two main classes are \jexttt{PolicyServer} and \jexttt{PolicyClient} which
are the partners in the negotiation protocol; see section
\ref{sec:policy-negotiation-protocol}. \jexttt{PolicyServer} is responsible for
the generation of behaviour descriptions, which are based on class
\jexttt{Policy}, while \jexttt{PolicyClient} has to
provide \jexttt{DeviceInfo} objects, and accept and execute behaviour
descriptions sent to it. This
functionality is dependent on the actual behaviour descriptions that are used,
so it is provided by implementations of the
\jexttt{ServerBehaviour}, \jexttt{ServerClientBehaviour} and
\jexttt{ClientBehaviour} interfaces. These are
explained later in section \ref{sec:usage-of-framework}.

It should be noted that the policy framework uses different names for component
and behaviour descriptions: A component description is an object of type
\jexttt{DeviceInfo}, whereas a behaviour description is represented by a
\jexttt{Policy} object. This is due to historical reasons.

Several server implementations (instances of \jexttt{ServerBehaviour} and
\jexttt{ServerClientBehaviour}) and client implementations (instances of
\jexttt{ClientBehaviour}) can be active at the same time on the same machine. 

The various concrete message classes were omitted from figure
\ref{fig:policy-framework-structure-uml}; there is one for every message type
used in the negotiation protocol; compare with section
\refnpage{sec:policy-negotiation-protocol}:

\begin{itemize}
  \item \jexttt{RegisterClient}: a client registers at a server
  \item \jexttt{RequestNegotiation}: a client starts a negotiation
  \item \jexttt{ServerRequestNegotiation}: a server starts a negotiation
  \item \jexttt{ProvideDeviceInfo}: a client provides a component description
  \item \jexttt{ProposePolicy}: a server proposes a behaviour description to a client
  \item \jexttt{AcceptPolicy}: the client accepts the behaviour description sent to it
	\item \jexttt{RejectPolicy}: the client rejects the behaviour description sent to it
  \item \jexttt{AliveMessage}: periodically sent by the client, so that the server can
    recognise failures in communication channels
  \item \jexttt{UnregisterClient}: used by a client that wants to stop its participation
    in the system
  \item \jexttt{NotUnderstood}: sent in reply to unexpected messages
\end{itemize}

All are subclasses of the abstract class \jexttt{PolicyMessage}. All messages
can be converted to and from a \gls{FIPA}-like text format, which is the format
that is used for sending the messages; the conversions are done by
\jexttt{toFipaContent()} and \jexttt{parse(...)} methods, respectively.

The clients register themselves at the server using the \jexttt{RegisterClient}
message.  Right now, there is no process implemented that enables clients to
search for a suitable server; a client has to know its server or aggregator.  See
sections \refnpage{sec:server-discovery} and
\refnpage{sec:negotiation-protocol-service-discovery} for a discussion on
various available methods. 

The reasoning behind the lack of such functionality is
that clients usually will be tied to a certain aggregating company, due to the
owner of the controlled component having entered into an agreement with this
aggregating company. This implies that a client will be configured with the
hostname of the aggregator server; the \gls{DNS} will then be used to provide
redundant aggregating services. 

The framework also supplies a number of classes that facilitate the creation
and execution of rule bases, an implementation of the service \gls{API} (see
\refnpage{sec:svc-service-api}), and a base class for trigger signals.


\section{Internals of the Framework} 
\label{sec:internals-of-framework}

The implementation of the negotiation protocol is done in two classes,
\jexttt{PolicyClient} and \jexttt{PolicyServer}, which implement the client
side and the server side of the protocol. Both classes extend the class
\jexttt{PolicyPartner}, owing to shared functionality.
\jexttt{PolicyServer} and \jexttt{PolicyClient} both implement a simple state
machine (one state machine for each connected client on the server side), which
tracks the state of the client(s).

The class \jexttt{PolicyUtils} provides several static helper classes, mostly
convenience methods, but also the method \jexttt{parsePolicyMessage()}, which
creates a message instance from the textual representation of one of the
\jexttt{PolicyMessage} types listed above. The \jexttt{setup()} method from the
\jexttt{PolicyUtils} class should
be called at the start of a program; it sets up logging, and reads in a
\gls{Java} properties file that can be used to configure the policy
implementation.

For sending and receiving messages the interface
\jexttt{CommunicationSystem} has been created, which has two actual
implementations, one using \gls{HTTP} as transport, the other using \gls{JADE},
i.e.~\gls{FIPA} messages. 
The exchange of data is realised in an asynchronous
manner, i.e.~using message passing.

\gls{JADE} was originally the only communication system provided by the policy
framework. That library has however some disadvantages that were discovered
while using it. Some of the issues with \gls{JADE} are that the library is rather
heavy-weight, it is difficult to set up and to shut down, and it was difficult to
use the same address for local and remote access.  These difficulties led to
the implementation of an \gls{HTTP}-based \jexttt{CommunicationSystem} which is
much simpler and easier to use.

A communication system implementation consists of three different classes: 

\begin{itemize}

  \item A \jexttt{CommunicationSystem} implementation: this includes methods to start
    and stop a communication system (e.g.~start and stop the \gls{HTTP} server) and
    methods to create addresses and agents.

  \item An implementation of an agent by implementing the
    \jexttt{AbstractAgent} interface. An agent handles sending and receiving of
    messages in the negotiation protocol.  Each
    \jexttt{PolicyClient} and \jexttt{PolicyServer} instance uses an agent to send
    and receive messages.

  \item An implementation of an address by implementing the
    \jexttt{AbstractAddress} interface. Each agent has its own unique address;
    for the \gls{HTTP} communication system, the address implementation just
    wraps a \gls{URL}; in the \gls{JADE} communication system, it wraps an
    agent ID object (\jexttt{jade.core.AID}). In the policy framework, an
    address consists of three
    parts: the hostname of the agent, a container name, and the agent name. The
    reason for employing this structure is that \gls{JADE} uses it, and the
    first version of the communication system was implemented using \gls{JADE}.

\end{itemize}

These classes are the main classes of the framework; other classes and
interfaces provide helper functionality (e.g.~for parsing messages, for signing
messages and for checking the signature of messages), or provide extension
points for actual implementations; the latter are explained in the next section.


\section{Usage of the Framework} 
\label{sec:usage-of-framework}

It is necessary to implement at least 5 abstract classes to use the framework:

\begin{itemize}

  \item \jexttt{Policy} implementations, which are behaviour
    descriptions.

  \item \jexttt{DeviceInfo} implementations, which are component
    descriptions. Behaviour descriptions will be created based on this
    information.

  \item A \jexttt{ServerBehaviour} implementation: one instance of this class runs on a
    server; it is responsible for creating \jexttt{ServerClientBehaviour}
    instances for every client that connects to the server.
    
  \item A \jexttt{ServerClientBehaviour} implementation: for each client that
    connects to a server, an instance of this class is created. The instance
    manages the state of the client on the server side, e.g.~the latest
    component description and behaviour description.
    
  \item A \jexttt{ClientBehaviour} implementation: this manages the state of
    a client on the client side.

\end{itemize}

Note that the term ``behaviour'' in \jexttt{ServerBehaviour},
\jexttt{ServerClientBehaviour} and \jexttt{ClientBehaviour} is not related to
the concept of behaviour descriptions; it rather denotes that those classes
implement some kind of behaviour of server- respectively client-side units.
The class names were chosen like this because \gls{JADE} calls these classes
behaviours.


\paragraph{Implementation of a \jexttt{Policy}}

Implementations of the interface \jexttt{Policy} are behaviour descriptions.
The only two methods that the interface contains are a
\jexttt{toFipaContent()} method for creating a string representation of the
behaviour description, and a method \jexttt{getTimeFrame()} that returns the
validity period of a behaviour description. The implementation must also
provide a matching \jexttt{parse} method for each policy implementation, which
is called from \jexttt{ServerClientBehaviour.parsePolicy()} and
\jexttt{ClientBehaviour.parsePolicy()}.


\paragraph{Implementation of a \jexttt{DeviceInfo}}

Implementations of the interface \jexttt{DeviceInfo} are component descriptions.
Therefore, the interface contains only a single method, \jexttt{toFipaContent()},
which is used to generate a string representation of the DeviceInfo that can be
sent in a message. Each implementation of \jexttt{DeviceInfo} should also contain
a static \jexttt{parse} method (or a constructor) that creates a \jexttt{DeviceInfo}
instance from that string representation. Such a method is required by
\jexttt{ServerClientBehaviour.parseDeviceInfo()}.


\paragraph{Implementation of a \jexttt{ServerBehaviour}}

The only method that has to be implemented by subclasses is
\jexttt{createServerBehaviour()}, which is used to create a
\jexttt{ServerClientBehaviour} instance for a client that registers itself with
the server. Additionally, implementations usually use this class to keep track
of all connected clients, and to have a central planner thread that
periodically creates behaviour descriptions for all connected clients.

An instance of this class has to be registered with the \jexttt{PolicyServer}
during initialisation. 


\paragraph{Implementation of a \jexttt{ServerClientBehaviour}}

A \jexttt{ServerClientBehaviour} instance is created for each client that is
supervised by the server; the central methods to implement are to accept a
\jexttt{DeviceInfo} object, and to create a \jexttt{Policy} for that client.


\paragraph{Implementation of a \jexttt{ClientBehaviour}}

The client behaviour has to be explicitly instantiated, in contrast to the
server side, where a \jexttt{ServerClientBehaviour} is created on demand when a
client registers itself. The \jexttt{ClientBehaviour} contains methods for
creating \jexttt{DeviceInfo} objects representing the device, for
accepting or rejecting a \jexttt{Policy}, and for activating that \jexttt{Policy}.


\paragraph{Implementation of Client and Server Applications}

After this has been done, server and client applications should be implemented.
Listings \ref{lst:example-server-application} and
\ref{lst:example-client-application} show two \gls{Java} applications, one for
starting a server, and one for starting a client. The main difference between
the two applications is that different implementations (server or client) have
to be created, and that on the client side the behaviour is explicitly
created, with information about which server to contact.


\lstinputlisting[language={[daku]java},numbers=left,float,caption={Example Server Application},label=lst:example-server-application]{listings/ServerAppExample.java}

\lstinputlisting[language={[daku]java},numbers=left,float,caption={Example Client Application},label=lst:example-client-application]{listings/ClientAppExample.java} 


