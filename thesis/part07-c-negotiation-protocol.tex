% up: part07---policies.tex
\section{Negotiation Protocol}
\label{sec:policy-negotiation-protocol}

The negotiation protocol provides the 
protocol for exchanging information about controlled components and 
behaviour descriptions that were created for the components.

This protocol is very similar to the
\glsfirst{CNP}~\cite{contract-net-interaction-protocol}, a protocol defined
by \gls{FIPA}; see section \refnpage{sec:related-cnp} for more information. 
Differences between the two protocols are explained below in section
\ref{sec:cnp-comparison}.

The protocol consists of a number of steps:
\begin{enumerate}
  \item The component is new, and registers with its supervisor
  \item The component requests a policy negotiation
  \item The supervisor starts a negotiation round
  \item The component sends a component description to its supervisor: the
    state it is in, the services it supports and the constraints it has to
    comply with
  \item The supervisor creates a behaviour description/policy and sends it to the component
  \item The component accepts or rejects the behaviour description; in case of
    a rejection, the component goes back to step 2
  \item The component activates the behaviour description as soon as it becomes valid
  \item Before the validity interval expires, the component starts a new negotiation (step 2)
\end{enumerate}

The supervisor keeps track of the state of each connected component: 
\begin{enumerate}
  \item \texttt{REGISTERED}; waiting for negotiation to start
  \item \texttt{NEGOTIATION\_STARTED}; waiting for device information
  \item \texttt{WAITING\_FOR\_POLICY}; Device information received; calculating
    behaviour description
  \item \texttt{POLICY\_SENT}; Behaviour description has been sent; waiting for
    acceptance or rejection
  \item \texttt{POLICY\_ACCEPTED}; Component has accepted the behaviour
    description
  \begin{itemize}
    \item In the case of rejection of the behaviour description, the next state
      will be 2
    \item The component usually has to wait for the behaviour description to
      become active, but this does not result in a change of state on the
      server side
    \item The component will stay in this state until the behaviour description
      is about to expire, or another request for negotiation is received
  \end{itemize}
\end{enumerate}

\label{sec:policy-client-states}
Clients have to keep track of their own state during the negotiation protocol:

\begin{enumerate}
  \item \texttt{SEARCHING\_SERVER}: When the client is not yet connected to a
    server it sends a \textit{Register} message to a supervisor.
  \item \texttt{NEGOTIATE}: When the supervisor responds, the agent immediately
    starts a negotiation round.
  \item \texttt{WAIT\_FOR\_POLICY}: After the client has sent the component
    description to its supervisor, it will have to wait for a behaviour
    description. This can take some time because supervisory controllers will
    often want to calculate behaviour descriptions for many clients at once.
  \item \texttt{ACTIVE}: The client has received and accepted a behaviour
    description. When the validity period of the active behaviour description
    nears its end, the client will start a new negotiation round and switch
    back to the \texttt{NEGOTIATE} state.
\end{enumerate}


The protocol consists of a number of messages that are being exchanged:
\begin{itemize}

  \item \textit{Register}: 
    With this message, a component registers itself with its supervisor.
    How it finds this supervisor depends on the setup of the whole system; this
    has not been implemented yet, but can be done in various ways, the easiest
    being the use of \gls{DNS} (see section
    \ref{sec:negotiation-protocol-service-discovery}).

  \item \textit{Start Negotiation from Component}: 
    After registering, the component will start the negotiation process.
    This message is used to start the process from the component side.

  \item \textit{Start Negotiation from Supervisor}: 
    The supervisor will reply by sending a negotiation request itself.  This
    message can also be used by the supervisor whenever it has to start a new
    negotiation process.

  \item \textit{Send Device Information}: 
    The component sends a component description. This includes general
    information about the component, the list of services it provides,
    the list of signals it can receive, and information about
    constraints it has to obey.

  \item \textit{Suggest a Behaviour Description}: 
    The supervisor creates a behaviour description for the component; 
    sometimes this will take some time, due to the supervisor waiting for more 
    units to connect.

  \item \textit{Reject the Behaviour Description}:
    The component may reject the behaviour description sent to it. This can
    happen for various reasons; one is that the constraints the component has
    to adhere to have changed in the meantime.

  \item \textit{Accept the Behaviour Description}:
    Alternatively, the component can accept the behaviour description; the
    behaviour description will be activated  when the validity period starts.

\end{itemize}

The negotiation process can be restarted at any time. This can be due to several reasons:

\begin{itemize}

  \item One of the messages did not reach its target, in which case the receiving
    party will restart the process. Usually, this will be one of the Start
    Negotiation messages.

  \item Before the end of the validity period of the currently active behaviour
    description, the concerned component will restart the process
    by sending a \textit{Start Negotiation} message.

  \item An external event has made it necessary to change the currently active
    behaviour description. This leads to the sending of one of the Start Negotiation
    messages, depending on which side is initiating the process again.

\end{itemize}

The negotiation protocol uses an additional message type that has not been listed
above, the \textit{Alive Message}. This is sent regularly from the client to
the server, and assures the server that the client is still reachable. The
message is only sent when the client has not sent any other message to the
server for a certain time span; this time span is by default set to 60 seconds.

When the server has not received a message from the client for a certain period
of time (currently 120 seconds), it will mark the client as such. Actual server
implementations can use this information to decide whether to calculate a
behaviour description for such a client.

A last message type that is used in the protocol is the \textit{Not Understood
Message}.  This is sent in reply to an unexpected message 
that cannot otherwise be handled. This will for example be sent when a
client receives a behaviour description from a server that it currently does not accept
behaviour descriptions from. This situation can happen when a client fails while it is
supervised by a server, and reconnects to a different server after it
has booted up again. 


\subsection{UML Notation of Protocol}
\label{sec:policy-negotiation-protocol-uml}

The general sequence of messages in the protocol is illustrated in figure
\ref{fig:negotiation-protocol-uml} as a \gls{UML} sequence diagram.  

\begin{figure}
  \centering
  \includegraphics[width=0.60\textwidth]{generated/negotiation-protocol.pdf}
  \caption{Sequence Diagram of the Negotiation Protocol}
  \label{fig:negotiation-protocol-uml}
\end{figure}

This sequence diagram does not cover the complete negotiation protocol,
however. Three behaviours have not been put into the diagram, since they do
not necessarily occur during a normal negotiation protocol exchange:

\begin{itemize}

  \item The sending of alive messages, because it is independent from the
    flow of the other messages

  \item Reactions to unexpected messages

  \item Restarting the protocol when no message was received for a longer period

\end{itemize}


\subsection{Comparison with CNP}
\label{sec:cnp-comparison}

The similarity between negotiation protocol and \gls{CNP} raises the question
of why the \gls{CNP} was not directly used to negotiate behaviour.
When comparing the two protocols (see figure
\refnpage{fig:fipa-cnp-sequence-diagram} for the CNP sequence diagram), the following
differences can be discerned:

\begin{itemize}

  \item In the negotiation protocol, clients have to explicitly send
    information about themselves to the supervisor. This is not done in the
    \gls{CNP}, since that protocol expects that the agents that could provide
    a certain service are already known to the requester agent.

  \item (Re-)Negotiations of the policy can be initiated by both sides in the
    negotiation protocol. This is not intended in the \gls{CNP}, because this
    was created to support the solicitation of bids to accomplish a task, and
    only one side in the protocol has tasks that have to be done.

\end{itemize}


\subsection{Locating a Server}
\label{sec:negotiation-protocol-service-discovery}

As implemented, the negotiation protocol does not contain a means for a client
to actually find a server that is suitable for it. This is because the
implementation of such a feature is not necessarily required. To find an
aggregator, the client will usually use a \gls{DNS} server (see section
\refnpage{sec:server-discovery}).  To reimplement the functionality of the
\gls{DNS} system is not necessary. 


\subsection{Reliability of the Negotiation Protocol}

Since reliability is a major concern for this work, the negotiation protocol itself must also
be reliable against failures.  Two major failure types are handled here:

\begin{enumerate}

  \item Failure of communication links

  \item Failure of software: Software does have bugs, some of which are only
    triggered in certain circumstances. This implies that the whole protocol
    process should be restarted, when one partner in the communication fails to
    adhere to the protocol.

\end{enumerate}

The correct behaviour for the first kind of failure is to use timeouts,
i.e.~when an expected message does not arrive in a certain time span, the
negotiation protocol is restarted from a safe point. A safe point is one where
the communication partners require the least amount of information to continue
their conversation. In the case of the negotiation protocol, these are the
\texttt{ServerRequestNegotiation} message for the server and the
\texttt{RequestNegotiation} and \texttt{RegisterClient} messages for the
client. See the \gls{CSP} definition of the protocol for more information; it
is given in the next section.

The remedy for the second type of failure is an immediate restart of the
communication as described in the previous paragraph.

