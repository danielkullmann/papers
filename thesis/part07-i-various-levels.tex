% up: part07---policies.tex
\section{Levels of Behaviour Descriptions}
\label{sec:levels-of-beh-desc}

The rule-based implementation of behaviour descriptions is just one example,
albeit a very flexible one, of implementing behaviour descriptions. Other options
to define behaviour descriptions are available. In fact, there is a whole
hierarchy of options with increasing flexibility:

\begin{enumerate}
  \item Fixed behaviour
  \item Fixed behaviour, depending on external signal
  \item Measurements/set points
  \item Activate single services
  \item \gls{API}-based system 
  \item Activate multiple services, with an obvious disambiguation strategy
  \item Activate multiple services, with a custom disambiguation strategy
  \item \glsfirstplural{MAS}, i.e.~more autonomous behaviour
\end{enumerate}

This list could have been organised by other categories, e.g.~by increasing
autonomy, but flexibility of behaviour is the most important feature of
behaviour descriptions as compared to other approaches.

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{generated/levels-of-policies.pdf}
  \caption{Levels of Behaviour Descriptions}
  \label{fig:levels-of-behaviour-descriptions}
\end{figure} 

% DONTDO Replace a)-d) in figure with 3., 4., 6. and 7.

Figure \ref{fig:levels-of-behaviour-descriptions} illustrates some of these
levels, which are explained in the following paragraphs. The outer frame in
each of the items a)-d) represents the message that is sent using that
method; the inner rectangles represent the parts of the message. Several of
the entries in the list of options are not shown in the figure, because they
either use no messages to convey behaviour (the fixed behaviour options), or
the messages they exchange are not constrained in any way (\gls{MAS}).

\paragraph{1. Fixed Behaviour}

These are components that have a behaviour that cannot be controlled. Examples
for this are traditional household appliances and uncontrollable generators such as 
wind turbines or \glspl{PV}.


\paragraph{2. Fixed Behaviour, Depending on External Signal}

These are components that have a fixed behaviour, but this behaviour can be
influenced by an external signal. A good example for this are ripple-controlled
components that consume more power during times when power is cheaper, another
is a power generator following a fixed droop curve, i.e.~the droop value cannot
be changed remotely.


\paragraph{3. Measurements/Set Points}

This is the typical \gls{SCADA}-controlled component which allows the exchange
of measurement values and control commands.


\paragraph{4. Activate Single Services}

This is in effect very similar to the measurements/set points method. It
offers a certain interface to the controller; this could include requesting
measurements and reacting to control signals. There are two major differences,
though:

\begin{itemize}

  \item The focus on autonomous behaviour: the component does not have to wait
    for control signals to arrive from the controller to act.

  \item The semantics of the interchange: a service activation (see section
    \refnpage{sec:service-definition}) is a self-contained piece of data
    that activates defined behaviour, and encapsulates all parameters for this
    service in one message. In \gls{SCADA} systems, services are often
    activated using several set points, e.g.~by separately sending the three
    setpoints for a \gls{PID} controller.

\end{itemize}



\paragraph{5. Activate Multiple Services with Obvious Disambiguation Strategy}

A step forward, towards more flexibility, is the combination of several
services, provided at the same time. If the execution of these services leads
to conflicts, e.g.~when both services want to control active power output, the
service with the highest priority is chosen; the priority of services is
predefined in this case.


\paragraph{6. API-based system}

This is described in more detail in section
\refnpage{sec:api-based-behaviour}; in its simplest form, this system maps
the functionality of activating multiple services to an \gls{API}, providing
the same level of flexibility. Extending the flexibility of that approach is
possible, e.g.~by emulating a custom disambiguation strategy, but
the complexity that would be introduced into the \gls{API} by this extension
would reduce its usability; therefore, a rule-based system is preferable for
providing that sort of flexibility.


\paragraph{7. Activate Multiple Services with Custom Disambiguation Strategy}

This is a variation of the previous item, with the difference that the
disambiguation strategy is not predefined, but is contained in the message.
This is the activation logic from figure
\ref{fig:levels-of-behaviour-descriptions}. 

An example for this kind of behaviour descriptions is the rule-based system
from section \refnpage{sec:implementation-using-rules}, but other
implementations have this characteristic as well, such as the state machine
implementation that have been described in section \refnpage{sec:state-machines}.


\subsection{Comparison}

These different options can be compared in various ways. The most interesting
comparison, in my opinion, is regarding the flexibility.  Figure
\ref{fig:flexibility-matrix} compares the flexibility of the various options,
and other schemes introduced in section
\refnpage{sec:related-power-systems}, in regard to two different types of
flexibility:

\begin{itemize}
  \item The flexibility to react to various trigger signals
  \item The flexibility to show different reactions to trigger signals
\end{itemize}

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{generated/flexibility-matrix.pdf}
  \caption{Flexibility Comparison}
  \label{fig:flexibility-matrix}
\end{figure}


Fixed behaviours obviously have the lowest flexibility in both dimensions.  The
two options \emph{using setpoints/measurements} and \emph{activating services}
provide a little more flexibility in both dimensions.  A market system is very
flexible in how to react to signals, but it only reacts to power prices. The
two options that support activation of multiple services are of course more
flexible in to how many signals they can react, with the custom logic option
being more flexible in how it can react.

\glspl{VPP} can use arbitrary algorithms for controlling the units constituting
the \gls{VPP}, therefore it is difficult to put them into an exact spot in this
figure. Nevertheless, \glspl{VPP} usually react only to few signals, but
have some flexibility in how to react to that signal.

Other categories for comparing the listed options are the degree of autonomy
that they allow and their scalability. Since behaviour descriptions are suited
best for situations were controlled autonomy is desired, full autonomy is not
the goal of this thesis.  That said, the two options that combine several
services at the same time make less supervisory intervention necessary. Adding
custom logic provides for the possibility to anticipate many different
situations and encode the correct response to each of them.

Scalability, on the other hand, is difficult to predict in a general manner.
The evaluation carried out in section \refnpage{sec:mathematical-evaluation}
showed at least that behaviour descriptions require less bandwidth than
synchronous control communication, which is not very surprising.

