% up: part07---policies.tex
\section{Latency and Bandwidth Considerations}
\label{sec:mathematical-evaluation}

Now that the behaviour description concept has been explained, and an example
implementation using rules has been demonstrated, the communication
requirements of this concept can be analysed and compared with a traditional
\gls{SCADA} system.

Since there is not a single authoritative \gls{SCADA} system, and behaviour
descriptions can also come in different formats, the analysis will be based on
assumptions concerning the size of data packets.

This analysis evaluates the
effects of different bandwidth and latency values of various communication
technologies on the quality of the provided service.

The third identified performance parameter, availability (see
section \refnpage{sec:comm-link-properties}), is not taken into account here.
The reason is that the effects of that parameter are difficult to quantify in this
kind of evaluation. This is due to the statistical nature of availability
effects. 

The \gls{SCADA} system uses a simple \gls{closed-loop} control scheme which 
executes the following loop once per second:

\label{data:scada-messages}
\begin{itemize}
  \item Request measurement: estimated 10 bytes
  \item Receive measured data: estimated 10 bytes
  \item Send control command: estimated 10 bytes
  \item Wait for acknowledgement of command: estimated 2 bytes
\end{itemize}


The behaviour description system negotiates a new behaviour description every
15 minutes. This duration can vary considerably for control
schemes, so a rather low value has been chosen. These are the messages that have to be
exchanged:

\label{data:bd-messages}
\begin{itemize}
  \item Request for negotiation: estimated 256 bytes
  \item Wait for acknowledgement: estimated 256 bytes
  \item Request component description: estimated 256 bytes
  \item Send component description: estimated 5120 bytes (5kb)
  \item Propose a behaviour description: estimated 10240 bytes (10kb)
  \item Accept behaviour description: estimated 5120 bytes (5kb); this message is
    large because the acknowledgement must reference the behaviour
    description it acknowledges
\end{itemize}

These two methods are evaluated for different types of communication
infrastructure, which have different values for average latency (in seconds)
and bandwidth (in bytes per second). Bandwidth values are given for downstream
and upstream transfer, since these values often differ substantially;
downstream bandwidth is usually significantly larger. Downstream is the direction 
from supervisor to controlled component, upstream is the opposite direction. 

The values in the following list have been taken from different sources
\cite{gprs-data,list-of-bandwidths}; it is difficult to get values for latency,
because these are usually not part of any standard and depend on the actual
setup, e.g.~on the number of routers the packets have to pass through. Therefore,
latency values have been estimated. The calculation is based on the assumption
that a \gls{TCP}/\gls{IP} connection is used.

\label{data:comm-tech}
\begin{itemize}
  \item Internet: 
    8/1 MBit/s bandwidth\footnote{Assuming an ADSL (G.DMT) connection}, 
    15ms latency
  \item \gls{UMTS} mobile: 
    384/384 kBit/s bandwidth, 
    150ms latency
  \item \gls{GPRS} mobile: 
    57.6/28.8 kBit/s bandwidth, 
    400ms latency
\end{itemize}


The evaluation consisted of calculating pure transfer times of the data packets
based on latency and bandwidth. Upstream and downstream messages are taken
separately into account due to different bandwidths. Latency is taken into
account once per sent message; due to the sliding window protocol used by
\gls{TCP} connections, latency 
has to be taken into account only once for messages with a size of up to 
$\mbox{size}_{\mbox{packet}} * \mbox{size}_{\mbox{window}}$ bytes, which 
usually is around $64k$ bytes.


The results of the evaluation of the two methods is listed in two tables.
Table \ref{tab:comm-analysis-1a} shows the number of seconds a single
transaction requires. A transaction is one loop for the \gls{SCADA} system, or
one negotiation for the system based on behaviour descriptions. Obviously,
a system based on behaviour description requires more time for one transaction. Table
\ref{tab:comm-analysis-1b} shows the accumulated seconds for the transactions
taking place in one hour. The communication time for the \gls{SCADA} system
is much higher; in one instance (\gls{GPRS}) the requirement of having one
transaction per second cannot be fulfilled.

\begin{table}
  \centering
  \begin{tabular}{lSSSSS}
    \input{generated/table-analysis-1a}
  \end{tabular}
  \caption{Seconds per Request}
  \label{tab:comm-analysis-1a}
\end{table}

\begin{table}
  \centering
  \begin{tabular}{lSSSSS}
    \input{generated/table-analysis-1b}
  \end{tabular}
  \caption{Accumulated Seconds per Hour}
  \label{tab:comm-analysis-1b}
\end{table}


In this analysis, the security of data, i.e.~encryption or digital signatures,
has been disregarded; the effect would be larger for the \gls{SCADA} system,
because it uses smaller packets, and the overhead in terms of data size
depends on the number of packets that are sent.

Availability has not been accounted for in this analysis, but the effects of
failures in the communication links can be easily summarised:

\begin{itemize}

  \item For a closed-control loop employing direct control,
    a loss of connectivity will be fatal: since no control is taking place, the
    control objective cannot be upheld.

  \item For a system based on behaviour descriptions, a loss of connectivity
    is only problematic during the negotiation of a new behaviour description,
    which happens only for short periods; the rest of the time, connectivity between
    supervisory controller and controlled component is not necessary.

\end{itemize}

