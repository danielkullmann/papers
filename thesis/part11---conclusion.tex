\chapter{Conclusion}

% TODO Emphasize coordination

In this thesis, an implementable solution has been developed that addresses the
main challenges of including the demand side and distributed generation in the
control of power systems:

\begin{itemize}

  \item Flexibility: the control system must be able to adapt to changes 
    in the power system.

  \item Extensibility: new types of components and services should be easy to
    integrate into the control system.

  \item Reliability: the result must be a stable and reliable power system,
    especially with respect to the unreliable communication links that the control
    system will have to use to communicate with the large number of small
    components distributed over the whole system.

  \item Scalability: the control system must be able to coordinate large
    numbers of power system components at the same time.

\end{itemize}

The developed solution is based on two methods:

\begin{enumerate}

  \item Use a strong service concept to exchange information about capabilities
    of power system components and the functionality that these components should
    provide.

  \item Use self-contained descriptions of behaviour to enable intelligent
    \mbox{(semi-)}au\-to\-no\-mous behaviour of power system components,
    i.e.~increased autonomy on the lower levels of the control hierarchy.

\end{enumerate}

The proposed strong service concept is specified by the \emph{service
definition} which consists of five parts: 

\begin{enumerate}

  \item A service name uniquely and unambiguously identifying that service.

  \item A service specification which exactly defines the semantics of the
    service name and describes the format of service descriptions and
    activations.

  \item A service description which describes in which form a specific
    component can provide the service, e.g.~which constraints the component has
    to adhere to.

  \item A service activation instructing a component to provide a certain
    service, optionally specifying parameters for that service.

  \item A service implementation that is executed on the local controller,
    which is capable of providing the service as described in service
    activations.

\end{enumerate}


A \emph{behaviour description} is a self-contained description of behaviour for a
specific component. This description can be acted upon semi-autonomously by the
component, meaning that the component is free to act within the boundaries
dictated by the behaviour description.
Various options for implementing  behaviour descriptions were introduced;
the option based on rules was implemented, due to the flexibility that rules
provide.  %TODO 2nd experiment: uses javascript?

A prerequisite of being able to use behaviour descriptions is a mechanism to
exchange component descriptions, which contain service descriptions, and
behaviour descriptions, which contain service activations. This is provided by
the negotiation protocol, a variant of the \gls{FIPA} \gls{CNP}.

To be able to evaluate the feasibility of the two concepts, the policy
framework was implemented. It implements the service framework as well as
the negotiation protocol, and provides for the possibility to implement
arbitrary component descriptions and behaviour descriptions that can be
exchanged through that protocol. Implementations of clients and servers produce
and process component descriptions and behaviour descriptions to achieve the
desired overall system behaviour. The policy framework also contains classes
that facilitate the use of rule-based behaviour descriptions.

The feasibility of the proposed solutions and the functionality of the policy
framework were demonstrated in an experiment and two simulations. % TODO: *two* experiments

This thesis also investigated how the two proposed concepts could be
standardised. \gls{IEC} 61970 was identified as the most fitting standard to be extended,
due to the rich data model it provides and the flexible \gls{XML} message
formats that can be defined using that data model. A set of classes from the
\gls{IEC} 61970 data model were determined that could be used inside messages
that describe services and behaviour descriptions; additionally, new classes
that provide missing functionality were described.

The two concepts address the challenges listed above in the following ways:

\begin{itemize}

  \item Flexibility: behaviour descriptions allow flexible specification of
    behaviour.

  \item Extensibility: introducing new components and new behaviours is easier,
    because behaviour is encapsulated in services. Communication standards
    do not have to be extended to support new services; instead, only the
    supervisors and components using that service must know of its existence.

  \item Reliability: due to independence from completely reliable communication
    links, the system is more reliable. Since behaviour descriptions are
    self-contained, behaviour is predictable even when no communication is
    possible for some time.

  \item Scalability: Behaviour descriptions increase the autonomy of components
    even when the actions of these components have to be monitored, since the
    components can assume responsibility for their own supervision. This reduces 
    the amount of supervision that a supervisory controller has to do.

\end{itemize}

Current research in control algorithms focuses on two different control
approaches, direct control and indirect control. Direct control assumes that
the supervisory controller can send raw control signals to the units, whereas
indirect control lets units react to artificial signals such as a power price
to indirectly control the behaviour of the unit. Behaviour descriptions
introduce a third type of control that combines the strengths of the other two
approaches: 

\begin{itemize}

  \item The need to control how units react to signals: A controlled unit has
    to act according to the rules encoded in the behaviour description; thus,
    it is possible to enforce deterministic behaviour of the controlled
    units.

  \item The need to have units behave more autonomously: The proper working of
    a controlled unit does not depend on having a reliable connection to the
    supervisory controller. This reduces the requirements on communication link
    quality and the computational and communication load of the supervisory
    controller.

\end{itemize}
 
The concept of \emph{service} can be used in all aspects of power system
control, whereas the application of behaviour descriptions is especially suited
for the provision of important system services. Many tasks in power systems can
also be coordinated using methods that allow for more autonomy; especially
market-based systems are being investigated at the moment. Nevertheless,
behaviour descriptions can also be employed in that context, instructing units
to participate in a market-based or price-based system, e.g.~by providing the
address of a market concentrator that should be contacted.

Is the more complex setup that is necessary for using behaviour descriptions
worthwhile? The advantages that have been listed are important for implementing
a reliable, flexible and scalable control system.  The implementation of the
policy framework demonstrates that realising such a system is not complicated.
Calculating behaviour descriptions that provide useful services takes some
effort, but this is the case for many sophisticated control schemes.

Additionally, the requirements on computational power on the client side are
not particularly high; every decent embedded computer is able to execute
behaviour descriptions, even the rule-based ones. For embedded computers that
are not able to handle rule-bases directly, the rule-base can also be compiled
into a program which is executed directly on the computer.

An additional advantage of a system based on behaviour descriptions is its
resilience against attacks on the communication infrastructure, due to the
reduced need for a continuous flow of data between the units in the system.
This means that attacks such as \gls{DDoS} attacks are less likely to impact
negatively on the performance of the whole system.


\section{Revisiting the Use Cases}
\label{sec:conclusion-use-cases}

All use cases that were presented in this thesis benefit from using behaviour
descriptions:

\begin{itemize}

  \item \emph{\nameref{sec:usecase1-services}}: small components often have to
    use unreliable communication links, because installing more reliable links
    is too expensive.  Behaviour descriptions provide independence from
    unreliable communication links and flexible, semi-autonomous behaviour of
    components.

  \item \emph{\nameref{sec:usecase2-demandside}}: Most appliances do not have a
    \gls{CPU} with enough processing power to be able to process behaviour
    descriptions. In that case, their control will be mediated through a home
    gateway, which is the local controller for these components.  Behaviour
    descriptions alleviate issues caused by communication glitches; these
    happen relatively often in such settings, because the home gateway will use
    the existing broadband Internet connection which is unreliable and also
    used by others.

  \item \emph{\nameref{sec:usecase2a-transforming-behaviour}}: This can easily
    be realised using services and behaviour descriptions. The individual
    components can contribute their part to the provision of a service;
    parameters to the service differ from component to component, so that
    the net result is smooth provision of the needed service.

  \item \emph{\nameref{sec:usecase2b-dynamic-activation}}: The negotiation
    protocol allows dynamic registration and de-registration of components at a
    supervisory controller, as well as the exchange of information about those
    components. This allows a supervisor to have up-to-date information about
    available components and their capabilities.

  \item \emph{\nameref{sec:usecase3-charging}}: Behaviour descriptions
    facilitate the coordination of \gls{EV} charging processes, providing the
    benefits listed above. Coordination of the charging processes is necessary
    because it prevents overloading of distribution grids by limiting the
    number of \glspl{EV} that are charged at the same time.

  \item \emph{\nameref{sec:usecase4-arbitration}}: this is a very different
    problem from the previous ones, because it is constrained to a small area.
    Behaviour descriptions fit into this use case by providing extra
    information about the steps needed to avoid overloading of the local grid
    or a substation transformer.

\end{itemize}


\section{Future Work}

Only certain types of control algorithms are suitable for behaviour
descriptions; these have not been researched very well, because control
algorithm researchers usually assume direct control over the components.
Market-based algorithms, on the other hand, do indirect control, i.e.
components are assumed to work autonomously (without supervisory control).
This implies a need for research in control algorithms that take advantage of
behaviour descriptions. 

Only  a few services have been described in this thesis, frequency control,
price-based control and schedules. If behaviour descriptions are used to
facilitate control of power systems, many more services have to be described,
and the actual service specifications have to be described in great detail.

The scalability of the behaviour description concept has not been shown
conclusively. The simulation results point to that conclusion, but a test in a
real power system with a large number of controlled components is necessary to
finally evaluate the complexity of such a system, especially in regard to the
planning process producing the behaviour descriptions. 
% TODO Should this paragraph stay as it is (it's a bit negative)

