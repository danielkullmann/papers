#!/bin/bash

ENVS=array,eqnarray,equation,figure,mathmatica,picture,table,tabular
for f in "$@"; do
  trap "rm -f /tmp/tmp$$.txt" INT TERM KILL PIPE HUP
  sed -e 's/\\label{[^}]*}//g' -e 's/\\\(begin\|end\){itemize}//g' $f | detex -c -e $ENVS - > /tmp/tmp$$.txt
  java -jar ~/local/LanguageTool/LanguageTool.jar -d WHITESPACE_RULE /tmp/tmp$$.txt | sed -e 's/^[0-9]*\.) Line \([0-9]*\), column \([0-9]*\), \(.*\)/'$f':\1:\2:\3/'
  rm -f /tmp/tmp$$.txt
done

