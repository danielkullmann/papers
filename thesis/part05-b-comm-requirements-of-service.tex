% up: part05---communication.tex
\section{Quantifying Provided Link Quality}
\label{sec:quantifying-provided}

When components are controlled via generic communication links like broadband
Internet, mobile communications and such, it is important that the provided
link quality, i.e.~latency, bandwidth and also availability, can satisfy the
required link quality. The provided values must be at least as good (latency:
lower is better, bandwidth and availability:: larger is better) than the
required value to have a well functioning service.  This and the next section
discuss how these two types of link quality can be quantified.

The provided quantities of latency and bandwidth do not have fixed values; they
do vary.  It will be best to describe them as averages and standard deviation
over a certain time period.  Availability can  be quantified as percentage,
even though this metric does not contain a lot of information, especially not
about the typical duration of a failure (see section
\refnpage{sec:comm-availability}). 

For low-cost broadband communication links, it will be difficult to
get a guarantee on any of the three properties. This is one of the reasons why control
communication using these links should not rely on synchronous communication
that expects fast response times and high availability.

% DONTDO Example: Broadband, technologies from \ref{sec:mathematical-evaluation}


\section{Quantifying Required Link Quality}
\label{sec:quantifying-required-quality}

The quantity of latency and bandwidth required by a certain control algorithm
can in principle be expressed as absolute values, such as up to $10\,\si{ms}$
latency and at least $10\,\si{KB/s}$ bandwidth.

The requirements can be difficult to quantify exactly, however;
a control system usually just works better on a better
communication link, and there might not be an absolute value that can
be provided as the minimum required for the provision of a certain
service. 

Nevertheless, a value can be given that acts as a guide or
rule-of-thumb to evaluate the fitness of a certain communication
technology to be used for that specific purpose.

To use an example, frequency control is usually a service that requires
fast reaction times to changes in the power system. The overall
control system latency should be well below one second. The 
required bandwidth for a frequency control service, on the other
hand, is usually quite low, because only single measurements
(frequency) and setpoints (power output) have to be exchanged. 
The bandwidth needed for the example frequency control service would be well
below 1 kB/s.

Another example is a component that reacts to a dynamic power price, which is
updated every 5 minutes. In this example, latency, bandwidth and availability
requirements are very low. 

A requirement on availability is difficult to quantify, because many control
systems stop working whenever their communication link fails for a longer
period; failures that last only for a few seconds are usually not enough to
prevent proper functioning of a synchronous control system. 

Ideally, an availability of 100\% would be desirable, i.e.~no failures at all,
but this is not possible in reality. Providing high availability guarantees
becomes very expensive. 

It is therefore necessary to weigh the importance of the provided service
against the cost for providing certain levels of availability.  In real-world
power systems, the supply of services will exceed the demand for those services
by a fairly large margin, due to the security margins required to operate a
system in a stable manner. This makes it possible to decrease the minimum
availability needed for a certain communication link: when that link
fails, the service that should have been provided by the component using that
link can instead be provided by other components.

