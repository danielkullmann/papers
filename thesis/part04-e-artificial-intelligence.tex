% up: part04---related-work.tex
\section{Artificial Intelligence}

Artificial intelligence is the science of creating software systems that expose
smart behaviour.  It has many different applications; the ones that are
interesting for this work are knowledge representation, ontologies and expert
systems.


\subsection{Knowledge Representation}
\label{sec:related-kr}

In distributed systems, at some point it becomes necessary to
exchange information about entities, physical objects as well as
non-physical concepts. This information has to be encoded somehow, in order
to be able to send that information over the communication channel.  The
interpretation of the encoded information on both sides of the communication
channel must be identical.  If this is not the case, the goal of the
communication act can fail, even though the actual communication act succeeds,
because the communication partners misunderstand each other.

In the context of control, an intelligent control system has to make sensible
decisions.  A prerequisite for that is the availability of information about
its environment, the relevant objects, what actions they can take, and what
effects these actions might have. This information has to be represented
somehow, and this is the essence of knowledge representation \cite{aima}.


\subsection{Ontologies}
\label{sec:related-ontologies}

Knowledge representations describe objects and abstract terms (such as \emph{action})
in terms of other, well-defined terms. An \gls{ontology} provides precise
definitions of these terms. Here is a definition of \gls{ontology} from the \gls{OWL}
guide:

\begin{quote}
  \Gls{ontology} is a term borrowed from philosophy that refers to the science
  of describing the kinds of entities in the world and how they are related.
  \cite{owl-guide}
\end{quote}

In other words, an \gls{ontology} contains descriptions of entities and of the
relationships between the entities.  Descriptions consist of a name, which
uniquely identifies the entity, and a list of properties, which are name-value
pairs.

The quasi-standard for expressing ontologies is the
\glsfirst{OWL}~\cite{owl-spec}. \gls{OWL} is an extension of the \glsfirst{RDF}
which is a standard for expressing metadata~\cite{rdf-spec}. An \gls{OWL}
\gls{ontology} contains a set of classes that have properties, the relationships
between those classes, and instances of these classes that are called
individuals.

Both \gls{IEC} 61970 and \gls{IEC} 61850 provide an \gls{ontology} for power systems
through their data models; \gls{IEC} 61970 via the \gls{UML} model, \gls{IEC}
61850 through \glspl{LN} and \glspl{CDC}. Comparing the two, the \gls{IEC}
61970 \gls{ontology} is more comprehensive.


\subsection{Rule-Based Systems}
\label{sec:related-rule-bases}

Rule-based systems are one of the very
successful results of artificial intelligence research \cite{Giarratano04}.

The basic premise of rule-based systems is that many decisions can be
structured into \texttt{if-then} cases.  This might sound overly
simplistic, but rule-based systems offer the capability to make one rule delegate
work to a certain set of other rules. This makes it possible to implement
complex behaviour using only rules.

Rule-based systems are used for two general purposes:

\begin{itemize}

  \item Use rule-based systems to infer new knowledge; \gls{Jess}
    \cite{jess-homepage} is an expert system that is often used for this.

  \item Use rule-based systems to decide on the correct action; \gls{Drools}
    \cite{jboss-drools} is a business-rule system that can be used for that.

\end{itemize}

These two approaches are not orthogonal to each other: new knowledge can be used
to decide on actions, and an action could store new knowledge in the rule
base. Even so, they are different mindsets, and some systems are more suited
for inferring new knowledge, while others allow easy interaction with the
environment of the rule base.

There are various programming languages and data formats available for
specifying rule-based systems. The Prolog language is an example for a
programming language based on rules, in this case expressed in first-order
logic \cite{prolog-book}. A Prolog program consists of a set of facts and
rules; facts express information about the current state of the system in
question, while rules process facts to deduce new facts and to initiate
actions. The format of Prolog rules follows a \texttt{if-then} structure.

\gls{Drools} is a \gls{Java} library that allows the use of rules in \gls{Java} programs;
it supports two different syntaxes, one an \gls{XML} format, the other one more
like a programming-language. The rule engine has the special feature that the
\texttt{then} part of the rule can contain arbitrary \gls{Java} code; as such
it is very well suited for rule-bases that decide on actions to take.

Several specifications have been developed, or are in the process of being
developed, to standardise the format of rule bases and the exchange of rule
bases. The \gls{Java} rule engine \gls{API} is one example for that \cite{jsr-94}; it
defines an API for accessing rule bases. Unfortunately, it does not specify a
data format for rules; this is left to the implementers of the rule engine API.

RuleML is another effort to standardise rule-bases; this initiative has
produced a number of rule languages such as RuleML Lite \cite{ruleml-lite} and
\gls{FOL} RuleML \cite{ruleml-fol}. RuleML Lite provides a simple XML syntax
for Prolog-style rule-bases. Unfortunately, these languages fall into the first
category, so they are not suited for this thesis.

\gls{Java} is the programming language that is used in this work, so a rule
engine that can be used with \gls{Java} had to be chosen. The \gls{Drools}
rule engine was chosen because it is implemented in \gls{Java} and can easily
interact with Java objects, both in the condition and in the action part of
rules.


