% up: part06---high-level-communication.tex
\section{Server Discovery}
\label{sec:server-discovery}

One of the challenges in achieving a flexible and reliable control structure
is how controlled units can find an appropriate supervisory controller.
This is a necessary prerequisite for achieving
a flexible and reliable control system. Units (both supervisory and
controlled) can fail and communication links can go down; the control
structure has to be able to deal with that. 

When a controlled unit loses its connection to a supervisor, it must be able to
switch to a different supervisory controller, just as a supervisory controller
must be able to deal with failures of his controlled units. These failures can
be both unit failures (software and hardware faults) and communication failures
(e.g.~the Internet connection goes down). 

Controlled units must find a new supervisory controller if the current one does
not reply to requests any more. For this, the unit first has to find out the
address, i.e~the host name or the \gls{IP} address, of an appropriate
supervisory controller, before it can start to communicate with it. 
Since supervisory controllers do not actively search for new units to be
supervised, they do not have to do this; instead, the supervised units
will search for a supervisor.

Several methods are available for finding a matching supervisory controller.
One is the \gls{DNS} service, which provides a reliable, redundant mapping from
host names to \gls{IP} addresses \cite{dns-1034}.  The reliability provided is
twofold: 

\begin{itemize}

  \item The \gls{DNS} servers are themselves redundant, so if one fails,
    another server can take over.

  \item The \gls{DNS} servers can store several \gls{IP} addresses for a single
    hostname, so if one supervisory controller fails, local controllers can
    register at the next available supervisory controller.

\end{itemize}

To be able to use the \gls{DNS} system, a controlled device will have to know
the host name of the supervisor in advance.

Service directories are another option, which allow searching for nodes 
offering a certain service. Examples are the \gls{DF} service that is
part of the \gls{JADE} library, or the \gls{UDDI} system available for
\glspl{web service} \cite{uddi-spec,df-spec}. The difference to \gls{DNS} is
that those are concerned with mapping services to \glspl{URL} providing those
services, whereas \gls{DNS} provides a mapping from host names to \gls{IP}
addresses.

A relatively new development is the \gls{ENS}, which provides a consistent
naming scheme for power system entities, and the availability to look up
information based on IDs of entities \cite{Dornberg11}. IDs are \glspl{URN} of the form

\begin{quote}
  \texttt{ioe:<region>:<class>:<type>:<id-type>:<instance>}
\end{quote}

The prefix \texttt{ioe} stands for \emph{Internet of Energy}; it is the \gls{URI}
scheme for these kinds of IDs.  Region is the geographical region the component
is associated with, like \texttt{de} for Germany; class can be one of
\texttt{role}, \texttt{person}, \texttt{element}, \texttt{service} or
\texttt{group}; type specifies the type inside the given class,
e.g.~\texttt{ev} for an \gls{EV} in the \texttt{element} class; id-type specifies
which type of ID is used; and instance specifies the ID of the entity with the
specific id-type. This scheme allows entities to have many different IDs with
different ID-types, classes and types.

An \gls{ENS} server can map IDs between different id-types, and provide more
information about a given ID. The \gls{ENS} is built on top of the \gls{DNS}
system.

Which of these method to actually use depends on the requirements and the
usage.  When the only service that is needed is redundant mapping of host names
to \gls{IP} addresses, the \gls{DNS} suffices entirely.  When mappings to
different communication system addresses are needed, a system like \gls{ENS}
should be used.  A service discovery system, however, is only necessary when
the supervisor is completely unknown in advance. This is not the case in our
scenario; owners of controlled components and supervisors are assumed to have
entered into a contract before the supervisory controller is allowed to send
commands to the controlled component


