#!/usr/bin/python
# coding=utf-8

import subprocess, glob, operator, argparse

# Tweak output here:
charsToStripFromEnd = """.,?!:;"""
nonWordChars = """=#'[]`�()"'*"""
minOccurrence = 30
minOutput = 10
skipWords = 'the of and a to in is you that it he was for on are as with his they I at be this have from or one had by word but not what all were we when your can said there use an each which she do how their if will up other about out many then them these so some her would make like him into time has look two more write go see number no way could my than first been call who its now find long down did get come made may e.g i.e'.split()
skipWords += 'also only such here very should could makes must means does keep called set'.split()

parser = argparse.ArgumentParser('Script to count the words in tex files, and to output an ordered list of the most-used words')
parser.add_argument( '-a', action='store_true', help='show whole word list')
parser.add_argument( '-n', action='store', type=int, help='number of items to show at least')
parser.add_argument( '-o', action='store', type=int, help='minimum number of occurences to display item (if not in first -n x position)')
args = parser.parse_args()
if args.n: minOutput = args.n
if args.o: minOccurrence = args.o

output = subprocess.check_output( ['detex', '-n'] + glob.glob('*.tex') )
wordList = output.split()
words = {}

def addTerm(word):
  global words
  if not word in words:
    words[word] = 1
  else:
    words[word] += 1

  if not word.endswith("s"):
    ws = word + "s"
    if not ws in words:
      words[ws] = 1
    else:
      words[ws] += 1

# also check terms with 2, 3 or 4 words in them
multiWordEntries = [[], [], []]

for w in wordList:
  w = w.rstrip(charsToStripFromEnd).lower()
  if len(w) <= 2: continue

  if w in skipWords: continue
  isARealWord = True
  for c in nonWordChars: 
    if c in w: 
      isARealWord = False
      break
  if not isARealWord: continue

  addTerm(w)

  for i in range(0, len(multiWordEntries)):
    wordLength = i+2
    multiWordEntry = multiWordEntries[i]
    if len( multiWordEntry ) >= wordLength:
      multiWordEntry = multiWordEntry[1:]
    multiWordEntry.append(w)
    multiWordEntries[i] = multiWordEntry
    if len( multiWordEntry ) == wordLength:
      entry = " ".join( multiWordEntry )
      addTerm(entry)


sorted = sorted(words.iteritems(), key=operator.itemgetter(1))
sorted.reverse()

words = [x[0] for x in sorted]
canhaz = {}

idx = 0
for item in sorted:
  if item[0] in canhaz or item[0]+"s" in canhaz: continue 
  if item[0].endswith("s") and item[0][:-1] in canhaz: continue
  
  if item[0].endswith("s") and item[0][:-1] in words: 
    print item[1], item[0][:-1]
  else:
    print item[1], item[0]
  canhaz[item[0]] = 1 
  if not args.a:
    if idx >= minOutput and item[1] < minOccurrence: break
  idx += 1





