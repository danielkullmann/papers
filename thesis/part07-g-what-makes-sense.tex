% up: part07---policies.tex
\section{Control Algorithms for Behaviour Descriptions}
\label{sec:control-algorithms-for-beh-desc}

Not all kinds of control algorithms and strategies are suitable for use with
behaviour descriptions. This section explores the kinds of algorithms that are
suitable for use in behaviour descriptions, and those that are not.

\subsection{Unsuitable Control Algorithms}
\label{sec:unsuitable-control-algorithms}

One type of control strategy that is not suitable is where the supervisory
controller requires immediate control over its controlled units, and it is not
possible to express the control decision through the behaviour description.  An
example for this is strategies that do real-time planning; the planning
algorithm requires global information about the system, so the planning cannot be
moved to the local controllers.

Another type of control strategies unsuitable for behaviour descriptions are ones
where the activation of a certain behaviour has future consequences that have to
be addressed from a global level. 

One example for this kind of control is Lünsdorf's work \cite{LS10}. There,
the power consumption curves of refrigerators are recorded, and generalised
consumption curves are aggregated to follow a certain given consumption curve.
The result is a set of schedules, one for each participating appliance. These
schedules have to be followed exactly to ensure that the overall behaviour of
all appliances agrees with the given consumption curve.

This algorithm could be used with behaviour descriptions  in two different ways,
both of which are not satisfactory:

\begin{itemize}

  \item By sending exactly one such schedule to a refrigerator which has to be
    followed exactly. It is not necessary to use behaviour descriptions for
    exchanging single schedules, however. Furthermore, when external conditions
    change, the supervisory controller might have to recalculate all schedules
    and send them to the refrigerators, which makes it necessary to have a
    stable communication channel between supervisory controller and local
    controller.

  \item By sending many precomputed schedules to a local controller, one for
    each possible scenario. This removes the need for additional communication
    between the two communication partners, but it is virtually impossible to
    create all these schedules in advance, due to the exponential growth of schedules
    resulting from covering all possible scenarios.

\end{itemize}

Behaviours that include planning in the local controllers can be
used with behaviour descriptions, but when doing this, one advantage
of using behaviour descriptions is lost: that the supervisory controller knows
how its units will react, provided the supervisory controller knows the values
of the trigger signals. It depends on the control scenario whether more
autonomous behaviour of units is possible and desirable.


\subsection{Suitable Control Algorithms}
\label{sec:suitable-control-algorithms}

Certain types of control schemes are especially well suited for use with
behaviour descriptions. Here is a list of general schemes that are suitable for
behaviour descriptions; they are elaborated on in the following paragraphs:

\begin{itemize}
  \item To arrange for autonomous execution of behaviour in the future
  \item To make decisions on several levels (multi-level behaviour
    descriptions)
  \item Select one of several possible options
  \item Tweaking settings for a certain service
  \item Provide additional coordination requirements
\end{itemize}


\paragraph{Execution of Autonomous Behaviour in the Future} 

This is basically what behaviour descriptions provide: a means to specify
future behaviour that can be executed autonomously by the local controllers.

Examples for these kinds of control schemes are schedules and droop control.
Even the participation in a power market can be facilitated using behaviour
descriptions: the description tells the component to participate in a power
market, e.g.~using a PowerMatcher-like system \cite{KWK+06}, and contains
information about which concentrator agent to contact.

\paragraph{Multi-level Behaviour Descriptions} 

In this scenario, behaviour descriptions consist of a collection of behaviours
where each behaviour reacts to an event signal on a different level, and the
different levels/behaviours are ordered in a hierarchy. The example from
section \refnpage{sec:example-rule-base} is an instance of this category,
combining frequency control (high priority), price response (medium priority)
and thermostat control (low priority). The higher-priority behaviours are allowed to
override the lower-priority behaviours at any time. 

\paragraph{Select One of Several Options} 

These are behaviour descriptions that select one of a set of behaviours which
have mutually exclusive triggering conditions.
An example is a set of schedules for different values of an input signal,
e.g.~a power price.

\paragraph{Tweaking Settings} 

A distributed control scenario could work as follows: different devices
communicate with each other to find the right settings for parameters
controlling the services they offer. An example for this is droop control:
Adjust droop values so that devices avoid working in suboptimal modes.

The computation of the setting could use a formula which
is part of the behaviour description.

\paragraph{Additional Coordination}

Use case 4 (Arbitration) presented a scenario where units have to coordinate
their behaviour to avoid violating external constraints; in the example given,
the constraint was provided by a substation transformer that should not be
overloaded. Organising such coordination can be part of a behaviour
description, either as a constraint of a specific service, or as a general
setting that is passed alongside the services. In the rule-based system
presented in section \ref{sec:implementation-using-rules}, this could be
implemented by adding a rule with a \texttt{true} condition which instructs the
component to participate in the coordination effort.





