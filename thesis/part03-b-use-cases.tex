% up: part03---problem-description.tex
\section{Use Cases}
\label{sec:use-cases}

Use cases are a valuable tool to define a problem space in a practical manner.
They describe instances of the basic challenge, namely communication for the
control of power systems. 

The basic scenario has already been described in section
\refnpage{sec:bg-power-systems}: Penetration levels for distributed, renewable
energy sources are increasing, a process that is expected to accelerate in the
near future; this makes a new structure for the control system necessary. 

The control scenario that is assumed by this work, an aggregator hierarchy, has
been described in section \ref{sec:aggregation-scenario}. The uses cases should
be examined in the context of this scenario.

The reasoning for presenting the use cases is to explain certain problem
spaces.  Therefore, the use cases are described in an informal fashion; they do
not have to be defined rigorously. The various stakeholders that are involved
in the use cases have been presented in section \refnpage{sec:stakeholders}.


\subsection{Use Case 1: Small Component Offers System Services}
\label{sec:usecase1-services}

The most basic use case is one where a small component, which is connected to
the power grid, offers some service to the grid. This can be ancillary services
as well as more basic services such as power generation or consumption.  That
small components are in fact able to provide ancillary services has been
investigated by others, e.g.~\cite{Nyeng2007,Morren05}.

These components, however, are too small to offer their services directly
on power markets. Due to this, aggregators are needed to aggregate the
capabilities of many such components. The aggregators can offer the
aggregated services on power markets.

This use case proposes the structure that was explained in section
\refnpage{sec:aggregation-scenario}.

\subsection{Use Case 2: Activating the Demand Side}
\label{sec:usecase2-demandside}

Traditionally, generating units are the only ones actively participating in the
control of the power system. This is illustrated by the motto ``generation
follow consumption''. This approach becomes problematic when dealing with large
penetration levels of fluctuating energy sources, because these sources cannot
be controlled like traditional power plants: the power output of a wind turbine
cannot be controlled in the same way as the power output of a gas turbine power
plant.

Therefore the desire to include the demand side actively in power system
control, changing the motto to ``consumption follows generation''. This
approach enormously increases the number of components actively involved in
system control.

As the previous use case, this also favours the setup of an aggregation system.

The two use cases that have already been introduced offer two interesting
extensions: transforming behaviour of components and dynamic activation of 
services.


\paragraph{Transforming Behaviour}
\label{sec:usecase2a-transforming-behaviour}

Small components, especially those on the demand side, offer only a small set
of capabilities. Nevertheless, many system services, even though they are not
directly supported by the single components, can be provided by a large set of
these components. Take as an example a droop control service offering a smooth
response, power output change, to a continuous trigger signal, the system
frequency. Most small components do not support this kind of response; they can
just be turned on or off. By combining the on/off switching actions of many
components, however, a droop control response can be emulated. This can be
realised by giving the components different values for the frequency at which
they should turn on or off, i.e.~by staggering the response.

Such a control requires that units can be addressed individually, so that the
different thresholds for the service in question can be set; the trigger signal
will be still the same for all units, but their responses to a given signal
will be different.


\paragraph{Dynamically Activate Capabilities of Demand Side Components}
\label{sec:usecase2b-dynamic-activation}

The switch from a power system that is mostly driven by large generators to one
with a large share of small components actively participating in the control of
the system needs a change to the manner in which system services are activated:
large power plants are always
online\footnote{except for cases of planned or unplanned downtime, of course},
and their main, if not their only, task is the generation of power. This is not
the case for many of the small components, especially those on the demand side:
their main task is the provision of some service to their owners, such as
cooling, cleaning and space heating, and the provision of system services is a
secondary task, which might even be unwanted at certain times: when one throws a
party, the beverages should be cooled. Due to that, the manner in
which services are provided will be much more dynamic with components
continuously opting in and out of the provision of a service. 

These dynamics apply to several aspects:

\begin{itemize}

  \item Dynamic discovery of components; components should register themselves
    with their respective aggregators; it is infeasible to use a system were
    aggregators need to know beforehand which components are available.
    %no hard-coded list of clients/components on the server side

  \item Dynamic discovery of component capabilities: just as with the discovery
    of the components themselves, information about their capabilities should
    be exchanged dynamically.
    % no hard-coded list of component capabilities on the server side

  \item Dynamic activation of capabilities: the activation of the capabilities
    of components should also be dynamic, because the set of components
    available for providing a service will change over time, just as the demand
    for certain services changes over time.
    %guided by the demand, example: increasing consumption leads to increased
    %generation as well as increasing provision of reserve power, balance power
    %and other nacillary services

\end{itemize}


\subsection{Use Case 3: Coordinating the Charging of Electrical Vehicles}
\label{sec:usecase3-charging}

One particular example for the active inclusion of the demand side into the
control system regards electric vehicles.  After many failed attempts, it seems
as if electric vehicles are now ready for the mass market. Not only is the
technology now mature enough to be mass-produced, but ecological concerns as
well as the limited supply of fossil fuels for traditional combustion engines
%, which has lead to increasing fuel prices,
increase customer demand for new vehicle motion technologies. Both \glspl{PHEV}
and pure \glspl{EV} are available on the automobile market.

The emerging wide-spread adoption of electric vehicles will increase the
loading of the distribution network \cite{stoeckl11,farmer10}. A system to coordinate
charging of \glspl{EV} will be necessary to prevent overloading of the
distribution grid.

\glsunset{V2G} % Treat V2G as already mentioned.
The energy stored in \gls{EV} batteries can be used to provide power to the
grid; this concept is called \glsfirst{V2G} \cite{edison-wp3}. This capability
could be used to reduce bottlenecks in times of peak loads.


\subsection{Use Case 4: Arbitration}
\label{sec:usecase4-arbitration}

This use case is concerned with the local optimisation of distribution
networks, particularly the prevention of substation transformer overloading in
the presence of controllable local generation and consumption. 

The scenario is as follows: controllable generation and/or
consumption units can potentially overload the substation
transformer. To prevent this, the operation of these units has to be
coordinated. This is done by the \gls{DSO}, which is the entity
owning the substation. The actual work of coordinating generation and
consumption is done by a system which is called \emph{arbitrator}. It
collects requests for generation or consumption and coordinates them
by making requests to the \gls{DSO}.

In the context of \glspl{VPP}, this problem is managed by a so-called
\emph{technical VPP} \cite{Pudjianto07}: a technical \gls{VPP} is a type
of \gls{VPP} that manages and aggregates \gls{DER} from a certain geographical
area, taking local loading conditions into account.

A similar scheme can be used when long distribution feeders experience over- or
under-voltages.

Such a system should be considered when system services are activated on the
distribution system level, because these services often have to cross the
substation boundary, and activating these services should not jeopardise the
stability of the local grid.


\subsection{Common Topics}


From the descriptions of the use cases, some common topics can be extracted.
One is the rising number of entities that have to be controlled and
coordinated. This is a divergence from the traditional power system, where just
a few units, maybe a few hundred, are controlled. In current power
systems, many thousands units are connected, and this figure could grow to
millions of units in the future, when small demand-side units are actively
integrated into system control.  This demonstrates on the one hand a demand for
fully automated control of the units, on the other hand it raises the question of
how well existing control schemes scale to that number of units.

Another common topic is the geographical distribution of units. This
implies long communication links between units that have to be
coordinated, which introduces reliability issues: Communication links have
limited capacity, and they fail, so these problems have to be addressed in
the control infrastructure.

