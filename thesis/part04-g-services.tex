%up: part04---related-work.tex
\section{Services}
\label{sec:related-ict-services}

Section \refnpage{sec:related-power-systems} introduced the concept of power
system services.  In the context of \gls{ICT}, the term \emph{\gls{service}} has also
been used for a long time. It denotes a task that is offered by one entity to
other entities.

The \gls{OASIS} offers the following definition of the term:

\begin{quote}
  A service is a mechanism to enable access to one or more capabilities, where
  the access is provided using a prescribed interface and is exercised
  consistent with constraints and policies as specified by the service
  description. \cite{soa-rm}
\end{quote}

The term \emph{\gls{web service}} describes a service that is provided via
\glstext{HTTP}, the protocol for the \glstext{WWW}. The \glstext{W3C} defines
\gls{web service} as follows:

\begin{quote}
  A \gls{web service} is a software system designed to support interoperable
  machine-to-machine interaction over a network. \cite{ws-glossary}
\end{quote}

The term \emph{service} has some ambiguity in the context of power systems.
Section \refnpage{sec:related-power-system-services} supplies a list of power
system services, which are services that are supplied from a component to the
whole system and that have something to do with the provision or consumption of
power. But other types of services, which are not directly concerned with
providing physical tasks, are also relevant for the \gls{Smart Grid};
examples for those services are:

\begin{itemize}
  \item An aggregation service: this is the service offered by an aggregator to
    the aggregated units
  \item A clearing agent in a market is also providing a service to bid agents
    and offer agents
\end{itemize}

The distinction between physical and non-physical services is interesting
because the roles of server (providing a service) and client (consuming a
service) are reversed for the two types: The aggregator provides
the service of aggregation, and the client consumes that service, but at the
same time the client offers the aggregated service, e.g.~power consumption, to
the server. In this thesis, server always denotes the supervisor, and client
always the controlled component.

A large collection of specifications and tools is available for facilitating
the use of \glspl{web service}. Examples for this are \gls{WSDL}, which is
used to describe \glspl{web service}, and \gls{UDDI}, which is a mechanism for
finding servers providing certain services \cite{wsdl-spec,uddi-spec}.

In recent years, the use of \glsplural{SOA} to build large-scale systems has
been increasingly popular \cite{soa-rm}. \glspl{SOA} split larger tasks into
smaller tasks, which are provided as \glspl{web service}. An example is a
web-shop, which uses (possibly external) services for address verification and
online payment. Expected advantages of using this kind of architecture are
increased re-use of code (services can be invoked from different parts of the
system), improved reliability (a service can be provided by more than one
server, probably even from different companies), and increased scalability.

The concept of service is central to this thesis; behaviour descriptions, which
are one of the suggested concepts put forth in this thesis, could be conceived
as a type of service-oriented architecture.  Chapters \ref{sec:services} and
\ref{sec:policies} investigate how services can be exploited for use in
control communication for power systems.

