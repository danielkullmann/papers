% up: part09---experiments.tex
\section{Experiment: Heater Control}
\label{sec:experiment-1}

This experiment demonstrated how the policy framework can be used to implement
reliable dynamic demand response in a real power system. It is related to
\nameref{sec:usecase1-services} and \nameref{sec:usecase2-demandside}.

\subsection{Setup}

The \gls{SYSLAB} facility was used to implement this proof-of-concept demonstration for
the control of heaters in a household \cite{SYSLAB07}. \gls{SYSLAB} is a small power
system designed to facilitate the development and testing of distributed
control algorithms for power systems. A part of \gls{SYSLAB} is \gls{PowerFlexhouse},
an intelligent office building equipped with a large number of sensors and
various types of controllable demand. An embedded controller in the building is
able to execute behaviour descriptions. The code for the temperature
measurement and switching of heaters comes from the \gls{PowerFlexhouse} code
base, which was not originally developed by the author. However, the author
has done a considerable amount of maintenance on that code base.

In the experiment, the building is set up to respond to two types of external
events: Changes in system frequency, and changes in a dynamic power price.
Depending on these parameters and the behaviour description in effect, the
building can either modify the setpoints of the heater thermostats in
individual rooms or switch heaters directly on or off, in order to decrease or
increase the overall power consumption.  The system frequency is measured
directly at the building's grid connection point.  The power price is generated
from the current state of the Danish power system, as described in the
introduction to this chapter.

The setup of the experiment is illustrated in figure
\ref{fig:experiment1-setup}. Each of the 8 rooms is controlled by its own
agent, which participates in the negotiation protocol with the supervisory
controller. Each room is equipped with either one or two heaters. Two trigger
signals can be used by the clients, system frequency and power price. A planner
generates the behaviour descriptions for the rooms, depending on the
information of the provided services.  Behaviour descriptions are provided as
rule-bases; \gls{Drools} \cite{jboss-drools} was used as rule-engine. 

Since only one computer for controlling the heaters is available in
\gls{PowerFlexhouse}, the client agents for each of the rooms were all executed
on that computer. Thus, that computer took the role of a home gateway. 

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{generated/heater-experiment-setup.pdf}
  \caption{Setup of Heater Controller Experiment}
  \label{fig:experiment1-setup}
\end{figure}


The environment for the local execution of rule bases consists of the following
three parts (compare this with the list of environment parts in
section~\ref{sec:parts-of-beh-desc-system}):

\begin{itemize}
  \item Measurements of frequency and dynamic power price
  \item Abstractions of the frequency control and price reaction services
  \item A service scheduler keeping track of the currently active service
\end{itemize}

\lstinputlisting[language=drools,float,
  basicstyle=\ttfamily\small,
  caption={Simplified Rule Base},
  label=lst:real-example-rulebase
]{listings/real-policy.drl}

Since the implementation is based on \gls{Java}, the environment of the rules
is also specified using \gls{Java} interfaces and classes. The measurements of
frequency and power are provided by a unified interface which can inject
these trigger signal values into the services. The services implement a common
interface which consists of several methods: 

\begin{itemize}
  \item A constructor to create an instance of the service with the required
    parameters
  \item A method to inject trigger signal values into the service
  \item Methods to start and stop the service; these are used by the service
    scheduler
  \item A method to evaluate whether a service should be active; only the first
    such service that is found in the rule base is actually made active
\end{itemize}

It is important to note the distinction between service activation and active
service: a client can receive several service activations in a behaviour
description, but when the behaviour description is executed, only one of these
services may be active at any given time.

One implementation issue was how to get service instances with the right
parameters into the rule base. The services require certain parameters to be
able to function; in the example case, both services accepted low and high
thresholds specifying the service deadband; additional required parameters are
references to the trigger signal objects and the heater control objects. The
problem lies in how these service instances are put into the environment of the rule
base. It would have been possible to send additional information about the
services along with the behaviour description, but this would have required to
transfer the service activations outside of the rule base.  The option that was
implemented adds an additional \texttt{setup} rule to the rule base that is
triggered exactly once. This extra rule contains code to initialise the
services and to register them in the environment. 

During the startup phase of a client agent, it would get access to the
available trigger signals, \texttt{SystemFrequency} and \texttt{PowerPrice}.
When a client received a behaviour description, it accepted it and started a
thread that was responsible for executing the rule base.
Listing~\ref{lst:real-example-rulebase} shows a cleaned up version of a rule
base that was actually used in the experiment. 

Executing the rule base for the first time triggers the \texttt{setup} rule,
which instantiates the service activations with the given threshold parameters,
trigger signal and heater control objects, and registers the service
activations with the rule base.

The rule-base thread executes the rulebase every second. Changes in system
frequency and power price are directly available to the services via the
trigger signal objects; the \texttt{shouldBeActive} method that is called in
the condition part of each rule compares the value of the appropriate trigger
signal with the thresholds given in the \texttt{setup} rule, and returns true
when the signal is outside the deadband. In that case, the action part of the
rule is executed, which instructs the service scheduler \texttt{sched} to mark
the service as the active one. This results in the start of a new thread
executing the service that should be active; a previously active service thread
is stopped before making a different service active. Active services respond
directly to changes in trigger signals; since services are treated as black
boxes, this is not part of the rule-base. The two rules \texttt{frequency-1}
and \texttt{price-2} exclude themselves mutually (this is accomplished with the
\texttt{activation-group} directive), so only the first of them that fires is
activated. Since the work of the services is hidden inside the black box of the
service activation implementation, the resulting rule base is very simple.


\subsection{Case Experiment}

The experimental setup of the example case controlled the eight rooms in
\gls{PowerFlexhouse} individually using behaviour descriptions. Each room was
controlled by its own client which offered maximally two services to the
system: a frequency control service, and a service that reacted to the dynamic
power price. The price reaction service was offered by all clients, but not all
offered the frequency control service. This was done to demonstrate that
behaviour descriptions can in fact be adapted to the capabilities of the
individual components. Both offered services are parametrised by high and low
thresholds. The services are kept simple on purpose, because it was not the
purpose of the experiment to demonstrate complex control algorithms.

A server program executing on a separate computer calculated the behaviour
descriptions for the single rooms in regular intervals, taking into account
which of the two services was supported by a client.  The thresholds for the
services were calculated in a manner to provide smooth reaction to changes in
frequency and power price.

The policies for the frequency service change every two hours: three different
widths of the frequency controller deadband are used. This was done to
demonstrate the flexibility of policies. This can be seen in the middle plot
of figure~\ref{fig:results-room6}.

The implementation of a sensible frequency control algorithm for the heaters
went through several steps, before an implementation was found that showed
acceptable behaviour in the experiment:

\label{sec:experiment-freq-algos}
\begin{enumerate}

  \item \emph{Change the thermostat setpoint when the frequency is
    outside the deadband}.  However, this algorithm proved to exhibit no
    discernible reaction to frequency changes, since the thermostat reaction
    is too slow to react to frequency changes.

  \item \emph{Switch heaters directly on and off}.
    The frequency control service switched heaters directly; the thermostatic
    control was only used when the frequency was inside the deadband, i.e.~for
    the price-based service.
    This time, the response to frequency changes was visible, but it was also
    too large, because the thermostat would only rarely get a chance to work
    towards a comfortable room temperature.

  \item A compromise between the two methods: \emph{Intermittent direct heater
    control}. The frequency control switched heaters directly, to have a timely
    response to frequency changes. To avoid large deviations from the target
    temperature, the frequency control service of each heater would only be
    active for a certain fraction of time, about 1.5 minutes every 15 minutes.
    The heaters supporting frequency control would alternate in providing the
    service.

\end{enumerate}

The major disadvantage of the chosen algorithm \#3 was that
the frequency response contribution from a
single heater was relatively small. Nevertheless, when many appliances are
controlled in this way, the accumulated frequency response can be considerable.

The chosen algorithm also needed the definition of a new service, since the
time slot allocated for the heater to provide frequency control had to be part
of the service activation.


\subsection{Observations}

\begin{figure}
  \centering
  \includegraphics[width=0.82\linewidth]{generated/graph2_powerconsumption.pdf}
  \caption{Overall Results}
  \label{fig:results-overall}
\end{figure}

The plots in figure~\ref{fig:results-overall} contain measured data from a
single, 10-hour run % DONTDO (time of the experiment (day/night, winter/autumn?))
of the experiment. During the experiment period, the power
price input signal decreases steadily with few and small upward excursions. The
system frequency input exhibits a slowly increasing trend, followed by a marked
drop towards the end of the period.

In the second subfigure, the overall power consumption of the building exhibits
a strong oscillatory pattern caused by the underlying thermostatic control of
the heaters. A correlation between power consumption and system frequency can
be observed. The price, on the other hand, does not appear to have a strong
impact on the consumption. This can be explained by the fact that the price
controller only acts as a secondary control loop, and is overridden whenever
the primary frequency controller is active. During the period of the
experiment, this was the case for most of the time.

The switching state of the individual heaters is shown in the bottom subplot.
Due to low outside temperatures during the experiment, some heaters can be seen
to be on almost all of the time.
% DONTDO PowerFlexhouse is badly insulated. Frequency control using normally
%insulated houses would need more heaters to be available. 

\begin{figure}
  \centering
  \includegraphics[width=0.87\linewidth]{generated/graph1_room6.pdf}
  \caption{Results for Room \#6}
  \label{fig:results-room6}
\end{figure}

Figure~\ref{fig:results-room6} details the behaviour of a single room during
the same experiment. In the first hours of the experiment, the temperature
setpoint (red) remains largely at 19\degree C due to the high power price and
low system frequency. The room temperature (black) follows the setpoint within
the hysteresis of the thermostatic control.

The second and third subplots show the relationship between the two input
signals - price and frequency - and the thresholds in effect according to the
active policy. The two-hour period for policy changes is easy to observe.

In hour 6 of the experiment, an increase in system frequency triggers the upper
frequency threshold. At nearly the same time, the power price begins to drop,
first below the upper, then below the lower threshold. This leads to an almost
constant temperature setpoint of 21\degree C for the remainder of the
experiment.

\subsection{Results}

This experiment demonstrated the feasibility to use behaviour descriptions in a
real, albeit very small, power system. It used behaviour descriptions that
activated several services, frequency control and a price-based service.
Conflicts were resolved using fixed priorities for each service.

Components could chose whether to provide the frequency control service, and
the supervisor respected that choice. Even though rather simple, this demonstrated
the flexibility of a control system based on behaviour descriptions.

The observations demonstrate that individual clients do react to the frequency
and power policies sent to them through the policy framework. 
In the experiment, the thermostatic control of the heaters and external
influences such as the outside temperature interfere with the effects caused by
the controller. 

%\todo{More content}

