%\documentclass{lni}
\documentclass[english]{lni}
\usepackage{color}
\IfFileExists{latin1.sty}{\usepackage{latin1}}{\usepackage{isolatin1}}
\usepackage{inputenc} % for acc\'ents
\usepackage{url}
\usepackage{graphicx}
\usepackage{wrapfig} % for  environment wrapfigure
%\usepackage{mdwlist}
\newenvironment{itemizex}{\begin{itemize}\setlength{\itemsep}{1pt}\setlength{\parskip}{0pt}\setlength{\parsep}{0pt}\setlength{\partopsep}{-5pt}\setlength{\topsep}{-15pt}}{\end{itemize}}
%\newenvironment{itemizex}{\begin{itemize*}}{\end{itemize*}}

\addtolength{\parskip }{0pt}
\newcommand{\cim}[1]{\emph{#1}}
\hyphenation{Control-Area-Generating-Unit}

%%%%%%%%%%
\author{Kai Heussen and Daniel Kullmann\\
Technical University of Denmark\\
Centre for Electric Technology ~~~~Ris\o \\
%Kgs. Lyngby Denmark\\
kh@elektro.dtu.dk ~~~~daku@risoe.dtu.dk}
\title{On the Potential of Functional Modeling Extensions to the CIM for Means-Ends Representation and Reasoning}

%%%%%%%%%%

\begin{document}
\maketitle

\begin{abstract}
%Engineering is the art of making complicated things work.
%There are few things an engineer can't do. Explaining his work to a computer may be one of them.

This paper introduces Functional Modeling with Multilevel Flow Models as an information modeling approach that explicitly relates the functions embedded in components of a system to their design objectives. It is suggested that a functional-modeling based extension of CIM may form a conceptual basis for the integration of DER (Distributed Energy Resources) with system operation and market concepts.
\end{abstract}


\section{Introduction}
The Common Information Model for Power Systems (CIM) \cite{CIM:2003} has been a major development and standardization effort by actors including the power industry, application developers and researchers. CIM is a published IEC Standard, described in the IEC-61968 and IEC-61970 series of standards, but the model is continuously extended \cite{Uslar:2010} by the members of the CIM User Group, which publishes new versions of the model from time to time. 


Opportunities and need for extensions to CIM has been pointed out in numerous publications which consider different use-aspects of the information modeled in CIM. Extensions to the CIM are explicitly allowed, either by contributing to the public version of the CIM User Group\footnote{\url{http://www.cimug.org}}, or by creating proprietary extensions to the model.


Recent research in the field of control in power systems has led to the insight that aggregation concepts are fundamental to the management and control of diverse unit portfolios (e.g.~\cite{Gehrke:2009}). Aggregators emerged also as new market actors that act as an interface between the services required for system operation (from a system perspective) and the technical operation capabilities present in the aggregator portfolios. 
%% daku: Even though such aggregators have been successful in offering particular types of services in a power market, such as demand shedding, their business models typically entail special agreements with utilities or and control is handled on a customer-by-customer basis, with a focus on large industry customers. 
The concept of virtual power plants extends the aggregation principle to include generalized automated services.

Standardization efforts in the power industry aiming at better (common) information models, including the CIM and also IEC 61850, take their point of departure in the technical structures and components that 'can be touched and measured'. The organization of these information models is based on physical structures of components and power system functions. In this context control is just a signal. In the modeling of business domains included in CIM, function-oriented representations of operations and components appear, for example in asset management and financial operations. These more abstract representations only model basic information relevant for the respective business processes. However, functional requirements specifying the context and purpose of a respective function, and its relation to specific actors and  components are hardly expressed.% -- however control and technical aspects of power system operation are hard to be found.}

The conceptual separation between the function that a device provides from the device represented as a component is a necessary abstraction for the control of a heterogeneous set of components such as different types of components, different revisions, or similar components from different suppliers. %(Functional modeling provides a functional view on a system by completely abstracting away the actual components.)

Recent work in the field of distributed power system control has pointed out the need for ``functional'' as opposed to  ``structural'' aggregation  concepts in the representation of power system operation \cite{Gehrke:2009}. 
Functional aggregation means that components that are different in one way, may be able to provide one and the same service, and thus appear as 'the same' from another perspective. Operational services, ancillary services, such as any active power regulation, are control services. Here, control governs the system state toward achievement of system operation objectives. 
%\footnote{``Functional'' here would have to be understood in terms of ``representation of functions with respect to control-purposes''. In terms of maintenance operations, of course it is quite ``functional'' to also identify the location of a device with respect to a spatial location.}
\footnote{The need for structural and functional aggregation concepts is often hard to convey. A  reason may be that it is easy for engineers to accept the conceptually fundamental difference of the language employed in power engineering and the language employed for (asset-)management -- but it is harder to acknowledge that the language for \emph{components} and that for \emph{control of components} ought to be distinguished as well.} 

Power system operation as it is today relies on the careful design and tuning of the controls governing the behavior of generation units. 
Future power system operations, supported by a much larger diversity of co-acting devices, will require as clear a specification of dynamic requirements on the aggregate service level as it has been implicit in the design of power plants. 
As ancillary services are always linked to the structure of a power system, they also depen on the power system's non-active functions (e.g. transmission constraints, inertia). A functional representation of a control system thus needs to include a reference to the passive functions of the system-in-view. 

Finally, also additional (possibly unanticipated) control services may be introduced in the future. For example, motivated by the interest to avoid overcapacities, localized control schemes could utilize local controllable resources to reduce transformer overloading. 


% These passive components act as the necessary buffer between the active components.
%, operation objectives and the functional realization need to be governed 
%The concurrent operation of large numbers of diverse devices toward common control objectives requires an effective coordination of control actions. The specification of a large technical systems requires that its 

% von Daniel
%Auf der einen Seite ist es oft sehr schwierig, die Dinge, die uns mit unserer Intelligenz so leichtfallen, von Computern machen zu lassen, z.B. indirekte Verbindungen zu erfassen oder zielgerichtet zu planen. Andererseits ist diese "neue" KI oft auch nicht f�r uns einsehbar, weil wir die Gr�nde f�r Entscheidungen nicht nachvollziehen k�nnen.

%Ich finde die ``alte'' KI interessant, weil ich im Prinzip nachvollziehen kann, wie der Computer zu seiner L�sung kommt, indem ich die Schritte, die er gemacht hat, nachvollziehe. Das macht es f�r Ingenieure m�glich, ein System zu evaluieren; ein Blackbox-System enth�lt immer die Gefahr, dass es in Randbedingungen vollkommen versagt.

Functional modeling enables a means-ends representation of the system in view. Beyond representation, it makes possible to relate system states to operational goals, and a structured generic representation can serve as a foundation for reasoning about this relation. Multilevel Flow Modeling (MFM) is such a representation which has been applied to a number of technical processes. 

It is the goal of this paper to motivate functional modeling in context of the Common Information Model and to convey how a means-ends approach may help preparing CIM for the requirements of a large-scale integration of distributed energy resources.


\subsection{Motivational Example}
Consider the representation of a tap changer device within CIM. A tap changer regulates the voltage on the low-voltage side of a transformer by adjusting the winding-ratio of the transformer. Its purpose is thus typically to support the lower voltage system in maintaining a constant voltage in spite of changes on the higher-voltage (transmission) level.

In CIM, the \cim{TapChanger} class is part of the package \cim{Wires}. To specify its context, the description of the control goal is provided by \cim{RegulatingControl} (package \cim{Wires}), and its connection to the \cim{PowerTransformer} is modeled through its association to the \cim{TransformerWinding} class, which also provides the relation to the \cim{VoltageLevel} class (via \cim{BaseVoltage}). \cim{VoltageLevel} ties the components together that live on the same voltage.

Contextual information that is not specified within CIM includes the purpose of having a tap changer. Further information not specified includes:

\begin{itemizex}
  \item Overall Goal: Stabilization of Distribution Voltage levels
  \item Control Objective: Maintain (lower voltage) within specified boundaries 
  \item Control Performance: If objective missed, correction within $t$ seconds. 
  \item Control function: Actuate tap changer $n$ up/down if (observed) voltage is low/high. 
%% daku:  \item Possible Failure Mode: for voltage-sags exceeding the power transfer capability of the feeding lines, the tap changer may cause voltage instability by keeping the load up due to raising the lower level voltage.
  \item Possible Failure Modes
  \item Further Assumptions: 
\begin{itemizex}
  \item LowerVoltage = WindingRatio $*$ TapRating $*$ HighVoltage 
  \item Lower voltage level is the uncontrolled voltage: demand and distributed generation, ... 
  \item Higher voltage level is the controlled voltage
\end{itemizex}
\end{itemizex}

These items may be obvious for a power system engineer, because that is simply what a tap changer does and the reason for having a tap changer. This type of information has thus been considered irrelevant for the simple reason that all relevant functions match a component and all components are known if its parametrization of a load-flow simulation is complete. Reverse-engineering of power system controls is easy as long as none of the basic concepts will be modified. 

As it is expected for the ``smart grid'', the variety of devices along with the variety of controls that will establish the future systems will increase. If a components function is not already defined by its existence, then a functional description layer may be necessary.
%% daku:
Additionally, functional descriptions can be helpful when different components have to work together: The components can exchange information about their purpose and usage.


%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Functional Modeling with Multilevel Flow Models}
\label{sec:method}

MFM is a functional modeling methodology that provides a library of control functions, energy- or mass-flow-functions and relations, depicted in Figure \ref{concepts}, that can be interconnected to a multi-level representation of causality and intention in flow processes \cite{Lind:2010}. Adding to the former variety of applications in process engineering, nuclear power plants and others, the field power systems has been developed recently \cite{Heussen:2009a, Heussen:2009b}. An MFM model enables situation-dependent reasoning about control situations, by relating system states to system and control objectives.

Applications of MFM include model based situation assessment and decision support for control room operators, hazop analysis \cite{RossingLindJensenJorgensen:2008}, alarm design, alarm filtering \cite{Larsson:1996} and planning of control actions \cite{Larsen:1993b}, \cite{GofukuTanaka:1997a}. 
It has been used for knowledge representation in AI planning for supervisory control systems \cite{DeSouzaVeloso:1996b}. 

Altogether MFM provides a rich ontology for modeling purpose-aspects of complex processes. MFM is supported by knowledge-based tools for model building and reasoning: a graphical modeling environment and a rule-based reasoning environment with graphical user interface.% (referred to as MFM Workbench in the following).
\begin{figure}[htb]
  \begin{center}
		\includegraphics[scale=0.8]{MFMConcepts3x3}
    \caption{\label{concepts}MFM concepts, entities and relations.}
  \end{center}
\end{figure}

\subsection{Means, ends and Functional Modeling} 
%ORIGIN:(MEANS AND ENDS IN SCENARIOS)

Overall goals, process objectives and the realization of the process in components and their behavior form a common dimension of ends and means (illustrated in Fig. \ref{fig:means-ends}). 

Consider a control action: \emph{In order to} save the power line from overloading, the relay is programmed to open its breaker. 
%% daku: \emph{In order to} keep the system frequency at 50Hz, power system frequency control observes the frequency and alters the generators' power input. %The frequency is kept at 50 Hz \emph{in order to} balance the power infeed. 

For all control actions: the intention to alter the state of a system is realized \emph{by} means of observing it and manipulating it.
Every control action entails concepts of the means-ends dimension.

%But how do you relate goals to the specifics of an implementation? This basic idea has been around for a long time and it is well-know that the problem of goal-decomposition is a non-trivial activity (e.g.~[Cameron et al 2005, Cognitive Work analysis, Abstraction Hierarchy] ...). One of the challenges is that goals are partly formulated as organizational character and partly on a process perspective - and that this decomposition is ``not necessarily unique since it depends on the modeller's understanding of the functions in the system under study'' (Cameron et al 2005). 

In power systems, the overall goal of reliable operation is decomposed into a number of control objectives such as power-balance (frequency stability), optimal transmission operation (voltage stability, reactive power management), etc. This decomposition of control objectives cannot be derived directly from the overall objective, but it is rooted in the engineering principles and properties of the involved electromechanical process. However, the decomposition can be understood on the basis of a domain model at the right level of abstraction (i.e.~a simple model). In the case of frequency control, for example, it is irrelevant to speak about voltage - the basic mechanisms are entirely based on a mechanical power balance \cite{Heussen:2009a,Heussen:2009b}. 

%% daku: alternative to the figure below, text flows around it
\begin{wrapfigure}{r}{0.45\textwidth}
 \vspace{-15pt}
 \begin{center}
	\includegraphics[width=0.40\textwidth]{means-end-gr.png}%{means-end.pdf}
 \end{center}
 \vspace{-15pt}
 \caption{The means-ends dimension.}\label{fig:means-ends}
\end{wrapfigure}

%\begin{figure}
% \begin{center}
%	\includegraphics[scale=0.5]{means-end-gr.png}%{means-end.pdf}
% \end{center}
% \caption{The means-ends dimension.}\label{fig:means-ends}
%\end{figure}

Functional modeling provides context to the goals, by relating to objectives and functions, by introducing intermediate levels of abstraction along the means-ends dimension\footnote{This abstraction is also normal in control design: Models for control should be as coarse as possible and only as detailed as necessary.}. %representing objectives and functions, thus 
Functional modeling is thus the modeling of activities (behavior) in relation to their purpose, and the context of the activity. 


%The word \emph{function} can have several different meanings, including the mathematical concept of function, which is not considered here. 
%%Function or functional is most commonly employed to attribute a useful property to a thing. Often those functions are observed as derived from the properties of a thing in its context. 
%A stone may have the function of keeping papers on the ground, or the function of being a weapon, depending on its use. These functions are not inherent in the properties of the stone, but they are attributes of its use (possibly related to the specific set of properties of the stone, yet not by the stone, but by an external purpose). As functions are attributed to things, their origin is external to the things but related to the purpose of their use. 
%

\subsection{Modeling of Control in MFM}

%... what is meant by ``causal system'' ? -- behavioural functions / flow structures / behavioural causation / causal roles?
%... what is meant by ``intentional systems''  -- intentional causation (action theory, basic changes -- basic actions) / intentional roles / action roles?

%An interconnection of flow-functions as a whole represents the behaviour of a process as intended by design.
% We can describe the behaviour of such a system as a result of the interplay between the systems parts. 
%
%The concept of causality Such a system is 'causal' in the classic sense as well as in the sense of control theory. 
%Here, the relation between the system behaviour and the fulfillment of its objectives is that of 'fit by design'.
%In contrast, for an intentional system, behaviour of the system is \emph{directed toward} fulfillment of an objective. A change of the objective will change the behaviour of an intentional system, whereas it could not change the behaviour of a causal system.

%% daku: A representation of control systems based on action theory has been added more recently to MFM\cite{Lind:2004b,Lind:2005a,Lind:2010}. The four elementary control functions, which are based on elementary action types, are found in Figure \ref{concepts}. 

%In the design of processes, it is often the case that 'intentional subsystems' are employed in order to modify the behaviour of a process. 

In contrast to the classical signals and systems perspective, control functions have a special role in the perspective of mean-ends modeling: Whereas a 'flow-structure' is a functional abstraction of a process, the 'control-structure' is a representation of the \emph{intentional structure} realized by a control system\footnote{In the control literature, the 'intentional system' is sometimes referred to as 'active' structure, whereas the the controlled system, here '(multi-level) flow-structure', is referred to as the 'passive' basis. This wording does not apply exactly for multilevel-flow-structures, as energy sources and sinks may well be part of the system.}. 
%(example: decoupling the rate of flow from a water tank from it's water level by controlling an outflow valve. )
This distinction becomes essential when reasoning about control systems, because the dominating principle in control structures is intention,  not causality.

\begin{figure}
 \begin{center}
		\includegraphics[scale=1.1]{roles-ex1.png}\hspace{2cm}
		\includegraphics[scale=1.1]{roles3x1.png}		
	\caption{Example MFM Model with energy flow-structure and control structure }
	\label{fig:roles-ex0}
	  \end{center}
\end{figure}

An example model of a control structure and a related flow-structure is given in Figure \ref{fig:roles-ex0}. It models a stereotypical balancing process, where both the energy-source on the left and the energy sink on the right influence the storage-level. Roles have been attached to some flow-functions indicating that the state of the respective function is determined or influenced by an external agent: The sink state is considered varying, corresponding to an uncontrollable load; the source-state is modeled as constant (e.g. a constant pressure) and the transport is actuated to influence the energy-flow from the source (as a valve would influence the steam-flow). The model conists of several ``components'' working together:
%	In this example, the process is balanced by means of a control which aims at maintaining the storage-level by means of influencing the energy transport from the source.
\begin{itemizex}
		\item \emph{Control-objective} \textbf{obj68} and \emph{control function} \textbf{mco70} are encapsulated in a \emph{control structure} \textbf{cfs73}.
	  \item Requirements to the performance of the control are formulated as an objective associated with the control structure (\emph{performance objective}, \textbf{obj74}).
		\item The control objective is associated via a \emph{means-objective-relation} (\emph{maintain}) with the \emph{mainfunction} (here \textbf{sto58}), the state of the mainfunction is subject of control.
		\item The control function is connected to the flow-structure via an \emph{actuation-relation}, \textbf{ac72}, targeting \textbf{tra57}. 
\end{itemizex}

In \cite{Heussen:2009a, Heussen:2009b} the authors have shown how this modeling of control can be applied to power systems.
In contrast to other modeling approaches, the explicit process-decomposition enables the modeling at different levels of abstraction.


\subsection{UML Diagram of MFM entities and relations}

For the implementation of MFM based applications a class hierarchy has been developed that may be represented in a UML diagram. Figure \ref{uml} presents a mock-up UML diagram of the MFM class concept. Both relations and entities follow strict connection rules. For simplicity of the UML diagram, not all MFM classes have been displayed, but only those which define the central structure of it. Also, for the sake of overview, the notation is not following exact UML notation, as association-classes are listed on the right, only indicating which classes they ought to be connecting.

\begin{figure}[htb]
  \begin{center}
    \includegraphics[width=1.0\textwidth]{MFM-UML-Association.png}
    \caption{\label{uml}Sketch of MFM concepts in UML notation}
%% daku: . Note that the there is a class hierarchy for relations on the right-hand side, the leaves of which are interpreted as association-classes (\emph{italic}). For the sake of overview, the classes connected by an association are only indicated by name.
  \end{center}
\end{figure}


%\subsection{MFM Applications}
%
%This model can be employed for a number of purposes relating to to supervisory control and control design.
%By relating the states of functions to neighbouring functions, control means and control objectives, MFM models enable situation-dependent dynamic reasoning about control situations.
%
%%
%%Applications of MFM include model based situation assessment
%%and decision support for control room operators [18],
%%hazop analysis [19], alarm design [20], alarm filtering [21]
%%and planning of control actions [22], [23]. It has been used
%%for knowledge representation in AI planning for supervisory
%%control systems [24]. MFM is supported by knowledge based
%%tools for model building and reasoning [25].
%%
%%Altogether MFM provides a rich ontology for modeling
%%purpose-aspects of complex processes. %MFM is supported by a graphical modeling environment (implemented on MSVisio) and a Jess-based reasoning environment.
%%
%
%Two example types of reasoning applications are: 
%\begin{enumerate}
%	\item{\textbf{MFM-based state identification:}}
%For root-cause analyis, function-states are discretized into normal and abnormal (high/low) states. An observed ``abnormal'' state will trigger the causal reasoning system, which then will generate possible causal explanations (root-causes), by matching functional information with observed data.
%The same reasoning framework could be used for mapping out consequences and thus for counter-action planning.
%%
%\item{\textbf{Causal Reasoning for Control-influence:}}
%%Task/procedure-formulation!
%If dynamic control functions are part of the system-in view, the overall system-state can be evaluated directly with reference to the control-objective which is to be achieved.
%This corresponds to performance monitoring of a control loop. %In Fault-Tolerant Control, in contrast, system-faults are also found with respect to the system-structure (by testing residual equations), which would be analog to the flow-strucutre-oriented root-cause analysis.}. 
%Using reasoning about causal influence, functions with the ability to influence the achievement of a control objective can be identified within the flow-structure, which may support the identification of control opportunities.
%
%\end{enumerate}
%
%

%%%%%%%%%%%%%%%%%%%%%%%%%

%Functional modeling is an approach used, amongst others, to map high-level requirements into more concrete technical objectives. Here, functions are seen as purposeful actions that can be attributed to devices or systems. 

%%%%%%%%%%%%%%%%%%%%%%




%Originating from the context of abstraction hierarchy and cognitive system modeling for human-machine systems, some key characteristics of MFM are: 
 
 %- whole-part and means-ends abstraction. which the multi-level nature: the explicit representation of means-end-relations between functions,  flow-structures and objectives, 
 
% - modeling of flow-processes (energy flow, material flow), by abstract flow-functions, modeling stereotypical flow-system functions: transportation and storage, balance and redistribution of flow, as well as system-boundaries by flow-sinks and flow-sources.
 
 %- means-end and causal relations that connect functions, flow-structures and objectives qualify special types of connections
 
  %- all elements of MFM are classified by a type-hierarchy
  
%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Toward Modeling of Control in CIM}
The modeling language MFM has been introduced above and special emphasis has been given on its capacity to represent control-related functions and roles. Even though the UML diagram shows the classes by which information is modeled in MFM, its strength is the consistent means-ends and whole part representation, which connects information to its proper context and abstraction level. 

In this section we explore the implications of this deeper understanding of control for the representation of control in CIM.


%%%%%%%%%%%%%%%

%[ The representation of control in CIM within the Measurements package is based on the severe misunderstanding that control is only one category in signals-based communication. 
%]
%\subsection{A Review of Control Modeling in CIM}
\input{control-in-cim.tex}

%%%%%%%%%%%%%%%
\subsection{Drawing Inspiration for Functional Modeling in CIM}

The means-end perspective that is so essential in MFM exists only implicitly in the CIM, such as in \cim{ControlAreaOperator} (representing the end) being interconnected to \cim{ControlAreaGeneratingUnit} (the means). 

Here is where the functional information becomes essential. The modeling of ancillary services is based on assumptions of control services that can be provided by a power plant. Typically today, these services and their descriptions are described in terms of conventional generating units. However, it has been shown in practice, that the same regulating services (functions) may also be offered by other types of devices.
It also should be considered that the relation of control functions to devices is that of many-to-many: The same device may provide a number of control functions (or ``services'')  and one control function may be performed by a number of units.

This type of information is both related to ``physical'' and engineering concepts and to the world of market operations and service descriptions. The introduction of an intermediary modeling layer based on functional modeling concepts enables the representation of these, possibly indirect, linkages between components, aggregators, and the services offered for power system operation.

Some elementary concepts that would form this layer can be found in MFM: Means-ends perspective, linking goals and objectives to the technical functions that enable their achievement; the modeling of control structures and flow-structures in multi-level relations as a representation of the interactions between intentional and causal functions. Further, the modeling of control functions according to MFM emphasizes the need for representing both Control Objective and Performance Objectives, the linkage of Control functions to the actuated system, including reference to the ``counter-agent'': origin of anticipated disturbances. 

The basic (flow) function types that MFM provides may be considered as specific states of connectedness,  implicitly giving in the (structural) classes that CIM provides. To list some examples, \cim{ACLineSegment} can be assumed to provide some Transport function, a \cim{GeneratingUnit} can be a Source of power, while a Sink function can be provided by \cim{EnergyConsumer} or one of the different Load classes. Still, the level of abstraction in CIM is different from the one in MFM. 

%In fact, the design of control function precedes the design of communication systems, because control would impose requirements to communication. communication is a means of control.


\section{Example Cases}

The following examples attempt to illustrate the type of information to be modeled. The first example is for an existing component, and the second example is for a hypothetical future scenario.

\subsection{Governor Control }

Frequency control in power systems is a typical example of decentralized control of large systems. The tuning of the individual controllers depends on the overall system context, beyond the individual generator.

In CIM we find the following representations: \cim{AncillaryService} (package \cim{Reservation}), and \cim{Control} (package \cim{Meas}). Further, CIM provides the context: \cim{ControlAreaGeneratingUnit} (package \cim{ControlArea}); \cim{GeneratingUnit} (package \cim{Generation::Production}); and \cim{ControlAreaOperator} (package \cim{Financial}).

Generation control provides frequency control, it should thus be represented in the Generation or Generation Dynamics packages. 
At the same time, frequency control of different types is an ancillary service (or divided into several services) to be contracted. As such it is important to specify performance requirements that define the quality of the service to be provided from a top-down perspective (system operator).

An MFM Model of frequency regulation including a representation of control areas has been presented in \cite{Heussen:2009b} including an objective decomposition. Figure \ref{freq-mfm} shows how the overall perspective would be represented in MFM. The goal decomposition shows how the objective of overall power balancing is related to frequency control. 

\begin{figure}
 \begin{center}
		\includegraphics[scale=0.85]{frq_ctrl_obj_hierarchy.png}
		\includegraphics[scale=0.75]{freq_prim_sec_areas-corrected.png}
	\caption{Left: Goal decomposition of frequency control. Right: Control hierarchy and flow structure of the system balancing with control areas. \cite{Heussen:2009b}}
	\label{freq-mfm}
	  \end{center}
\end{figure}

% TODO Regarding Figure \ref{freq-mfm}: The objectives O2,Ax and O3,Ax don't show up on the left-hand side

%Here it can be observed that CIM does not (yet) support power system entities that are not classical generating units to participate in area and frequency control. 
% TODO (what entities are missing?)



\input{example-overloading.tex}

%\section{Causal Reasoning in MFM}
%
%Apart from the ability to provide a compact template for the modeling of control functions MFM allows the modeling of causal relations between process functions. 
%
%...
%
%On the basis of the functional information, it is possible to identify failure of proper operation as well as relating this failure to possible root-causes: a) external voltage deviation in High-voltage or low-voltage level causing saturation of tap changer b) internal failure of tap changer.
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\subsection{Causal Reasoning example}
%
%In this example we model the main control loops of a thermal power plant supplying a varying electrical load in island mode3. The modeled process is illustrated in Figure \ref{plant-mfm}. The power plant model is simplified by assuming a fixed cooling- and smoke-power loss. % TODO mode3: what is that?
%
%An MFM model of the process is presented in Figure \ref{plant-mfm}. The model comprises two flow structures, modeling the process at the relevant abstraction level, and two control structures representing the main control loop objectives of the power plant: fresh-steam pressure setpoint and frequency control (to encapsulate the disturbance of varying power demand). 
%
%\begin{figure}
% \begin{center}
%		\includegraphics[scale=0.6]{power-plant-reduced.png}
%		\includegraphics[scale=0.8]{power-plant-MFM-2-high-arrows.png}
%	\caption{MFM model for the main control loops of the thermal power plant (taken from \cite{Heussen:2010}). Please note that the green bubbles contain a reference to a related entity. The boiler-feedwater control-loop displayed in Figure 8 is not modeled as a control loop but function for the system, illustrating the consideration of abstraction levels introduced in Section III-A. The functions bal2 and tra3 and their causal relations, capture the effect that always as much water is pumped into the boiler as is being evaporated. The wide arrows in the background illustrate
%the causal paths for obj76 from the actuator- and disturbant-agents.}
%	\label{plant-mfm}
%	  \end{center}
%\end{figure}
%
%\emph{Controllability Analysis:} 
%The causal-path analysis (see also arrows in Figure \ref{plant-mfm}) reveals that load variation, the disturbant on sin17 does influence the state of sto50 (kinetic energy, frequency), but not the state of sto31 (steam pressure). The intended behavior of the power plant, is to adjust its energy conversion setpoint (eventually, the fuel supply, sou14) according to the load-demand, a demand-driven process which requires upstreampropagation of information. 
%
%This upstream propagation is provided by the two interleaved control structures with control objectives obj76 and obj77 aiming to determine the energy states of sto50 and sto31. By manipulation of the mass-flow of tra6 and the supplied energyflow of sou14 the control functions effect the process on its upstream end.
%
%%\emph{CIM component:} ThermalGeneratingUnit (package Generation::Production)
%
%\subsection{High abstraction network example}
%
%This example of an AC power network connected with an HVDC link to another AC system (Figure \ref{network-mfm}) is presented to illustrate the value of a modeling tool that can model systems in terms of their causal interconnection. The flow across the HVDC line power can be controlled and may thus play a role in the overall control architecture.
%
%\begin{figure}
% \begin{center}
%		\includegraphics[scale=0.6]{grid-2-area-OLD.png}
%		\includegraphics[scale=0.8]{grid-2-area.png}
%	\caption{One line diagram and MFM model, illustrating the network modeled in Example 4 (taken from \cite{Heussen:2010}). Note the combination of two AC networks (synchronous areas) with a DC link. In the MFM model, mainfunctions associated with the control objectives are marked in red.}
%	\label{network-mfm}
%	  \end{center}
%\end{figure}

% Removed:
%CIM components: 
%\begin{itemizex}
%\item GeneratingUnit (package Generation::Production)
%\item Load (package LoadModel) \emph{or} EnergyConsumer (package Wires)
%\item ACLineSegment, DCLineSegment (package Wires)
%\item Terminal (package Core) for connections
%\end{itemizex}


\section{Conclusion}
The intention of this paper was to demonstrate the functional modeling approach and its value for representing power system operations within information models. It has been shown that on the basis of a generic means-ends framework, control objectives and control means can be modeled in a common framework. The modeling is meaningful with respect to representation of control functions, especially for cases in which the realization of a control function itself has to be considered a black box. %In such cases it is still necessary to identify control objectives, control means and performance requirements in order to maintain interoperability of control functions. 

A direction toward integrating a functional representation layer with the Common Information Model has been presented and some motivating examples have been given to highlight potential benefits. 
A functional representation within CIM, introduced as an intermediary layer between the representation of business processes and the current structural modeling approach in IEC 61970-301 would induce a number of benefits, starting from asking the right kind of questions to qualify for control functions to enabling the design and inclusion of new types of control policies which may or may not be anticipated at this time.

As this paper is a first attempt to identify a connection between previously unrelated domains of research, much ground has been left open for further elaboration and discussion. Also, MFM-based reasoning concepts have been omitted in this review. 

\vspace{-5pt}
\section{Acknowledgment}
The functional modeling with MFM has been and is being developed by Morten Lind and his collaborators. %The author would like to thank  Daniel Kullmann for continuous reflections about functional modeling in the context of UML, CIM, possible applications in the Energy-IT context, and on this specific paper.

\vspace{-5pt}
%%%%%%%%%%%%%%%%%%%%%%%
\bibliography{MfmWork,OwnWork,OwnBooks,CentralKH,cim}


\end{document}



