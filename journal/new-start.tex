\todo{Increasing penetration of DER/RES. Need to control large numbers of
components.  Control system needs to be scalable, reliable, and cheap.}

Today's power systems experience an increasing penetration of renewable energy
sources (RES), due to the limited amount of fossil fuels and ecological
concerns.  However, the production of RES is inherently fluctuating.  It is
widely acknowledged that the active inclusion of the demand side into the
control system of the power system (from here on, this will be just called
"control system") will help to alleviate this problem (cite). This includes
industrial and commercial as well as domestic consumption.

Communication is central to control systems, especially so in distributed
systems. However, in many control schemes, communication is taken for granted, and
assumed to be cheap \emph{and} reliable; an example for this are closed-loop
control schemes, which rely on fast response times and high reliability. This
false assumption has been
captured in the "Eight Fallacies of Distributed Computing", which have been
formulated by Peter Deutsch \cite{deutsch-8-fallacies}; The first three
fallacies are: "The network is reliable", "Latency is zero", and "Bandwidth is
infinite".  A control system must be able to deal with the reality of imperfect
computer networks.

The assumed control structure for this article, without loss of generality, is
a redundant
hierarchical control scheme. This means that (at least some) components
connected to the power grid belong to a supervisory controller that is
responsible for telling the component how to behave. The need for a system like
this arises from the need to provide critical system services, ancillary
services, in the power grid, which is impossible to do without some
coordination happening.  The goal is to build a control system that does the
necessary amount\footnote{\todo{how do you measure that?}} of coordination for
providing the critical system services in a reliable manner.

\reduwave{Modern Smart Grid technologies (e.g.~VPPs \todo{cite}) still work in
the measurement/setpoint mindset, probably because control systems have always
worked like this}. However, this concept of getting measurements and sending
setpoints limits the flexibility of control communication in power systems,
especially in the context of the emerging Smart Grid: New capabilities of
components need to be supported by the communication protocol, otherwise they
can't be activated by the supervisory controller. When these capabilities are
activated using setpoints, however, the whole communication standard has to be
changed for each change in the capabilites of components. This is not flexible.
A communication protocol that enables new capabilities to be easily added, or
existing capabilities to be easily extended, would help to make the control
system more flexible. \todo{There are different types of flexibility; what this
paragraph is about is actually extensibility}

\section{Using service as a central concept}

In power system operations, the concept of \emph{ancillary service} has been
used for a long time. The concept of service is central to power
systems: not only ancillary services need to be provided, but also the primary
services power generation and power consumption. However, communication in
power systems is still mostly done using the setpoint/measurement model.

Some work is already underway to define tasks done by power system components
using the \emph{service} metaphor. An example for this is the work on a "Smart
Inverter" standard that is currently in progress \cite{SmartInverters10}.  This
shows that researchers are beginning to understand that it makes sense to use
the \emph{service} idea as a central concept in power systems. This article
shows that a \emph{service} model is important for creating the control
infrastructure of the future Smart Grid.

The setpoint/measurement model has some shortcomings in comparison to a
service-based model of communication:

\begin{itemize}

  \item A Service has a definition of what it does associated with it, coming
    from authoritative sources like operational manuals of grid operators 
    or (future) standards.  A
    setpoint is in principle just a number, with a much weaker coupling to a
    meaning.

    The semantics of a value become important when different systems from
    different manufacturers have to communicate with each other: A value of 5
    alone does not contain much information, but if you have additional
    information, like "droop value in percent of rated power", it is much more
    difficult to get problems because of misunderstandings or
    misinterpretations.

  \item Communication of data without semantics (metadata) has long been the
    standard for communication, not only in power systems. The development of
    distributed control systems, however, makes this communication by
    convention more difficult, because more and more parties are involved in
    the communication, and the probability of one misinterpreting a particular
    value increases. Service-based communicatrion, on the other hand, puts
    strong emphasis on the semantics of transferred data.
    
  \item Setpoints are usually tied to a specific device. A certain
    controller will define a set of setpoints, each being identified by a
    number (e.g.~the modbus interface of the DEIF AGC2 controller defines
    setpoint 4101 as "frequency controller deadband"). A service, on the other
    hand, uses names instead of numbers, which makes errors less likely.

  \item Setpoints and measurements are often very device-specific, and similar
    capabilities of components can be addressed in various different ways.
    Using services, this problem is mitigated, because the service specifies
    exactly what parameters it takes, what unit they are in, and what they mean.

\end{itemize}

For supporting a service-based communication system, a meta-standard for
specifying services is very useful. This is because new services can be added
without having to change the communication standard used; the only requirement
is that the new service adhere to the meta-standard, so that information about
the service can be exchanged using that standard.  \todo{move outside the
list?}

The concept of \emph{service} can be extended to not only include ancillary
services, but also other services, such as the aforementioned power generation
or power consumption services. In the context of demand response, specialised
types of consumption services are feasible, e.g.~the different types of
timeshifted consumption that can be provided by various types of loads.

The concept of a power system service, as opposed to the concept of "just" a
communication service, such as provided by a web service, can help to look
differently at how control communication can be done in power systems.

%\todo{Put into the right place: } What services a component can provide
%depends on the capabilities of the component, e.g.~on whether it can control
%active or reactive power output, and the internal trigger events it can use to
%control the capabilities, e.g.~a frequency measurement device would enable the 
%activation of a frequency control service.

Current software development practice puts much emphasis on the modularisation
of software systems. This means that software systems are split into modules
that each have clearly defined responsibilities and dependencies. This makes
the maintenance of software systems, especially large ones, easier: First, it
makes it easier to understand those software systems, because single modules
can be inspected separately; second, it makes testing easier, because single
modules can be tested (without modularisation, the whole system has to be
tested, which makes the localisation of error \todo{reasons} more difficult);
third, it makes it easier to reuse parts of the source code for other purposes,
because the dependencies between modules are clearly marked. 
\todo{find cite for modularisation. e.g.~OSGi}

This emphasis on modularisation can also be observed by the emergence of
Service-Oriented Architectures (SOA) \cite{soa-rm}. This means that the
functionality of modules is exposed as services, usually web services.  By
doing this, services that depend on each other are clearly separated by the
containers that provide the communication protocol, which is HTTP in the case
of web services. \todo{protocol vs. message content.}

\todo{Apply existing design principles, e.g.~from ICT, to power systems.
E.g.~splitting up the communication into two parts: how to talk to each other,
and what to talk about. And making a system modular, where it is easy to add
new pieces of functionality. Reference other work!}

\todo{Syntax vs. semantics? protocol vs. content of protocol.}

\todo{Find the right place:} Service-Oriented Architectures (SOA), which is a
big topic in software development practice right now, has also been proposed for
communication in power systems, e.g. for data exchange. It is important to
understand that the term \emph{service} in the context of SOA means an ICT
service, e.g.~the implementation of a certain typoe of data exchange. The
authors of this article could not find a reference in current research that
explicitly mention power system services as the \emph{service} in SOA.  



Replacing a message containing a setpoint with a message containing the
activation of a service is just the first step in the direction of facilitating
a service concept for power system control communication. Messages can also
contain multiple services to be activated. In that case, special care must be
taken to prevent the activation of conflicting services at the same time. An
example for conflicting services is two services that both control the active
power output of a component, but with different goals, such as a frequency
control service and a cost optimizing service (dynamic power prices are
currently a subject of research).  Thus, a system for selecting one of the
conflicting services to be actually provided is necessary. This disambiguation
mechanism, as it is called here, can be done by simply ordering the supplied
services by importance, e.g.~a frequency control service will take precedence
over a cost optimizing service. The disambiguation mechanism could however also
be provided by some logic which is also part of the message containing the
services; this enables the very flexible combination of different services.

\reduwave{Now, the question of why to use such a system must be answered.} The overhead of
using services instead of simple setpoints must have some tangible benefits to
be of use. \todo{Reword?}

\begin{itemize}

  \item Autonomy: The Smart Grid idea envisions a future where a large number
    of components is actively participating in the control of power systems.
    For this to work, a centralized approach is hardly appropriate (does not
    scale to millions of components, single points of failure). This implies a
    need for greater autonomy at the component level. Services provide that
    autonomy, because the execution of a service can happen mostly independent
    of the supervisory controller that ordered the component to execute that
    service.
  
  \item Flexibility: the flexibility to respond in different ways to events
    that occur in power systems is an important feature that a future
    communication infrastructure for the Smart Grid should provide. The authors
    see two major types of flexibility: 1) the flexibility to react in
    different ways to the same signal (e.g.~a frequency measurement or a power
    price); 2) the flexibility to react to multiple signals (e.g.~to react to
    frequency when it is too low or too high, and to react to power price when
    the frequency is within reasonable limits). When multiple services are
    activated, and a proper disambiguation strategy is in place, both types of
    felxibility are supported.

  \item Less reliance on reliable communication links: control architectures
    presented in current research silently assume that communication is
    reliable. This is, however, not completely true, especially when existing
    communication links, such as broad-band Internet connections, are used for
    cost-efficiency reasons. Communication links can fail completely for a
    limited time, but they can also suffer from reduced performance,
    i.e.~increased latency or decreased bandwidth. Especially increased latency
    poses a significant problem to classical closed-loop control schemes,
    because those depend on fast measurements and fast response times. Services
    mitigate the effects of unreliable communication links, because they make
    it possible to move the entire closed-loop control logic to the local
    controller of a power system component. The broadband Internet connection
    would then be only used to transmit the parameters for the used algorithm.

  \item \todo{split up into message containing services, and triggers
    activating services: is this a direct benefit, or should it be rather put
    into the main text?}

  \item Extensibility: The envisioned service-based communication is based on
    the concept of defined services. Services will be defined in multiple
    standards, and components can support services that come from multiple
    standards. A common service ontology is used by the communication parties
    to exchnage information about services that the components can provide
    (service descriptions) and the actual services that should be provided by
    the component (service activations). Such a system of a meta-procol for
    communicating about services enables the seamless integration of new
    services into the whole system.

  \item Reliablity: More autonomy of the individual components, paired with
    reliability against communication failures, make this system more reliable
    than when using the traditional centralized control approach.

  \item Scalability: In the end, all the advantages listed here will lead to
    greater scalability of the control system.

  \item Cost-efficiency: This comes mainly from one factor: the ability to use
    existing communication infrastructure, instead of having to build
    dedicated, more reliable communication links. This is \todo{word:
    contrasted/opposed} by the need to have a fairly complex controller at each
    component. However, such a controller will very likely exist anyway,
    e.g.~in the form of a house gateway at each house.

\end{itemize}

\subsection{A Service Concept}

The ability to use services in the most flexible way depends on how services
are defined, and how they can be activated. The basic prerequisite for that is
a strong service definition \todo{Why?}

In principle, a service is just a concept, and idea of how some functionality
will be offered. To communicate about a service, we need to have a name for it.
The name serves as a stand-in for a detailed definition of the service, and 
is necessary so that the partners of a communication exchange can be sure that
they mean the same thing when using the name. A good \todo{prägnant} name is
important, so that humans are less likely to make an error using that name.

The description of the service has to be attached to the name, so that when a
service name is used, it must be unambiguous which service is meant by that.

To actually activate a service, a set of parameters has to be specified, e.g.~a
droop value for a frequency control service. Which parameters are needed not
only depends on the service itself, but also on how the service is implemented.
Devices that can't ramp up or down power production, for example, could provide
a frequency control service based on a deadband: the service will be provided
when the frqeuncy is outside the deadband range.

Since services can be activated in different ways, the name of the service must
contain a reference to how it is activated. We could for example have three
different frequency control services: \texttt{freq-ctrl-droop},
\texttt{freq-ctrl-deadband}, and \texttt{freq-ctrl-deadband-hysteresis}%
\footnote{The service names have been shortened here to make the article more
readable, but in practice, service names should be as unambiguous as possible,
which suggests using full names like \texttt{frequency-control-droop}}. Those
services differ in the set of parameters needed to activate them.

Here, a distinction has already been made between the service description and a
service activation. The meaning of a service description is that a particular
component can offer this service, while a service activation describes a
certain instance of a service that should be provided by a component. Following
the frequency control example, one possible service activation would contain
the service \texttt{freq-ctrl-droop} together with a certain value for the
droop parameter. The distinction between service description and service
activation is also mirrored in communication between a component and its
supervisory controller: The component will tell its supervisor which services
it supports (service descriptions), and the supervisor can at some point tell
the component at provide a certain service (service activation).

The authors propose a central service registry which contains the names of all
known services, together with some description of the service, e.g.~in which
standard it has been defined. This will provide not only semantics to the
service name, but also make it impossible to use the same service name for
different services. This service registry would be similar to the IANA, the
Internet Assigned Numbers Authority, which is the authoritative source for
Internet top-level domain names, IP address allocation, and Internet protocol
numbers.

For the purposes of communication using services, three parts of a service must
be provided:

\begin{itemize}

  \item A \emph{service description} is basically just the name of the service
    and references the actual definition of the service in some way, e.g.~via
    the registration mechanism that was mentioned \todo{above}.

  \item A \emph{service activation} is a service description plus values for
    all parameters that are needed to actually provide the service.

  \item A \emph{service implementation} is the actual implementation of the
    service which understand service activations and can control the actual
    local component (e.g.~a PV panel) according to the service activation
    (e.g.~provide a frequency control service).

\end{itemize}

While the first two parts are textual representations that can easily be
exchanged between local controller and supervisory controller, the
\emph{service implementation} is an actual computer program located at the
local controller which can provide the actual physical service that the
\emph{service description} and \emph{service activation} describe. This means
that the service implementation controls the actual component, such as a PV
panel.


\section{Levels of Behaviour Descriptions}

Packing one or more services into a single message defines a certain behaviour
for a component. Therefore, this concept has been coined behaviour descriptions
by the authors\footnote{Another name that has been used is "policies", but this
carries too much misleading notions of politics in it}.

\begin{figure}
  \centering
  \includegraphics[width=0.5\linewidth]{levels-of-policies.pdf}
  \caption{The 4 levels of policies}
  \label{fig:4-levels}
\end{figure}

In the previous section, 4 different types of behaviour descriptions were
mentioned. These represent different levels of sophistication in the
communication between supervisory controller and the components it controls.
These are listed in the following list, and also illustrated in figure
\ref{fig:4-levels}:

\begin{enumerate}

  \item Setpoints/Measurements: This is the lowest level, which follows the
    typical closed-loop control scheme which was discussed above.

  \item Activate a single service: This is slightly more flexible than the
    first item, because the provided functionality is encapsulated in a service
    description.

  \item Activate multiple services, with a common disambiguation strategy:
    Activating multiple services at the same time makes much more flexible
    behaviour possible, because different services can react to different
    trigger signals (e.g.~frequency, power price).

  \item Activate multiple services, with a specified disambiguation strategy:
    This is even more flexible than the previous item, because the logic
    determining which service to activate can take local requirements into
    consideration.
\end{enumerate}

Current research investigates a number of different schemes for scaling up the
control system. These can be compared to the 4 levels from the previous list:

\begin{itemize}

  \item Market-based agent system: This is a very flexible system, but it only
    uses a single trigger signal, the power price. The agents are very
    autonomous in their actions, which can be a disadvantage, especially when
    considering the activation of critical system services using such a system.

  \item Virtual Power Plants: \todo{more text}

\end{itemize}

All of these approaches could use behaviour description to negotiate behaviour,
and benefit from the controllable autonomy behaviour descriptions provide.

How the 4th level of behaviour descriptions, services plus a logic to select
one of the services, works, can be explained by an analogy. It is similar to an
operator in a control room picking up the phone and telling the operator of a
remote power plant what to do. The operator is represented by the logic of the
behaviour description, while the things that the power plant operator can do
are represented by the services. The difference lies in that the exchange is
automated, and that it actually happens at the site of the remote power plant
(because the behaviour description has already been transmitted to the power
plant). This means that the "phone" can't really fail, which is possible in the
control room analogy.


An important piece of the behaviour description-based control system is the
ability to deal with conflicting services, e.g.~two services that both want to
control active power output. The number of actual capabilities of components
that can be controlled using services is limited; active and reactive power
output are the most important ones in power systems. This means that many
services will be defined based on those limited capabilities. When services can
conflict, a good conflict resolution strategy is necessary. This can be a
fixed, like in level 3 described above, or dynamic, like in level 4. What is
better suited depends on the application and the capabilities of the
controllers to handle dynamic conflict resolution.


\section{Conflicting services}
\todo{Consolidate naming: disambiguation of services/conflicting services}

When activating multiple services, it is important to handle conflicts between
services in a sensible manner. This is especially important because the number
of capabilities services are based on is very limited: The two most important
capabilities of power system components is the ability to control active and
reactive power output. Other capabilities do exist, but are often of limited
usability in this context (inclusion of small energy sources and the demand
side): One example is the ability to blackstart (part of) a power system,
another example is protection capabilities. \todo{more text..} 

When services are prone to conflict with each other, how can they be defined so
that conflicts are minimized?

\todo{How can services be defined so that conflicts are minimized? By
describing services in terms of the component's capabilities they use, and by
the conditions they are needed for (e.g.~blackstart capability would not
interfere with other control services, bacause the grid will be \todo{down}
when the blackstart functionality is needed.}


\section{Flexibility}

\todo{Look at different VPP implementations, and see where they fit into the
flexibility matrix figure.}

\begin{figure}
  \centering
  \includegraphics[width=\linewidth]{flexibility-matrix.pdf}
  \caption{Flexibility of different approaches}
  \label{fig:flexibility-matrix}
\end{figure}


\section{Modular Standardization}

Using services as the basis of control communication, in the way it has been
described in this article, makes standards higly modular, because the
definitions of the services that can be used by the standard are located
outside of the standard. The standard is then mostly used to exchange
information about services, so it is a meta-standard, a protocol that uses
services as a central concept.

%\todo{Explain advantages of modularity.}

Modular standards have several advantages over monolithic standards. A
monolithic standard is a standard that contains definitions for all or most of
the functionality that it provides. IEC 61850 is an example for a monolithic
standard \todo {Is that true?}.

A standard such as IEC 61850 provides a large set of functionality, and
components will usually only need a piece of that functionality. A modular
standard makes it possible to have small pieces of functionality encaspulated
in a module. Dependencies between these modules will be obvious, so it is easy
to pick a set of all modules necessary to implement a certain set of
functionalities. This means that implementors can easily pick just the parts
(services) that they have to or want to support, and they still are fully
standard-compliant.

A modular standard also makes it easier to deal with changes. Standards have
the common problem that they have to avoid breaking existing equipment that
uses the standard; therefore, backward-compatibility is an important property
of standards \todo{Just look at Windows...}. However, this means that standards
tend to accumulate "cruft" over time, which is functionality that is rarely
used by anyone, but is kept in the standard to keep it backwards compatible.  A
modular standard can also help with this, because changes will often only
affect external modules, and therefore they will not add to the size of the
standard.  Backward-compatibility only means that old version of modules have
to be supported, but that is easier to maintain, because these modules are
separate from the main standard.

%Functionality can be encapsulated. It
%is easier to manage dependencies between functionalities. When functionalities
%do not depend on each other, it is very easy to just pick the pieces you need;
%even when there are dependencies, you just have to add the dependent pieces to
%the set of functionality you need. When implementing a monolithic (as opposed
%to a modular) standard, you usually have to implement every part of it. It is
%possible to make a standard internally modular, and to create the standard in
%such a way that only parts of it can be implemented by an implementor, but it
%is much easier to manage changes when having a modular standard in the first
%place.  Changing existing functionality is dangerous, because someone might
%depend on the current functionality. This means that standards usually need to
%be backwards-compatible, which implies that the standard accumulates a lot of
%old cruft over time, which can't be removed when it must be prevented that
%existing implementations of the standard break. A modular standard is better
%manageable, because implementors can pick which modules they implement, and
%obsoleted modules don't make the standard larger, because they are not part of
%the standard anyway.}

%\todo{Refer to OGEMA project, which also proposes a modular approach, using
%OSGi}
An example for a project that uses a modular structure is the OGEMA project
\todo{cite}, which proposes a certain modular setup of home gateways, based on
Java and OSGi. OSGi is a framework that enables modules to be loaded at
runtime.

