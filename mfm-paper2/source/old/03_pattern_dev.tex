% !TEX root = ../article.tex
\section{Pattern Development}

\subsection{Representation of power system one-line diagram components in MFM}
In order to automatic generate a model of a power system in MFM, patterns representing the components in a power system need to be developed. The representation of the components is dependent on the parameters that should be analyzed. The elements of a power system to be represented are the following:
\begin{itemize}
	\item Bus
	\item Generator
	\item Transmission line
	\item Frequency independent load
	\item Frequency dependent load
	\item Transformer
\end{itemize}
In this case the active power flows in the system were subject of interest and electrical losses in the system were not of concern. In the following the representation of each element is described.

\subsubsection{Bus}
In one-line diagram of a power system a bus is a node where loads or generators are connected. These nodes are interconnected through transmission lines, cables or transformers. In a power system model where only the active power flow is of interest a bus can be represented as a balance in MFM. According \cite{Lind:2011} a balance in MFM is a function block that balances between the total rates of income and output. Figure \ref{fig:bus} shows on the left hand-side the bus symbol in a one-line diagram and on the right the representation in MFM.
\begin{figure}[htbp]
	\begin{center}
		\subfloat[]{
		\begin{tikzpicture}[scale=1, auto, line width=1pt]
 			\draw[line width=2pt, color=black](0.0,0.0)  -- +(0,-.5)node[anchor = west]{X} -- +(0.0,.5);
		\end{tikzpicture}
		\label{fig:one_bus}}
		\hfil
	\subfloat[]{
		\includegraphics[width = 0.3in] {bus_signs.jpg}
		\label{fig:mfm_bus}}
		\caption{Representation of a bus in \ref{fig:one_bus} one line diagram and \ref{fig:mfm_bus} MFM}
		\label{fig:bus}
	\end{center}
\end{figure}
\subsubsection{Generator}

In the representation of a generator in MFM, a distinction between the generator types has to be made, depending on the visibility of the inertia of the generator seen from the power system. A generation unit that is decoupled from the rest of the system through e.g. power electronics does not provide inertia to the system and its power output is not affected by changes in the system frequency. Consequently, the pattern developed in MFM is realized by a non-interacting source (see figure \ref{fig:syn_no_generator}). 
\begin{figure}[htbp]
	\begin{center}
		\subfloat[]{
		\begin{tikzpicture}[scale=1.0, auto, line width=1pt]
 			\node[circle, draw,  fill=white, line width=1pt] (0) at (0,0) {$G_X$} ;
 			\draw (0) --+ (1.0,0.0);
 			\draw[line width=2pt, color=black!30](1.0,0.0)  -- +(0,-.5)node[anchor = west]{X} -- +(0.0,.5);
		\end{tikzpicture}
		\label{fig:one_no_generator}}
		\hfil
	\subfloat[]{
		\includegraphics[width = 1.5in] {generator_no_signs.jpg}
		\label{fig:mfm_no_generator}}
		\caption{Representation of a generator in \ref{fig:one_no_generator} one line diagram and \ref{fig:mfm_no_generator} MFM}
		\label{fig:syn_no_generator}
	\end{center}
\end{figure}
Contrary, the inertia of a synchronous generator is visible to the rest of the power system, which is due to the direct correlation between rotation speed and system frequency. Consequently, the inertia of a synchronous generator has to be represented in a model representation (see figure \ref{fig:syn_generator}). This is done by including an energy storage to the prior defined simple generator model. The source of active power was represented as MFM source function and is connected to a storage function via an unidirectional transport. The causal roles to these connections are realized by an upstream agent and a recipient. These causal rules imply, that the storage does not have an influence on the flow coming from the source. Hence, the flow is solely determined by the source itself, which corresponds to a constant mechanical power input. The storage is chosen to represent the inertia of the machine. The storage is again connected by a transport function to a balance, which represents a bus in a power system. The causal rules used for this connection are an upstream and a downstream agent, so that the energy flow can be influenced by the storage as well as the balance. 
\begin{figure}[htbp]
	\begin{center}
		\subfloat[]{
		\begin{tikzpicture}[scale=1, auto, line width=1pt]
 			\node[circle, draw,  fill=white, line width=1pt] (0) at (0,0) {$G_X$} ;
 			\draw (0) --+ (1.0,0.0);
 			\draw[line width=2pt, color=black!30](1.0,0.0)  -- +(0,-.5)node[anchor = west]{X} -- +(0.0,.5);
		\end{tikzpicture}
		\label{fig:one_generator}}
		\hfil
	\subfloat[]{
		\includegraphics[width = 2.5in] {generator_signs.jpg}
		\label{fig:mfm_generator}}
		\caption{Representation of a generator in \ref{fig:one_generator} one line diagram and \ref{fig:mfm_generator} MFM}
		\label{fig:syn_generator}
	\end{center}
\end{figure}

\subsubsection{Load}
Similar to the representation of generators, a distinction between different load types has to be carried out. In the following two different load types are discussed, which is  on the one hand an ohm resistive load and on the other hand a frequency dependent load (e.g. electric machine load). The representation of an electrical machine load is fairly similar to the MFM model of a synchronous generator, due to the inertia of the machine, there is a energy storage function present (see Figure~\ref{fig:mfm_emotor}).

\begin{figure}[htbp]
	\begin{center}
		\subfloat[]{
		\begin{tikzpicture}[scale=1, auto, line width=1pt]
 			\node[circle, draw,  fill=white, line width=1pt] (0) at (0,0) {M} ;
 			\draw (0) --+ (1.0,0.0);
 			\draw[line width=2pt, color=black!30](1.0,0.0)  -- +(0,-.5) -- +(0.0,.5);
		\end{tikzpicture}
		\label{fig:one_emotor}}
		\hfil
	\subfloat[]{
		\includegraphics[width = 2.5in] {emotor_signs.jpg}
		\label{fig:mfm_emotor}}
		\caption{Representation of a electric machine load in \ref{fig:one_emotor} one line diagram and \ref{fig:mfm_emotor} MFM}
	\end{center}
\end{figure}
%\begin{figure}
%	\begin{center}
%		\includegraphics[width = 2.5in] {emotor_signs.jpg}		
%		\caption{Representation of a electrical machine load in MFM}
%		\label{fig:mfm_emotor}
%	\end{center}
%\end{figure}

Not frequency dependent loads can be represented as non-interacting sinks in MFM. Hence the model consists of a sink connected directly to a balance via a transport function. The causal roles are thereby chosen to be a downstream agent and a sender, which poses the non-interaction of the sink in MFM.
\begin{figure}[htbp]
	\begin{center}
		\subfloat[]{
		\begin{tikzpicture}[scale=1, auto, line width=1pt]
 			\draw[line width=2pt, color=black!30](0.0,0.0)  -- +(0,-.5) -- +(0.0,.5);
			\draw[-stealth](0,0) -- +(0.5,0)-- +(0.5,-0.5);
		\end{tikzpicture}
		\label{fig:one_load}}
		\hfil
	\subfloat[]{
		\includegraphics[width = 1.5in] {load_signs.jpg}
		\label{fig:mfm_load}}
		\caption{Representation of a classic load in \ref{fig:one_load} one line diagram and \ref{fig:mfm_load} MFM}
	\end{center}
\end{figure}

%\begin{figure}
%	\begin{center}
%		\includegraphics[width = 1.5in] {load_signs.jpg}
%		\caption{Representation of a classic load in MFM}
%		\label{fig:mfm_load}
%	\end{center}
%\end{figure}

\subsubsection{Transmission line and Transformer}
\label{sec:transmission-line}
Due to the assumption of a lossless power transmission, the representation of a transmission line is fairly simple with a transport function, that connects to balances (buses). A transmission line allows power transport in both direction and, therefore, a bidirectional transport function is used. 

%Depending on, if transmission lines are considered to be lossless or not, there are different options to model it. The one option, when modeling a lossless transmission line, is to use just one bidirectional transport function and an up- and downstream agent to connect to another bus, represented by a balance. The other option, when losses are associated with the transmission line, is to use a serial connection of a bidirectional transport function, a balance and a second bidirectional transport function. This configuration would have the advantage of providing an easy way to implement transmission losses by adding a sink to the first balance (see firgure ...). In this case, where the aim of the model is to find the reason for an overloaded transmission line, the first option seems preferable, because the reasoning can be started by the state of bi-transport function, but not from a state change in a balance function. Furthermore it was not intended to implement the transmission losses in the model and therefore no reason for using the more complex representation was seen.

Figure \ref{fig:mfm_transmission} shows the chosen representation of a transmission line connected to two buses represented by two balances. 

\begin{figure}[htbp]
	\begin{center}
		\subfloat[]{
		\begin{tikzpicture}[scale=1, auto, line width=1pt]
 			\draw[line width=2pt, color=black!30](0.0,0.0)  -- +(0,-.5) -- +(0.0,.5);
			\draw(0,0) -- +(2.0,0);
			\draw[line width=2pt, color=black!30](2.0,0.0)  -- +(0,-.5) -- +(0.0,.5);
		\end{tikzpicture}
		\label{fig:one_transmission}}
		\hfil
	\subfloat[]{
		\includegraphics[width = 1.5in] {trans_signs.jpg}
		\label{fig:mfm_transmission}}
		\caption{Representation of a transmission line in \ref{fig:one_transmission} one line diagram and \ref{fig:mfm_transmission} MFM}
	\end{center}
\end{figure}

%\begin{figure}[!t]
%	\begin{center}
%		\includegraphics[width = 1.5in] {transmission_line_signs.jpg}
%		\caption{Representation of a transmission line in MFM with the connections to two busses}
%		\label{fig:mfm_transmission}
%	\end{center}
%\end{figure}
In this example the branch \verb+B+ connects the bus \verb+X+ with the bus \verb+Y+.

%The bidirectional transport function for every branch is defined as a main function, with the purpose of maintaining the flow within certain limits. This allows to change the state of the bidirectional transport function and thereby to start the reasoning process. 

%\begin{figure}[!t]
%	\begin{center}
%		\includegraphics[width = 2.5in] {trans_losses.jpg}
%		\caption{Representation of a transmission line in MFM with the connection to two busses and associated losses}
%		\label{fig:mfm_transmission_losses}
%	\end{center}
%\end{figure}

In order to model transformers, the same pattern as for transmission lines was used, because in terms of active power and for the loss less case it is like the transmission line just a power transporting device. 
%\subsubsection{Transformer}
%As mentioned before, in this case of analysing power systems the evaluation of losses is not the purpose. Hence a transformer is assumed to be ideal and can be represented as a transmission line (see figure \ref{fig:mfm_transmission}) in the single-line diagram, when the results of a load flow calculation are further analysed.
%
%It should be noted at this point, that the load flow calculation results in the format defined by the IEEE (see section \ref{sec:ieeeformat}) provide specific information about a connected transformer and its parameters. At this point of the modelling, this information is not of importance and is not further considered. But with further development of MFM, the information attached to the branch data can gain importance.
