% !TEX root = ../article.tex
\section{Pattern Development}

\subsection{Representation of power system one-line diagram components in MFM}
In order to automatically generate a model of a power system in MFM, patterns representing the components in a power system need to be developed. The representation of the components is dependent on the parameters that should be analyzed. The elements of a power system to be represented are the following \textbf{Why these? Explained later?}:
\begin{itemize}
	\item Bus
	\item Active power generation unit
	\item Transmission line
	\item Load
	\item Transformer
\end{itemize}
In this case the active power flows in the system were the subject of interest; electrical losses in the system were not of concern. In the following the representation of each element is described.

\subsubsection{Bus}
In one-line diagram of a power system a bus is a node where loads or generators are connected. These nodes are interconnected through transmission lines, cables or transformers. In a power system model where only the active power flow is of interest a bus can be seen as a simple connection point between the different components (losses are ignored); thus, a bus can be represented as a balance in MFM. According \cite{Lind:2011} a balance in MFM is a function block that balances between the total rates of income and output. Figure \ref{fig:bus} shows on the left hand-side the bus symbol in a one-line diagram and on the right the representation in MFM.
\begin{figure}[htbp]
	\begin{center}
		\subfloat[]{
		\begin{tikzpicture}[scale=1, auto, line width=1pt]
 			\draw[line width=2pt, color=black](0.0,0.0)  -- +(0,-.5)node[anchor = west]{X} -- +(0.0,.5);
		\end{tikzpicture}
		\label{fig:one_bus}}
		\hfil
	\subfloat[]{
		\includegraphics[width = 0.3in] {bus_signs.png}
		\label{fig:mfm_bus}}
		\caption{Representation of a bus in \ref{fig:one_bus} one line diagram and \ref{fig:mfm_bus} MFM}
		\label{fig:bus}
	\end{center}
\end{figure}
\subsubsection{Active power generation unit}
The active power generation unit is a component that injects active power into the grid. This can be a synchronous generator driven by a steam turbine or the power injection at a point of common coupling (PCC) of any kind of electric generation facility. Due to the analysis of steady state conditions in this case a modeling of the dynamic behavior is redundant. Hence, a relatively simple model has been chosen for representing the active power generation unit (see figure \ref{fig:syn_no_generator}).
\begin{figure}[htbp]
	\begin{center}
		\subfloat[]{
		\begin{tikzpicture}[scale=1.0, auto, line width=1pt]
 			\node[circle, draw,  fill=white, line width=1pt] (0) at (0,0) {$G_X$} ;
 			\draw (0) --+ (1.0,0.0);
 			\draw[line width=2pt, color=black!30](1.0,0.0)  -- +(0,-.5)node[anchor = west]{X} -- +(0.0,.5);
		\end{tikzpicture}
		\label{fig:one_no_generator}}
		\hfil
	\subfloat[]{
		\includegraphics[width = 1.5in] {generator_no_signs.png}
		\label{fig:mfm_no_generator}}
		\caption{Representation of a generator in \ref{fig:one_no_generator} one line diagram and \ref{fig:mfm_no_generator} MFM}
		\label{fig:syn_no_generator}
	\end{center}
\end{figure}
The injection of active power is represented by the source function \emph{souX1}. The connection and power transmission to the next bus is realized by the transport function \emph{traX3}. The source and the transport are connected via an influencer. This means that the source can influence the flow through the transport. Contrarily, the balance (representing the bus) is connected with a participant and, therefore, it will not influence the transport function. The \textbf{indirect} connection in this way makes sense, because in a load flow calculation, generation units are generally connected to PV-buses, where the injected active power and voltage magnitude is fixed. 

\subsubsection{Load}
The load components are representing all power consuming devices. These can be as simple as an ohm resistive load (figure \ref{fig:one_load}) or somewhat more complex like an electric machine load (figure \ref{fig:one_mload}). The corresponding MFM model solely consists of a sink and a transport function (figure \ref{fig:mfm_load}).
\begin{figure}[htbp]
	\begin{center}
		\subfloat[]{
		\begin{tikzpicture}[scale=1, auto, line width=1pt]
 			\node[circle, draw,  fill=white, line width=1pt] (0) at (0,0) {M} ;
 			\draw (0) --+ (1.0,0.0);
 			\draw[line width=2pt, color=black!30](1.0,0.0)  -- +(0,-.5)node[anchor = west]{X} -- +(0.0,.5);
		\end{tikzpicture}
		\label{fig:one_mload}}
		\hspace{0.5cm}
		\subfloat[]{
		\begin{tikzpicture}[scale=1, auto, line width=1pt]
 			\draw[line width=2pt, color=black!30](0.0,0.0)  -- +(0,-.5)node[anchor = west]{X} -- +(0.0,.5);
			\draw[-stealth](0,0) -- +(0.5,0)-- +(0.5,-0.5);
		\end{tikzpicture}
		\label{fig:one_load}}
		\hfil
	\subfloat[]{
		\includegraphics[width = 1.5in] {load_signs.png}
		\label{fig:mfm_load}}
		\caption{Representation of a classic load in \ref{fig:one_load} one line diagram and \ref{fig:mfm_load} MFM}
	\end{center}
\end{figure}
The power consumption is represented by the sink function \emph{sinX1}. The connection to the bus and the power transfer are represented\textbf{/realized} by the transfer function. Buses that have loads are connected are modeled as PQ-buses in load flow calculations. Those buses do have constant active and reactive power consumption, in case of loads. Consequently, the sink determines the energy flow. This is realized by an influencer connection from the sink to the transport function. However, the state in the balance (bus) does not influence the transport function and, consequently, the connection is a participant.

\subsubsection{Transmission line and Transformer}
\label{sec:transmission-line}
In a lossless power system and with regards to active power transmission, a transmission line and a transformer can be represented by the same model, because the exclusive function is to transfer power. A transfer of energy is expressed in MFM through a transport function. Due to the ability of power to flow in both directions, a bidirectional transfer function is used to model transmission lines (see fig. \ref{fig:mfm_transmission}. The influence relations are influencers on both sides, because the flow can be influenced by both balances (buses).
\begin{figure}[htbp]
	\begin{center}
		\subfloat[]{
		\begin{tikzpicture}[scale=1, auto, line width=1pt]
 			\draw[line width=2pt, color=black!30](0.0,0.0)  -- +(0,-.5)node[anchor = west]{X} -- +(0.0,.5);
			\draw[-latex'](0.5,0.2) -- +(1.0,0);
			\draw(0,0) -- +(2.0,0);
			\draw[line width=2pt, color=black!30](2.0,0.0)  -- +(0,-.5)node[anchor = west]{Y} -- +(0.0,.5);
		\end{tikzpicture}
		\label{fig:one_transmission}}
		\hfil
	\subfloat[]{
		\includegraphics[width = 1.5in] {trans_signs.png}
		\label{fig:mfm_transmission}}
		\caption{Representation of a transmission line in \ref{fig:one_transmission} one line diagram and \ref{fig:mfm_transmission} MFM}
	\end{center}
\end{figure}

