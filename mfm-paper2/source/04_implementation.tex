\section{Model Translation}

\subsection{One-line diagram}
\label{sec:ieeeformat}
The level of detail with which the model of the power system can be built depends strongly on the data available.
\textbf{It is assumed} that the load flow data in the form of the IEEE standard\cite{ieeeformat} is available and the information provided by the standard is utilised.
In order to build up the one-line diagram, it was assumed that solved load flow data are available for the analysed power system. Because the proposed method is used to identify deviations to a planned system condition, this assumption holds. 

To conform with set standards, it was chosen, that the load flow calculation results are present in the common format for exchange of solved load flow data developed by IEEE in 1973. A description of the common data format can be found in \cite{ieeeformat}. The provided data in that format is sufficient to build a one-line diagram of the power system, but at the same time introduces certain constraints. The bus data does not allow to differentiate between different types of generators or loads. Consequently, all the generators are assumed to be synchronous generators and the loads are modeled to consume constant power. The IEEE format offers detailed information about transformers and phase shifters. Transformers were in this work assumed to be lossless and were therefore modelled as a lossless transmission lines in the MFM model (see section \ref{sec:repMFM}).

The data provided in the IEEE format for each bus and branch are presented in tablse \ref{tab:ieeebus} and \ref{tab:ieeebranch}.
\begin{table}[htdp]
	\caption{Content of the bus data given in the common IEEE format \cite{ieeeformat}}
		\begin{center}
			\begin{tabular}{|c|c|}
				\hline
				Bus Data & Description \\\hline
				Bus Number &  four digit number used in the load flow \\\hline
				Bus Name &  maximum 12 characters; suggested: \\
				 & Area $6-7$, name $8-14$ and kV $15-17$ \\\hline
				 Bus Area &  two digit integer, region where bus is located \\
				 & $0$ indicates unavailable data \\\hline
				 Bus Type &  Type $0$: Unregulated bus, Load Bus (PQ-bus) \\
				 & Type $1$: reactive power generation within limits  \\
				 & Type $2$: Regulated Generator Bus (PV-bus) \\
				 & Type $3$: Reference Bus (Swing Bus) \\\hline
				 Final Voltage & final bus voltage  in $p.u.$ \\
				 & from solved load flow calculation \\\hline
				 Angle & Final voltage angle in degrees \\\hline
				 Load & active and reactive power demand of load \\\hline
				 Generation & final active and reactive generation \\\hline
				 Capacitors & - \\
				 and Reactors & \\\hline
				 Loss Zone & Optional: see \cite{ieeeformat} \\\hline
				 Base & Optional: appropriate $kV$ base \\\hline
			\end{tabular}
		\end{center}
	\label{tab:ieeebus}
\end{table}%

\begin{table}[htdp]
	\caption{Content and format of the branch data given in the common IEEE format \cite{ieeeformat}}
		\begin{center}
			\begin{tabular}{|c|c|}
				\hline
				Branch Data & Description \\\hline
				Terminal &  "from" and "to" bus numbers \\
				Identification &  \\\hline
				Circuit Number & numbering of parallel lines \\\hline
				Branch & $R+j*X$ in $p.u.$ \\
				Impedances & \\\hline
				Line Charging & Total line charging in $p.u.$ \\\hline
				Branch Type &  Type $0$: transmission line \\
				& for more information see \cite{ieeeformat} \\\hline
				Line Area & Optional: Region the branch is located \\\hline
				Loss Zone &  Optional: see \cite{ieeeformat} \\\hline
				Line Ratings &  space for three apparent power ratings \\
				&   with the first being the lowest \\
				&   \\\hline
			\end{tabular}
		\end{center}
	\label{tab:ieeebranch}
\end{table}%


\subsection{Creating a Jess Fact Base}
\label{sec:jess_fact}
To convert the power system moedl into an equivalent MFM model, using the prior defined patterns, a corresponding jess fact base has to be generated. The types of used jess facts are represented in table \ref{tab:jess_fact_types}. The jess fact base contains the following facts:
\begin{itemize}
  \item \texttt{model}: name of the model
  \item \texttt{node}:
  \begin{itemize}
  	\item \emph{functions} of types \texttt{source, sink, transport, storage, balance} or \texttt{bitransport}; the name is the short-hand for the \emph{function-type} (\texttt{sou, sin, tra, sto, bal, btr}) plus a unique number of the node 
  	\item \emph{links}  of the types \texttt{upstreamagent, downstreamagent, sender} and \texttt{receiver}, with the short-hand names being \texttt{up, do, se} and \texttt{re}, respectively
  \end{itemize}
  \item Main functions additionally get an \texttt{attribute} fact (see table \ref{tab:jess_fact_types}) and are necessary to trigger the reasoning
  \item Whole-part relationships are mapped to \texttt{whole-part} facts (see table \ref{tab:jess_fact_types}); the \texttt{\emph{whole}} is defined using a \texttt{node} fact with type \texttt{control (cfs)} or \texttt{energy (efs)}, for control or energy flow-structures
  \item The connections are also mapped to \texttt{link} facts (see table \ref{tab:jess_fact_types}); determining which functions are connected to each other. The \texttt{\emph{port-number}} describes to which port of the function the connection is linked and determines for instance the direction of a transport function.
  \item In order to set the states of the functions to initial values \texttt{evidence} facts are used (see table \ref{tab:jess_fact_types}). 
\end{itemize}
\begin{table}[htdp]
	\caption{Generated jess facts representing a power system in MFM}
	\begin{center}
		%\begin{tabular}{|c|c|c|}
		\begin{tabular}{|c|c|}
		\hline
		Fact type & Syntax \\\hline %& Description \\\hline
		model &  \texttt{(model} \emph{file}\texttt{)}\\\hline % & Model definition \\\hline
		node & \texttt{(node } \emph{function-type} \emph{function-name}\texttt{)}\\\hline % & Node definition for a function\\
		 & \texttt{(node} \emph{link-type} \emph{link-name}\texttt{)}\\\hline % & Node definition for a link \\\hline
		whole-part & \texttt{(whole-part }\emph{whole} \emph{part}\texttt{)} \\\hline %& Defining membership \\\hline
		link & \ \texttt{(link }\emph{link-name} \texttt{begin} \emph{function-name} \emph{port-number}\texttt{)} \\\hline %& Defining beginning of a connection \\
		& \ \texttt{(link }\emph{link-name} \texttt{end} \emph{function-name} \emph{port-number} \texttt{)} \\\hline %&  Defining end of a connection \\\hline
		attribute &  \texttt{(attribute} \emph{attribute-type} \emph{function-name} \emph{parameter}\texttt{)} \\\hline %& Optional:  \\
		 &  \texttt{(attribute} \emph{attribute-type} \emph{function-name}\texttt{)} \\\hline %&  Assign attribute to functions\\
	          &  \texttt{(attribute} \emph{attribute-type} \emph{function-name1} \emph{function-name2}\texttt{)} \\\hline %& \\\hline	
%	          evidence &  \texttt{(evidence} (type \emph{function-type}) (function \emph{function-name}\texttt{)}  & Setting a function state \\
%	           & \texttt{(state} \emph{function-state}\texttt{))} &\\\hline		     
		\end{tabular}
	\end{center}
	\label{tab:jess_fact_types}
\end{table}%



\subsection{Generation of Jess Fact Base}
In order to generate the Jess Fact Base for a power system with the data given from the load flow calculation results, a MATLAB function \verb+extract_data()+ was developed. The function analyses the set of bus and branch data and generates the Jess Facts necessary to represent each element of the power system.
\\\\
Four power system elements can be identified from the given data (generators, loads, Transmission lines and transformer). Transmission lines are represented as seen in figure \ref{fig:mfm_transmission}. As mentioned before transformer are assumed to be lossless and are therefore represented in the same way as a transmission line. The generators are all represented as shown in figure \ref{fig:mfm_generator} and loads are chosen to be represented by the simple model shown in figure \ref{fig:mfm_load}. If the load is a electric machine and should be represented by the more complex model (see figure \ref{fig:mfm_emotor}), it can not be identified with the data provided.
\\\\
The function \verb+extract_data()+ takes as an input the filename of the load flow calculation results in the IEEE format and returns a text file (named \verb+filename_out.txt+) containing the Jess Fact Base. In order to import the load flow calculation results, the MATLAB function \verb+readcdf()+ (see section \ref{sec:readcdf}) is utilised. The Jess Fact types used for the representation were described before in section \ref{sec:jess_fact}. In the following the steps carried out by the function are explained.
\begin{itemize}
	\item execute the function \verb+readcdf()+
	\item create the \verb+model+ fact \texttt{model \emph{filename}}
	\item create for each area an energy flow-structure \texttt{node energy efs\emph{\#area-number}}
	\item create a vector containing the bus numbers of generator buses and one containing the number of the load buses
	\item generator, load and branch facts:
	\begin{itemize}
		\item create the \verb+node+ facts for all the functions and connections
		\item create the \verb+whole-part+ facts for all the functions defined
		\item create the \verb+link+ facts determining the order and the connection of the functions 
		\item create the \verb+attribute+ facts with advanced information about the functions and connections
	\end{itemize}
\end{itemize}
It is important to note here, that the \texttt{\emph{port-numbers}} are only meaningful when connecting a \verb+transport+-function. The reason therefore is that a transport function always has an upstream and a downstream side. The flow of energy always goes from port $1$ to port $4$, hence, in case of a generator, the \verb+source+ is connected via an \verb+upstreamagent+ to port $1$ of the \verb+transport+-function. Whereas the subsequent \verb+balance+ is connected to port $4$ via a \verb+receiver+. 

\subsubsection{Initial values transmission lines}
In order to analyse the calculation results given from the load flow calculation, the states of the \verb+bitransport+-functions have to be set accordingly. The load flow calculation results in the IEEE format provide information about the charging as well as the rating of the transmission line (see table \ref{tab:ieeebranch} on page \pageref{tab:ieeebranch}). These information are utilised in order to define the flow through the bitransport. The states, which can be taken by the bitransport function and the conditions determining the states, are described below:
\begin{itemize}
	\item \emph{undef}: assigned if no rating is defined for the branch
	\item \emph{normal}: the absolute value of the line charging is smaller than the rating 
	\item \emph{hiflowup}: the absolute value of the line charging exceeds the rating and is negative
	\item \emph{hiflowdo}: the absolute value of the line charging exceeds the rating and is positive
\end{itemize}
It should be noted that a negative flow thereby corresponds to a flow contrary to the direction defined by the "from" and "to" bus (see table {tab:ieeebranch} on page \pageref{tab:ieeebranch}). Hence a positive flow corresponds to a flow in the direction of the defined "from" and "to" buses. A common transport function has beside the states \texttt{\emph{hiflow}} (high flow) and \texttt{\emph{normal}} (normal flow) a \texttt{\emph{loflow}} (low flow) state. The information about a low flow in a transmission line has so far no importance, because the focus is on identifying overloaded lines and the reasons therefore. A low flow state becomes of interest, if a control function is implemented and lines with low power flow could be used to by-pass power flow from overloaded lines.
\\\\
A MATLAB function \verb+line_state()+ was implemented to set the state of each branch according to the results of the load flow calculation. The function generates an \emph{evi}-file, that contains \verb+evidence+ facts for each branch. The input to the function is the file-id of the evidence file, a struct containing the information about the actual branch and the branch number. Given these inputs the function generates a \verb+evidence+ fact (see table \ref{tab:jess_fact_types} on page \pageref{tab:jess_fact_types}) and writes it in the evidence file.

\input{source/seq_operation.tex}
