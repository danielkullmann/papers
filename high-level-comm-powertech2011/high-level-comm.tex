\documentclass[journal]{IEEEtran}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{amsmath} 
\usepackage{color} 
\usepackage{mdwlist} 
\usepackage{listings}
\interdisplaylinepenalty=2500

%\newcommand{\todo}[1]{\textcolor{red}{#1}}
%\setlength{\parskip}{0.8\parskip}

% This is for listings of the Drools language
\lstset{ %
  basicstyle=\ttfamily\footnotesize, % font type and size
  backgroundcolor=\color{white},  % background color
  showspaces=false,               % show spaces adding particular underscores
  showstringspaces=false,         % underline spaces within strings
  showtabs=false,                 % show tabs within strings adding particular underscores
  frame=single,	                  % adds a frame around the code
  tabsize=2,	                    % tabsize is 2 spaces
  captionpos=b,                   % caption-position: bottom
  breaklines=false,               % no automatic line breaking
  breakatwhitespace=false,        % sets if automatic breaks should only happen at whitespace
  morekeywords={package,import,rule,when,then,end,and,or}, % keywords for the Drools language
  morecomment=[l]{\#} % comment in Drools: from "#" to the end of the line 
}

\pagestyle{empty}

\begin{document}

\title{High-level Communication for the Control\\of Power Systems}
\author{Daniel~Kullmann, Henrik~Bindner%
\thanks{Daniel Kullmann is PhD student at Risø, DTU, Roskilde, Denmark}%
\thanks{Henrik Bindner is Senior Scientist at Risø DTU, Roskilde, Denmark}}%

\markboth{}{}
\maketitle
\thispagestyle{empty}

\begin{abstract}
  The integration of small, renewable energy generators and active consumers into power systems leads to a number of problems that have to be dealt with. The control structures for these new power systems must be highly scalable, they must be robust against failures, and they must be flexible enough to handle the different types of components in the system.  
  This paper examines those challenges and proposes a new type of high-level communication, policies, that helps to deal with them.
\end{abstract}

\begin{IEEEkeywords}
  Smart grids, Communication systems, Intelligent systems
\end{IEEEkeywords}


\section{Introduction}

The integration of large amounts of renewable energy sources (RES) into the power system is a big topic in research right now. One of the issues arising from that is that the system has to deal with the fluctuations inherent in the power production of RES. This led to the wish to include the consumption side actively into the control of the power system, which in turn means that large numbers of components have to be controlled in parallel.

The new reality in the power system is subsumed under the term Smart Grid, which has no single authoritative definition, but most descriptions (like in \cite{MGI07-b}) contain the same parts: Usage of information and communication technology (ICT), including the consumption side, making the grid more reliable, enabling market-driven approaches, including different kinds of devices (generation, consumption and storage).

Control implies communication, and communication lies at the very heart of the Smart Grid concept: Distributed components, both renewable energy sources and consumption components, have to be coordinated to achieve the goal of a stable system. Different control system concepts put different demands on the underlying communication system: Most importantly, the communication has to be fast enough, both in terms of latency and bandwidth. But the existing communication infrastructure also can make it necessary to adapt the control system used: When communication failures can happen, the control system must be able to deal with them. % TODO This is difficult, especially when the whole control system ought to be automated as much as possible.

All those components that are connected to the grid can offer a number of simple and complex services to the grid, such as power generation and consumption, frequency control, power balancing and voltage control. The components come from many different suppliers and have different types.

The communication system to be developed for enabling the Smart Grid must in effect be device-agnostic, because of the different kinds of devices present in the system. This suggests that the communication ought to be based on standards, and important concepts that are missing from today's standards ought to be added to them.

The type of control that the Smart Grid proposes stands in stark contrast to the traditional way to control power systems: A typical means of control is the closed-control loop, which demands very good communication capabilities between controller and controlled device. But when many distributed, often small units have to be controlled, it is unrealistic to set up dedicated communication channels. Thus, the Smart Grid control infrastructure has to be based on the existing communication infrastructure, which consists e.g.~of broadband Internet and mobile network connections. %These communication systems can't give latency or broadband guarantees, they can even fail completely for a short time. The new control system for Smart Grids must be able to work in this circumstances.

%\begin{figure}
%  \begin{center}
%    \includegraphics[scale=0.4]{wind-power.png}%
%  \end{center}%
%  \caption{Typical wind power output\label{fig:wind-power}}
%\end{figure}

% List of topics:
%  * Wanted: integration of more renewable energy resources (e.g.~20-20-20 plan)
%  ! => Many small devices connected to the grid
%  ! Electric vehicles would add much load as well
%    * Charging of EVs has to be coordinated 
%  ! Renewable energy fluctuates
%  ! => Wish to include the demand side
%  ! => Even more small devices
%  ! All those devices have to be controlled in a coordinated fashion
%  ! Those devices have different types and can provide different services to the grid
%  ! Those services are defined in the grid codes, so they differ from nation to nation
%  * Centralized control has problems:
%   * Hard to scale
%   * Introduces single points of failure
%  ! Control implies communication
%  ! * Standardization is important
%
% Problems to be solved (put maybe into introduction part?):
%  * Communication should be: cheap, reliable, flexible
%  * Reliability is a problem, especially when communication must be cheap
%  * Broad-band internet connections are already available in many places; alternatively, mobile internet connections (UMTS, GPRS) could be used
%  * Two-way vs. one-way communication
%    * Frequency and voltage as easy-to-measure values
%    * Price as an easy-to-distribute single value


\section{State of the Art}

To reach the goal of having a Smart Grid, a number of approaches have been proposed. Some of them use power markets as a central concept, such as the PowerMatcher system \cite{KWK+06} or the DEZENT project \cite{WeddeLRK08}. Other approaches split up the system into smaller parts that are easier to control; examples for these are Microgrids \cite{Microgrids} and energinet.dk's Cell project~\cite{CellProject}.

Aggregation approaches have been suggested as a way to deal with the complexity of the system, both on the consumption side and on the generation side. An example for this is research on Virtual Power Plants (VPPs), e.g.~\cite{DPCR06}. Aggregation is greatly simplified when components can be accessed through unified interfaces; \cite{OG08} proposes a role-based approach to organise components in such a fashion. By this, the actual type of the component is abstracted away, and only capabilities relevant to the task are considered.

%All of these approaches ignore communication to some degree; they just assume it is possible.
All these approaches assume implicitly that communication just happens; the possibility of failure is silently ignored.

Concerning communication, there are two standard families that are of major importance to the field of Smart Grids: one is the IEC 61850 family of standards \cite{iec61850-1}, the other the IEC~61970 and IEC~61968 family of standards \cite{iec61970-1}, also known as CIM (Common Information Model). The two families have some overlaps such as similar models of power system components, but they have also very different approaches: IEC 61850 is mostly concerned with communication \emph{with} the actual components, whereas CIM has a strong focus on communication \emph{about} components. IEC 61850 provides an abstract communication system interface (ACSI), an API to talk with different kinds of components. The CIM offers a power system ontology, with the possibility to define messages on the base of this ontology.

The aim of the Smart Grid idea is to get a control system that exposes intelligent behaviour. Many approaches use multi-agent systems \cite{Wooldridge09} to ensure a higher reliability of the whole system \cite{KWK+06,WeddeLRK08,CellProject}. FIPA, The Foundation for Intelligent Physical Agents, has developed a number of standards for multi-agent systems, e.g.~\cite{FIPA-00037}.

Agent-based systems are a way to structure a distributed system, but they are not in themselves intelligent. Expert systems, usually in the form of rule-based systems \cite{Giarratano04}, are often used for that. They use a relatively simple concept, if-then rules, to create complex behaviour or to support reasoning about a system.
Drools \cite{Drools} is an example of such a rule-based system.

% Things to mention:
% * Many different approaches to control the Smart Grid have been proposed
% ! * (Definition of Smart Grid is not necessary)
% * Microgrids: two European projects
% * Cells (energinet.dk)
% * Aggregation
%   * Market-based solutions (PowerMatcher)
%   * VPPs (also Virtual Devices)
% * DEZENT (Wedde's approach, very fast markets)
% * Distributed Systems
% * Many of these are agent-based systems
% * Standards are important
%   * IEC 61850/61400-25: Talk to components
%   * IEC 61968/61970 (CIM): Talk about components
% * Systems that have intelligent behaviour
%   * Expert systems
%   * Multi-agent systems (FIPA)
%   * It is important to balance the amount of autonomy a supervised system has, against the needs of the supervisor to know what's going on.
%   * Rule-based systems (CLIPS, Jess, Drools)


\section{High-level Communication}

The challenge to achieve the Smart Grid is to build a distributed control system that shows intelligent behaviour. A richer way of communication helps in reaching this goal.
To put this high-level communication into perspective, let's juxtapose it with low-level communication: It is rather device-specific and is concerned mostly with measurements and set-points. That means it works with the low-level concepts of getting and setting values. In contrast, high-level communication is device-independent, it uses ontologies, and it uses more general abstract data formats for communication. In that respect, the 61850 and CIM standards already employ high-level communication to some degree.
% TODO functionality/service

%\todo{Is this paragraph helpful?}
%Another way to explain the difference low-level from high-level communication is to distinguish between functionality and service. low-level communication uses the direct access to functionality of a component, such as the power set point, whereas high-level functionality accesses the services a component offers, e.g.~frequency control.

The shift from low-level to high-level communication makes it also possible to communicate \emph{about} components, instead of just communicating \emph{with} components. 
%\todo{This is enabled by the use references to ``things'', so that we can communicate about this thing. The distinction between a ``thing'' and a reference to that ``thing'' is very important, especially when you don't have direct access to the ``thing''. }
% TODO Identification systems necessary, plus mappings between these systems.

Even when using high-level communication, a part of the control system for a single components will still employ low-level communication, such as needed by a closed-control loop. This part will have to stay close to the component, because of the high demands on the communication quality. Another part of the control can be moved away, and the coordination of those two parts can use high-level communication (see Fig.~\ref{fig:closed-loop}). % . This is illustrated in Fig.~\ref{fig:closed-loop}.

The CIM can play an important part in high-level communication, because it provides an ontology, and makes it possible to communicate \emph{about} components. This communication is in form of CIM messages that can for instance be expressed in XML. But just using the CIM is not sufficient to achieve an intelligent grid: The nodes that communicate have to exchange information not only about the physical structure of components, but also about their behaviour: what kinds of behaviour/services they are able to supply, what behaviour they plan to do, and in what way this behaviour can be influenced. It might also be important to express how the different behaviours a component can execute relate to each other.

% Structure:
% * Lead-in to high-level comm. (``What we need is..'')
% * Explain what it is
%   * Start with what low-level comm. is
%   * Then explain what high-level comm. is
%   * The goal of h-l c is to enable a distributed system that shows intelligent behaviour
% Things to mention:
% * low-level communication is:
%   * measurements
%   * set-points
%   * device-specific
%   * low-level concepts (get/set values)
%   * functionality (e.g. power set-points)
% * high-level communication is:
%   * device-independent (61850 is already kind of high-level)
%   * uses ontologies
%   * uses references to things instead of direct representations of things
%     * this makes it possible to talk ``about'' things, in comparison to talking ``to'' things 
%   * communicate about possible/planned behaviour and how it can be influenced
%   * services (e.g. ancillary services), droop is an example of something that is already a service, but has the status of a functionality, because its concept is so simple


\section{Behaviour Descriptions / Policies}

Behaviour descriptions are one way to enable high-level communication in power systems. 
The way this is done is that the supervisory unit sends a description of expected future behaviour to the controlled unit, instead of sending just commands to be executed immediately. The behaviour description tells the unit how to react to all possible kinds of situations. The important thing to understand here is that this description can be sent to the unit long before it is actually needed. Usually, such a description will have a certain validity period.

The behaviour of the unit depends on events the unit can observe locally, like the system frequency or a system price that is broadcast. Using behaviour descriptions makes extra communication between the supervisor and the unit unnecessary.

\begin{figure}
  \centering
  \includegraphics[scale=0.6]{closed-loop-to-policy-based-control.pdf}
  \vspace{-5pt}
  \caption{Decoupling supervisor from controlled unit\label{fig:closed-loop}}
  \vspace{-5pt}
\end{figure}

These descriptions of behaviour are called ``policies'' here. They can be compared to contracts: The supervisor negotiates a certain behaviour with the unit, and then it can be sure that the unit will actually follow the policy given to it. The policy can additionally be made legally binding, by having the unit digitally sign it. This also makes the system more secure, because the unit is authenticated by the signature, and the unit also cannot claim never to have gotten the policy (this characteristic is called \emph{non-repudiation}).

% This behaviour description is sent before the component actually needs it, and it is valid for some length of time. 

% Things to mention:
% * Split up closed-control loop and supervisory control
% * Tell devices how to behave
% * Behaviour is negotiated before it is needed, not just-in-time
% * Like a contract
%   * Supervisory control can be sure that devices will follow policy, because they have signed it
%   * Devices are sure that policies comes from supervisory unit, because they were signed
% * Protocol is needed to negotiate a policy
%   * FIPA provides the contract net interaction protocol, a modified version of which can be used (cfp step is optional in policies)
%   * Policies can't be completely expressed reasonably using FIPA's communicative acts, only as part of a request message
%     * Theoretically, a list of request-whenever messages could be used, but that would be impractical, because the policy would have to be split up into single rules 
% * ECA Rules for expressing policies
%   * Event, Condition, Action (event could be left away)
%   * Can be used to express many things
% * Policies can be used on different levels; one or more rules for every level
%   * Load-following
%   * Secondary/Tertiary control
%   * Frequency control
%   * This way, a unit can "serve different masters" (i.e.~follow different objectives)
% * ..or ..
%   * Contracts about taking part in a certain group (e.g.~a VPP)
%   * Control of coalition building
%   * Control of actual units
% * Coordinated behaviour, yet preventing oscillating behaviour: staggering rules


\section{Expressing Policies}

Policies can be expressed in many different ways. The options range from very abstract, like ``keep the frequency at 50 Hz'', to very specific, like ``set the power output to the value of this formula''. Here it was decided to use a rule-based system to express policies. It is a good fit for several reasons:
\begin{itemize*}
  \item easy to understand for both computers and humans
  \item expressive and flexible
  \item rules can be used to describe complex behaviour
  \item rules can be generated e.g.~by an optimisation process
  \item rules can be prioritised
  %\item \todo{rules are in a sweet spot between autonomy and control} 
\end{itemize*}

The flexibility of rules comes from the fact that not only the settings of constants used in rules can be changed (such as the frequency threshold in a rule concerned with frequency control), but a rule base can also be changed to include completely different rules. The only limitations lie in the kind of information that is available to the rule (system frequency, voltage, power price, \dots) and the types of actions the component supports (power setpoints, droop settings, load schedules, \dots). Rule sets can be arbitrarily complex, making complex behaviour possible.

Rules must be able to access some information from outside, e.g.~the system frequency, the current power price, or some status of the component the rule was made for. Because rules must in practise be device-agnostic, this implies the usage of a common ontology. CIM provides such a common ontology for power systems, so it should be used as a base ontology for the rule system.

The possibility to prioritise rules makes it possible for a unit to act on several levels of control: e.g.~can a unit be made follow a certain consumption schedule, unless a large drop in frequency arises that makes other behaviour necessary.


\section{Example policy}

The example used in this paper is based on charging many electric vehicles. In principle, the rules created for this example can be used for other kinds of demand-side participation, such as controlling electric heaters, but the rules must of course be changed to reflect the different constraints the devices have to obey.

On the lowest level, the electric vehicles will follow a load schedule that has been calculated by the aggregator of these vehicles, based on power price forecasts and the constraints that come from the electric vehicles. Those constraints reflect the needs of the owners of the car, such as the next time the car will be needed again, and the capacity that has to be charged into the battery by that time.

On the middle level, the electric vehicle can be told to respond to extreme power prices, which might occur when e.g.~wind power outputs differ significantly from their forecasts. On the highest level, the vehicles can participate in basic frequency control by reacting to to frequency peaks or drops.

This behaviour can be expressed in 5 rules, one for following the load schedule, and two each for reacting to power prices and frequency. The need for two rules for price and frequency arises from the fact that there must be one rule for a lower threshold, and one for an upper threshold.

Fig.~\ref{fig:example-ev} shows an example rule-base for this scenario. The syntax used for this example comes from the Drools \cite{Drools} rules system. As noted above, the rule base must access some information about the environment it exists in.
In this example, the rules have access to the objects \emph{PowerPrice}, \emph{SystemFrequency} and \emph{Charger}. The \emph{Settings} object must be sent along with the rule set; it contains the schedule data and thresholds for price and frequency. Standardising the means to access the needed information is necessary to make the concept widely acceptable. The CIM could provide a common ontology for that.

Here it can be seen how rules can in fact be used to make policies flexible. The values in the \emph{Settings} object can be changed, and new rules can be introduced that completely change the behaviour of the component. An example is two new rules that react faster to changes in frequency.

\begin{figure}
  \centering
  \lstinputlisting{evc1.drl}
  \vspace{-5pt}
  \caption{Rule base for example case}\label{fig:example-ev}
  \vspace{-5pt}
\end{figure}

%  * Charging of electric vehicles
%  * Top-level: following a forecasted wind generation curve
%  * Middle-level: adjust actual battery charging input to more short-term forecast (1 minute forecast)
%  * Bottom-level: adjust battery charging to provide ancillary services, e.g.~via

\section{Conclusion}

The aim of this paper was to introduce behaviour descriptions or ``policies'' as a good way to structure communication in power systems. Because the execution of the behaviour is decoupled from the communication of the behaviour, communication network problems, like a large increase in latency, or even a complete failure of a link, do not impede the execution of the behaviour. This makes the whole system much more robust. Rule systems can be quite complex, enabling intelligent behaviour of the system.

Policies can be used with all kinds of control approaches, e.g.~market-based, agent-based, or hierarchical; using policies adds the possibility to decouple communication from execution, making the system less prone to communication failures.

\bibliographystyle{IEEEtran.bst}
\bibliography{../bibliography,local}

%%\begin{biographynophoto}{Daniel Kullmann}
%\begin{IEEEbiography}[{\includegraphics[width=1in,height=1.25in,clip,keepaspectratio]{daniel-kullmann}}]{Daniel Kullmann}
%  Daniel Kullmann is a PhD student at Risø National Laboratory for Sustainable Energy, Technical University of Denmark. 
%  He graduated from the Technical University of Darmstadt, Germany, in 2002 with a diploma degree in Computer Science. After working for several 
%  years in the IT industry, he moved 2008 to Denmark and started his PhD there
%  His topic of research is high-level communication in power systems.
%  \todo{biography like this, or like Yi did it?}
%  \todo{add biography for Henrik}
%\end{IEEEbiography}

\end{document}

