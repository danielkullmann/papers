%\documentclass[journal,letterpaper]{IEEEtran}
\documentclass[journal,12pt,draftclsnofoot,onecolumn,letterpaper]{IEEEtran}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{amsmath} 
\usepackage{color} 
%\usepackage{mdwlist} 
\usepackage{setspace}
\usepackage[letterpaper,margin=1in]{geometry}
\usepackage{listings}
\interdisplaylinepenalty=2500
\doublespacing % only for 12pt, onecolumn setup
\newcommand{\degree}{\ensuremath{^{\circ}}}
% TODO: remove the two commands
%\newcommand{\todo}[1]{\textcolor{red}{#1}}
%\newcommand{\comment}[1]{}

% To finally squeeze the article into two pages
\newcommand{\lessSpaceAboveSections}{}%\vspace{-5pt}}

\pagestyle{empty}

% This is for listings of the Drools language
\lstset{ %
  %basicstyle=\ttfamily\footnotesize, % font type and size
  basicstyle=\ttfamily\scriptsize, % font type and size
  backgroundcolor=\color{white},  % background color
  showspaces=false,               % show spaces adding particular underscores
  showstringspaces=false,         % underline spaces within strings
  showtabs=false,                 % show tabs within strings adding particular underscores
  frame=single,                    % adds a frame around the code
  tabsize=2,                      % tabsize is 2 spaces
  captionpos=b,                   % caption-position: bottom
  breaklines=false,               % no automatic line breaking
  breakatwhitespace=false,        % sets if automatic breaks should only happen at whitespace
  morekeywords={package,import,rule,when,then,end,and,or,activation,group}, % keywords for the Drools language
  morecomment=[l]{\#} % comment in Drools: from "#" to the end of the line 
}

\pdfinfo
{ /Title (Using Behaviour Descriptions for the Coordination of Demand Response)
  /Author (Daniel Kullmann, Oliver Gehrke and Henrik Bindner)
  /CreationDate (D:20110930000000) % this is the format used by pdf for date/time
  /Subject ()
  /Keywords (Smart grids, Communication systems, Intelligent systems)
}

\begin{document}

\title{Using Behaviour Descriptions for the Coordination of Demand Response}
\author{Daniel~Kullmann, Oliver~Gehrke and Henrik~Bindner\\
Risø DTU National Laboratory for Sustainable Energy\\
 Roskilde, Denmark}

\markboth{}{}
\maketitle
\thispagestyle{empty}

\begin{abstract}
  % Setting the scene
  The integration of large numbers of of small, renewable and distributed
  energy resources poses a substantial challenge to electrical power systems.
  % Research question
  The control structure needed to coordinate the components in the power system
  of the future needs a sophisticated communication system to manage the
  requirements and constraints of a dynamically changing distributed system.
  Some of the challenges are scalability, flexibility, extensibility, and
  the reliability of communication links.
  % Research gap
  Especially the last point has not been addressed in research. 
  % Solution
  Asynchronous communication alleviates several of the problems, and allows to
  decouple a controlled device from its controller.  
  % Method?
  This article presents behaviour descriptions, a concept that enables
  asynchronous communication for the control of power systems. A rule-based
  implementation of behaviour descriptions is presented, and the feasibility of
  the approach is demonstrated using an example case.
  % Concluding statement?
\end{abstract}

\begin{IEEEkeywords}
  Smart grids, Communication systems, Intelligent systems
\end{IEEEkeywords}


\lessSpaceAboveSections
\section{Introduction}
\label{sec:introduction}

Today, electrical power systems face the challenge to integrate large numbers
of renewable energy sources (RES). There are several reasons for that; one is
the finite amount of fossil fuels, another environmental concerns due to the
emission of greenhouse gases or the production of toxic wastes. RES differ from
more traditional power plants (e.g.~coal, nuclear) in that their power
production fluctuates. The need to compensate for the fluctuations inherent in the power
production of RES led to the desire to include the consumption side, industrial
and commercial as well as residential, actively into the control of the system.
This means that large numbers of very different components, located mostly at
the distribution level of the power grid, have to be controlled simultaneously.

The resulting system is a large distributed system, consisting of a large number
of mostly small units that are dispersed over a large geographical area.
A number of challenges have to be addressed when trying to build this system:
\begin{itemize}
  \item Scalability: the problem of controlling a large number of
    units simultaneously.
  \item Flexibility: the problem of having to control a heterogeneous
    set of components.
  \item Extensibility: the integration of new types of components that will 
    be developed into the system, with only a minimum of change to the
    existing system.
  \item Reliability: measures how well the system can cope with failures; the
    resulting control system must at least be as stable and robust in the case
  of failures as the existing one.
\end{itemize}

Scalability has been a main focus of research, and several
concepts based on hierarchies and aggregation have been put forth to address 
that challenge.
% TODO Move to background
Some of them use power markets as a central concept, such as the
PowerMatcher system~\cite{KWK+06} and the DEZENT project~\cite{WeddeLRK08},
which both employ multi-agent systems. Other approaches split up the system
into smaller parts that are easier to control such as the Microgrids 
project~\cite{Microgrids} and the Cell project of the Danish TSO
(Transmission System Operator) Energinet.dk~\cite{CellProject}.  
Virtual Power Plants (VPPs)~\cite{DPCR06} also fall into this category.

%TODO: Moved from Related Work
%Aggregation is greatly simplified when components can be accessed through
%unified interfaces; \cite{OG08} proposes a role-based approach to
%organise components in such a fashion.
%
%Much research has been conducted for optimal control of appliances
%\cite{YiZong10, nyeng10, LS10}. This research focused on the local optimal
%behaviour of appliances. To integrate such concepts into a Smart Grid, the
%appliances of many households have to be coordinated, so that local constraints
%are obeyed, and that the appliances work together to reach a common goal
%(such as providing a certain power system service).


Flexibility and extensibility are concerned with how a heterogeneous set of 
components can easily be integrated into power systems: the components have 
different types, support different services, are owned
by different stakeholders, and have to operate under different constraints. New
types of components are being developed.

%There is an abundance of communication system choices: wired or wireless internet
%(IPv4 and IPv6), mobile communications (GPRS, UMTS), Zigbee, ZWave, Bluetooth,
%and many more. They offer different qualities of communication regarding
%latency, bandwidth and reliability. However, it is not easy to assign exact
%values for these metrics; it depends on the actual installation and on the
%momentary state.

Control in a distributed system implies communication: 
components distributed over the whole system
have to be coordinated using communication technology to achieve the goal of a
stable system. Different control system
concepts put different demands on the underlying communication system. Most
importantly, the communication has to be fast enough for the specific control
system in use, both in terms of latency and bandwidth. The existing
communication infrastructure can also make it necessary to adapt the control
system used: If communication failures can happen, the control system must be
able to handle them gracefully. 

Smart Grid research has not been focused on this area; usually, the
availability of reliable and cost-effective communication
is just taken for granted. However, there is a trade-off between cost on the
one hand, and reliability and quality of communication links on the other hand.
When many small, distributed sources
have to be connected to the communication system, cost-efficiency will be the
deciding factor. 
This implies that communication equipment will be installed that does not offer
the best capabilities in terms of availability, latency and bandwidth. 
These three metrics have to be taken into account when designing the
control architecture for power systems.

In this article, the concept of behaviour descriptions is presented. Its aim is to
provide a paradigm for control communication, so that the challenges listed above are addressed;
a possible implementation of behaviour descriptions using a rule-based system is shown.

% TODO Direct vs. Indirect Control, closed-loop vs. open-loop

This article assumes a specific structure of a control system for demand-side
components: An aggregation hierarchy that controls many, mostly small,
resources. This is exemplified in Figure~\ref{fig:dsm-aggregation}. The
components that are controlled, in this case household appliances, are directly
controlled by a house gateway. Because the capacity of a single household to
provide system services is not large enough to be of interest to the system, an
aggregator aggregates the capabilities of multiple households. This aggregator
controls the components indirectly, via the house gateway. 

This control structure
is also applicable in other control scenarios, such as the charging control of
electric vehicles, a topic that has recently gained traction, due to the
appearance of pure electric vehicles on the market.

% TODO: why not use direct control? 
% TODO: why does a distributed system have the same structure?


\begin{figure}
  \begin{center}
    \includegraphics[width=0.9\linewidth]{dsm-aggregation.pdf}
  \end{center}
  %\vspace{-5pt}%
  \caption{Assumed System Structure}
  \label{fig:dsm-aggregation}
\end{figure}


\lessSpaceAboveSections
\section{Related Work}
\label{sec:related-work}

Standards have been universally accepted as a key factor for the
success of the Smart Grid vision. This is demonstrated by the list of standard
surveys that have been created in the last few years
\cite{DKERoadMap,IECRoadMap,NISTStandardRoadmap}. The main reason for
the importance of standards is interoperability, i.e.~that components 
of different types or from different manufacturers can work together.
The two major communication
standards for the Smart Grid are IEC~61850~\cite{iec61850-1} and 
IEC~61970~\cite{iec61970-1}. 

IEC 61850 has been positioned as a standard for SCADA\footnote{Supervisory
Control And Data Acquisition}-like systems. It defines a set of data classes
for different types of data, such as a timestamped integer value, a set of
component descriptions that use these data classes, such as a circuit breaker,
an XML configuration language for describing components, and an Abstract
Communication Service Interface (ACSI) for communicating with these components.
The only standardized mapping for the ACSI that is available right now uses 
MMS\footnote{Manufacturing Message Specification}.

IEC 61970, that is also called Common Information Model or short
CIM%\footnote{which is not to be confused with the Common Information Model for
%computer systems, which is a different standard}
, is a UML\footnote{Unified
Modeling Language} model that defines an ontology for power systems, containing
physical components like circuit breakers as well as more abstract concepts
like bids on a market. It provides a rich ontology for many different purposes,
and it provides a framework for defining XML messages that use the concepts of
the ontology, but does not define any messages. 
The development of custom extensions to the CIM is encouraged; they can be
incorporated into the standard at a later time.

Recent work has emphasized the use of services as a building block for control
systems for power systems. An example is the work on a smart inverter standard
\cite{SmartInverters10}.

%TODO Oliver Gehrke has laid the groundwork for the work on behaviour
%descriptions in his PhD thesis \cite{OG08}.


\lessSpaceAboveSections
\section{Synchronous and Asynchronous Communication}

The predominant communication form in today's power systems is
synchronous communication, where a
controlled device responds immediately to the control command of a controller.
An example for this is the classical closed-loop control
(figure~\ref{fig:closed-loop-control}), as employed e.g.~in the droop control of
a generator. Here, fast real-time communication between controller and controlled
system is important because effects of control impulses have to be visible to
the controller (via the feedback loop) as soon as possible.

\begin{figure}
  \centering%
  \includegraphics[width=0.9\linewidth]{Feedback_loop_with_descriptions.pdf}%
  %\vspace{-5pt}%
  \caption{Closed-loop control}\label{fig:closed-loop-control}%
\end{figure}

The opposite of synchronous communication is asynchronous communication, where 
an immediate response from the controlled component is not expected.

The difference between synchronous and asynchronous communication can be
demonstrated by looking at the structure of the aggregator example
introduced in section \ref{sec:introduction}. The specific structure of this 
example can be generalised to the structure shown in 
figure~\ref{fig:supervisory-control}, which identifies the three basic entities
device, device controller and supervisory controller/aggregator.
The communication between device controller (the home gateway) and the actual
device (the appliance) usually happens using synchronous communication. 
This is necessary because of tight timing requirements for the control loop.
The supervisory communication between supervisory controller (the aggregator)
and the device controller, on the other hand, can employ
asynchronous communication. The controller does not depend on an
immediate response from the device, which leads to a relaxed coupling between
the two parties. 

This decoupling also allows for more autonomous behaviour of the component;
the supervisory controller can delegate part of its decisions making to the 
controlled component. This might reduce the computational load on the supervisory 
controller. 

\subsection{A Hybrid Solution}

In order to use the full technical potential of a controllable energy resource, it has to 
be individually addressable (unicast), and the communication must be bidirectional in order 
to close the control loop. Depending on the services offered by the resource, a minimum 
bandwidth and/or maximum communication latency may be required. Generally, the communication 
must be reliable. If the number of controlled energy resources is large, communication must 
most importantly be low-cost.

For small resources such as controllable loads at the household level, these requirements are 
difficult to fulfil at the same time. Typically, a solution will be chosen based on cost, and 
the control system will be limited by the properties of the communication channel.

A solution to this dilemma can be found using a divide-and-conquer approach: If the individual 
communication acts, which together form a communication process, could be organised in such a 
way that each of the individual communication acts has lower requirements to the communication 
media compared to the whole communication process, a combination of different low-cost 
technologies could offer equal performance to a single, more expensive technology.

In particular, the communication for many control tasks can be separated into communication acts 
which require unicast addressing and bidirectional communication but are not time-critical, and 
other communication acts which demand reliable communication with low latency but can be served 
by a unidirectional broadcast medium. %\todo{Examples are power price and system frequency.} 
Both technologies are inexpensive and readily available: consumer DSL connections offer unicast 
and bidirectional communication but are unreliable and do not give any latency guarantees. 
Broadcast media have been used for the control of power systems for decades; the most common 
variants are FM transmitters and tone-frequency ripple control systems. These provide high 
reliability and defined latency, but do not support bidirectional communication or unicasting.

We will call the first type of communication act a ``behaviour description'' and the second type 
a ``trigger signal''. The trigger signal contains very little information, and is either a locally 
observable quantity or can be transmitted by unidirectional broadcast medium. The behaviour 
description, on the other hand, provides a flexible way to specify behaviour. 

\begin{figure}
  \centering%
  \includegraphics[width=0.9\linewidth]{supervisory-control.pdf}%
  %\vspace{-5pt}%
  \caption{Structure of example case}\label{fig:supervisory-control}%
\end{figure}


\lessSpaceAboveSections
\section{Behaviour Descriptions / Policies}

Behaviour descriptions facilitate the combination of synchronous and asynchronous communication in power systems:
The supervisory unit sends a description of \emph{expected
future} behaviour to the controlled unit, instead of sending just commands to
be executed immediately. The behaviour description tells the controlled unit
how to react to different kinds of trigger signals. It is important to
understand that this description can be sent to the controlled unit long
before it is actually needed. %\comment{A behaviour description encodes a 
%strategy for a component.}

A simple example for such an behaviour description is to couple consumption to
a power price: The appliances in a household will consume as much power as possible when the
power price is low, and try to reduce consumption when the price is high. This
depends of course on the constraints of the appliance: a heater has to provide a 
certain comfort, i.e. it should not be too hot or too cold in the room. 
Most behaviour descriptions will be more complex than this simple
example: Different kinds of behaviours can be tied to different kinds of
signals, and these can be ordered hierarchically, so that conflicting behaviours
are not activated simultaneously.

The execution of the behaviour description can happen mostly independent from
the supervisory unit, because the behaviour description already contains all
the information the controlled component needs. This makes additional communication between the
supervisory unit and the controlled unit in most cases unnecessary, and it
means that communication problems (too much latency, not enough bandwidth,
temporary complete failure) don't cause the difficulties they cause in control
systems that need synchronous communication. 

The trigger signals used to activate certain behaviours use one-way, broadcast communication, 
which can be realised both reliable and cost-effective.
% TODO Repeat trigger signal mantra: one-way, broadcast communication, which can be realised
% both reliable and cost-effective
%It also implies that the device
%must base its decisions mostly on events it can observe locally, like system
%frequency, which can just be measured at the point of common coupling, and
%power price, which is a single-value, one-way communication act and is
%therefore very well suited to be broadcast by various, highly reliable means.

A feature of behaviour descriptions is that they are negotiated individually
with each device, so that the capabilities and local constraints of the component
can be taken into account. The capabilities of a component tell the supervisory 
controller not only which services are supported, but also which local measurements
and events are available to the component.
As an example for constraints, consider the urgency of charging an electric
vehicle: An emergency
ambulance will always be charged as quickly as possible, whereas a vehicle that
is used only on weekends can usually be charged whenever the power price is best.

The provision of the event information, like system frequency and power price,
to the controlled device should be based on standards. The supervisory controller
can then just assume that this information is available. The IEC 61970 communication
standard \cite{iec61970-1} provides a power system ontology that could be used
to facilitate information retrieval in the local devices.

Behaviour descriptions can be interpreted as contracts: The supervisory
controller negotiates a certain behaviour with the controlled device, and both
parties agree on following the behaviour description: the device will follow
the terms of contract from the behaviour description, and the supervisory
controller will compensate the owner of the device in some way for the work the
device has done. When the contract is broken, some kind of penalty will have to
be paid, which can also be part of the behaviour description.
%\comment{; but in most cases this will be implicit, not explicit information}.


\lessSpaceAboveSections
\section{Negotiation Protocol}

The behaviour description approach needs two main features to be implemented: One
is the actual content of the behaviour descriptions; an example for that is
provided in the next section. The other is a way to negotiate these behaviour
descriptions. This is done using the behaviour description negotiation
protocol, which is similar to the Contract Net Interaction Protocol
\cite{contract-net-interaction-protocol} that has been specified by the
Foundation for Intelligent Physical Agents, FIPA.
The negotiation protocol provides the means for supervisory controller and
controlled component to exchange the information necessary to set up a
behavioural contract.

In the negotiation process, controlled components pass through 4 major states:
\begin{enumerate}
  \item Unconnected
  \item In negotiation
  \item Waiting to activate
  \item Active
\end{enumerate}
The supervisory controller will also track the state of the components it controls.

When a component is connected to the power system, it starts in the unconnected
state. It has to find the appropriate supervisory controller and connect to it. In the next
state, ``in negotiation'', the actual behaviour description is negotiated.
This is a process that consists of several steps in its own. First, the
component has to send all necessary information about itself to the supervisory controlleer.
The supervisory controlleer will then construct a behaviour description, and send it to the
component, which has to accept it.

The third state, ``waiting to activate'', lets the component wait until the
behaviour description becomes active. Behaviour descriptions have a
validity period, that might be sometime in the future.  When a behaviour
description is activated, the component switches over to the active state.
Some time before the validity period ends, the device will go back into the ``in
negotiation'' state, while still executing the behaviour of the active behaviour
description. The supervisory controller can also initiate a new negotiation round when
necessary.

The negotiation protocol must be able to handle communication failures
gracefully. To achieve this, a communication partner (supervisory controller or
controlled component) will restart the negotiation process when it notices that
it does not receive an expected message. Furthermore, messages are cryptographically
signed to detect message corruption and to prevent tampering.


\lessSpaceAboveSections
\section{Expressing Behaviour Descriptions}

Behaviour descriptions can be expressed in many different ways. The option that
was chosen for this article was rule-based systems
They use the simple concept of if-then rules to create complex
behaviour of a system. An example rule for our charging example is:
\texttt{\small if (power\_price > High\_Price) then reduce\_consumption.}

Rule-based systems have several features that make them a viable language
for behaviour descriptions:
\begin{itemize}
  \item Easy to understand for both computers and humans
  \item Expressive and flexible
  \item Rules can be used to describe complex behaviour
  \item Rules can be generated, e.g.~by an optimisation process
  \item Rules can be prioritised
\end{itemize}

The specific rule system used for this work is JBoss Drools \cite{jboss-drools}.
A behaviour description simply consists of a set of rules. This article suggests
activating power system services in the action part of a rule; the 
reasoning behind that is given in section \ref{sec:standards-services}. The 
condition part encodes the circumstances in which a service should be executed. 

The flexibility of rules comes from the fact that conditions as well as actions
can be adjusted to the specific circumstances. The only limitations
are the kind of information that is available to the rule (system frequency,
voltage, power price, \dots) and the types of actions the component supports
(power set points, droop settings, load schedules, \dots). Rule sets can be
arbitrarily complex, making complex behaviour possible. Thus, policies can be
used with different kinds of control strategies, such as market-based or
hierarchical approaches.

The possibility to prioritise rules makes it possible for a unit to act on
several levels of control: e.g.~can a unit be made follow a certain consumption
schedule (1$^{\text{st}}$ level), unless changes in the power price
(2$^{\text{nd}}$ level) or in the system frequency (3$^{\text{rd}}$ level)
make other behaviour necessary. 


\lessSpaceAboveSections
\section{Standardisation and Services}
\label{sec:standards-services}

The importance of standards has already been acknowledged in section
\ref{sec:related-work}. The main reason is that in a power system,
many different components from various manufacturers, having various capabilities,
belonging to different entities
(home owners, balance-responsible parties etc.), have to be coordinated. 

Behaviour descriptions should also be standardized, to enable interoperability
between components from different manufacturers.
Three separate parts of a system based on behaviour
description need standardizing:
\begin{enumerate}
  \item The behaviour description itself 
  \item The negotiation protocol: This needs a specification of the messages
    that are exchanged during behaviour negotiation; the behaviour description
    is an independent payload in this context
  \item The environment: this specifies the context the behaviour description
    will be executed in on the local component; it describes how a behaviour
    description based controller will get access to local measurements and
    external events, and how it can influence the actual behaviour of the
    component it is controlling.
\end{enumerate}

Both standards that were referred in section \ref{sec:related-work} can be used
here. IEC 61850 is useful when a component has to be controlled, while IEC
61970 can be used for the more generic descriptions of the behaviour
description, the messages of the negotiation protocol, and the description of
the environment.

%\todo{keep paragraph?
%Service-based architectures like SOA are widely suggested for Smart Grid
%integration, e.g.~\cite{postina10}. However, the term ``service'' usually
%refers to ICT services, such as web services. However, power system services, like
%ancillary services, have been used as a basic building block for control
%systems for a long time. This should also be reflected in the communication
%subsystem. Behaviour descriptions are very well suited to use power system
%services as a building block: In the rule-based system explained above, the
%action part of a rule could just activate a power system service. }

Using services as a building block also helps in making communication more
generic. The supervisory controller does not have to know the exact type of
each component it is controlling, it just needs to know which services each
component can provide, and how the services can be activated.

In this context, a broader understanding of the term ``power system service'' should be
applied. Traditionally it has been interpreted as applying only to
ancillary services. This understanding should be broadened to include all
services that are provided over the power grid, including the primary services
power generation and power consumption.


\lessSpaceAboveSections
\section{Example case}
\label{sec:example-case}

In this example, a behaviour description is used to control space heaters.
The behaviour descriptions activate three different kinds of services:
frequency control, price-based control, and thermostat control.

For this example, the rule base shown in figure \ref{fig:example-rulebase}
was created. It uses the syntax of the Drools rules engine and consists of 4 rules: 
\begin{enumerate}
  \item A setup rule that creates the three specific controllers, specifying
    the parameters for the services, such as the deadbands for the frequency
    and price-based controllers
  \item A rule for activating the frequency controller
  \item A rule for activating the price-based controller
  \item A rule for activating the thermostat controller
\end{enumerate}

\begin{figure}
  \begin{center}
    \lstinputlisting{policy.drl}
  \end{center}
  %\vspace{-5pt}%
  \caption{Simplified Example Rule Base}
  \label{fig:example-rulebase}
\end{figure}

The rule sets for each heater differed in the parameter values used in the 
setup rule, and in the existence of a frequency controller; some of the 
heaters were not offering that service. 

The last three rules for activating the various services are really simple.
This is due to two facts:
\begin{enumerate}
  \item Services were used in the action part of rules. The services can
    be executed autonomous, and the rule base is only used to select which 
    service to activate
  \item No local constraints are being enforced by the rule base; adding 
    constraints would add more terms to the conditions part of rules.
\end{enumerate}

The \texttt{activate} flag in the rules two and three refers to the 
internal feature of the controllers deciding whether that service should 
be activated. This depends on the deadband settings that were injected into
the service controllers in rule one.

\lessSpaceAboveSections
\section{Experiment}

An experiment has been conducted to show the feasibility of this approach. 
It was conducted in the SYSLAB system, a small experimental power grid
that can be used, among other things, to test distributed control strategies.
Part of SYSLAB is the PowerFlexhouse, an office building that is equipped with 
many sensors and actuators for light, space and water heaters, and air conditioning.

The experimental setup controlled the heaters in the eight rooms of
PowerFlexhouse individually using behaviour descriptions, using the behaviour
behaviour descriptions from section \ref{sec:example-case}. Each room was
controlled by its own client which offered maximally two services to the
system: a frequency control service, and a service that reacted to the dynamic
power price. The price reaction service was offered by all clients, but not all
offered the frequency control service. This was done to demonstrate that
behaviour descriptions can in fact be adapted to the capabilities of the
individual components. Both offered services are parametrised by a deadband 
parameter. The services are kept simple on purpose, because it was not the
purpose of the experiment to demonstrate complex control algorithms, but rather
to show that a system based on behaviour descriptions actually works, and to
evaluate how behaviour descriptions can combine simple services to allow for
more complex behaviour.

A server program running on a different computer calculated the behaviours for
the single rooms. The deadband values for the services are calculated in a way to
provide smooth reaction to changes in frequency and power price.

The policies for the frequency service change every two hours: Three different
widths of the frequency controller deadband are used. This was done to
demonstrate the flexibility of policies. 

\subsection{Results}

The plots in figure~\ref{fig:results-overall} contain measured data from a
single, 10-hour run of the experiment. During the experiment period, the power
price input signal decreases steadily with few and small upward excursions. The
system frequency input shows a slowly increasing trend, followed by a marked
drop towards the end of the period.

\begin{figure}
  \includegraphics[width=.85\linewidth]{graph2_powerconsumption.pdf}
  \caption{Overall results}
  \label{fig:results-overall}
\end{figure}

In the second subfigure, the overall power consumption of the building exhibits
a strong oscillatory pattern caused by the underlying thermostatic control of
the heaters. A correlation between power consumption and system frequency can
be observed. The price, on the other hand, does not appear to have a strong
impact on the consumption. This can be explained by the fact that the price
controller only acts as a secondary control loop, and is overridden whenever
the primary frequency controller is active. During the period of the
experiment, this was the case for most of the time.

The switching state of the individual heaters is shown in the bottom subplot.
Due to low outside temperatures during the experiment, some heaters can be seen
to be on almost all of the time.

Figure~\ref{fig:results-room6} details the behaviour of a single room during
the same experiment. In the first hours of the experiment, the temperature
setpoint (red) remains largely at 19\degree C due to the high power price and
low system frequency. The room temperature (black) follows the setpoint within
the hysteresis of the thermostatic control.

The second and third subplots show the relationship between the two input
signals - price and frequency - and the thresholds in effect according to the
active policy. The two-hour period for policy changes is easy to observe.

\begin{figure}
  \includegraphics[width=.85\linewidth]{graph1_room6.pdf}
  \caption{Results for Room \#6}
  \label{fig:results-room6}
\end{figure}

In hour 6 of the experiment, an increase in system frequency triggers the upper
frequency threshold. At nearly the same time, the power price begins to drop,
first below the upper, then below the lower threshold. This leads to
an almost constant temperature setpoint of 21\degree C for the
remainder of the experiment.

The observations demonstrate that individual clients do react to the frequency
and power policies sent to them through the policy framework. However, more and
longer experiments as well as a deeper analysis of measured data is needed to
determine and quantify the precise effect of changing policies. In the
experiment presented in this paper, the thermostatic control of the heaters and
external influences such as the outside temperature interfere with the effects
caused by the controller. Further statistical analysis is needed to separate
these effects.


\lessSpaceAboveSections
\section{Conclusion}

In this paper, behaviour descriptions or ``policies'' were introduced as a
way to structure control communication in power systems. Because the execution of the
behaviour is decoupled from the communication of the behaviour, communication
network problems, like a large increase in latency, or even a complete failure
of a link, do not impede the execution of the behaviour. This makes the whole
system much more robust. Rule systems can be quite complex, enabling
intelligent behaviour of the local component.

Behaviour Descriptions can be used with all kinds of control strategies; they 
provide a framework for the flexible activation of arbitrary control algorithms.

Looking back at the list of challenges from section \ref{sec:introduction}, it
can be seen that behaviour descriptions help to alleviate the problem of
unreliable communication links. It does so by decoupling the execution of
behaviour from the negotiation of that behaviour. Flexibility and extensibility
are supported by the standard-based and service-based approach, and the flexible
data format. 

Behaviour descriptions don't address scalability issues directly, but they do
lessen the computational and communication load on supervisory controllers,
except during the time periods when behaviour descriptions are created and negotiated.
Since the approach does not impose a certain control algorithm or control
structure, any of the other approaches mentioned in section
\ref{sec:related-work} can be used together with behaviour descriptions, as
long as they don't require permanent communication between supervisory
controller and the controlled components.

\bibliographystyle{IEEEtran.bst}
\bibliography{bibliography,standards}

\end{document}

