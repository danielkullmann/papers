#LyX 1.6.4 created this file. For more info see http://www.lyx.org/
\lyxformat 345
\begin_document
\begin_header
\textclass IEEEtran
\begin_preamble
\usepackage{listings}
\end_preamble
\options conference
\use_default_options true
\language british
\inputencoding auto
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\float_placement h
\paperfontsize default
\spacing single
\use_hyperref false
\pdf_title "Policy-based Control"
\pdf_author "Daniel Kullmann"
\pdf_subject "Introduces the background and topics of author's Ph.D. thesis"
\pdf_keywords "smart grid, inteligent energy systems"
\pdf_bookmarks true
\pdf_bookmarksnumbered false
\pdf_bookmarksopen false
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder true
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle true
\pdf_quoted_options "plainpages=false"
\papersize default
\use_geometry false
\use_amsmath 1
\use_esint 1
\cite_engine basic
\use_bibtopic false
\paperorientation portrait
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\defskip medskip
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\author "" 
\author "" 
\end_header

\begin_body

\begin_layout Title
An Introduction to Policy-based Control
\end_layout

\begin_layout Author
Daniel Kullmann, Henrik W.
 Bindner
\begin_inset Newline newline
\end_inset


\size normal
Risø National Laboratory for Sustainable Energy, Technical University Denmark
 (DTU)
\end_layout

\begin_layout Abstract
The increasing penetration of renewable and distributed energy resources
 makes new approaches to power system control necessary.
 Control implies communication, and that is where many issues of the current
 control system are coming from: Speed, reliability, flexibility and scalability
 of communication are necessary for the coordination of many units, as well
 as keeping down the costs.
 Yet new approaches to power system control that have been proposed in recent
 years have weaknesses in the communication issues.
 This article presents the concept of policy-based control that looks promising,
 especially in respect to the above-mentioned communication issues.
 
\end_layout

\begin_layout Section
Background
\end_layout

\begin_layout Standard
The challenges that the power system has to deal with, now and in the near
 future, make new approaches for the control of power systems necessary.
 In Denmark, the political goal of a 50% share of wind power in 2025 means
 that the wind penetration level has to double in only 16 years.
 The introduction of electric vehicles (EV) seems to be likely now; charging
 these new vehicles will put additional load on the power system.
 The limited long term security of supply for fossil resources means that
 we have to look towards alternative ways of generating energy.
 
\end_layout

\begin_layout Standard
Renewable energy resources are often smaller than their conventional counterpart
s, so more units are connected to the grid.
 The existing control structure was built having few large power plants
 in mind, but it must be able to deal with the increasing number of energy
 resources.
 These units have many different types with different characteristics and
 different types of services they can provide.
 
\end_layout

\begin_layout Standard
\begin_inset Float figure
placement b
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename flexibility.pdf
	width 95col%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Controlling many devices of different types with different services
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Standard
A major problem of renewable energy resources is that the production of
 energy fluctuates.
 This implies that it is important to control the consumption of energy
 as well, so that consumption can follow production.
 Many types of power consumers can be shifted in time, like fridges, freezers,
 air conditioning, warm water generation and heating (space and water).
 Stopping those units for a short time does not impact their function negatively.
 
\end_layout

\begin_layout Standard
The coordination of increasing numbers of resources is important to achieve
 the goal of a stable system.
 But many units do not offer a good way to control them yet.
 Especially household appliances consume power, when they want it; there
 is no way to tell them to stop or start consuming.
 Recently, small wind turbines have been enabled to contribute to the control
 of the power system, e.g.
\begin_inset space ~
\end_inset

by disconnecting from the grid in high-power situations.
 Most small energy resources are connected to the low-voltage distribution
 network, where monitoring equipment is still scarce.
 The trend is going towards having more information and control about energy
 resources
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
, e.g.
\begin_inset space ~
\end_inset

through smart metering
\end_layout

\end_inset

.
 This should be utilised to enable better power system control.
 Automation will play a key role there, because humans are bad at dealing
 with large amounts of information.
\end_layout

\begin_layout Standard
The currently dominating centralised control paradigms are not very well
 suited to deal with the problems that arise.
 They are prone to scaling problems when the number of controlled devices
 becomes very large.
 The controllers introduce single points of failures that could break the
 whole system when one of them ceases to function correctly.
 
\end_layout

\begin_layout Standard
Basically, control implies communication: when many units have to coordinate
 their behaviour to reach a common goal, they have to communicate.
 This means that communication is a major issue when trying to find new
 control structures for power systems.
 Therefore the goal of this article is to look at the current state of communica
tion in the power system, and to propose a new way of dealing with the problems.
\end_layout

\begin_layout Section
Problems to solve 
\begin_inset CommandInset label
LatexCommand label
name "sec:Problems-to-solve"

\end_inset


\end_layout

\begin_layout Standard
The major underlying objective for this Ph.D.
 project is to (1) enable large penetration levels of renewable energy resources
 in the power system.
 This implies other objectives: (2) fluctuating power production must be
 dealt with, and therefore (3) small energy resources must participate in
 the system control.
 These objectives lead to a number of problems to be solved, which will
 be introduced in this section.
\end_layout

\begin_layout Standard
When looking at the available means to provide communication inside the
 system, one of the main problems is that communication links are unreliable,
 i.e.
\begin_inset space ~
\end_inset

communication is not available at all times.
 Upgrading links for better reliability is expensive, so it is better to
 work with what is available.
 
\end_layout

\begin_layout Standard
There are two reliable ways of communication between the power system and
 resources: system frequency and voltage.
 They are available everywhere power is transferred.
 The problem with them is that only two specific values are transferred.
 Nevertheless, these values help when trying to stabilise a power system,
 because frequency and voltage contain information about the general health
 of the system.
\end_layout

\begin_layout Standard
Combining different communication means can make communication more expressive
 and reliable.
 This is easy when communication is low-bandwidth and one-way.
 Radio could e.g.
\begin_inset space ~
\end_inset

be used to transmit the current power price to all interested parties.
\end_layout

\begin_layout Standard
This information, system frequency, system voltage and energy price, can
 help creating intelligent behaviour of power systems.
\end_layout

\begin_layout Standard
A centralised control system introduces single points of failure into the
 system.
 A central controller is too important to fail, but a good control system
 should be able to handle localised failures, when small parts of the system
 fail, gracefully.
 
\end_layout

\begin_layout Standard
Aggregation of many distributed energy resources results in the possibility
 of their participation in power markets.
 Wind turbines, solar panels and small consumers have low marginal production
 costs, so they can profit from market participation.
\end_layout

\begin_layout Standard
There are already several standards available that define data models for
 usage in power systems (see section 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:State-of-the-Art"

\end_inset

).
 Standards play a vital role in an industry where devices from different
 companies have to work together.
 Unfortunately, the existing standards do not support more advanced control
 strategies.
 They have to be extended so that distributed power system control becomes
 possible.
\end_layout

\begin_layout Standard
To summarise: the basic problem that has to be solved is to define and build
 a control structure which
\end_layout

\begin_layout Itemize
handles many units, and can be scaled to handle more
\end_layout

\begin_layout Itemize
works using mostly unreliable communication links
\end_layout

\begin_layout Itemize
utilises available information about the power system
\end_layout

\begin_layout Itemize
handles units of many types providing different services
\end_layout

\begin_layout Itemize
is robust against failure, misuse and attacks
\end_layout

\begin_layout Itemize
allows human operators to review and to intervene
\end_layout

\begin_layout Itemize
enables market participation of energy resources
\end_layout

\begin_layout Itemize
is cost-effective
\end_layout

\begin_layout Itemize
can provide services to the grid
\end_layout

\begin_layout Standard
This is what new control systems have to provide.
\end_layout

\begin_layout Section
State of the Art
\begin_inset CommandInset label
LatexCommand label
name "sec:State-of-the-Art"

\end_inset


\end_layout

\begin_layout Standard
This section introduces several concepts for new control paradigms, some
 of the existing communication standards for power systems, and some important
 concepts from information and communication technology.
\end_layout

\begin_layout Subsection
Smart Grids
\end_layout

\begin_layout Standard
Smart Grids is a general term that refers to the expected wide-spread use
 of information and communication technology (ICT) in the power system.
 In the near future, most of the infrastructure of the power system should
 be monitored and controlled using ICT.
 The Modern Grid Initiative (MGI) developed a list of seven features that
 help to define smart grids 
\begin_inset CommandInset citation
LatexCommand cite
after "p.~8-9"
key "MGI07"

\end_inset

:
\end_layout

\begin_layout Enumerate
Self-healing: detect, analyse and respond to failures autonomously
\end_layout

\begin_layout Enumerate
Motivates and includes consumers
\end_layout

\begin_layout Enumerate
Resists attacks and natural disasters
\end_layout

\begin_layout Enumerate
Provide power quality for 
\begin_inset Formula $21^{st}$
\end_inset

 century needs
\end_layout

\begin_layout Enumerate
Accommodate all generation and storage options
\end_layout

\begin_layout Enumerate
Enables markets
\end_layout

\begin_layout Enumerate
Optimises assets and operates efficiently
\end_layout

\begin_layout Standard
\begin_inset Quotes eld
\end_inset

Smart Grid
\begin_inset Quotes erd
\end_inset

 is a helpful term when it comes to talk about the control of power systems,
 but it is a general term, and therefore does not offer tangible solutions
 to the stated issues.
\end_layout

\begin_layout Subsubsection
Address FP7
\end_layout

\begin_layout Standard
The Address project 
\begin_inset CommandInset citation
LatexCommand cite
key "B08"

\end_inset

 is an EU project working on enabling the Smart Grid.
 It examines the possibilities for active participation of the demand side
 in the power system.
 The main catchword is 
\begin_inset Quotes eld
\end_inset

Active Demand
\begin_inset Quotes erd
\end_inset

: the active participation of small consumers in power system markets, to
 enable large penetration levels of renewable energy resources in the power
 system.
 It started 2008 and will run until 2012.
 
\end_layout

\begin_layout Subsection
Microgrids
\end_layout

\begin_layout Standard
The term Microgrid refers to a local community grid, where small generators
 provide part of the local demand.
 It is a small-scale, more 
\begin_inset Quotes eld
\end_inset

grass-roots
\begin_inset Quotes erd
\end_inset

 approach than smart grids, which look at the power system as a whole.
 Microgrids should work both connected to the rest of the power system and
 in islanding mode.
 
\begin_inset Quotes eld
\end_inset

Microgrids
\begin_inset Quotes erd
\end_inset

 
\begin_inset CommandInset citation
LatexCommand cite
key "H06"

\end_inset

 and 
\begin_inset Quotes eld
\end_inset

More Microgrids
\begin_inset Quotes erd
\end_inset

 
\begin_inset CommandInset citation
LatexCommand cite
key "Microgrids"

\end_inset

 are two EU initiatives that research microgrids.
\end_layout

\begin_layout Standard
Microgrids look exclusively at local control, and therefore they are not
 much concerned with scaling issues.
 Whether several Microgrids can coordinate their work to better utilise
 renewable energy resources remains to be seen.
\end_layout

\begin_layout Standard
Dimeas 
\begin_inset CommandInset citation
LatexCommand cite
key "DH04"

\end_inset

 implemented a control system for a small Microgrid using a multi-agent
 system.
 In that system, all agents communicate with each other to find their price
 for power.
 A central agent collects the contracts of agents to ensure accountability.
 Scalability issues are not directly addressed in this work, but further
 work is done in that direction.
\end_layout

\begin_layout Subsection
Cells
\end_layout

\begin_layout Standard
The cell project 
\begin_inset CommandInset citation
LatexCommand cite
key "CellProject"

\end_inset

 is carried out by energinet.dk, the Danish transmission system operator.
 The idea is to divide the power system into cells, which are defined as
 the distribution network below a 150/60 kV feeder.
 These cells are controlled by an autonomous cell controller, which can
 provide all necessary system management functions in the case of a power
 failure in the transmission network.
 The control system is based on a multi-agent system.
\end_layout

\begin_layout Standard
The feasibility of this approach has already been demonstrated in several
 experiments in Denmark.
 Whether it will scale to larger power systems still has to be shown.
\end_layout

\begin_layout Subsection
Virtual Power Plants
\end_layout

\begin_layout Standard
This is a concept that tries to reduce the complexity of having many small
 energy producers, by combining these energy resources into one big one.
 Energy resources might be both generators and loads, but the emphasis lies
 on generation.
\end_layout

\begin_layout Standard
The main difference to cells is that the devices that make up a virtual
 power plant can be distributed over a larger area, whereas a cell is restricted
 to a certain area.
\end_layout

\begin_layout Standard
FENIX 
\begin_inset CommandInset citation
LatexCommand cite
key "DPCR06"

\end_inset

 is a project that researches the incorporation of a large number of distributed
 energy resources into the power system, by aggregating their generating
 resources and service capabilities using virtual power plants.
 The project is looking at the technical, commercial and regulatory implications
 of virtual power plants.
 
\end_layout

\begin_layout Subsection
Aggregation
\end_layout

\begin_layout Standard
The PowerMatcher project 
\begin_inset CommandInset citation
LatexCommand cite
key "KWK+06"

\end_inset

 from ECN in the Netherlands developed a market-based control concept for
 energy networks.
 Each device is represented by an agent, and several agents are aggregated
 by so-called PowerMatcher nodes.
 At the top of the aggregation hierarchy sits the root node, which is connected
 to markets.
 When a new price finding is necessary, the root node sends a request for
 bids through the aggregation tree.
 The bids are aggregated and sent up the tree again.
 The root node then calculates the equilibrium price, which is sent back
 to the devices.
 The price is used by device agents to determine the power used by the device.
 Agents have to stick to the price that they originally bid with, so they
 have to wait for the next market period if they want to change their behaviour.
\end_layout

\begin_layout Subsection
Communication Standards in Power Engineering
\end_layout

\begin_layout Standard
The International Electrotechnical Commission (IEC) has published a number
 of standards that deal with communication in the power system.
 
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
The standards are in evolution, so changes to enable flexible communication
 between units are possible.
 
\end_layout

\end_inset

In June 2009, the Electric Power Research Institute (EPRI) published a report
 
\begin_inset CommandInset citation
LatexCommand cite
key "DVDea09"

\end_inset

 that looks at all the standards that could be involved in the working of
 the power system, and at the gaps that have to be filled.
 Here is a list of three important existing standards.
\end_layout

\begin_layout Subsubsection
IEC 61850
\end_layout

\begin_layout Standard
This standard defines an information model to describe the components of
 a power system, down to power cables and breakers.
 It can be used to monitor and control power system components.
 There are several mappings from the abstract data model of IEC 61850 to
 communication standards such as MMS.
\end_layout

\begin_layout Standard
IEC 61850 describes the structure of the system, and gives a supervisory
 unit the possibility to request information and to change set points.
 It lacks more abstract ways of communication between parts of the system.
\end_layout

\begin_layout Subsubsection
IEC 61400-25
\end_layout

\begin_layout Standard
This standard uses the information model defined by IEC 61850 to define
 an information model for wind turbines.
 It can be used for monitoring and control of wind power plants.
 
\end_layout

\begin_layout Subsubsection
IEC 61970
\end_layout

\begin_layout Standard
This standard defines the Common Information Model (CIM), a UML
\begin_inset Foot
status collapsed

\begin_layout Plain Layout
Unified Modeling Language, 
\begin_inset Flex URL
status open

\begin_layout Plain Layout

http://www.uml.org/
\end_layout

\end_inset


\end_layout

\end_inset

 data model that can be used to describe power systems.
 It differs from IEC 61850 in its scope and the way the data model is defined,
 but many parts of the two standards overlap as well.
 CIM is a purely structural model, and does not contain means to send control
 commands.
\end_layout

\begin_layout Subsection
\begin_inset CommandInset label
LatexCommand label
name "sub:ECA-Rules"

\end_inset

Expert systems
\end_layout

\begin_layout Standard
An expert system is a relatively simple approach to implement reasoning
 in software systems.
 It encodes expert knowledge about a certain domain in rules.
 This can be done using event-condition-action (ECA) rules 
\begin_inset CommandInset citation
LatexCommand cite
after "p.~46"
key "aima"

\end_inset

.
 Those rules contain three parts:
\end_layout

\begin_layout Enumerate
An external event; when the event is triggered (e.g.
\begin_inset space ~
\end_inset

an observable variable changes) the rest of the rule is evaluated
\end_layout

\begin_layout Enumerate
A condition that decides whether the following action should be executed
\end_layout

\begin_layout Enumerate
An action that is executed when the condition evaluates to true
\end_layout

\begin_layout Standard
Expert systems are quite effective in dealing with many problem settings;
 what they don't support is planning ahead.
\end_layout

\begin_layout Subsection
\begin_inset CommandInset label
LatexCommand label
name "sub:Multi-Agent-Systems"

\end_inset

Multi-Agent Systems
\end_layout

\begin_layout Standard
Multi-agent systems are a concept that comes from the combination of distributed
 systems with artificial intelligence.
 Agents are autonomous computer programs that communicate with each other,
 and multi-agent systems can be geographically distributed.
 They are a popular approach in the research of intelligent power systems
 (see e.g.
\begin_inset space ~
\end_inset


\begin_inset CommandInset citation
LatexCommand cite
key "Rehtanz03"

\end_inset

 and 
\begin_inset CommandInset citation
LatexCommand cite
key "DH04"

\end_inset

).
\end_layout

\begin_layout Standard
A system is called an agent system when it has certain properties 
\begin_inset CommandInset citation
LatexCommand cite
key "PW04"

\end_inset

:
\end_layout

\begin_layout Itemize
Situated: it is situated in an environment and can react to percepts by
 executing actions
\end_layout

\begin_layout Itemize
Autonomous: it acts on its own
\end_layout

\begin_layout Itemize
Reactive: It reacts to changes in its environment
\end_layout

\begin_layout Itemize
Proactive: It pursues its goals
\end_layout

\begin_layout Itemize
Flexible: It has several plans to achieve its goals
\end_layout

\begin_layout Itemize
Robust: It can deal with failure, and try different ways to achieve its
 goals
\end_layout

\begin_layout Itemize
Social: The agents that make up an agent system communicate with each other
 to pursue their goals
\end_layout

\begin_layout Standard
Multi-agent systems provide a useful abstraction for the problem of controlling
 power systems, as can be seen by looking at the list of properties.
 One issue that can be debated is whether single units should be seen as
 truly autonomous.
 There has to be some kind of coordination between the units, and this constrain
s their freedom to act on their own will.
 But they still are autonomous in the sense that they have to deal with
 local issues on their own.
 
\end_layout

\begin_layout Standard
It is important to understand that multi-agent systems are distributed systems
 where actions are not necessarily closely coordinated.
 Agents act autonomously to reach a common goal, and will communicate with
 other agents, but not to the extent that each action the agent takes will
 first be coordinated with other agents in the system.
\end_layout

\begin_layout Standard
The Foundation for Intelligent Physical Agents FIPA
\begin_inset Foot
status collapsed

\begin_layout Plain Layout
\begin_inset Flex URL
status open

\begin_layout Plain Layout

http://www.fipa.org
\end_layout

\end_inset


\end_layout

\end_inset

 has published a number of specifications to standardise agents.
 They cover a wide range of topics, e.g.
\begin_inset space ~
\end_inset

agent communication and agent management.
\end_layout

\begin_layout Standard
The Java Agent Development Environment JADE
\begin_inset Foot
status collapsed

\begin_layout Plain Layout
\begin_inset Flex URL
status open

\begin_layout Plain Layout

http://jade.tilab.com/
\end_layout

\end_inset


\end_layout

\end_inset

 is a Java framework for developing agent systems that implements the FIPA
 specifications.
 It is used to implement this system.
\end_layout

\begin_layout Section
Policy-based control
\begin_inset CommandInset label
LatexCommand label
name "sec:policy-based-control"

\end_inset


\end_layout

\begin_layout Standard
Policy-based control is a concept that tries to address the issues mentioned
 in the previous sections.
 The main idea behind it is to decouple the agreement on behaviour from
 the execution of that behaviour.
 A device is told in advance how to behave in certain situations.
 This description of behaviour is the policy.
 The device then acts on this policy, regardless of the availability of
 communication to the supervisory controller.
 Policies are used between units that have to coordinate their behaviour,
 e.g.
 an energy resource and an aggregator unit.
 Policies decouple the controller and device from each other.
 Most devices will still have a device controller using a closed-control
 loop to monitor the device, but the communication between supervisory controlle
r and device controller will be policy-based.
 This means that part of the control stays close to the device, and another
 part of the control can move away from the device (see figure 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:From-closed-loop-control"

\end_inset

).
\end_layout

\begin_layout Standard
\begin_inset Float figure
placement t
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename closed-loop-solution.pdf
	width 95col%
	groupId figures

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
\begin_inset CommandInset label
LatexCommand label
name "fig:From-closed-loop-control"

\end_inset

From closed-control loop to policy-based control
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Standard
A way to understand policies is to view them as contracts: devices that
 have to cooperate in a certain way agree on a contract.
 This ensures them that the other party will behave correctly in the situations
 covered by the contract.
 Devices operated by different companies could use policies to encode the
 contracts that were concluded between these companies.
 Contracts are often only implicitly encoded in the programming of the devices.
 When making the contract explicit, software can reason about the contract,
 which makes more autonomous behaviour possible.
 
\end_layout

\begin_layout Standard
A key feature of policy-based control is that policies are very flexible.
 Since the behaviour description is rather abstract, different types of
 units can react to the same policy, and different kinds of behaviours can
 be implemented using policies.
\end_layout

\begin_layout Subsection
ECA rules
\end_layout

\begin_layout Standard
There are many ways how policies could be actually implemented.
 One could use very general policies, the most extreme being 
\begin_inset Quotes eld
\end_inset

keep the power system stable
\begin_inset Quotes erd
\end_inset

, or very specific policies that amount to setting some set points.
 These options are not very flexible, and they can't be extended to handle
 new situations.
 What is necessary is a more general approach.
\end_layout

\begin_layout Standard
A good way of representing policies is to use event-condition-action rules
 (see section 
\begin_inset CommandInset ref
LatexCommand ref
reference "sub:ECA-Rules"

\end_inset

).
 The three parts of an ECA rule for power system policies are as follows
 (see figure 
\begin_inset CommandInset ref
LatexCommand ref
reference "fig:policy-structure"

\end_inset

):
\end_layout

\begin_layout Paragraph
Events
\end_layout

\begin_layout Standard
This is the list of input signals that is of interest for this specific
 rule.
 A rule is only taken into consideration when one of its input events has
 triggered, i.e.
 changed its value.
 A simple example is the power frequency.
 Timer events are also possible (e.g.
 when a rule should be checked periodically).
\end_layout

\begin_layout Paragraph
Condition
\end_layout

\begin_layout Standard
The condition decides whether the action of this rule is appropriate to
 handle the current system condition.
 A simple example is to check whether the power frequency falls outside
 a certain safe range, like 49.75-50.25 Hz.
\end_layout

\begin_layout Standard
Conditions are essentially Boolean expressions.
 They consist of the normal Boolean operators, comparison operators, and
 the values that
\color red
 
\color inherit
can be compared.
 These values can either be constants defined in the policy, values of event
 signals (e.g.
 system frequency), and other values derived somehow from the environment
 (e.g.
\begin_inset space ~
\end_inset

using an external function call).
\end_layout

\begin_layout Standard
The condition basically answers two questions: Is the current situation
 interesting for this rule, and can this rule do something about that situation?
 
\end_layout

\begin_layout Paragraph
Action
\end_layout

\begin_layout Standard
The action is executed when the condition of the rule evaluates to true.
 It handles the situation that the condition describes.
 The possible actions depend on the type of the energy resource.
 A simple example for an action is to make a consumer unit increase or decrease
 its consumption.
 
\end_layout

\begin_layout Standard
\begin_inset Float figure
placement b
wide false
sideways false
status open

\begin_layout Plain Layout
\align center
\begin_inset Graphics
	filename policy-structure.pdf
	width 95col%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption

\begin_layout Plain Layout
Structure of ECA policies
\begin_inset CommandInset label
LatexCommand label
name "fig:policy-structure"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Paragraph
Disambiguation
\end_layout

\begin_layout Standard
When a policy consists of several rules, it is generally possible that several
 rules apply to the current set of conditions.
 In this case, a conflict-resolution system must decide which action(s)
 are to be executed.
 This could be a very simple process, by just using the first rule that
 matches, or an elaborate one in which conflict resolution or planning is
 involved.
 
\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
Generally, the effect of a rule system should be easy to predict, because
 that is the main reason to use a simple system.
\end_layout

\end_inset


\end_layout

\begin_layout Subsection
Synchronised Behaviour
\end_layout

\begin_layout Standard
It can be problematic when many units react in unison to changes in the
 power system.
 As an example, consider 1000 small loads of 5 kW each that all switch off
 at 49.9 Hz.
 Now, when that threshold is reached, the system load is suddenly decreased
 by 5 MW.
 This will push the frequency up again, which will make all the units take
 up their load again.
 This results in a vicious cycle, which can destabilise the power system.
\end_layout

\begin_layout Standard
Policies can be used to prevent this from happening, by staggering the response
 of units.
 Instead of setting the frequency threshold of all units to 49.9 Hz, the
 threshold can be staggered in small steps from e.g.
\begin_inset space ~
\end_inset

49.9 Hz to 49.75 Hz.
 This means only a part of the units will respond to a threshold, thereby
 reducing the oscillation effect described above.
\end_layout

\begin_layout Standard
This means that policies enable the usage of broadcast messages together
 with individual responses: Every unit in the system gets the same broadcast
 message, but each unit has its own way to deal with it.
 This way, the specifics of units can be taken into account, e.g.
\begin_inset space ~
\end_inset

local constraints that the unit has to obey.
\end_layout

\begin_layout Subsection
Intelligent behaviour
\end_layout

\begin_layout Standard
How intelligent can single units be in a such a system? We want the whole
 system to be as smart as possible, but intelligent behaviour on lower levels
 of the system makes it hard for the higher levels of the system to anticipate
 their behaviour.
 An extreme example of how intelligent behaviour can sometimes be not as
 convenient as we'd like it to be are neural nets.
 They often give good results in decision processes, but they give no reasons
 for their decisions, so it is hard to evaluate how good a decision is.
\end_layout

\begin_layout Standard
How is the intelligence distributed among the different units? Three general
 approaches are possible: 1) aggregation nodes are intelligent; 2) device
 nodes are intelligent; 3) both have intelligent behaviour.
\end_layout

\begin_layout Standard
It is probably more sensible to put more intelligence into the aggregation.
 This is mainly because they have more information about the system, so
 it can be expected that they make better decisions.
\end_layout

\begin_layout Subsection
Implementation of ECA rules using a multi-agent system
\end_layout

\begin_layout Standard
A multi-agent system seems to be be well suited for implementing a policy-based
 control system with ECA rules.
 This is because multi-agent systems provide the concepts necessary to implement
 such a system: The controllers are represented by agents, and those agents
 exchange messages negotiating policies.
\end_layout

\begin_layout Section
Case Application
\end_layout

\begin_layout Standard
In this section, an example for a policy for charging electric vehicles
 is introduced.
 The electric vehicle is mostly used for commuting, and it is connected
 to a charging post when not used, at home as well as at work.
 The charging post contains a computer system responsible for controlling
 the charging.
 It decides on the optimal time for charging the car, depending on several
 pieces of information, such as the time at which the car is needed again,
 the state of charge it must have at that time and its current state of
 charge.
 Communication happens between the vehicle and the charging post, and between
 the charging post and a power market or a DSO.
 
\end_layout

\begin_layout Subsection
Example policy
\end_layout

\begin_layout Standard
A simple policy for this setup is illustrated by the following set of rules,
 where the vehicle is supposed to charge only when it is beneficial to do
 so:
\end_layout

\begin_layout Enumerate
Low frequency leads to stopping of charging
\end_layout

\begin_layout Enumerate
Normal and high frequency leads to starting of charging
\end_layout

\begin_layout Enumerate
High power price leads to stopping of charging
\end_layout

\begin_layout Enumerate
Low power price leads to starting of charging
\end_layout

\begin_layout Standard
We need to disambiguate the usage of rules; in this example, the rules are
 checked in order, and the first rule to apply is used; all rules coming
 after a matching rule are ignored.
 This policy can easily be encoded in a message, e.g.
\begin_inset space ~
\end_inset

in an XML format.
\end_layout

\begin_layout Section
Conclusion
\end_layout

\begin_layout Standard
This article showed that policies provide a flexible approach to more decentrali
sed control.
 They can be used to deal with the problems listed in section 
\begin_inset CommandInset ref
LatexCommand ref
reference "sec:Problems-to-solve"

\end_inset

.
 The policy-based approach is a compromise between decentralised control
 and coordination.
 It removes both communication and computational load from aggregators,
 and it accepts unreliable communication.
\end_layout

\begin_layout Standard
For the policy-based approach to work, it is necessary that local units
 can come to a decision without having access to global state information
 about the power system.
 The system frequency can e.g.
 be used to give important information about the state of the whole system.
\end_layout

\begin_layout Standard
Do policies provide a useful abstraction of the problem, i.e.
\begin_inset space ~
\end_inset

does it help to think of the communication between nodes on higher levels
 as the exchange of policies? It seems to be the case, because policies
 provide a general framework to exchange information about how the power
 system is controlled.
 This information is furthermore made explicit, so it can be reasoned about
 by computer systems, and checked by humans.
\end_layout

\begin_layout Standard
\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "/home/daku/projects/papers/bibliography"
options "ieeetr"

\end_inset


\end_layout

\end_body
\end_document
