#LyX 1.6.5 created this file. For more info see http://www.lyx.org/
\lyxformat 345
\begin_document
\begin_header
\textclass article
\begin_preamble
\pagestyle{empty}
\date{}
\end_preamble
\use_default_options true
\language british
\inputencoding utf8
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\paperfontsize default
\spacing single
\use_hyperref true
\pdf_title "Conditions for using policy-based control"
\pdf_author "Daniel Kullmann, Henrik Bindner"
\pdf_bookmarks true
\pdf_bookmarksnumbered false
\pdf_bookmarksopen false
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder true
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle true
\papersize a4paper
\use_geometry false
\use_amsmath 1
\use_esint 1
\cite_engine basic
\use_bibtopic false
\paperorientation portrait
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\defskip medskip
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\author "" 
\author "" 
\end_header

\begin_body

\begin_layout Title
Elements to enable high-level communication for power system control
\end_layout

\begin_layout Author
Daniel Kullmann, Risø DTU, Denmark
\begin_inset Newline newline
\end_inset

Henrik Bindner, Risø DTU, Denmark
\end_layout

\begin_layout Standard
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash
thispagestyle{empty}
\end_layout

\end_inset


\begin_inset VSpace bigskip
\end_inset


\end_layout

\begin_layout Standard
\align center
Daniel Kullmann
\begin_inset Newline newline
\end_inset

Frederiksborgvej 399
\begin_inset Newline newline
\end_inset

4000 Roskilde
\begin_inset Newline newline
\end_inset

Denmark
\begin_inset Newline newline
\end_inset

daku@risoe.dtu.dk
\begin_inset Newline newline
\end_inset

Tel +45 4677 5076
\begin_inset Newline newline
\end_inset

Fax +45 4677 5083
\end_layout

\begin_layout Standard
\align center
\begin_inset Newpage pagebreak
\end_inset


\end_layout

\begin_layout Standard
\align center

\size largest
Elements to enable high-level communication for power system control
\end_layout

\begin_layout Abstract
The changes that happen in the power system, towards larger number of production
 units, and towards having the consumers participate actively in the power
 system, make new ways to control such systems necessary.
 One new approach is policy-based control.
 In its essence, policy-based control means that components in the power
 system act according to a set of behaviour rules (a policy), which are
 negotiated between a unit and its supervising unit.
 This poster investigates some of the prerequisites necessary for using
 policy-based control, amongst others standards for communication and ontologies
 as basis for communication.
\end_layout

\begin_layout Standard
The idea of policy-based control
\begin_inset CommandInset citation
LatexCommand cite
key "PbCSubmitted"

\end_inset

 comes from the observation of a number of facts: 
\end_layout

\begin_layout Itemize
In future, many units have to controlled
\end_layout

\begin_layout Itemize
Those units have different types and are able to provide different services
\end_layout

\begin_layout Itemize
The existing communication infrastructure should be used 
\end_layout

\begin_layout Itemize
This communication infrastructure has limited quality concerning availability,
 bandwidth and latency
\end_layout

\begin_layout Standard
In policy-based control, descriptions of behaviours are negotiated between
 units that have to coordinate their behaviour, before that behaviour is
 actually needed.
 A unit then acts according to this behaviour, so the need for communication
 between the unit and its supervisor is reduced.
 The description of behaviour must be flexible to support different types
 of behaviour, so a rule-set approach was adopted.
\end_layout

\begin_layout Paragraph*
Communication
\end_layout

\begin_layout Standard
Policies are an abstract way of communication, and when doing communication
 one has to answer two questions: What is communicated? How is it communicated?
\end_layout

\begin_layout Standard
The first question concerns the structure of the messages and the ontology
 the messages are based upon.
 An ontology is necessary because the communication is high-level, and high-leve
l constructs have to be defined in a precise manner, so that communicating
 parties have a common understanding of the concepts.
 The ontology should be based on standards, because it must be universally
 accepted to be useful.
\end_layout

\begin_layout Standard
The second question boils down to which standards are used to communicate.
 Since power systems consist of many different units from many different
 vendors, standards are a necessity.
\end_layout

\begin_layout Paragraph*
Messages
\end_layout

\begin_layout Standard
Different kinds of information are transported in messages used for policy-based
 control, such as: 
\end_layout

\begin_layout Itemize
static and dynamic information about components (e.g.
\begin_inset space ~
\end_inset

power rating, current and forecasted state)
\end_layout

\begin_layout Itemize
topology information (how components are interconnected)
\end_layout

\begin_layout Itemize
service descriptions (e.g.
\begin_inset space ~
\end_inset

frequency control or load balancing)
\end_layout

\begin_layout Itemize
policies (a set of rules describing a behaviour)
\end_layout

\begin_layout Paragraph*
Ontologies
\end_layout

\begin_layout Standard
An ontology provides the basis for the communication between nodes.
 Several pieces of information must be present in an ontology for power
 systems:
\end_layout

\begin_layout Itemize
How to describe static and dynamic information about components: which fields
 are available, and what units the values for these fields have (e.g.
 power rating in kW)
\end_layout

\begin_layout Itemize
How to describe topology information: components, connectors, cables and
 their composition
\end_layout

\begin_layout Itemize
How to describe services (see below)
\end_layout

\begin_layout Paragraph*
Service descriptions
\end_layout

\begin_layout Standard
Service descriptions come in two flavours: The first are abstract service
 definitions, such as frequency control, that describe the basic function
 of the service.
 The second are concrete service descriptions as defined in grid codes or
 elsewhere.
 They are refined from the abstract definitions.
\end_layout

\begin_layout Paragraph*
Standards
\end_layout

\begin_layout Standard
In the domain of smart grids, the two IEC standards 61970 and 61850 are
 very important.
 Several standardisation road maps that have been published recently, such
 as 
\begin_inset CommandInset citation
LatexCommand cite
key "IECRoadMap"

\end_inset

, have recognised this fact.
 It must be investigated whether the existing standards provide sufficient
 means to support higher-level communication.
 Parts of the standards can already be used, e.g.
\begin_inset space ~
\end_inset

for setting up ontologies for power systems.
\end_layout

\begin_layout Paragraph
Implementation and Testing
\end_layout

\begin_layout Standard
The policy-based control system must be implemented and tested to prove
 its feasibility.
 A framework for exchanging policies has already been implemented in Java,
 and several simple use cases have been implemented on that basis to check
 the design.
 A more complex example that uses actual generators and loads in Risø's
 experimental power system SYSLAB
\begin_inset CommandInset citation
LatexCommand cite
key "SYSLAB07"

\end_inset

 is under development right now.
 Ontologies and service descriptions will be incorporated into this system.
 Standard-based communication support is planned.
\end_layout

\begin_layout Standard
\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "/home/daku/projects/papers/bibliography"
options "plain"

\end_inset


\end_layout

\end_body
\end_document
