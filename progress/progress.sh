#!/bin/bash

exec > progress.log 2>&1

CONTINUE=

if test "$1" == "-c"; then
  CONTINUE=1
fi

if test ! -f figures/; then
  ln -s ../../figures/ .
fi

if test -z $CONTINUE; then
  rm -f progress.txt wordcount.txt
  if test -d p/thesis; then
    cd p/thesis
    hg pull -u
  else
    hg clone ~/projects/papers p
    cd p/thesis
  fi
else
  cd p/thesis
  hg pull
fi

if test -n "$CONTINUE"; then
  REVSET="sort(descendants(children(.)),date)"
else
  REVSET="sort(all(),date)"
fi
REVS=$(hg log -r "$REVSET" --template "{rev}\n" .)
echo revs: $REVS

for rev in $REVS; do
  echo ==== rev $rev
  hg revert --all --no-backup
  hg up -r $rev
  if test ! -d generated; then mkdir generated; fi
  echo ==== build daku-thesis.pdf
  yes "" | make -B -i daku-thesis.pdf 1>> /dev/null
  if test $rev -ne 589 && test $rev -ne 590; then
    echo ==== build just-the-text.pdf
    while true; do echo ""; echo "\\end{document}"; done | make -B -i just-the-text.pdf 1>> /dev/null
  fi
  echo -n $(hg log -r "$rev" --template "{date|isodate}" | sed -e "s/ /-/g") "" >> ../../progress.txt
  echo -n $(pdfinfo -meta _output/daku-thesis.pdf | grep Pages: | egrep -o "[0-9]+") "" >> ../../progress.txt
  echo -n $(pdfinfo -meta _output/just-the-text.pdf | grep Pages: | egrep -o "[0-9]+") "" >> ../../progress.txt
  echo "" >> ../../progress.txt
  echo -n $(hg log -r "$rev" --template "{date|isodate}" | sed -e "s/ /-/g") "" >> ../../wordcount.txt
  echo -n $(detex -n part*.tex unfiled*.tex acronyms.tex | wc -w | sed -e 's/  \+/ /g') "" >> ../../wordcount.txt
  echo -n $(pdftotext _output/daku-thesis.pdf - | wc -w | sed -e 's/  \+/ /g') "" >> ../../wordcount.txt
  echo "" >> ../../wordcount.txt
  rm -rf _output generated *.pdf
done

cd ../../

sed -i -e 's/-+0[0-9]00 / /' progress.txt
sed -i -e 's/-+0[0-9]00 / /' wordcount.txt
gnuplot plot.gnuplot



