set terminal png
set output "progress.png"
set xdata time
set timefmt "%Y-%m-%d-%H:%M"
set format x "%m"
set xlabel "Date"
set ylabel "# of pages"
set grid
plot "progress.txt" using 1:2 with lines title "Thesis",\
     "progress.txt" using 1:3 with lines title "Text"

set output "wordcount.png"
set ylabel "# of words"
plot "wordcount.txt" using 1:2 with lines title "Words (text)",\
     "wordcount.txt" using 1:3 with lines title "Words (PDF)"

