%\documentclass[journal]{IEEEtran}
\documentclass[journal,12pt,draftclsnofoot,onecolumn]{IEEEtran}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{amsmath} 
\usepackage{color} 
\usepackage{mdwlist} 
\interdisplaylinepenalty=2500

\newcommand{\todo}[1]{\textcolor{red}{#1}}
\newcommand{\comment}[1]{}

% To finally squeeze the articel into two pages
\newcommand{\lessSpaceAboveSections}{}%\vspace{-5pt}}

\pagestyle{empty}

\begin{document}

\title{Asynchronous Communication for the Smart Grid}
\author{Daniel~Kullmann, Oliver~Gehrke and Henrik~Bindner%
\thanks{Daniel Kullmann, Oliver Gehrke and Henrik Bindner all work at Risø DTU National Laboratory for Sustainable Energy, Roskilde, Denmark}}

\markboth{}{}
\maketitle
\thispagestyle{empty}

\begin{abstract}
  Today, electrical power systems face the challenge to integrate large numbers of small, renewable and distributed energy resources. The control structure needed to coordinate such a system needs a sophisticated communication system to help it cope with the requirements and constraints of a dynamically changing distributed system. Asynchronous communication alleviates several of the problems, and allows to decouple a controlled device from its controller.
  This article presents behaviour descriptions, a concept that enables advanced asynchronous communication.
\end{abstract}

\begin{IEEEkeywords}
  Smart grids, Communication systems, Intelligent systems
\end{IEEEkeywords}

\lessSpaceAboveSections
\section{Introduction}

Today, electrical power systems face the challenge to integrate large numbers of renewable energy sources (RES). The need to compensate for the fluctuations inherent in the power production of RES led to the desire to include the consumption side actively into the control of the system. This means that large numbers of very different components, located mostly at the distribution level, have to be controlled at the same time.

The term Smart Grid has been coined to describe the challenges the electrical power system faces. It has no single authoritative definition, but most descriptions (like in \cite{MGI07-b}) agree on the some common elements: Usage of information and communication technology (ICT), inclusion of the consumption side, improvement of grid reliability, enabling market-driven approaches, inclusion of different kinds of devices (generation, consumption and storage).

Control implies communication: components distributed over the whole system have to be coordinated to achieve the goal of a stable system. Different \comment{(``Various'' does not fit)} control system concepts put different demands on the underlying communication system: Most importantly, the communication has to be fast enough for the specific control system, both in terms of latency and bandwidth. The existing communication infrastructure can also make it necessary to adapt the control system used: When communication failures can happen, the control system must be able to handle them gracefully. 

%% Next paragraph is not really relevant to statement of paper:
% All the components connected to the grid not only come from many different suppliers and have different types, they can also offer a number of simple and complex services, such as ancillary services, to the grid. The capabilities of the components should be fully utilised to make their use most effective.

\lessSpaceAboveSections
\section{State of the Art}

A number of approaches have been proposed to realise the Smart Grid. Some of them use power markets as a central concept, such as the PowerMatcher system \cite{KWK+06} or the DEZENT project \cite{WeddeLRK08}. Other approaches split up the system into smaller parts that are easier to control; examples for these are Microgrids \cite{Microgrids} and energinet.dk's Cell project~\cite{CellProject}.

Aggregation approaches have been suggested as a way to cope with the complexity of the system%, both on the consumption side and on the generation side. 
An example for this is the research on Virtual Power Plants (VPPs), e.g.~\cite{DPCR06}. 
%Aggregation is greatly simplified when components can be accessed through unified interfaces; \cite{OG08} proposes a role-based approach to organise components in such a fashion. 
%By this, the actual type of the component is abstracted away, and only capabilities relevant to the task are considered.

All these approaches assume implicitly that communication just happens; the possibility of failure is silently ignored. Yet, a control structure that allows failures to happen increases the reliability of a system.

Furthermore, control systems often use synchronous communication, where a controlled device responds immediately to the control command of a controller. An example for this is the classical closed-loop control (Fig.~\ref{fig:closed-loop-control}), as employed e.g.~in the droop control of a generator. Here, real-time communication between controller and controlled system is important because effects of control impulses have to be visible to the controller (via the feedback loop) as soon as possible.

\begin{figure}
  \centering%
  \includegraphics[width=0.45\textwidth]{Feedback_loop_with_descriptions.pdf}%
  %\vspace{-5pt}%
  \caption{Closed-loop control}\label{fig:closed-loop control}%
\end{figure}

\lessSpaceAboveSections
\section{Asynchronous Communication}

An important step towards realising the Smart Grid is to build a distributed control system that displays intelligent behaviour. Components have to become more flexible and more autonomous to make that possible. That is best shown using an example.

Recently, much research has been conducted for the integration of electric vehicles into the electrical power grid, e.g.~in the Danish EDISON project \cite{CBDG+10}. Throughout this paper, an example on charging electrical vehicles will be used to illustrate the concepts that are introduced: It is assumed that many electrical vehicles are connected to the power system at the distribution level. The charging of these vehicles has to be coordinated to prevent unnecessary load peaks from happening. This coordination happens through a supervisory unit, which in this case is usually called aggregator. This structure can be used in other control scenarios, such as the control of household appliances (fridges, electrical heaters), a scenario relevant in demand-side participation research. The general structure of the example is shown in Fig.~\ref{fig:supervisory-control}.

The communication between device controller and the actual device happens using synchronous communication, but the supervisory communication between supervisory controller and the device controller can employ a looser way of communication, asynchronous communication. The controller does not depend on an immediate response from the device, which leads to a looser coupling between the two parties.
Behaviour descriptions are one way to enable asynchronous communication, which decouple the device even further from its controller.

\begin{figure}
  \centering%
  \includegraphics[width=0.45\textwidth]{supervisory-control.pdf}%
  %\vspace{-5pt}%
  \caption{Structure of example case}\label{fig:supervisory-control}%
\end{figure}

\lessSpaceAboveSections
\section{Behaviour Descriptions / Policies}

Behaviour descriptions facilitate asynchronous communication in power systems. When using them,
the supervisory unit sends a description of \emph{expected future} behaviour to the controlled unit, instead of sending just commands to be executed immediately. The behaviour description tells the controlled unit how to react to different kinds of situations. The important thing to understand is that this description can be sent to the controlled unit long before it is actually needed. The execution of the behaviour then can happen mostly independent from the supervisory unit. This makes extra communication between the supervisory unit and the controlled unit in most cases unnecessary, and it means that communication problems (too much latency, not enough bandwidth, temporary complete failure) don't cause the difficulties they cause in control systems that need synchronous communication. 

The behaviour of the controlled device has to depend on events it can observe locally, like the system frequency or a system price that is broadcast. The provision of this information to the controlled device should be based on standards, so that devices from different suppliers can work together seamlessly. 
The IEC 61970 communication standards \cite{iec61970-1} provide a power system ontology that could be used to facilitate information retrieval. 
Additionally, the IEC 61850 communication standard \cite{iec61850-1} provides an AP\hspace{0.75pt}I (Application Programming Interface) that allows synchronous communication with many different kinds of devices.

In this paper, these behaviour descriptions are called ``policies''. They can be compared to contracts: The supervisory controller negotiates a certain behaviour with the controlled device, and this ensures that the device will follow that policy. 
The digital signing of policies increases the security of the whole process, because the involved parties are authenticated, and they also cannot claim never to have agreed on the policy (this characteristic is called \emph{non-repudiation}). 

%Policies make semi-autonomous behaviour possible: Device controllers can act autonomously, but they still have to act according to the constraints in the policy. 
%Since power grids have to be kept stable, and autonomy could pose some kind of risk to stability, this characteristic could prove to be important for Smart Grids.

\lessSpaceAboveSections
\section{Expressing Policies}

Policies can be expressed in many different ways. The options range from very abstract, like ``keep the frequency at 50 Hz'', to very specific, like ``set the power output to the value of formula \emph{f}''. 

Rule-based systems (such as Drools \cite{Drools}) can be used to express policies. 
% They are the most common way to implement expert systems, which are an application of artificial intelligence which has found widespread use to make systems more intelligent.
They use the simple concept of if-then rules to create complex behaviour of a system. An example rule for our charging example is: \texttt{\small if (power\_price > High\_Price) then stop\_charging.}

Rules are a good way for expressing policies: % for several reasons:
They are easy to understand for both computers and humans;
they are expressive and flexible;
they can be used to describe complex behaviour;
they can be prioritised;
and they can be generated, e.g.~by an optimisation process.
%% Alternative as a list:
%\begin{itemize*}
%  \item easy to understand for both computers and humans
%  \item expressive and flexible
%  \item rules can be used to describe complex behaviour
%  \item rules can be generated, e.g.~by an optimisation process
%  \item rules can be prioritised
%\end{itemize*}

The flexibility of rules comes from the fact that not only the settings of constants used in rules can be changed (such as the price threshold in the example rule), but rules can also be completely exchanged. The only limitations lie in the kind of information that is available to the rule (system frequency, voltage, power price, \dots) and the types of actions the component supports (power set points, droop settings, load schedules, \dots). Rule sets can be arbitrarily complex, making complex behaviour possible. Thus, policies can be used with different kinds of control strategies, such as market-based or hierarchical approaches.

%% Has already been mentioned above:
% Rules must be able to access some information from outside, e.g.~the system frequency or current power price.
% The access to information like should be facilitated using standards, e.g.~using the CIM as a common power system ontology.

The possibility to prioritise rules makes it possible for a unit to act on several levels of control: e.g.~can a unit be made follow a certain consumption schedule (1$^{\text{st}}$ level), unless changes in the power price (2$^{\text{nd}}$ level) or in the system frequency (3$^{\text{rd}}$ level) makes other behaviour necessary. 

\lessSpaceAboveSections
\section{Conclusion}

In this paper, behaviour descriptions or ``policies'' were introduced as a good way to structure communication in power systems. Because the execution of the behaviour is decoupled from the communication of the behaviour, communication network problems, like a large increase in latency, or even a complete failure of a link, do not impede the execution of the behaviour. This makes the whole system much more robust. Rule systems can be quite complex, enabling intelligent behaviour of the system.

Policies can be used with all kinds of control strategies; using policies adds the possibility to decouple communication from execution, making the system less prone to communication failures.

\bibliographystyle{IEEEtran.bst}
\bibliography{../bibliography,../standards,local}

\end{document}

