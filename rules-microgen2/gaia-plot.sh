#!/usr/bin/env gnuplot

set datafile separator ";"
set xr [600:720]
set terminal svg
set output "wind-power.svg"
set border 31 linewidth 2
set xlabel "Time [min]"
set ylabel "Power [kw]"
set size 1,0.75
plot  "../../syslab/data/gaia-data/gaia-20100912.csv" using (($1-1284249601)/60):5 title 'Wind Power' with lines lw 2

