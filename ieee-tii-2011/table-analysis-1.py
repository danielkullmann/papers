#!/usr/bin/env python

# TODO: 

PACKET_SIZE = 1518

kB=1024
kBit=kB/8
MB=1024*1024
MBit=MB/8

APPROACHES = [ 
    { 
      "name": "SCADA", 
      # alternating downstream/upstream starting with downstream
      "bytes": [10,10,10,2], # [bytes]
      "frequency": 1, # [1/s], every second
    },

    { 
      "name": "Beh.~desc.",
      # alternating downstream/upstream starting with downstream
      "bytes": [0,200,5120,10240,5120], # [bytes] 
      "frequency": 1.0/(5*60), # [1/s], every 15 minutes
      "": "",
    },
]


COMM_TECH = [
    {
      "name": "Local",
      "bandwidth-down": 100*MBit, # [bytes/s]
      "bandwidth-up": 100*MBit, # [bytes/s]
      "latency": 0.001, # [s]
    }, {
      "name": "Internet",
      "bandwidth-down": 8*MBit, # [bytes/s]
      "bandwidth-up": 1*MBit, # [bytes/s]
      "latency": 0.015, # [s]
    }, {
      "name": "UMTS",
      "bandwidth-down": 384*kBit, # [bytes/s]
      "bandwidth-up": 384*kBit, # [bytes/s]
      "latency": 0.150, # [s]
    }, {
      "name": "GPRS",
      "bandwidth-down": 80*kBit, # [bytes/s]
      "bandwidth-up": 20*kBit, # [bytes/s]
      "latency": 0.400, # [s]
    }, {
      "name": "Serial",
      "bandwidth-down": 56*kBit, # [bytes/s]
      "bandwidth-up": 56*kBit, # [bytes/s]
      "latency": 0.004, # [s]
    },
]


table1 = open( "generated/table-analysis-1a.tex", "w" )
table2 = open( "generated/table-analysis-1b.tex", "w" )
tables = [table1, table2]

for tech in COMM_TECH:
  for t in tables:
    t.write( " & " )
    t.write( "\\multicolumn{1}{c}{" + tech["name"] + "}"  )

for t in tables:
  t.write( "\\\\\\hline\n" )

for approach in APPROACHES:
  bytes = approach["bytes"]
  sumBytesDown = sum( [ bytes[i] for i in range(len(bytes)) if i%1==0 ] )
  sumBytesUp = sum( [ bytes[i] for i in range(len(bytes)) if i%1==1 ] )
  messagePackets = [ (x+PACKET_SIZE+1)/PACKET_SIZE for x in bytes if x != 0 ]
  print messagePackets
  numMessages = sum( messagePackets )

  for t in tables:
    t.write( approach["name"] + "&" )

  for tech in COMM_TECH:
    secondsPerRequest = sumBytesDown / tech["bandwidth-down"] + sumBytesUp / tech["bandwidth-up"] + numMessages * tech["latency"]
    secondsInAnHour = secondsPerRequest * 3600 * approach["frequency"]
    table1.write( "%.2f" % secondsPerRequest )
    table2.write( "%.2f" % secondsInAnHour )
    for t in tables:
      t.write( "&" )

  for t in tables:
    t.write( "\\\\\n" )

for t in tables:
  t.close()

