\documentclass[journal]{IEEEtran}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{amsmath} 
\usepackage{color} 
\usepackage{mdwlist} 
%\usepackage{hyperref}
\usepackage{listings}
\interdisplaylinepenalty=2500

%\newcommand{\todo}[1]{\textcolor{red}{#1}}
\newcommand{\comment}[1]{}
\newcommand{\ra}{\ensuremath{\Rightarrow}}
\newcommand{\degree}{\ensuremath{^{\circ}}}

% To finally squeeze the articel into 6 pages
%\newcommand{\lessSpaceAboveSections}{\vspace{-1.5pt}}
\newcommand{\lessSpaceAboveSections}{}

% space between figure and caption
\setlength{\abovecaptionskip}{-3pt}
\setlength{\belowcaptionskip}{-6pt}
% when more space is available:
%\setlength{\abovecaptionskip}{0pt}


\pagestyle{empty}

\pdfinfo
{ /Title (Asynchronous Communication for Control of Distributed Energy Resources)
  /Author (Daniel Kullmann, Oliver Gehrke and Henrik Bindner)
  /CreationDate (D:20110728000000) % this is the format used by pdf for date/time
  /Subject (Article for the ICNC 2012 conference)
  /Keywords (Smart grids, Communication systems, Intelligent systems)
}

% This is for listings of the Drools language
\lstset{ %
  basicstyle=\ttfamily\footnotesize, % font type and size
  %basicstyle=\ttfamily\scriptsize, % font type and size
  backgroundcolor=\color{white},  % background color
  showspaces=false,               % show spaces adding particular underscores
  showstringspaces=false,         % underline spaces within strings
  showtabs=false,                 % show tabs within strings adding particular underscores
  frame=single,	                  % adds a frame around the code
  tabsize=2,	                    % tabsize is 2 spaces
  captionpos=b,                   % caption-position: bottom
  breaklines=false,               % no automatic line breaking
  breakatwhitespace=false,        % sets if automatic breaks should only happen at whitespace
  morekeywords={package,import,rule,when,then,end,and,or,activation,group}, % keywords for the Drools language
  morecomment=[l]{\#} % comment in Drools: from "#" to the end of the line 
}

\begin{document}

\title{Asynchronous Communication for Control of Distributed Energy Resources}
\author{Daniel~Kullmann, Oliver~Gehrke and Henrik~Bindner\\
Risø National Laboratory for Sustainable Energy, Roskilde, Denmark}

\markboth{}{}
\maketitle
\thispagestyle{empty}

\begin{abstract}
The term Smart Grid describes the effort to enable the integration of large
numbers of renewable distributed energy resources into the power grid. The
fluctuations inherent in renewable energy resources imply the need to also
integrate the demand side actively into the control of the power system. For
this effort to succeed, a new control infrastructure has to be put into
place. The power system is a distributed system, and it needs a sophisticated
communication framework to cope with communication problems, such as delays
and failures. Recently, behaviour descriptions have been proposed as an
alternative to synchronous communication in power systems, especially with
small distributed energy resources. This article presents the rationale
behind behaviour descriptions, a framework for implementing behaviour
descriptions, and an experiment that has been carried out to
evaluate the feasibility of such an approach.
\end{abstract}

\begin{IEEEkeywords} % TODO should I use that here?
  Smart grids, Communication systems, Intelligent systems
\end{IEEEkeywords}

\lessSpaceAboveSections
\section{Behaviour Descriptions}
\label{sec:behaviour-descriptions}

Behaviour descriptions facilitate asynchronous communication in power systems.
A behaviour description is basically a description of \textit{expected future
behaviour} that is sent from the supervisory controller to the local device
controller. This is a high-level description of behaviour, as opposed to the
simple measurement/setpoint exchange that is typical for SCADA systems.

A simple example for a behaviour description is to tie the power
consumption of the controlled component to a dynamic power price that is
broadcast: If the power price is high, the unit will try to reduce consumption
a smuch as possible; if it is low, the unit will try to increase consumption
whenever it makes sense. The behaviour description for this example would be a
computer-readable representation of the previous sentence.

Behaviour descriptions are sent to the controlled device some time before the
behaviour is actually needed. This means that communication between supervisory
controller and controlled device needs only to be available during the negotiation
of the behaviour description, but not necessarily during the execution of the
behaviour description. This is possible because the execution of the behaviour
description happens autonomously.

The behaviours that can be put into a behaviour description depend on the
capabilities of the device: Which data can it obtain from its environment, and
what services can it provide based on that data? In the simple example above,
the component can obtain a dynamic power price from its environment, and it can
provide a simple consumption-control service.
%which could replace balance-power
%services usually provided by much larger power plants.

A behaviour description can be understood as a kind of contract: Both parties,
supervisory controller and controlled device, agree on that contract, and then
both sides have to adhere to that contract: The controlled device has to
provide the service that was agreed upon, and the supervisory controller must
provide the compensation for provision of that service, such as a lower power
price. Behaviour descriptions are also similar to contracts in the sense that a
behaviour description is a binding document which forces the component to
behave in a certain way. If the component fails to do that, it breaches the
contract, which will result in some kind of penalty.

Behaviour descriptions offer several kinds of flexibility: First of all, they do not depend
on a certain type of equipment; they only depend on the ability to communicate
using the negotiation protocol (see below) that is used to find a suitable
behaviour description. 
Second, they can activate any service that is supported by the component.
Third, behaviour descriptions can be tailored to the 
capabilities of the specific device. This includes the ability to respect the
constraints a component has to comply to. 
Fourth, many different measurements or events can be used to guide the
behaviour of the component.

The flexibility of behaviour descriptions comes from the fact that the
communication is not based on an exchange of measurements and setpoints, but
rather on an exchange of capabilities, constraints and services. A controlled 
component sends the set of services it can support and the set of constraints
it has to adhere to to the supervisory controller. The supervisory controller
then can create a behaviour description and send it to the component. The
behavior description will reference the services the component can support, and
give the necessary parameters to the service so it can be executed on the
component. Example for parameters for a service is the droop value for a
droop controller, or an actual consumption schedule when the service is
``scheduled consumption''.


\section{Behaviour Negotiation Protocol}

A negotiation protocol is needed to be able to
use behaviour descriptions. The protocol provides
the means for supervisory controller and controlled
component to exchange the information necessary
to set up a behavioural contract.

In the negotiation process, controlled components pass through 4 major states:
\begin{enumerate}
  \item Unconnected
  \item In negotiation
  \item Waiting to activate
  \item Active
\end{enumerate}
When a
component is connected to the power system, it starts in the unconnected
state. It has to find the appropriate aggregator and connect to it. In the
next state, ``in negotiation'', the actual behaviour description is negotiated.
This is a process that consists of several steps in its own. First, the
component has to send all necessary information about itself to the aggregator.
The aggregator will then construct a behaviour description, and send it to the
component, which has to accept it.

The third state, ``waiting to activate'', lets the component wait until the
behaviour description becomes active. Behaviour descriptions have a
validity period, that might be sometime in the future.  When a behaviour
description is activated, the component switches over to the active state.
Some time before the validity period ends, the device will go back into the ``in
negotiation'' state, while still executing the behaviour of the active behaviour
description. The aggregator can also initiate a new negotiation round when
necessary.

\section{Services as Main Component of Behaviour Descriptions}

The example in section 
\ref{sec:behaviour-descriptions}
shows that behaviour description depend heavily on the existence of
generic service descriptions: The component must be able to tell the controller
which services it supports, and the controller has to activate these services
with the right parameters in the behaviour descriptions.

This implies that behaviour descriptions need a standardized format for the description of
services. There are two different description formats that are needed: 

\begin{itemize}
  \item The service description is sent to the supervisory controller; it tells
    the controller that this service is supported by the controlled component
  \item The service activation is part of the behaviour description; it
    references the service description and adds the necessary parameters for
    this particular instance of the service
\end{itemize}

For the sake of simplicity, a very simple service description format has been
chosen for this article: A service description is simply a character string
that uniquely identifies the service.

%consists of three pieces of
%information, each being a short character string:
%
%\begin{enumerate}
%  \item The basic type of service that is provided (e.g.~``frequency control'')
%  \item The way in which this service is provided (e.g.~``dynamic power price
%    based consumption control'')
%  \item The specific version of that service; this could be a standard name, or
%    just a numerical value
%\end{enumerate}
%
%These three strings are concatenated, thus forming a single string that defines
%the service. \todo{include?}

A service activation just uses the exact same string, and adds a set of
key/value pairs that specify the parameters for the service. How these
parameters are exactly provided depends on the service description.


\section{Implementation of Behaviour Descriptions using Rules}
\label{sec:rules}

How service activations are expressed has been described in the prevous section.
Using service activations as complete behaviour descriptions, however, is not a
particular flexible way to implement behaviour descriptions. This is
exemplified by the fact that constraints can't be expressed.

In this work, behaviour descriptions are actually implemented using a
rule-based system \cite{Giarratano04}. Here, a behaviour description consists
of a set of rules. A rule has the form \texttt{if} \emph{condition}
\texttt{then} \emph{actions}. The \texttt{if} part contains the condition of
the rule, i.e.~a Boolean expression. When the expression evaluates to true, the
actions in the \texttt{then} part are executed. The basic structure of a rule
set is illustrated in Figure~\ref{fig:behaviour-descriptions}. The rule base is
embedded in the environment of the component. The conditions reference values
from the environment, and the actions change the environment. 

The actual behaviour of the components is encapsulated in service descriptions,
therefore actions just activate services, and conditions reflect the
circumstances for a service activation.

\begin{figure}[ht]
  \begin{center}
    \includegraphics[width=0.8\linewidth]{behaviour-wide.pdf}
  \end{center}
  \caption{Behaviour Description with Rules}
  \label{fig:behaviour-descriptions}
\end{figure}

An important part of the rule-based system is the environment the rule is
situated in; without it, a rule would have no access to the state of the
physical system (e.g. measurements) and would not be able to perform any
actions. Because behaviour descriptions are meant to be a generic expression of
control, it is important that the environment is standardised. This implies the
need for a common ontology. A suitable environment consists of the following
parts:
\begin{itemize}
  \item A facility to get access to events generated by the outside world, such
    as local measurements and broadcast messages received.
  \item Descriptions of services (e.g.~``Frequency Response'')
  \item Descriptions of service activations, i.e.~service descriptions plus
    a set of parameters needed to use the service (e.g.~the droop curve used in
    a droop controller)
  \item A mechanism to activate a particular service
\end{itemize}

Often, several rules apply to the same situation. If the actions of those rules
conflict, a single rule has to be selected for execution. This process is
called disambiguation. The easiest disambiguation strategy is the
``first-one-wins'' strategy: The rules are set in a particular order, and they
are evaluated in that order. The first rule whose condition is true is the one
to be executed.

Rules are a good way to express behaviour descriptions: First of all, rules are
easy to understand for both humans and computers, making human supervision of
the control system possible. Rules can also be generated easily with a computer
program, because of their strict structure. Flexibility is a key feature of
behaviour descriptions, and rules do provide this flexibility: rule sets can be
arbitrarily complex, and rules can be adapted to obey local constraints of
components.


\lessSpaceAboveSections
\section{Policy Framework}

%\todo{Daniel, Dein policy framework braucht dringend einen catchy Namen (nicht
%für dieses Paper, aber generell!) ;-)}
The implementation of the example case explained in the next section is based
on the policy framework, a Java library developed by the authors.  It provides
a mechanism for using arbitrary behaviour descriptions in a control system. 
% TODO
%The policy framework was originally developed on top of the JADE multi-agent
%platform, but has since been extended to also support a simple http-based
%transport layer which is easier to configure and use. 
The framework does not
put any constraints on the type of behaviour descriptions that can be used;
this allows for experimentation with different ways of expressing them.

The policy framework implements the negotiation protocol for negotiating policies.
%\begin{enumerte}
%  \item The component is new, and registers at its aggregator
%  \item The aggregator starts a negotiation round
%  \item The component sends information about itself to the aggregator: the
%    state it is in, the services it supports and the constraints it has to
%    comply with
%  \item The aggregator creates a behaviour description/policy and sends it to
%    the component
%  \item The component accepts the behaviour description
%  \item The component activates the behaviour description as soon as it becomes
%    valid
%  \item Before the validity interval expires, the component starts a new
%    negotiation 
%\end{enumerate}
To implement a specific control system, several classes have to be implemented
that have to be registered in the framework during runtime:
\begin{enumerate}
  \item The actual behaviour description(s) used; this includes serialisation
    of the descriptions into a text format that is transferred between server
    and client
  \item Descriptions of the components of the client side; this includes the
    description of supported services, and of the constraints the clients have
    to obey
  \item The server implementation; this part is responsible for creating the
    behaviour descriptions for each client
  \item The client implementation; it provides the component descriptions from
    item \#2, receives the behaviour descriptions, and acts according to them
\end{enumerate}

\lessSpaceAboveSections
\section{Implementation}

A proof-of-concept demonstration for the control of heaters in a household has
been implemented at the SYSLAB facility \cite{SYSLAB07}. SYSLAB is a small
power system designed to facilitate the development and testing of distributed
control algorithms for power systems. A part of SYSLAB is PowerFlexhouse, an
intelligent office building equipped with a large number of sensors and various
types of controllable demand. An embedded controller in the building is able to
execute behaviour descriptions. In the following example, we will demonstrate
how the policy framework can be used to implement reliable dynamic demand
response.

In the example, the building is set up to respond to two types of external
events: Changes in system frequency, and changes in a dynamic power price.
Depending on these parameters and the behaviour description in effect, the
building can modify the setpoints of the heater thermostats in individual
rooms, in order to decrease or increase the overall power consumption.
The system frequency is measured directly at the building's grid connection
point. The Nordic power system does not currently have a mechanism for the
generation or broadcast of fast dynamic power price signals, although such a
mechanism is likely to exist in the future. For the sake of this demonstration,
a real-time artificial price signal has been generated, based on production and
consumption data in the Danish power system. The price calculation is based on
the rationale that a high penetration of wind energy works towards lower spot
prices, since the marginal production cost of wind energy is very low.

The implementation uses rules that access service objects as black boxes. That
means that the actual implementation of the service is unknown to the rule, and
only accessible through a uniform interface. In this way, any kind of service
can be used in a rule-based system, without having to expose the implementation
details of the service. 

The syntax of the rules comes from the Drools expert system, a Java-based rule
engine \cite{jboss-drools}. The Drools system supports two different syntaxes,
one a XML format, the other one more programming-language like, which is the
one used here.

Another issue is how to provide the rule bases with the environment they have
to work in. In our example case, the environment consists of the following
parts (compare this with the list of environment parts in
section~\ref{sec:rules}):
\begin{itemize}
  \item Measurements of frequency and dynamic power price
  \item Abstractions of the frequency control and price reaction services
  \item A mechanism to tell the system to activate a certain service, i.e.~a
    service scheduler
\end{itemize}

The two kinds of services (frequency control and price control) have a similar
API for usage in the rule bases:
\begin{itemize}
  \item A method to create an instance of a service
  \item Methods to start and stop the service; these are used by the service
    scheduler
  \item A method to evaluate whether a service would like to be activated; only
    the first such service that is found in the rule base is actually activated
\end{itemize}

\begin{figure}
  \begin{center}
    \lstinputlisting{policy.drl}
  \end{center}
  \caption{Simplified Example Rule Base}
  \label{fig:example-rulebase}
\end{figure}

Since the implementation is based on Java, the environment of the rules is also
specified using Java interfaces and classes. The measurements of frequency and
power are provided by a unified interface which can inject measured (or
received) values into the services. The services provide a common interface
which has three main functions: a method to inject measured values into the
service, a method to ask a service whether it wants to be activated, and a
means to activate a service. The service activation is provided by a scheduling
agent that is always present in the environment of the rule base.

A big implementation issue is how to get service instances with the right
parameters into the rule base. The services need certain parameters to be able
to function; in the example case, both services worked with low/high
thresholds, so two parameters are needed for each service activation. The
problem is how these service instances are put into the environment of the rule
base. It would have been possible to send additional information about the
services along with the behaviour description, but this meant that a separate
data format for service activations would be necessary. The option that is
implemented adds an additional ``setup'' rule to the rule base that is
triggered exactly once. This extra rule contains code to initialise the
services and to register them in the environment. 

An example rule base is shown in Figure~\ref{fig:example-rulebase}. This is a
cleaned-up version of a rule base that was actually used in the experiment;
names and values have been shortened to make it more readable. The first rule
\texttt{setup} is the setup rule mentioned in the last paragraph; it sets up
the two service activations with the appropriate thresholds and registers them
in the environment. The other two rules, \texttt{frequency-1} and
\texttt{power-1}, are the actual work rules of the rule base. They just
evaluate whether the service in question wants to be activated (i.e.~the
measurement is outside the service deadband), and if this is true the service
scheduler is instructed to activate the service. The two rules exclude
themselves mutually (this is done with the \texttt{activation-group}
directive), so only the first of them that fires is activated. The rule base is
very simple because the work of the services is hidden inside the blackbox of
the service activation implementation.


\lessSpaceAboveSections
\section{Case Experiment}

The experimental setup of the example case controlled the eight rooms in
PowerFlexhouse individually using behaviour descriptions. Each room was
controlled by its own client which offered maximally two services to the
system: a frequency control service, and a service that reacted to the dynamic
power price. The price reaction service was offered by all clients, but not all
offered the frequency control service. This was done to demonstrate that
behaviour descriptions can in fact be adapted to the capabilities of the
individual components. Both offered services are parametrised by high and low
thresholds. The services are kept simple on purpose, because it was not the
purpose of the experiment to demonstrate complex control algorithms, but rather
to show that a system based on behaviour descriptions actually works, and to
evaluate how behaviour descriptions can combine simple services to allow for
more complex behaviour.

A server program running on a different computer calculated the behaviours for
the single rooms. The thresholds for the services are calculated in a way to
provide smooth reaction to changes in frequency and power price.

The policies for the frequency service change every two hours: Three different
widths of the frequency controller deadband are used. This was done to
demonstrate the flexibiulity of policies. This can be seen in the middle plot
of figure~\ref{fig:results-room6}.

\lessSpaceAboveSections
\section{Observations}
The plots in figure~\ref{fig:results-overall} contain measured data from a
single, 10-hour run of the experiment. During the experiment period, the power
price input signal decreases steadily with few and small upward excursions. The
system frequency input shows a slowly increasing trend, followed by a marked
drop towards the end of the period.

\begin{figure}
  \begin{center}
    \includegraphics[width=\linewidth]{graph2_powerconsumption.pdf}
  \end{center}
  \caption{Overall results}
  \label{fig:results-overall}
\end{figure}

In the second subfigure, the overall power consumption of the building exhibits
a strong oscillatory pattern caused by the underlying thermostatic control of
the heaters. A correlation between power consumption and system frequency can
be observed. The price, on the other hand, does not appear to have a strong
impact on the consumption. This can be explained by the fact that the price
controller only acts as a secondary control loop, and is overridden whenever
the primary frequency controller is active. During the period of the
experiment, this was the case for most of the time.

The switching state of the individual heaters is shown in the bottom subplot.
Due to low outside temperatures during the experiment, some heaters can be seen
to be on almost all of the time.

Figure~\ref{fig:results-room6} details the behaviour of a single room during
the same experiment. In the first hours of the experiment, the temperature
setpoint (red) remains largely at 19\degree C due to the high power price and
low system frequency. The room temperature (black) follows the setpoint within
the hysteresis of the thermostatic control.

The second and third subplots show the relationship between the two input
signals - price and frequency - and the thresholds in effect according to the
active policy. The two-hour period for policy changes is easy to observe.

\begin{figure}
  \begin{center}
    \includegraphics[width=\linewidth]{graph1_room6.pdf}
  \end{center}
  \caption{Results for Room \#6}
  \label{fig:results-room6}
\end{figure}

In hour 6 of the experiment, an increase in system frequency triggers the upper
frequency threshold. At nearly the same time, the power price begins to drop,
first below the upper, then below the lower threshold. This leads to an almost
constant temperature setpoint of 21\degree C for the remainder of the
experiment.

The observations demonstrate that individual clients do react to the frequency
and power policies sent to them through the policy framework. However, more and
longer experiments as well as a deeper analysis of measured data is needed to
determine and quantify the precise effect of changing policies. In the
experiment presented in this paper, the thermostatic control of the heaters and
external influences such as the outside temperature interfere with the effects
caused by the controller. Further statistical analysis is needed to separate
these effects.

\lessSpaceAboveSections
\section{Conclusion}

This article explained the concept behind behaviour descriptions, demonstrated
an implementation in the Java programming language, and showed the results of
an experimental test of behaviour descriptions in Risø's SYSLAB facility.

Behaviour descriptions enable control systems to deal with communication
failures, and they reduce computational and communication load on supervisory
controllers, because they decouple supervisory controller and controlled
components and make more autonomous, yet still controlled, behaviour of
components possible.

Behaviour descriptions don't rely on specific services that components offer to
the power system. This is an intentional feature of behaviour descriptions:
They provide a container for the flexible and reliable execution of services;
the only things needed from the services is the means to describe and to
control them.

The experimental results show that behaviour descriptions can have a positive
effect on the power system by controlling the power consumption of demand-side
consumers. The services used in the experiment are very simple, so the effect
of more sophisticated services can be expected to be better.

\bibliographystyle{IEEEtran.bst}
\bibliography{../bibliography,local}

\end{document}

