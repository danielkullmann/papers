%\documentclass[journal]{IEEEtran}
\documentclass[a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{amsmath} 
\usepackage{color} 
\usepackage{mdwlist} 
\interdisplaylinepenalty=2500
\linespread{1.3} % one-and-a-half line spacing
%\linespread{1.6} % double line spacing


\pagestyle{empty}

\begin{document}

\title{Policy-Based Communication for Smart Grid Control}
\author{Daniel~Kullmann \\ Risø National Laboratory for Sustainable Energy, Roskilde, Denmark}

\markboth{}{}
\maketitle
\thispagestyle{empty}

\section*{Asynchronous Communication}

An important step towards realising the Smart Grid is to build a distributed control system that displays intelligent behaviour. Components have to become more flexible and more autonomous to make that possible. That is best shown using an example.

Recently, much research has been conducted for the integration of electric vehicles into the electrical power grid, e.g.~in the Danish EDISON project \cite{CBDG+10}. Throughout this paper, an example on charging electrical vehicles will be used to illustrate the concepts that are introduced: It is assumed that many electrical vehicles are connected to the power system at the distribution level. The charging of these vehicles has to be coordinated to prevent unnecessary load peaks from happening. This coordination happens through a supervisory unit, which in this case is usually called aggregator. This control structure is also applicable in other control scenarios, such as the control of household appliances (fridges, electrical heaters), a scenario relevant for demand-side participation. The general structure of the example is shown in Fig.~\ref{fig:supervisory-control}.

The communication between device controller (the charging post) and the actual device (the electric vehicle) happens using synchronous communication, but the supervisory communication between supervisory controller (the aggregator) and the device controller can employ a looser way of communication, asynchronous communication. The controller does not depend on an immediate response from the device, which leads to a looser coupling between the two parties. In many cases, an ``open'' control loop between supervisory controller and device controller will be sufficient, such as when the feedback happens over system frequency or power counters. This is called an open loop here because the communication loop is open, whereas the loop on which the service is provided (the power grid) is closed. 

Behaviour descriptions are one way to enable asynchronous communication, which decouple the device even further from its controller.

\begin{figure}
  \centering%
  \includegraphics[width=0.9\textwidth]{supervisory-control.pdf}%
  %\vspace{-5pt}%
  \caption{Structure of example case}\label{fig:supervisory-control}%
\end{figure}

\section*{Behaviour Descriptions}

Behaviour descriptions facilitate asynchronous communication in power systems. When using them,
the supervisory unit sends a description of \emph{expected future} behaviour to the controlled unit, instead of sending just commands to be executed immediately. The behaviour description tells the controlled unit how to react to different kinds of situations. The important thing to understand is that this description can be sent to the controlled unit long before it is actually needed. 

A simple example for such an behaviour description is to couple consumption to a power price: The charging post will try to charge when the power price is low, and try not to charge when the price is high. This depends of course on the constraints of the charging process: if the vehicle has to be fully charged at some point, it might not be possible to defer charging in high-price situations. Most behaviour descriptions will be more complex than this simple example: Different kinds of behaviours can be tied to different kinds of events, and these can be ordered hierarchically to make it obvious which behaviour should be executed in which situation.

The execution of the behaviour description can happen mostly independent from the supervisory unit. This makes additional communication between the supervisory unit and the controlled unit in most cases unnecessary, and it means that communication problems (too much latency, not enough bandwidth, temporary complete failure) don't cause the difficulties they cause in control systems that need synchronous communication. It also implies that the device must base its decisions mostly on events it can observe locally, like system frequency, which can just be measured at the point of common coupling, and power price, which is a single-value, one-way communication act and is therefore very well suited to be broadcast by various, highly reliable means.

A feature of behaviour descriptions is that they are negotiated individually with each device, so local constraints can be taken into account. As an example, consider the urgency of charging an electric vehicle: An emergency ambulance will always be charged as quickly as possible, whereas a vehicle that is used only on weekends can be charged whenever the power price is best.

The provision of the event information, like system frequency and power price, to the controlled device should be based on standards, so that devices from different suppliers can work together seamlessly. 
The IEC 61970 communication standards \cite{iec61970-1} provide a power system ontology that could be used to facilitate information retrieval. 
Additionally, the IEC 61850 communication standard \cite{iec61850-1} provides an Application Programming Interface (API) that allows synchronous communication with many different kinds of devices.

Behaviour descriptions can be interpreted as contracts: The supervisory controller negotiates a certain behaviour with the controlled device, and both parties agree on following the behaviour description: the device will follow the terms of contract from the behaviour description, and the supervisory controller will compensate the owner of the device in some way for the work the device has done. When the contract is broken, some kind of penalty will have to be paid, which can also be part of the behaviour description; but in most cases this will be implicit, not explicit information.

\bibliographystyle{IEEEtran.bst}
\bibliography{../bibliography,../standards,local}

\end{document}

